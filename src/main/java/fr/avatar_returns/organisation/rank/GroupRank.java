package fr.avatar_returns.organisation.rank;

import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.GroupRankPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.PlayerRankPacket;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GroupRank extends Rank {

    private int id;
    private String groupType;
    private int maxLand;
    private int maxVassal;
    private int maxPlayer;
    private boolean canVassalize;

    private static ArrayList<GroupRank> lstGroupRank = new ArrayList<>();

    public GroupRank(String name, int order, String groupType, int maxLand, int maxVassal, int maxPlayer, boolean canVassalize){

        super(name, order);

        this.groupType = groupType;
        this.maxLand = maxLand;
        this.maxPlayer = maxPlayer;
        this.maxVassal = maxVassal;
        this.canVassalize = canVassalize;

        GroupRank.lstGroupRank.add(this);
        this.id = DBUtils.addRank(this);

        GeneralMethods.sendPacket(new GroupRankPacket(this, OrganisationPacket.Action.CREATE));
    }
    public GroupRank(int id, String name, int order, String groupType, int maxLand, int maxVassal, int maxPlayer, boolean canVassalize, ArrayList<Objective> lstObjective){

        super(name, order, lstObjective);

        this.id = id;
        this.groupType = groupType;
        this.maxLand = maxLand;
        this.maxPlayer = maxPlayer;
        this.maxVassal = maxVassal;
        this.canVassalize = canVassalize;

        GroupRank.lstGroupRank.add(this);
    }


    @Override public int getId() {
        return this.id;
    }
    @Nullable public static GroupRank getById(int id){
        for(GroupRank groupRank : lstGroupRank){
            if(groupRank.getId() == id)return groupRank;
        }
        return null;
    }
    @Override public void remove(boolean storeDb) {

        GroupRank.lstGroupRank.remove(this);
        if(storeDb)DBUtils.removeRank(this);

        // On modifie automatiquement l'ordre des groupes.
        GroupRank newRank = null;
        for(GroupRank groupRank : GroupRank.getLstGroupRank()){

            if(!groupRank.getGroupType().equalsIgnoreCase(this.getGroupType()))continue;

            if (groupRank.getOrder() == this.getOrder()-1)newRank = groupRank; // Si jamais on supprimer le rang le plus élevé, il faut descendre d'un rang.
            if (groupRank.getOrder() == this.getOrder())newRank = groupRank;

            if(groupRank.getOrder() > this.getOrder()){
                groupRank.order = groupRank.getOrder()-1;
                if(storeDb)DBUtils.setRankOrder(groupRank);
            }
        }

        // Tous les groupes qui avaient ce Rang sont automatiquement passé sur un rang existant.
        for (LargeGroup largeGroup : LargeGroup.getLstLargeGroup()){
            if(largeGroup.getRank().equals(this)){
                largeGroup.setRank(newRank, storeDb);
            }
        }
    }
    @Override public boolean setOrder(int newOrder, boolean storeDb) {


        if(this.getOrder() == newOrder)return true;
        if(this.getOrder() > newOrder){
            for(GroupRank rank: lstGroupRank){
                if(rank == this)continue;
                if(!rank.getGroupType().equalsIgnoreCase(this.getGroupType()))continue;
                if(rank.getOrder() < newOrder)continue;
                if(rank.getOrder() > this.getOrder())continue;

                rank.order += 1;
                if(storeDb) DBUtils.setRankOrder(rank);
            }
        }
        else{
            for(GroupRank rank: lstGroupRank){
                if(rank == this)continue;
                if(!rank.getGroupType().equalsIgnoreCase(this.getGroupType()))continue;
                if(rank.getOrder() < this.getOrder())continue;
                if(rank.getOrder() > newOrder)continue;

                rank.order -= 1;
                if(storeDb) DBUtils.setRankOrder(rank);
            }
        }

        this.order = newOrder;
        if(storeDb) DBUtils.setRankOrder(this);
        return true;
    }

    public boolean setGroupType(String groupType){ return this.setGroupType(groupType, true); }
    public boolean setGroupType(String groupType, boolean storeDb) {

        this.groupType = groupType;
        if(storeDb) DBUtils.setRankGroupType(this);
        return true;
    }

    public boolean setMaxLand(int nbMaxLand){ return this.setMaxLand(nbMaxLand, true);}
    public boolean setMaxLand(int nbMaxLand, boolean storeDb) {

        if (nbMaxLand <= 0)return false;
        this.maxLand = nbMaxLand;
        if(storeDb) DBUtils.setRankNbMaxLand(this);
        return true;
    }

    public boolean setMaxPlayer(int nbMaxPlayer){
        return this.setMaxPlayer(nbMaxPlayer, true);
    }
    public boolean setMaxPlayer(int nbMaxPlayer, boolean storeDb) {

        if (nbMaxPlayer <= 0)return false;
        this.maxPlayer = nbMaxPlayer;
        if(storeDb) DBUtils.setRankMaxPlayer(this);
        return true;
    }

    public boolean setMaxVassal(int maxVassal) { return this.setMaxVassal(maxVassal, true);}
    public boolean setMaxVassal(int maxVassal, boolean storeDb) {

        if (maxVassal  <= 0 ) return false;
        this.maxVassal = maxVassal;
        if(storeDb)DBUtils.setRankMaxVassal(this);
        return true;
    }

    public boolean setCanVassalize(boolean canVassalize) { return this.setCanVassalize(canVassalize, true);}
    public boolean setCanVassalize(boolean canVassalize, boolean storeDb) {
        this.canVassalize = canVassalize;
        if(storeDb) return DBUtils.setRankCanVassalize(this, canVassalize);
        return true;
    }

    public String getGroupType(){ return this.groupType;}
    public int getMaxLand() { return maxLand; }
    public int getMaxPlayer() { return maxPlayer; }
    public int getMaxVassal() { return maxVassal; }
    public boolean isCanVassalize() {
        return canVassalize;
    }

    public static ArrayList<GroupRank> getLstGroupRank(){
        Collections.sort(GroupRank.lstGroupRank, new Comparator<GroupRank>() {
            @Override
            public int compare(GroupRank grpRank1, GroupRank grpRank2) {
                return Integer.compare(grpRank1.getOrder(), grpRank2.getOrder());
            }
        });
        return GroupRank.lstGroupRank;
    }
    public static void checkProgression(Group group) {

        boolean rankUpdate = true;

        for(GroupObjective groupObjective : group.getLstGroupObjective()){
            rankUpdate = groupObjective.isAcomplished();
            if(!rankUpdate) break;
        }

        if (rankUpdate){
            // On vas monter le rang du groupe.
            GroupRank upperRank = GroupRank.getRankByOrder(group.getRank().getOrder() + 1, (group instanceof LargeGroup)? "FACTION" : "GUILDE");
            group.setRank(upperRank);
        }
        else{
            // On vas regader s'il faut réduire le rang du groupe.
            boolean rankDownGrade = false;
            GroupRank lowerRank = GroupRank.getRankByOrder(group.getRank().getOrder() - 1, (group instanceof LargeGroup)? "FACTION" : "GUILDE");

            if(lowerRank.getId() == group.getRank().getId())return; // Si c'est le même rang on ne fait aucune vérifications.

            for(Objective objective : lowerRank.getLstObjectif()){
                if(objective.getType().equalsIgnoreCase(Objective.Type.MINVASALL.toString())){
                    if(group instanceof LargeGroup){
                        if(((LargeGroup) group).getLstVassal().size() < objective.getObjectifValue()){
                            rankDownGrade = true;
                            break;
                        }
                    }
                }
                else if(objective.getType().equalsIgnoreCase(Objective.Type.MINLAND.toString())){
                    if(group instanceof LargeGroup){
                        if(((LargeGroup) group).getLstVland().size() < objective.getObjectifValue()){
                            rankDownGrade = true;
                            break;
                        }
                    }
                }
                else if(objective.getType().equalsIgnoreCase(Objective.Type.MINPLAYER.toString())){

                    if(group.getLstMember().size() < objective.getObjectifValue()){
                        rankDownGrade = true;
                        break;
                    }
                }
            }

            if(rankDownGrade) {
                group.setRank(lowerRank);
            }
        }
    }

    public static GroupRank getRankByOrder(int order, String type){

        GroupRank minRank = null;
        GroupRank maxRank = null;

        for(GroupRank groupRank : getLstGroupRank()){

            if(!groupRank.getGroupType().equalsIgnoreCase(type))continue;
            if(groupRank.getOrder() == order)return groupRank;

            if(minRank == null)minRank = groupRank;
            if(maxRank == null)maxRank = groupRank;


            if(groupRank.getOrder() > maxRank.getOrder()) maxRank = groupRank;
            if(groupRank.getOrder() < minRank.getOrder()) minRank = groupRank;

        }

        if(order < maxRank.getOrder())return minRank;
        return maxRank;
    }
}
