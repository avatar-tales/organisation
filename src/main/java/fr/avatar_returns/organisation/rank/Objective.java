package fr.avatar_returns.organisation.rank;

import com.google.gson.annotations.Expose;
import fr.avatar_returns.organisation.storage.DBUtils;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

import java.beans.Transient;
import java.util.ArrayList;

public class Objective {

    public enum Type {

        RESSOURCE("RESSOURCE"),
        MINVASALL("MINVASALL"),
        BOOLEAN("BOOLEAN"),
        MINPLAYER("MINPLAYER"),
        MINLAND("MINLAND");

        public String type;
        Type(String type) {
            this.type = type;
        }

        public String getStrType(){return this.type;}
        public boolean equals (String type){ return type.equals(this.type);}
        public boolean equalsIgnoreCase(String type) {return type.equalsIgnoreCase(this.type);}
        public static boolean isValidType(String type) {

            for(String validType : Type.getLstType()){
                if(validType.equalsIgnoreCase(type))return true;
            }

            return false;
        }

        @Override public String toString() {
            return this.getStrType();
        }
        public static ArrayList<String> getLstType() {
            ArrayList<String> lstType = new ArrayList<>();

            lstType.add(RESSOURCE.toString());
            lstType.add(MINVASALL.toString());
            lstType.add(MINLAND.toString());
            lstType.add(MINPLAYER.toString());
            lstType.add(BOOLEAN.toString());

            return lstType;
        }
    }

    private int rankId;
    private String rankType;
    private String type;
    private String name;
    private String description;
    private transient ItemStack item;
    private int objectifValue; // BOOLEAN (1 = True, 0 = False) VASAL (nbVassalToHave), RESSOURCE (item amount)

    public Objective(Rank rank, String name, String description, String type,  int objectifValue, ItemStack item, boolean storeDB){
        this.rankId = rank.getId();
        this.rankType = (rank instanceof GroupRank)? "GROUP_RANK" : "PLAYER_RANK";
        this.type = type;
        this.name = name;
        this.description = description;
        this.item = item;
        this.objectifValue = objectifValue;

        if(storeDB) DBUtils.addGRankObjectif(this);
    }

    public String getName() {return name;}
    public String getDescription() {return description;}
    public int getObjectifValue() {return objectifValue;}
    @Nullable public Rank getRank() {
        if(rankType.equals("GROUP_RANK"))return GroupRank.getById(this.rankId);
        else return PlayerRank.getById(this.rankId);
    }
    public String getType() {
        return type;
    }
    public ItemStack getItem() {
        return item;
    }

    public boolean setDescription(String description) {return this.setDescription(description, true);}
    public boolean setDescription(String description, boolean storeDb) {
        this.description = description;
        if(storeDb) DBUtils.setObjectiveDescription(this);
        return true;
    }
    public boolean setObjectifValue(int objectifValue) { return this.setObjectifValue(objectifValue, true);}
    public boolean setObjectifValue(int objectifValue, boolean storeDb) {

        if(objectifValue < 1)return false;
        this.objectifValue = objectifValue;
        if(storeDb) DBUtils.setObjectiveValue(this);

        return true;
    }
}
