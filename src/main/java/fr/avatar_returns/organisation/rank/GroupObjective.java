package fr.avatar_returns.organisation.rank;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class GroupObjective {

    private int groupId;
    private Objective objective;
    private int currentObjectiveValue; // BOOLEAN (1 = True, 0 = False) MINVASALL/MINLAND/MINPLAYER (min to have), RESSOURCE (item amount)

    public GroupObjective(Group group, Objective objective){
        this.objective = objective;
        this.groupId = group.getId();
        this.currentObjectiveValue = 0;
        DBUtils.addObjectiveProgression(this);
    }
    public GroupObjective(Group group, Objective objective, int currentObjectiveValue){
        this.objective = objective;
        this.groupId = group.getId();
        this.currentObjectiveValue = currentObjectiveValue;
    }

    public boolean isAcomplished(){

        if(this.objective.getType().equalsIgnoreCase(Objective.Type.BOOLEAN.toString())){
            return this.getCurrentObjectiveValue() == 1;
        }
        else if(this.objective.getType().equalsIgnoreCase(Objective.Type.RESSOURCE.toString())){
            return this.getCurrentObjectiveValue() >= objective.getObjectifValue();
        }
        else if(this.objective.getType().equalsIgnoreCase(Objective.Type.MINVASALL.toString())){

            if(this.getGroup() instanceof LargeGroup){
                return ((LargeGroup) this.getGroup()).getLstVassal().size() >= this.objective.getObjectifValue();
            }
            else {System.out.println("TODO");}

            return false;
        }
        else if(this.objective.getType().equalsIgnoreCase(Objective.Type.MINLAND.toString())){

            if(this.getGroup() instanceof LargeGroup){
                return ((LargeGroup) this.getGroup()).getLstVland().size() >= this.objective.getObjectifValue();
            }
            else {System.out.println("TODO");}
        }
        else if(this.objective.getType().equalsIgnoreCase(Objective.Type.MINPLAYER.toString())){
            return this.getGroup().getLstMember().size() >= objective.getObjectifValue();
        }

        return true;
    }
    public boolean update(int currentObjectiveValue){
        return this.update(currentObjectiveValue, true);
    }
    public boolean update(int currentObjectiveValue, boolean storeDb){
        this.currentObjectiveValue = currentObjectiveValue;
        if(storeDb) DBUtils.updateObjectiveProgression(currentObjectiveValue, this);
        return true;
    }

    public static ArrayList<GroupObjective> resetObjective(Group group) {

        ArrayList<GroupObjective> lstGroupObjective = DBUtils.getGroupRankObjective(group);
        for (GroupObjective groupObjective : new ArrayList<>(lstGroupObjective)){
            if (!group.getRank().getLstObjectif().contains(groupObjective.getObjective())) {
                lstGroupObjective.remove(groupObjective);
            }
        }

        // On s'assure d'avoir bien tous les objectifs qui existent
        for (Objective objective : group.getRank().getLstObjectif()){
            boolean find = false;
            for (GroupObjective groupObjective : lstGroupObjective){
                if(groupObjective.getObjective().equals(objective)){
                    find = true;
                    break;
                }
            }

            if(!find){
                lstGroupObjective.add(new GroupObjective(group, objective));
            }
        }

        // On remet le compteur a 0 des objectifs de ressources
        for (GroupObjective groupObjective : lstGroupObjective){
            Organisation.log.severe("RESET OBJECTIF AT STARTUP");
            if(groupObjective.getObjective().getType().equalsIgnoreCase(Objective.Type.RESSOURCE.toString()))groupObjective.update(0);
        }

        return lstGroupObjective;
    }

    //Getters
    public ItemStack getIcon() {

        ItemStack icon = null;

        if (this.objective.getType().equalsIgnoreCase(Objective.Type.RESSOURCE.toString()))icon = this.objective.getItem().clone();
        else if (this.objective.getType().equalsIgnoreCase(Objective.Type.BOOLEAN.toString())) icon = SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/2d5b58d6e7d4cd987d081b91efe09ee0d2078f0db43f50d0a6cf3b32770aea4f");
        else if (this.objective.getType().equalsIgnoreCase(Objective.Type.MINVASALL.toString())) icon = new ItemStack(Material.IRON_HELMET);
        else if (this.objective.getType().equalsIgnoreCase(Objective.Type.MINLAND.toString())) icon = SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/b1dd4fe4a429abd665dfdb3e21321d6efa6a6b5e7b956db9c5d59c9efab25");
        else if (this.objective.getType().equalsIgnoreCase(Objective.Type.MINPLAYER.toString())) icon = SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/cff32a8f4f0e4732985a73327a22d2b8a165d61bc29986d2575b00846ce3ec69");
        else icon = SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/ac4970ea91ab06ece59d45fce7604d255431f2e03a737b226082c4cce1aca1c4");

        if(icon == null)icon = new ItemStack(Material.REDSTONE_TORCH);

        ItemMeta meta = icon.getItemMeta();
        ArrayList<Component> lore = new ArrayList<>();

        lore.add(Component.text("§8Description : §7"+objective.getDescription()));

        if(this.objective.getType().equalsIgnoreCase(Objective.Type.RESSOURCE.toString())){
            lore.add(Component.text("§8Etat : §7"+this.currentObjectiveValue+"/" + this.objective.getObjectifValue()));
        }
        if(this.objective.getType().equalsIgnoreCase(Objective.Type.MINVASALL.toString())){

            if(this.getGroup() instanceof LargeGroup){
                int nbVassal = ((LargeGroup) this.getGroup()).getLstVassal().size();
                lore.add(Component.text("§8Etat : §7" + nbVassal + "/" + this.objective.getObjectifValue()));
            }
        }
        if(this.objective.getType().equalsIgnoreCase(Objective.Type.MINLAND.toString())){

            if(this.getGroup() instanceof LargeGroup){
                int nbLand = ((LargeGroup) this.getGroup()).getLstLand().size();
                lore.add(Component.text("§8Etat : §7" + nbLand + "/" + this.objective.getObjectifValue()));
            }
        }
        if(this.objective.getType().equalsIgnoreCase(Objective.Type.MINPLAYER.toString())){
            lore.add(Component.text("§8Etat : §7" + this.getGroup().getLstMember().size() + "/" + this.objective.getObjectifValue()));
        }

        if(this.isAcomplished()) meta.addEnchant(Enchantment.DIG_SPEED,1, true);
        String status = (this.isAcomplished())? "§aAccomplis": "§cNon accomplis";
        lore.add(Component.text("§8Status : " + status));

        meta.lore(lore);
        meta.setDisplayName("§6"+objective.getName().replace("_"," "));
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);

        icon.setItemMeta(meta);
        return icon;
    }

    public Objective getObjective() {
        return objective;
    }
    public Group getGroup() {
        return Group.getGroupById(this.groupId);
    }
    public int getCurrentObjectiveValue() {
        return currentObjectiveValue;
    }

}
