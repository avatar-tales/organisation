package fr.avatar_returns.organisation.rank;

import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.GroupRankPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.PlayerRankPacket;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public class PlayerRank extends Rank {

    private int id;
    private int minHeart;
    private int maxSlotInv;
    private boolean unlockLeftHand;
    private static ArrayList<PlayerRank> lstPlayerRank = new ArrayList<>();

    public PlayerRank( String name,  int order, int minHeart, int maxSlotInv, boolean unlockLeftHand){

        super(name, order);

        this.minHeart = 4;
        this.maxSlotInv = 9;
        this.unlockLeftHand = false;

        this.id = DBUtils.addRank(this);
        GeneralMethods.sendPacket(new PlayerRankPacket(this, OrganisationPacket.Action.CREATE));
    }
    public PlayerRank(int id, String name, int order, ArrayList<Objective> lstObjective, int minHeart, int maxSlotInv, boolean unlockLeftHand){

        super(name, order, lstObjective);

        this.minHeart = minHeart;
        this.unlockLeftHand = unlockLeftHand;
        this.maxSlotInv = maxSlotInv;

        PlayerRank.lstPlayerRank.add(this);
    }

    public int getId(){
        return this.id;
    }
    public static ArrayList<PlayerRank> getLstPlayerRank(){
        return PlayerRank.lstPlayerRank;
    }

    @Nullable public static PlayerRank getById(int id){
        for(PlayerRank playerRank : lstPlayerRank){
            if(playerRank.getId() == id)return playerRank;
        }
        return null;
    }
    @Override public boolean setOrder(int order, boolean storeDb) {

        if (order < 0) order = 0;
        int maxOrder = -1;

        for(PlayerRank rank: lstPlayerRank){
            if(rank.getOrder() >= maxOrder) maxOrder = rank.getOrder();
            if (rank.getOrder() >= order) {
                rank.order += 1;
                if(storeDb) DBUtils.setRankOrder(rank);
            }
        }

        if (order > maxOrder) order = maxOrder +1;

        this.order = order;
        if(storeDb) DBUtils.setRankOrder(this);
        return true;
    }

    public boolean setMinHeart(int nbMinHeart){
        return this.setMinHeart(nbMinHeart, true);
    }
    public boolean setMinHeart(int nbMinHeart, boolean storeDb){

        if (nbMinHeart < 1) return false;
        this.minHeart = nbMinHeart;
        if(storeDb)DBUtils.setRankNbMinHeart(this);

        return true;
    }

    public boolean setMaxSlotInv(int nbMaxSlotInv){
        return this.setMaxSlotInv(nbMaxSlotInv, true);
    }
    public boolean setMaxSlotInv(int nbMaxSlotInv, boolean storeDb){

        if(nbMaxSlotInv < 9 || nbMaxSlotInv > 9*3)return false;
        this.maxSlotInv = nbMaxSlotInv;
        if(storeDb)DBUtils.setRankMaxSlotInv(this);
        return true;
    }

    public boolean setUnLockLeftHand(boolean unlockLeftHand){return this.setUnLockLeftHand(unlockLeftHand, true);}
    public boolean setUnLockLeftHand(boolean unlockLeftHand, boolean storeDb){
        this.unlockLeftHand = unlockLeftHand;
        if(storeDb)DBUtils.setRankUnlockLeftHand(this);
        return true;
    }

    public int getMinHeart() {return minHeart;}
    public int getMaxSlotInv() {return maxSlotInv;}

    public boolean isUnlockLeftHand() { return unlockLeftHand; }

    public void remove(boolean storeDb){
        PlayerRank.lstPlayerRank.remove(this);
        if(storeDb) DBUtils.removeRank(this);
    }
}
