package fr.avatar_returns.organisation.rank;

import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.storage.DBUtils;
import org.jetbrains.annotations.Nullable;
import org.bukkit.inventory.ItemStack;
import java.util.ArrayList;

public abstract class Rank {

    private String name;
    protected int order;
    private ArrayList<Objective> lstObjective;

    public Rank(String name, int order){

        this.name = name;
        this.order = order;
        lstObjective = new ArrayList<>();
    }
    public Rank(String name, int order, ArrayList<Objective> lstObjective){

        this.name = name;
        this.order = order;
        this.lstObjective = lstObjective;
    }

    public abstract int getId();

    public String getName() {return name;}
    public int getOrder() { return order;}

    public ArrayList<Objective> getLstObjectif() {return lstObjective;}

    public boolean hasObjectif(String objectifName){
        for(Objective objective : lstObjective){
            if(objective.getName().equalsIgnoreCase(objectifName)){
                return true;
            }
        }
        return false;
    }

    public void setName(String newName){this.setName(newName);}
    public void setName(String newName, boolean storeDB){
        this.name = newName;
        if(storeDB)DBUtils.setRankName(this, newName);
    }

    public void remove(){this.remove(true);}
    public abstract void remove(boolean storeDb);


    @Nullable public Objective getObjectiveByName(String name){
        for(Objective objective : this.lstObjective){
            if(objective.getName().equalsIgnoreCase(name))return objective;
        }
        return null;
    }

    public void addObjectif(String name, String description, int objectifValue,ItemStack item, String type){
        this.addObjectif(name, description, objectifValue, item, type, true);
    }
    public void addObjectif(String name, String description, int objectifValue, ItemStack item, String type, boolean storeDb){
        Objective objective = new Objective(this, name, description, type, objectifValue, item, storeDb);
        this.lstObjective.add(objective);
    }

    public boolean setOrder(int order){
        return this.setOrder(order, true);
    }
    public abstract boolean setOrder(int order, boolean storeDb);

    public boolean deleteObjectif(String objectifName){return this.deleteObjectif(objectifName, true);}
    public boolean deleteObjectif(String objectifName, boolean storeDb){

        for(Objective objective : lstObjective){
            if(objective.getName().equalsIgnoreCase(objectifName)){

                //On retirer la progression de l'objectif sur les groupe qui l'avait.
                for(Group group : Group.getLstGroup()){
                    for(GroupObjective groupObjective: new ArrayList<>(group.getLstGroupObjective())){
                        if(groupObjective.getObjective() == objective) group.getLstGroupObjective().remove(groupObjective);
                    }
                }

                if(storeDb)DBUtils.deleteRankObjectif(objective);
                return lstObjective.remove(objective);
            }
        }
        return  false;
    }

}
