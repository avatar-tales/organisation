package fr.avatar_returns.organisation.hook;

import fr.avatar_returns.organisation.Organisation;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class Hook<T extends JavaPlugin> {

    private final T plugin;

    public Hook(T plugin) {
        this.plugin = plugin;
        Organisation.log.info("Hooked with " + plugin.getName());
    }

    public T getPlugin() {
        return this.plugin;
    }

}
