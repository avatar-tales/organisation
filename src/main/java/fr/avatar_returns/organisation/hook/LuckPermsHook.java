package fr.avatar_returns.organisation.hook;

import fr.alessevan.api.AvatarAPI;
import fr.alessevan.api.hooks.defaults.ILuckPermsHook;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;
import net.luckperms.api.node.NodeBuilder;
import org.bukkit.ChatColor;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Author : Aless


public class LuckPermsHook implements ILuckPermsHook<LuckPerms> {

    public LuckPermsHook() {

        Pattern prefixNamePattern = Pattern.compile("\\{prefix_(\\w{2,16})}", Pattern.CASE_INSENSITIVE);
        AvatarAPI.getAPI().registerStringHook(prefixNamePattern, message -> {
            final Matcher matcher = prefixNamePattern.matcher(message);
            final String name = matcher.results().toList().get(0).group(1);

            return ChatColor.translateAlternateColorCodes('&', this.getPrefix(name));
        });
        Pattern prefixUUIDPattern = Pattern.compile("\\{prefix_(\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12})}", Pattern.CASE_INSENSITIVE);
        AvatarAPI.getAPI().registerStringHook(prefixUUIDPattern, message -> {
            Matcher matcher = prefixUUIDPattern.matcher(message);
            UUID uuid = UUID.fromString(matcher.results().toList().get(0).group(1));

            return ChatColor.translateAlternateColorCodes('&', this.getPrefix(uuid));
        });

        Pattern suffixNamePattern = Pattern.compile("\\{suffix_(\\w{2,16})}", Pattern.CASE_INSENSITIVE);
        AvatarAPI.getAPI().registerStringHook(suffixNamePattern, message -> {
            Matcher matcher = suffixNamePattern.matcher(message);
            String name = matcher.results().toList().get(0).group(1);

            return ChatColor.translateAlternateColorCodes('&', this.getSuffix(name));
        });
        Pattern suffixUUIDPattern = Pattern.compile("\\{suffix_(\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12})}", Pattern.CASE_INSENSITIVE);
        AvatarAPI.getAPI().registerStringHook(suffixUUIDPattern, message -> {
            Matcher matcher = suffixUUIDPattern.matcher(message);
            UUID uuid = UUID.fromString(matcher.results().toList().get(0).group(1));

            return ChatColor.translateAlternateColorCodes('&', this.getSuffix(uuid));
        });

        Pattern rankNamePattern = Pattern.compile("\\{rank_(\\w{2,16})}", Pattern.CASE_INSENSITIVE);
        AvatarAPI.getAPI().registerStringHook(rankNamePattern, message -> {
            Matcher matcher = rankNamePattern.matcher(message);
            String name = matcher.results().toList().get(0).group(1);

            return ChatColor.translateAlternateColorCodes('&', this.getPrefix(name)) +
                    name + ChatColor.translateAlternateColorCodes('&', this.getSuffix(name));
        });
        Pattern rankUUIDPattern = Pattern.compile("\\{rank_(\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12})}", Pattern.CASE_INSENSITIVE);
        AvatarAPI.getAPI().registerStringHook(rankUUIDPattern, message -> {
            Matcher matcher = rankUUIDPattern.matcher(message);
            UUID uuid = UUID.fromString(matcher.results().toList().get(0).group(1));
            return ChatColor.translateAlternateColorCodes('&', this.getPrefix(uuid)) +
                    uuid + ChatColor.translateAlternateColorCodes('&', this.getSuffix(uuid));
        });
    }

    @Override public LuckPerms getHooked() {
        return LuckPermsProvider.get();
    }

    @Override public String getPrefix(String username) {
        if (username.equalsIgnoreCase("CONSOLE") || username.isBlank())
            return "";
        User user = this.getHooked().getUserManager().getUser(username);
        if (user != null) {
            String prefix = user.getCachedData().getMetaData().getPrefix();
            return prefix == null ? "" : prefix;
        } else {
            try {
                UUID uuid = this.getHooked().getUserManager().lookupUniqueId(username).get(50L, TimeUnit.MILLISECONDS);
                return getPrefix(uuid);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                return "";
            }
        }
    }
    @Override public String getSuffix(String username) {
        if (username.equalsIgnoreCase("CONSOLE") || username.isBlank())
            return "";
        User user = this.getHooked().getUserManager().getUser(username);
        if (user != null) {
            String suffix = user.getCachedData().getMetaData().getSuffix();
            return suffix == null ? "" : suffix;
        } else {
            try {
                UUID uuid = this.getHooked().getUserManager().lookupUniqueId(username).get(50L, TimeUnit.MILLISECONDS);
                return getPrefix(uuid);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                return "";
            }
        }
    }
    @Override public String getPrefix(UUID uuid) {
        try {
            User user = this.getHooked().getUserManager().loadUser(uuid).get(50L, TimeUnit.MILLISECONDS);
            String prefix = user.getCachedData().getMetaData().getPrefix();
            this.getHooked().getUserManager().cleanupUser(user);
            return prefix != null ? prefix : "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getPrefixColors(UUID uuid){
        String lastColor = "";
        try {

            User user = LuckPermsProvider.get().getUserManager().loadUser(uuid).get(50L, TimeUnit.MILLISECONDS);
            String prefix = user.getCachedData().getMetaData().getPrefix();
            LuckPermsProvider.get().getUserManager().cleanupUser(user);

            String regex = "&[0-9a-gA-Gk-oK-O]";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(prefix);

            // Find and store the last match
            while (matcher.find()) {
                lastColor = matcher.group();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return lastColor.replace("&","§");
    }

    @Override public String getSuffix(UUID uuid) {
        try {
            User user = this.getHooked().getUserManager().loadUser(uuid).get(50L, TimeUnit.MILLISECONDS);
            String suffix = user.getCachedData().getMetaData().getSuffix();
            this.getHooked().getUserManager().cleanupUser(user);
            return suffix != null ? suffix : "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    @Override public boolean hasPermission(UUID uuid, String permission) {
        try {
            User user = this.getHooked().getUserManager().loadUser(uuid).get(50L, TimeUnit.MILLISECONDS);
            boolean hasPermission = user.getCachedData().getPermissionData().checkPermission(permission).asBoolean();
            this.getHooked().getUserManager().cleanupUser(user);
            return hasPermission;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    @Override public void addPermission(UUID uuid, String... permissions) {
        this.addPermission(uuid, new HashMap<>(), permissions);
    }
    @Override public void addPermission(UUID uuid, Map<String, String> context, String... permissions) {
        try {
            this.getHooked().getUserManager().modifyUser(uuid, user -> {
                for (String permission : permissions) {
                    NodeBuilder<?, ?> node = Node.builder(permission);
                    for (Map.Entry<String, String> entry : context.entrySet()) {
                        node = node.withContext(entry.getKey(), entry.getValue());
                    }
                    user.data().add(node.build());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
