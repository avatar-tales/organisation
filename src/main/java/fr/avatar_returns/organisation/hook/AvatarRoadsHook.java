package fr.avatar_returns.organisation.hook;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.bakaaless.avatarroads.RoadPlugin;
import fr.bakaaless.avatarroads.roads.Road;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;

public class AvatarRoadsHook extends Hook<RoadPlugin> {

    private static ArrayList<OfflinePlayer> LstPlayerRoadDisable = new ArrayList<>();

    public AvatarRoadsHook(Plugin plugin) {
        super((RoadPlugin) plugin);
    }

    public static boolean isRoadEnableForPlayer(OfflinePlayer player){
        return !AvatarRoadsHook.LstPlayerRoadDisable.contains(player);
    }

    public void disableRoadsForPlayer(Player player) {
        try{
            if(!AvatarRoadsHook.LstPlayerRoadDisable.contains(player))AvatarRoadsHook.LstPlayerRoadDisable.add(player);
        }
        catch (ConcurrentModificationException e){}
        this.getPlugin().addMaskedPlayer(player);
    }
    public void disableRoadsForPlayer(Player player, long duration) {
        this.getPlugin().addMaskedPlayer(player, duration);
    }

    public void enableRoadsForPlayer(Player player) {

        try {
            if (AvatarRoadsHook.LstPlayerRoadDisable.contains(player))
                AvatarRoadsHook.LstPlayerRoadDisable.remove(player);
        }
        catch (ConcurrentModificationException e){}
        this.getPlugin().removeMaskedPlayer(player);
    }
    public void disableMarkerInRegion(ProtectedRegion region) {
        this.getPlugin().getServer().getScheduler().runTaskAsynchronously(this.getPlugin(), () ->
                Road.getRoads().stream()
                        .flatMap(road -> road.getMarkers().stream())
                        .filter(marker -> region.contains(marker.location().getBlockX(), marker.location().getBlockY(), marker.location().getBlockZ()))
                        .forEach(marker -> marker.setEnabled(false))
        );
    }
    public void enableMarkerInRegion(ProtectedRegion region) {
        this.getPlugin().getServer().getScheduler().runTaskAsynchronously(this.getPlugin(), () ->
                Road.getRoads().stream()
                        .flatMap(road -> road.getMarkers().stream())
                        .filter(marker -> region.contains(marker.location().getBlockX(), marker.location().getBlockY(), marker.location().getBlockZ()))
                        .forEach(marker -> marker.setEnabled(true))
        );
    }
}
