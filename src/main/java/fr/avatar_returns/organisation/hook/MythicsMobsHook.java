package fr.avatar_returns.organisation.hook;

import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.RP;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine;
import fr.bakaaless.avatarroads.RoadPlugin;
import io.lumine.mythic.api.adapters.AbstractLocation;
import io.lumine.mythic.bukkit.MythicBukkit;
import io.lumine.mythic.core.mobs.ActiveMob;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;

public class MythicsMobsHook extends Hook<MythicBukkit> {


    public MythicsMobsHook(Plugin plugin) {
        super((MythicBukkit) plugin);
    }

    public static void despawnMobsHostileInZoneRP(){

        for(ActiveMob mythicsMob : MythicBukkit.inst().getMobManager().getActiveMobs()){

            AbstractLocation mobLocation = mythicsMob.getLocation();
            if(mobLocation == null)continue;

            double x = mobLocation.getX();
            double y = mobLocation.getY();
            double z = mobLocation.getZ();
            World world = Bukkit.getWorld(mobLocation.getWorld().getName());

            Location location = new Location(world, x, y,z);

            LandSubTerritory landSubTerritory = LandSubTerritory.getLandSubTerritoryByLocation(location);
            if(!(landSubTerritory instanceof RP || landSubTerritory instanceof Mine))continue;

            mythicsMob.despawn();
        }
    }
}
