package fr.avatar_returns.organisation.hook;

import dev.lone.itemsadder.api.*;
import org.bukkit.block.Block;

import java.util.HashMap;


public class ItemAdderHook {

    private static Boolean hooked = null;
    private static HashMap<Block, Long> mapDoorOpenTime = new HashMap<>();

    public static boolean isItemAdderHooked(){
        if(ItemAdderHook.hooked != null) return ItemAdderHook.hooked.booleanValue();
        try{
            ItemsAdder.getAllItems();
            ItemAdderHook.hooked = true;
        }
        catch (NoClassDefFoundError exception){
            ItemAdderHook.hooked = false;
        }
        catch (NoSuchMethodError exception){
            ItemAdderHook.hooked = false;
        }

        return ItemAdderHook.hooked;
    }

    public static boolean isCustomBlock(Block block) {

        if(!ItemAdderHook.isItemAdderHooked())return false;

        if(block == null)return false;

        CustomBlock customBlock = CustomBlock.byAlreadyPlaced(block);
        if (customBlock != null) return true;

        CustomFurniture customFurniture = CustomFurniture.byAlreadySpawned(block);
        if (customFurniture != null) return true;

        CustomFire customFire = CustomFire.byAlreadyPlaced(block);
        if (customFire != null)return true;

        CustomCrop customCrop = CustomCrop.byAlreadyPlaced(block);
        if (customCrop != null) return true;

        return false;
    }

    public static boolean isFactionBoard(Block block){

        if(!ItemAdderHook.isItemAdderHooked())return false;
        if(block == null)return false;

        CustomFurniture customFurniture = CustomFurniture.byAlreadySpawned(block);
        if (customFurniture != null){
            if(customFurniture.getId().equals("medieval_market_decoration_v2_board_1"))return true;
        }

        return false;
    }

    public static boolean isDoor(Block block){

        if(!ItemAdderHook.isItemAdderHooked())return false;
        if(block == null)return false;

        CustomFurniture customFurniture = CustomFurniture.byAlreadySpawned(block);
        if (customFurniture != null){
            if(customFurniture.getId().contains("jf1_door_"))return true;
        }
        return false;
    }
    public static void openDoor(Block block){

        if(!isDoor(block))return;
        if(mapDoorOpenTime.containsKey(block)){
            if(mapDoorOpenTime.get(block) > System.currentTimeMillis())return;
        }

        CustomFurniture customFurniture = CustomFurniture.byAlreadySpawned(block);
        String newNameSpaceId = (customFurniture.getNamespacedID().contains("open"))? customFurniture.getNamespacedID().replace("open", "closed") : customFurniture.getNamespacedID().replace("closed", "open");

        customFurniture.replaceFurniture(newNameSpaceId);
        mapDoorOpenTime.put(block, System.currentTimeMillis() + 1000);
    }
}
