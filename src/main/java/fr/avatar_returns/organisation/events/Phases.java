package fr.avatar_returns.organisation.events;

import fr.avatar_returns.organisation.events.phasesObjectives.PhasesObjectives;
import fr.avatar_returns.organisation.storage.DBUtils;

import java.util.ArrayList;

public class Phases {

    private String name;
    private int order;
    private int eventId;
    private long timeElapsed;       // En secondes;
    private  long maxTimeDuration;  // En secondes;

    private ArrayList<PhasesObjectives> lstObjectives;
    private ArrayList<Rewards> lstRewards;

    public Phases(int eventId, String name, long maxTimeDuration, long timeElapsed, int order) {

        this.eventId = eventId;
        this.name = name;
        this.maxTimeDuration = maxTimeDuration;
        this.timeElapsed = timeElapsed;
        this.order = order;
        this.lstObjectives = new ArrayList<>();
        this.lstRewards = new ArrayList<>();

        //DBUtils.
    }
    public Phases(int eventId, String name, long maxTimeDuration, long timeElapsed, ArrayList<PhasesObjectives> lstObjectives, int order, ArrayList<Rewards> lstRewards) {

        this.eventId = eventId;
        this.name = name;
        this.maxTimeDuration = maxTimeDuration;
        this.timeElapsed = timeElapsed;
        this.order = order;
        this.lstObjectives = lstObjectives;
        this.lstRewards = lstRewards;
    }

    public int getEventId() { return eventId; }
    public String getName() {
        return name;
    }
    public String getDisplayName() {return this.getDisplayName().replace("_"," ");}

    public void addPhasesObjectives(PhasesObjectives phasesObjectives){this.addPhasesObjectives(phasesObjectives, true);}
    public void addPhasesObjectives(PhasesObjectives phasesObjectives, boolean storeDb){
        this.lstObjectives.add(phasesObjectives);
    }
    public void removePhasesObjectives(PhasesObjectives phasesObjectives){this.removePhasesObjectives(phasesObjectives, true);}
    public void removePhasesObjectives(PhasesObjectives phasesObjectives, boolean storeDB){this.lstObjectives.remove(phasesObjectives);}
    public ArrayList<PhasesObjectives> getLstObjectives() {
        return lstObjectives;
    }

    public void setMaxTimeDuration(long maxTimeDuration) {this.setMaxTimeDuration(maxTimeDuration, true);}
    public void setMaxTimeDuration(long maxTimeDuration, boolean storeDB) {
        this.maxTimeDuration = maxTimeDuration;
        if(storeDB)DBUtils.setPhaseMaxTimeDuration(this);
    }
    public long getMaxTimeDuration() {
        return maxTimeDuration;
    }

    public void setTimeElapsed(long timeElapsed) {this.setTimeElapsed(timeElapsed, true);}
    public void setTimeElapsed(long timeElapsed, boolean storeDB) {
        this.timeElapsed = timeElapsed;
        if(storeDB)DBUtils.setPhaseTimeElapsed(this);
    }
    public long getTimeElapsed() {
        return timeElapsed;
    }

    public void addReward(Rewards rewards){this.addReward(rewards, true);}
    public void addReward(Rewards rewards, boolean storeDb){
        this.lstRewards.add(rewards);
        if(storeDb)DBUtils.addReward(rewards);
    }
    public void removeReward(Rewards rewards){this.removeReward(rewards, true);}
    public void removeReward(Rewards rewards, boolean storeDB){
        this.lstRewards.remove(rewards);
        if(storeDB)DBUtils.removeReward(rewards);
    }
    public ArrayList<Rewards> getLstRewards() {
        return lstRewards;
    }

    public int getOrder() {return order;}
    public void setOrder(int order) {this.setOrder(order, true);}
    public void setOrder(int order, boolean storeDb) {
        this.order = order;
        if(storeDb)DBUtils.setPhaseOrder(this);
    }

}
