package fr.avatar_returns.organisation.events.phasesObjectives;

import fr.avatar_returns.organisation.events.Rewards;

import java.util.ArrayList;
import java.util.UUID;

public abstract class PhasesObjectives {


    private String name;
    private UUID playerAcomplished;
    private ArrayList<Rewards> lstRewards;

    protected PhasesObjectives(String name) {
        this.name = name;
        this.playerAcomplished = null;
        this.lstRewards = new ArrayList<>();
    }
    protected PhasesObjectives(String name, UUID playerAcomplished, ArrayList<Rewards> lstRewards) {
        this.name = name;
        this.playerAcomplished = playerAcomplished;
        this.lstRewards = lstRewards;
    }

    public String getName() {
        return name;
    }

    public void addReward(Rewards rewards){this.addReward(rewards, true);}
    public void addReward(Rewards rewards, boolean storeDb){this.lstRewards.add(rewards);}
    public void removeReward(Rewards rewards){this.removeReward(rewards, true);}
    public void removeReward(Rewards rewards, boolean storeDB){this.lstRewards.remove(rewards);}

    public ArrayList<Rewards> getLstRewards() {
        return this.lstRewards;
    }

    public abstract boolean isAcomplished();
}
