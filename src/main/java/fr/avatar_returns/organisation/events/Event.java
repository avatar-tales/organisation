package fr.avatar_returns.organisation.events;

import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.storage.DBUtils;

import java.util.ArrayList;
import java.util.UUID;

public class Event {

    int id;
    String name;
    String date;
    String timeRepetition;
    boolean repeat;

    ArrayList<String> lstArea;
    ArrayList<Phases> lstPhases;

    Group groupWiner;
    UUID playerWinerUUID;

    public Event (String name, String date, String timeRepetition, boolean repeat, ArrayList<String> lstArea, ArrayList<Phases> lstPhases) {


        this.name = name;
        this.date = date;
        this.timeRepetition = timeRepetition;
        this.repeat = repeat;
        this.lstArea = lstArea;
        this.lstPhases = lstPhases;

        this.id = DBUtils.addEvent(this);

    }

    public Event (int id,  String name, String date, String timeRepetition, boolean repeat, ArrayList<String> lstArea, ArrayList<Phases> lstPhases) {

        this.id = id;
        this.name = name;
        this.date = date;
        this.timeRepetition = timeRepetition;
        this.repeat = repeat;
        this.lstArea = lstArea;
        this.lstPhases = lstPhases;

    }

    public int getId() { return id; }

    public void setName(String name) {this.setName(name, true);}
    public void setName(String name, boolean storeDB) {
        this.name = name;
        if(storeDB) DBUtils.setEventName(this);
    }
    public String getName() { return name; }

    public void setDate(String date) {this.setDate(date, true);}
    public void setDate(String date, boolean storeDB) {
        this.date = date;
        if(storeDB) DBUtils.setEventDate(this);
    }
    public String getDate() { return date; }

    public void setTimeRepetition(String timeRepetition) {this.setTimeRepetition(timeRepetition, true);}
    public void setTimeRepetition(String timeRepetition, boolean storeDB) {
        this.timeRepetition = timeRepetition;
        if(storeDB) DBUtils.setEventTimeRepetition(this);
    }
    public String getTimeRepetition() { return timeRepetition; }

    public ArrayList<String> getLstArea() { return lstArea;}
    public ArrayList<Phases> getLstPhases() {return lstPhases;}

    public void setRepeat(boolean repeat) {this.setRepeat(repeat, true);}
    public void setRepeat(boolean repeat, boolean storeDb) {
        this.repeat = repeat;
        if(storeDb) DBUtils.setEventTimeRepeat(this);
    }
    public boolean isRepeat() { return repeat; }

}
