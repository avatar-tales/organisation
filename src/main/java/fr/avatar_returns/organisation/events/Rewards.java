package fr.avatar_returns.organisation.events;

import fr.avatar_returns.organisation.storage.DBUtils;
import org.bukkit.inventory.ItemStack;

public class Rewards {

    int eventId;
    String phaseName;
    String name;
    ItemStack item;
    int amount;

    public Rewards(int eventId, String phaseName, ItemStack item, String name, int amount) {

        this.eventId = eventId;
        this.phaseName= phaseName;

        this.item = item;
        this.name = name;
        this.amount = amount;
    }

    public int getEventId() {return eventId;}
    public int getAmount() {return amount;}
    public String getPhaseName() {return phaseName;}
    public String getName() {return name;}
    public ItemStack getItem() {return item;}
    public int getAmout() {return amount;}

}
