package fr.avatar_returns.organisation.relation;

import com.destroystokyo.paper.event.profile.PreLookupProfileEvent;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.storage.DBUtils;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public abstract class Relation {

    private static ArrayList<Relation> lstRelation = new ArrayList<>();

    final int id;
    private final long startRelationTime;
    private long endRelationTime;
    private String rolePlay;
    protected String phase;
    private final LargeGroup initiator;
    private final LargeGroup target;
    private OfflinePlayer relationEnder;

    Relation(LargeGroup initiator, LargeGroup target){

        this.startRelationTime = System.currentTimeMillis();
        this.endRelationTime = -1;

        this.initiator = initiator;
        this.target  =target;

        this.rolePlay = "";
        this.phase = "";

        lstRelation.add(this);
        this.id =DBUtils.addRelation(this);
    }
    Relation(int id, LargeGroup initiator, LargeGroup target, long startRelationTime, long endRelationTime, String rolePlay, String phase){

        this.id = id;
        this.startRelationTime = startRelationTime;
        this.endRelationTime = endRelationTime;

        this.initiator = initiator;
        this.target  = target;

        this.rolePlay = rolePlay;
        this.phase = phase;

        lstRelation.add(this);
    }

    //Setter
    public void setPhase(String phase){this.setPhase(phase, true);}
    public void setPhase(String phase, boolean storeDb) {
        if(storeDb)DBUtils.setRelationPhase(this,phase);
        this.phase = phase;
    }

    public boolean setRolePlay(String rolePlay){return this.setRolePlay(rolePlay, true);}
    public boolean setRolePlay(String rolePlay, boolean storeDb){
        if(this.isActive()){
            if(storeDb)DBUtils.setRelationRolePlay(this,rolePlay);
            this.rolePlay = rolePlay;
        }
        return this.isActive();
    }

    //Getter
    public int getId() {
        return this.id;
    }

    public boolean isActive(){
        return this.getEndRelationTime() < this.getStartRelationTime() && System.currentTimeMillis() < this.endRelationTime;
    }
    public LargeGroup getInitiator() {
        return initiator;
    }
    public LargeGroup getTarget() {
        return target;
    }

    public long getStartRelationTime() {
        return startRelationTime;
    }
    public long getEndRelationTime() {
        return endRelationTime;
    }

    public void setEndRelationTimeSynchro(long endRelationTime){this.setEndRelationTime(endRelationTime, false);}
    protected void setEndRelationTime(long endRelationTime){this.endRelationTime = endRelationTime;}
    protected void setEndRelationTime(long endRelationTime, boolean storeDb){
        if(storeDb)DBUtils.setEndRelationTime(this,endRelationTime);
        this.endRelationTime = endRelationTime;
    }

    public String getPhase() {
        return phase;
    }
    public String getRolePlay() {
        return rolePlay;
    }

    public static ArrayList<Relation> getLstRelation() {
        return lstRelation;
    }

    public OfflinePlayer getRelationEnder() {
        return this.relationEnder;
    }
}
