package fr.avatar_returns.organisation.relation;

import fr.avatar_returns.organisation.organisations.LargeGroup;

public class Ally extends Relation{

    public Ally (LargeGroup initiator, LargeGroup target){
        super(initiator,target);
    }
    public Ally (int id, LargeGroup initiator, LargeGroup target, long startRelationTime, long endRelationTime, String rolePlay, String phase){
        super(id, initiator,target, startRelationTime, endRelationTime, rolePlay, phase);
    }
}
