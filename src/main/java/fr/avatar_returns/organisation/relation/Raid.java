package fr.avatar_returns.organisation.relation;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.hook.AvatarRoadsHook;
import fr.avatar_returns.organisation.listener.LandPlayerEventManager;
import fr.avatar_returns.organisation.rabbitMQPackets.player.BossBarPacket;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Totem;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Raid extends Relation{

    @Nullable private  Land targetLand;
    private CopyOnWriteArrayList<OrganisationPlayer> lstAttacker = new CopyOnWriteArrayList<>();
    private static CopyOnWriteArrayList<Raid> lstRunningRaid = new CopyOnWriteArrayList<>();

    final private static long matchMakingDuration = GeneralMethods.getLongConf("Organisation.Raid.matchMakingMinuteDuration") * 60000;
    final private static long raidDuration =  GeneralMethods.getLongConf("Organisation.Raid.raidMinuteDuration") * 60000;
    final private static long timeToCapture  = GeneralMethods.getLongConf("Organisation.Raid.secondeToCapture");
    final private static double captureRayAroundTotem =  GeneralMethods.getDoubleConf("Organisation.Raid.captureRayAroundTotem");

    final private static long timeToBeExperimentedAsPlayer = GeneralMethods.getLongConf("Organisation.Raid.timeToBeExperimentedAsPlayer");
    final private static long timeToBeExperimentedAsOrganisationMember = GeneralMethods.getLongConf("Organisation.Raid.timeToBeExperimentedAsOrganisationMember");
    final private static double acceptableMultiplicatorAttackerExperimentedPlayer = GeneralMethods.getLongConf("Organisation.Raid.acceptableMultiplicatorAttackerExperimentedPlayer");
    final private static int howManyNewsPlayerAsToBeConsiderAsExperimtentedPlayer = GeneralMethods.getIntConf("Organisation.Raid.howManyNewsPlayerAsToBeConsiderAsExperimtentedPlayer");

    private long defenderTickets;
    private long attackerTickets;

    private HashMap<PositionPointInterest, Long> mapLastSecondTime;
    private HashMap<PositionPointInterest, Integer> mapPositionCapturedSeconde;
    private HashMap<PositionPointInterest, LargeGroup> mapPositionGroupOwner;
    private HashMap<PositionPointInterest, ArrayList<Player>> mapPositionPlayerOnPlate;
    private ArrayList<OfflinePlayer> lstDefenderRespawn = new ArrayList<>(); // La liste de joueur autorisé a respawn dans le Land le temps du Raid.

    public Raid (LargeGroup initiator, Land land){

        super(initiator,land.getOwner());

        this.targetLand = land;
        this.defenderTickets = 0;
        this.attackerTickets = 0;
        this.mapPositionGroupOwner = new HashMap<>();
        this.mapPositionPlayerOnPlate = new HashMap<>();
        this.mapPositionCapturedSeconde = new HashMap<>();
        this.mapLastSecondTime = new HashMap<>();
        // Initiate target land owner as owner of each totem;
        for(Totem totem : this.targetLand.getLstTotem()) {
            for (PositionPointInterest positionPointInterest : totem.getPositionPointOfInterest()) {
                if (positionPointInterest.getContext() == PositionPointInterest.Type.TOTEM) {
                    this.mapPositionGroupOwner.put(positionPointInterest, targetLand.getOwner());
                    this.mapPositionPlayerOnPlate.put(positionPointInterest, new ArrayList<Player>());
                    this.mapPositionCapturedSeconde.put(positionPointInterest, 0);
                }
            }
        }
        this.setPhase("MATCHMAKING");

        if(Organisation.getHook(AvatarRoadsHook.class).isPresent()) {
            Organisation.getHook(AvatarRoadsHook.class).get().disableMarkerInRegion(this.targetLand.getWorldGuardRegion());
            for(OrganisationPlayer oPlayer: this.getLstAttacker()){
                GeneralMethods.sendPlayerMessage(oPlayer,"Commands.Raid.DisableRoad", true);
            }
            for(OrganisationPlayer oPlayer: this.getTarget().getLstGroupConnectedOrganisationPlayer()){
                GeneralMethods.sendPlayerMessage(oPlayer,"Commands.Raid.DisableRoad", true);
            }
        }
//        if(Organisation.getHook(ProjectKorraHook.class).isPresent()) {
//
//            ArrayList<OrganisationPlayer> lstToRemoveDisableActionBar = new ArrayList<>(this.getTarget().getLstGroupConnectedOrganisationPlayer());
//            lstToRemoveDisableActionBar.addAll(this.getLstAttacker());
//
//            for (OrganisationPlayer organisationPlayer : lstToRemoveDisableActionBar) {
//                Organisation.getHook(ProjectKorraHook.class).get().disableActionBar(organisationPlayer);
//            }
//        }

        Raid.lstRunningRaid.add(this);
    }
    public Raid (int id, LargeGroup initiator, LargeGroup target, long startRelationTime, long endRelationTime, String rolePlay, String phase){
        super(id, initiator,target, startRelationTime, endRelationTime, rolePlay, phase);

        this.targetLand = null;
        this.defenderTickets = 0;
        this.attackerTickets = 0;
        this.mapPositionGroupOwner = new HashMap<>();

    }

    private boolean progress(){

        // On autorise les joueurs qui ont rejoint le land a pouvoir respawn automatiquement dedans.
        for(OfflinePlayer offlinePlayer : targetLand.getPlayerOnLand()){
            if(targetLand.getOwner().getLocalPlayers().contains(offlinePlayer) && !this.lstDefenderRespawn.contains(offlinePlayer)) {
                this.lstDefenderRespawn.add(offlinePlayer);
                GeneralMethods.sendPlayerMessage(offlinePlayer,"Vous venez de rejoindre le land RAID. Pendant la durée du RAID vous respawnerez donc aléatoirement dans ce land.");
            }
        }

        if(this.getPhase().equals("MATCHMAKING")){

            long timeBeforeRaid = this.getStartRelationTime() + Raid.matchMakingDuration - System.currentTimeMillis();

            long minutes = (long)(timeBeforeRaid/60000);
            long secondes = (long)((timeBeforeRaid/1000) - (minutes*60));

            String minutesStr = (minutes<10)?"0"+minutes : ""+minutes;
            String secondesStr = (secondes<10)?"0"+secondes : ""+secondes;

            String msg = "§7Temps avant le début du raid : §6"+minutesStr+":"+secondesStr;
            for(OrganisationPlayer oPlayer: this.getTarget().getLstGroupConnectedOrganisationPlayer()){
                String bossBarMsg = "§cUn raid va démarrer sur " + targetLand.getName().replace("_", " ");
                if(oPlayer.getOfflinePlayer() == null) GeneralMethods.sendPacket(new BossBarPacket(oPlayer, bossBarMsg));
                if(oPlayer.getOfflinePlayer().getPlayer() == null) GeneralMethods.sendPacket(new BossBarPacket(oPlayer, bossBarMsg));

                Player player = oPlayer.getOfflinePlayer().getPlayer();
                if(player == null){
                    GeneralMethods.sendPacket(new BossBarPacket(oPlayer, bossBarMsg));
                    continue;
                }

                LandPlayerEventManager.initPlayerBossBar(player);
                LandPlayerEventManager.mapPlayerPriorBossBarMessage.put(player, bossBarMsg);
            }

            for(OrganisationPlayer oPlayer: this.getLstAttacker()){
                oPlayer.sendActionBar(msg);
            }
            for(OrganisationPlayer oPlayer: this.getTarget().getLstGroupConnectedOrganisationPlayer()){
                oPlayer.sendActionBar(msg);
            }

            if(System.currentTimeMillis() > this.getStartRelationTime() + Raid.matchMakingDuration ){

                for(OrganisationPlayer oPlayer: this.getTarget().getLstGroupConnectedOrganisationPlayer()) {
                    if(oPlayer.getOfflinePlayer() == null) GeneralMethods.sendPacket(new BossBarPacket(oPlayer, ""));
                    if(oPlayer.getOfflinePlayer().getPlayer() == null) GeneralMethods.sendPacket(new BossBarPacket(oPlayer, ""));
                    LandPlayerEventManager.mapPlayerPriorBossBarMessage.put(oPlayer.getOfflinePlayer().getPlayer(), "");
                }
                for(Totem totem : this.targetLand.getLstTotem()) {
                    for (PositionPointInterest positionPointInterest : totem.getPositionPointOfInterest()) {
                        if (positionPointInterest.getContext() == PositionPointInterest.Type.TOTEM) {
                            this.mapLastSecondTime.put(positionPointInterest, System.currentTimeMillis()/1000);
                        }
                    }
                }


                this.setPhase("RAID");
            }
        }
        else if (this.getPhase().equals("RAID")){

            long timeBeforeEndRaid = this.getStartRelationTime() + Raid.matchMakingDuration + Raid.raidDuration- System.currentTimeMillis();

            long minutes = (long)(timeBeforeEndRaid/60000);
            long secondes = (long)((timeBeforeEndRaid/1000) - (minutes*60));

            String minutesStr = (minutes<10)?"0"+minutes : ""+minutes;
            String secondesStr = (secondes<10)?"0"+secondes : ""+secondes;

            String msg = "§7Temps avant la fin du raid : §c"+minutesStr+":"+secondesStr;

            ArrayList<Player> onPlatePlayer = new ArrayList<>();
            for(PositionPointInterest positionPointInterest : mapPositionGroupOwner.keySet()){

                if(mapPositionGroupOwner.get(positionPointInterest).equals(this.getInitiator())){
                    this.attackerTickets ++;
                }
                else {
                    this.defenderTickets ++;
                }

                LargeGroup tmpGroupOwner = this.whoIsOnTheTotem(positionPointInterest);
                long currentSecondTime = System.currentTimeMillis()/1000;
                if(tmpGroupOwner == null){
                    this.mapLastSecondTime.put(positionPointInterest, currentSecondTime);
                    if(this.mapPositionPlayerOnPlate.get(positionPointInterest).isEmpty())this.mapPositionCapturedSeconde.put(positionPointInterest, 0);
                    continue;
                }
                else if(!tmpGroupOwner.equals(mapPositionGroupOwner.get(positionPointInterest))){

                    long count = (Raid.timeToCapture - this.mapPositionCapturedSeconde.get(positionPointInterest));
                    String msgCapture = "§dCapture de la plaque dans : §6"+((count<10)? "0"+count : count);
                    for(Player player : this.mapPositionPlayerOnPlate.get(positionPointInterest)){
                        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(msgCapture));
                        onPlatePlayer.add(player);
                    }

                    if(this.mapLastSecondTime.get(positionPointInterest) < currentSecondTime){

                         int capturedSecond = this.mapPositionCapturedSeconde.get(positionPointInterest);
                         capturedSecond += (currentSecondTime -  this.mapLastSecondTime.get(positionPointInterest));
                        this.mapPositionCapturedSeconde.put(positionPointInterest, capturedSecond);
                        this.mapLastSecondTime.put(positionPointInterest,currentSecondTime);
                    }

                    if(this.mapPositionCapturedSeconde.get(positionPointInterest) > Raid.timeToCapture){

                        mapPositionGroupOwner.put(positionPointInterest, tmpGroupOwner);
                        String locationStr = "§7("+positionPointInterest.getLocation().getBlockX()+","+positionPointInterest.getLocation().getBlockY()+","+positionPointInterest.getLocation().getBlockZ()+")";
                        this.sendAllMessage("§6"+tmpGroupOwner.getName() + " possède désormais le Totem en "+locationStr+"§6 sur la province "+this.targetLand.getProvince().getName());
                        this.mapPositionCapturedSeconde.put(positionPointInterest, 0);
                    }
                }
                else{
                    this.mapLastSecondTime.put(positionPointInterest, currentSecondTime);
                    this.mapPositionCapturedSeconde.put(positionPointInterest, 0);
                }

                this.mapPositionPlayerOnPlate.put(positionPointInterest, new ArrayList<>());
            }

            for(OrganisationPlayer oPlayer: this.getLstAttacker()){

                if(oPlayer.getOfflinePlayer() == null){
                    oPlayer.sendActionBar(msg);
                    continue;
                }
                if(oPlayer.getOfflinePlayer().getPlayer() == null){
                    oPlayer.sendActionBar(msg);
                    continue;
                }

                Player player = oPlayer.getOfflinePlayer().getPlayer();
                if(!onPlatePlayer.contains(player))oPlayer.sendActionBar(msg);
            }
            for(OrganisationPlayer oPlayer: this.getTarget().getLstGroupConnectedOrganisationPlayer()){
                if(oPlayer.getOfflinePlayer() == null){
                    oPlayer.sendActionBar(msg);
                    continue;
                }
                if(oPlayer.getOfflinePlayer().getPlayer() == null){
                    oPlayer.sendActionBar(msg);
                    continue;
                }

                Player player = oPlayer.getOfflinePlayer().getPlayer();
                if(!onPlatePlayer.contains(player))oPlayer.sendActionBar(msg);
            }

            if(System.currentTimeMillis() > this.getStartRelationTime() + Raid.matchMakingDuration + Raid.raidDuration)this.setPhase("END");
        }
        else if(this.getPhase().equals("END")){

            boolean newOwner = this.attackerTickets > this.defenderTickets;

            String title = (newOwner)? "§6Victoire" : "§cDéfaite";
            String subTitle = (this.targetLand.isMainLand())? "" : (newOwner)? "§6Vous posséder désormais le land " + this.targetLand.getName() : "§cVous n'avez pas réussi a capturer le land "+this.targetLand.getName();
            this.sendAttackerTitle(title,subTitle);

            title = (newOwner)? "§cDéfaite" : "§6Victoire";
            subTitle = (this.targetLand.isMainLand())? "" : (newOwner)? "§cVous perdez le land" + this.targetLand.getName() : "§6Vous conservez le land "+this.targetLand.getName();
            this.sendDefenderTitle(title,subTitle);

            if(newOwner){
                if(this.targetLand.isMainLand()){
                    this.setRolePlay((newOwner)? this.getInitiator().getName() + " a échoué face à "+this.getTarget().getName() + " sur le land "+this.targetLand.getName() :
                            ""+this.getInitiator() + " a gagné le raid sur le land "+this.targetLand.getName() + " de "+this.getTarget().getName());

                }
                else {
                    GroupRank.checkProgression(this.getInitiator());
                    GroupRank.checkProgression(this.getTarget());

                    this.targetLand.setOwner(this.getInitiator());
                    this.setRolePlay((newOwner)? "Le raid de "+this.getInitiator().getName() + " a échoué face à "+this.getTarget().getName() + " sur le land "+this.targetLand.getName() :
                            ""+this.getInitiator() + " a capturé le land "+this.targetLand.getName() + " de "+this.getTarget().getName());

                    for(PositionPointInterest positionPointInterest : this.targetLand.getPositionPointOfInterest()){
                        PositionPointInterest.setBanner(positionPointInterest, this.getInitiator());
                    }

                }
            }

            if(Organisation.getHook(AvatarRoadsHook.class).isPresent()) {
                Organisation.getHook(AvatarRoadsHook.class).get().enableMarkerInRegion(this.targetLand.getWorldGuardRegion());
                for(OrganisationPlayer oPlayer: this.getLstAttacker()){
                    GeneralMethods.sendPlayerMessage(oPlayer,"Commands.Raid.EnableRoad", true);
                }
                for(OrganisationPlayer oPlayer: this.getTarget().getLstGroupConnectedOrganisationPlayer()){
                    GeneralMethods.sendPlayerMessage(oPlayer,"Commands.Raid.EnableRoad", true);
                }
            }
            this.getInitiator().setDailyRaid(this.getInitiator().getDailyRaid()+1);
            this.setEndRelationTime(System.currentTimeMillis());

            this.lstDefenderRespawn =  new ArrayList<>();
            this.remove();
            return true;
        }
        return false;
    }
    private void remove(){
        this.targetLand = null;
    }
    @Nullable public LargeGroup whoIsOnTheTotem(PositionPointInterest pointInterest){

        boolean attakerOwn = false;
        boolean defenderOwn = false;

        for(OrganisationPlayer oAttacker : lstAttacker){
            if(oAttacker.getOfflinePlayer() == null)continue;
            if(oAttacker.getOfflinePlayer().getPlayer() == null)continue;
            if(oAttacker.getOfflinePlayer().getPlayer().isDead())continue;
            if(!isOnTotem(oAttacker.getOfflinePlayer().getPlayer(), pointInterest))continue;

            attakerOwn = true;
        }
        for(OfflinePlayer defender : this.getTarget().getLocalOnlinePlayers()){

            if(((Player) defender).isDead())continue;
            if( !isOnTotem((Player) defender, pointInterest))continue;

            defenderOwn = true;
        }

        if(attakerOwn && defenderOwn)return null;
        else if(attakerOwn){
            return this.getInitiator();
        }
        else if(defenderOwn)return this.getTarget();

        return null;
    }

    public CopyOnWriteArrayList<OrganisationPlayer> getLstAttacker() {
        return lstAttacker;
    }
    public void interrupt(){

        for(OrganisationPlayer oPlayer: this.getTarget().getLstGroupConnectedOrganisationPlayer()) {
            if(oPlayer.getOfflinePlayer() == null) GeneralMethods.sendPacket(new BossBarPacket(oPlayer, ""));
            if(oPlayer.getOfflinePlayer().getPlayer() == null) GeneralMethods.sendPacket(new BossBarPacket(oPlayer, ""));
            LandPlayerEventManager.mapPlayerPriorBossBarMessage.put(oPlayer.getOfflinePlayer().getPlayer(), "");
        }
        this.setPhase("END");
    }
    private void sendAllMessage(String message){
        for(OrganisationPlayer oPlayer : this.getInitiator().getLstGroupConnectedOrganisationPlayer())GeneralMethods.sendPlayerMessage(oPlayer,message);
        for(OrganisationPlayer oPlayer : this.getTarget().getLstGroupConnectedOrganisationPlayer())GeneralMethods.sendPlayerMessage(oPlayer,message);
    }
    private boolean isOnTotem(Player player, PositionPointInterest positionPointInterest){


        if(player.getLocation().getX() < positionPointInterest.getLocation().getX() - Raid.captureRayAroundTotem || player.getLocation().getX() > positionPointInterest.getLocation().getX() + Raid.captureRayAroundTotem) return false;
        if(player.getLocation().getZ() < positionPointInterest.getLocation().getZ() - Raid.captureRayAroundTotem || player.getLocation().getZ() > positionPointInterest.getLocation().getZ() + Raid.captureRayAroundTotem) return false;
        if(player.getLocation().getBlockY() < positionPointInterest.getLocation().getY() || player.getLocation().getBlockY() > positionPointInterest.getLocation().getBlockY() + 3)return false;

        this.mapPositionPlayerOnPlate.get(positionPointInterest).add(player);
        return true;
    }
    public static int getOnLineExperimentedPlayer(LargeGroup largeGroup){

        // System.out.println("Player experimented time : "+Raid.timeToBeExperimentedAsPlayer);
        // System.out.println("GroupMember experimented time : "+Raid.timeToBeExperimentedAsOrganisationMember);

        int experimentedPlayer = 0;
        for(OrganisationMember organisationMember : largeGroup.getLstMember()){

            if(!organisationMember.isConnected())continue;

            // System.out.println(organisationMember.getOrganisationPlayer().getLastEffectivePlayedTime() +" >= "+ Raid.timeToBeExperimentedAsPlayer);
            // System.out.println(organisationMember.getLastEffectivePlayedTime() +" >= "+ Raid.timeToBeExperimentedAsOrganisationMember);

            if(organisationMember.getLastEffectivePlayedTime() >= Raid.timeToBeExperimentedAsOrganisationMember) experimentedPlayer++;
            else if(organisationMember.getOrganisationPlayer().getLastEffectivePlayedTime() >= Raid.timeToBeExperimentedAsPlayer) experimentedPlayer++;
        }
        return experimentedPlayer;
    }
    public int getMaxAttacker(){

        int experimentedPlayer = Raid.getOnLineExperimentedPlayer(this.getTarget());
        int newPlayer = this.getTarget().getLstGroupConnectedOrganisationPlayer().size() - experimentedPlayer;

        return (int)((experimentedPlayer * Raid.acceptableMultiplicatorAttackerExperimentedPlayer) + (newPlayer/Raid.howManyNewsPlayerAsToBeConsiderAsExperimtentedPlayer));
    }

    public static CopyOnWriteArrayList<Raid> getLstRunningRaid(){return lstRunningRaid;}
    public static ArrayList<Land> getTargetLandRaid(){
        ArrayList<Land> lstTargetLandRaid = new ArrayList<>();
        for(Raid raid : lstRunningRaid){
            lstTargetLandRaid.add(raid.targetLand);
        }
        return lstTargetLandRaid;
    }
    @Nullable public static Raid getRaidByAttacker(LargeGroup attackerGroup){
        for(Raid raid : Raid.lstRunningRaid){
            if(raid.getInitiator().equals(attackerGroup))return raid;
        }
        return null;
    }
    @Nullable public static Raid getRaidByDefender(LargeGroup defenderGroup) {
        for(Raid raid : Raid.lstRunningRaid){
            if(raid.getTarget().equals(defenderGroup))return raid;
        }
        return null;
    }
    @Nullable public static Raid getRaidByTargetLand(Land land) {
        for(Raid raid : Raid.lstRunningRaid){
            if(raid.targetLand.equals(land))return raid;
        }
        return null;
    }

    public void sendDefenderTitle(String title, String subTitle) {
        for (OrganisationPlayer oPlayer : this.getTarget().getLstGroupConnectedOrganisationPlayer()) {
            oPlayer.sendFullTitle(10, 50, 10, title, subTitle);
        }
    }
    public void sendAttackerTitle(String title, String subTitle){
        for(OrganisationPlayer oPlayer : this.getInitiator().getLstGroupConnectedOrganisationPlayer()){
            oPlayer.sendFullTitle( 10, 50, 10, title , subTitle);
        }
    }

    public int getCapturedSecond(Player player, PositionPointInterest positionPointInterest) {

        // Function which return capture time if player is allowed to see it.
        // Otherwise, this function return -1

        LargeGroup playerGroup = (lstAttacker.contains(player))? this.getInitiator() : (lstAttacker.contains(player))?this.getTarget() : null;

        if(playerGroup == null)return -1;
        if(!this.mapPositionGroupOwner.containsKey(positionPointInterest))return -1;

        LargeGroup ownerGroup = this.mapPositionGroupOwner.get(positionPointInterest);
        if(ownerGroup.equals(playerGroup))return -1;
        if(this.whoIsOnTheTotem(positionPointInterest) == null)return -1;

        return this.mapPositionCapturedSeconde.get(positionPointInterest);
    }
    public boolean canPlayerRespawnInLand(OfflinePlayer player){
        return this.lstDefenderRespawn.contains(player);
    }

    @Nullable public Location getBestPlayerLocationPoint(OfflinePlayer player){

        if(!this.canPlayerRespawnInLand(player))return null;

        HashMap<PositionPointInterest, Double> mapSpawnDistance = new HashMap<>();

        for(PositionPointInterest positionPointInterest : this.targetLand.getPositionPointOfInterest()){
            if(!positionPointInterest.getContext().equals(PositionPointInterest.Type.LAND_RESPAWN_POINT))continue;

            double nearestDistance = Double.MAX_VALUE;

            for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
                if(this.targetLand.getOwner().getLocalPlayers().contains(onlinePlayer))continue;
                Location playerLocation = onlinePlayer.getLocation();
                double distance = positionPointInterest.getLocation().distance(playerLocation);
                if (distance < nearestDistance) {
                    nearestDistance = distance;
                }
            }
            mapSpawnDistance.put(positionPointInterest, nearestDistance);
        }
        if(mapSpawnDistance.size() <= 0)return null;

        ArrayList<PositionPointInterest> lstUsabePositionPointOfInterest =  new ArrayList<>();
        for(PositionPointInterest respawnPoint : mapSpawnDistance.keySet()){
            if(mapSpawnDistance.get(respawnPoint) > 15) lstUsabePositionPointOfInterest.add(respawnPoint);
        }

        if(lstUsabePositionPointOfInterest.size() == 0) {
            List<PositionPointInterest> lstPosition = new ArrayList<>(mapSpawnDistance.keySet());
            Collections.shuffle(lstPosition);
            return lstPosition.get(0).getLocation();
        }

        Collections.shuffle(lstUsabePositionPointOfInterest);
        return lstUsabePositionPointOfInterest.get(0).getLocation();
    }

    public static void progressAll(){
        ArrayList<Raid> lstRaidToRemove = new ArrayList<>();
        for(Raid runningRaid : Raid.lstRunningRaid){
            if(runningRaid.progress()) lstRaidToRemove.add(runningRaid);
        }

        for(Raid raidToRemove : lstRaidToRemove)Raid.lstRunningRaid.remove(raidToRemove);
    }
}