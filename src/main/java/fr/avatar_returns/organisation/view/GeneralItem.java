package fr.avatar_returns.organisation.view;

import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public abstract class GeneralItem {

    public static ItemStack getPreviousButton(){
        ItemStack previous = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6"), "§8§lPrécédent");
        return previous;
    }


    public static ItemStack resetLore(ItemStack item){

        ArrayList<Component> lore = new ArrayList<>();
        ItemMeta meta = item.getItemMeta();
        meta.lore(lore);
        item.setItemMeta(meta);

        return item;
    }



    public static ItemStack renameItemStackWithName(ItemStack item, String name){

        if(item == null)return item;

        ItemMeta prevMeta = item.getItemMeta();
        prevMeta.setDisplayName(name);
        item.setItemMeta(prevMeta);

        return item;
    }

    public static ItemStack getItemStackWithName(Material material, String name){
        ItemStack item = new ItemStack(material);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(name);
        item.setItemMeta(itemMeta);

        return item;
    }

    public static void setLoreFromList(ItemStack item, List<String> list){

        ArrayList<Component> lore = new ArrayList<>();
        ItemMeta meta = item.getItemMeta();
        for(String value : list) lore.add(Component.text(value));
        meta.lore(lore);
        item.setItemMeta(meta);
    }

    public static void enchant(ItemStack item, Enchantment enchantment){
        ItemMeta meta = item.getItemMeta();
        meta.addEnchant(enchantment, 0, true);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        item.setItemMeta(meta);
    }

    public static void cleanItemMeta(ItemStack item){
        ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
        meta.addItemFlags(ItemFlag.HIDE_DYE);
        meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        item.setItemMeta(meta);
    }
}
