package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstGroupMemberUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstOrganisationPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ChangeLstOMemberFilterButton extends InventoryButton {

    private LstGroupMemberUI lstGroupMemberUI;
    private String filter;

    public ChangeLstOMemberFilterButton(ItemStack itemToClick, LstGroupMemberUI lstGroupMemberUI, String filter) {

        super(itemToClick, lstGroupMemberUI);
        this.lstGroupMemberUI = lstGroupMemberUI;
        this.filter = filter;
    }

    @Override public boolean perform(Player player) {

        this.lstGroupMemberUI.setFilter(this.filter);
        this.lstGroupMemberUI.reload();
        return true;
    }
}
