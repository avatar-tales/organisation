package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstGroupUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstLandUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class LstGroupChangeViewButton extends InventoryButton {

    private LstGroupUI lstGroupUI;
    public LstGroupChangeViewButton(ItemStack itemToClick, LstGroupUI lstGroupUI) {

        super(itemToClick, lstGroupUI);
        this.lstGroupUI = lstGroupUI;
    }

    @Override public boolean perform(Player player) {
        lstGroupUI.changeView();
        lstGroupUI.reload();
        player.openInventory(lstGroupUI.getInv());
        return true;
    }
}
