package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LandUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.RoleOrganisationUI;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class LandButton extends InventoryButton {

    private VLand vLand;

    public LandButton(ItemStack itemToClick, VLand vLand,InventoryUI ui) {

        super(itemToClick, ui);
        this.vLand = vLand;
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();
        player.openInventory(new LandUI(this.vLand, motherUI, this.getItemToClick()).getInv());
        return true;
    }
}
