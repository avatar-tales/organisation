package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.exception.InvitationGroupException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class InviteSend extends InventoryButton {

    private OrganisationPlayer playerToInvite;
    private OrganisationPlayer playerHost;
    private  Group group;

    public InviteSend(ItemStack itemToClick, OrganisationPlayer playerHost, OrganisationPlayer playerToInvite, Group group, InventoryUI ui) {

        super(itemToClick, ui);
        this.playerToInvite = playerToInvite;
        this.playerHost = playerHost;
        this.group = group;
    }

    @Override public boolean perform(Player player) {

        Invitation invitation;
        try {
            invitation = new Invitation(this.playerHost,this.group,this.playerToInvite);
        } catch (InvitationGroupException e) {
            return false;
        }

        for(Invitation inv : InviteCommand.lstInvitation){
            if(inv.getGroupHost().equals(this.group) && inv.getOPlayerInvite().equals(this.playerToInvite)) {
                return false;
            }
        }

        InviteCommand.lstInvitation.add(invitation);
        DBUtils.addInvitation(invitation);

        InventoryUI linkedUI = this.getUi().getMotherUI();
        if(!InventoryUI.getLstInventoryUi().contains(linkedUI))InventoryUI.getLstInventoryUi().add(linkedUI);

        linkedUI.reload();
        player.openInventory(linkedUI.getInv());

        GeneralMethods.sendPlayerMessage(player,invitation.getOPlayerInvite().getLastSeenName() + " a bien été invité dans " + invitation.getGroupHost().getName());
        GeneralMethods.sendPlayerMessage(this.playerToInvite,"Vous avez été invité a rejoindre "+this.group.getName()+" par "+this.playerHost.getName() + "\nPour le rejoindre, utilisez /o join "+this.group.getName());

        return true;
    }
}
