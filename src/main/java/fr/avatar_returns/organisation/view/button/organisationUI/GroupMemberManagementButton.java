package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.territory.Province;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.GroupMemberOrganisationUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstLandUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class GroupMemberManagementButton extends InventoryButton {

    private OrganisationMember oMember;
    public GroupMemberManagementButton(ItemStack itemToClick, OrganisationMember oMember, InventoryUI ui) {

        super(itemToClick, ui);
        this.oMember = oMember;
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();
        player.openInventory(new GroupMemberOrganisationUI(this.oMember, motherUI).getInv());
        return true;
    }
}
