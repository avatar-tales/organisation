package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.MainOrganisationUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.PermissionOrganisationUI;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ChatLockedButton extends InventoryButton {

    private OrganisationPlayer oPlayer;
    private MainOrganisationUI mainOrganisationUI;

    public ChatLockedButton(ItemStack itemToClick, MainOrganisationUI mainOrganisationUI, OrganisationPlayer oPlayer) {

            super(itemToClick, mainOrganisationUI);
            this.oPlayer = oPlayer;
            this.mainOrganisationUI = mainOrganisationUI;
        }

        @Override public boolean perform(Player player) {

            if(!player.getUniqueId().equals(this.oPlayer.getUuid()))return false;

            if(this.oPlayer.getChatLocked().equals("GLOBAL")){
                this.oPlayer.setChatLocked("MUTE");
            }
            else if(this.oPlayer.getChatLocked().equals("MUTE")){
                this.oPlayer.setChatLocked("FACTION");
            }
            else if(this.oPlayer.getChatLocked().equals("FACTION")) {
                this.oPlayer.setChatLocked("GUILDE");
            }
            else if(this.oPlayer.getChatLocked().equals("GUILDE")){
                this.oPlayer.setChatLocked("DEFAUT");
            }
            else {
                this.oPlayer.setChatLocked("GLOBAL");
            }

            this.mainOrganisationUI.clear();
            player.openInventory(this.mainOrganisationUI.generate());
            return true;
        }
}
