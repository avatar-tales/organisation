package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstOrganisationPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class LstOPlayerButton extends InventoryButton {

    public LstOPlayerButton(ItemStack itemToClick, InventoryUI ui) {
        super(itemToClick, ui);
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();
        player.openInventory(new LstOrganisationPlayer(motherUI).getInv());

        return true;
    }
}
