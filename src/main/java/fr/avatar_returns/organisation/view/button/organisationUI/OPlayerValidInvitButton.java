package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.OPlayerUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.OPlayerValidInvit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class OPlayerValidInvitButton extends InventoryButton {

    private OrganisationPlayer oPlayerTarget;
    private Invitation invitation;

    public OPlayerValidInvitButton(ItemStack itemToClick, Invitation invitation, OrganisationPlayer oPlayerTarget, InventoryUI ui) {
        super(itemToClick, ui);
        this.oPlayerTarget = oPlayerTarget;
        this.invitation = invitation;
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();
        if(!player.getUniqueId().equals(this.oPlayerTarget.getUuid()))return false;

        player.openInventory(new OPlayerValidInvit(this.invitation, motherUI).getInv());
        return true;
    }
}
