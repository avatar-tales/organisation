package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.territory.Province;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.view.button.InventoryButton;

import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstLandUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class ProvinceButton extends InventoryButton {

    private final Province province;
    public ProvinceButton(ItemStack itemToClick, Province province, InventoryUI ui) {

        super(itemToClick, ui);
        this.province = province;
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();
        ArrayList<VLand> lstVLand = new ArrayList<>();

        for(VLand vLand : VLand.getLstVland()){
            if(vLand.getProvinceName().equals(this.province.getName())){
                lstVLand.add(vLand);
            }
        }

        if(!lstVLand.isEmpty()) player.openInventory(new LstLandUI(lstVLand,"Province", motherUI, this.province.getProvinceItem()).getInv());
        return true;
    }
}
