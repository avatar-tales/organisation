package fr.avatar_returns.organisation.view.button.objectiveButton;

import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveRessourceDepositUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CheckButton extends ObjectiveButton{

    ObjectiveRessourceDepositUI objectiveRessourceDepositUI;

    public CheckButton(ItemStack checkButton, ObjectiveRessourceDepositUI objectiveRessourceDepositUI){
        super(checkButton, objectiveRessourceDepositUI.getGroupObjective(), objectiveRessourceDepositUI);
        this.objectiveRessourceDepositUI = objectiveRessourceDepositUI;
    }

    @Override public boolean perform(Player player) {

        ItemStack item = objectiveRessourceDepositUI.getInv().getItem(13);

        if(item == null) return false;
        if(item.getAmount() <= 1) return false;
        if(this.objectiveRessourceDepositUI.getGroupObjective().isAcomplished())return false;

        int oldAmount = item.getAmount();
        item.setAmount(1);

        if(item.equals(this.objectiveRessourceDepositUI.getGroupObjective().getObjective().getItem())){

            int maxObjectiveValue = this.objectiveRessourceDepositUI.getGroupObjective().getObjective().getObjectifValue();
            int newAmount = this.objectiveRessourceDepositUI.getGroupObjective().getCurrentObjectiveValue() + (oldAmount -1);

            if(newAmount > maxObjectiveValue) {
                // On rend le surplus au joueur (s'il y en a).
                item.setAmount(newAmount-maxObjectiveValue);
                player.getInventory().addItem(item);
                newAmount = maxObjectiveValue;
            }

            this.objectiveRessourceDepositUI.getGroupObjective().update(newAmount);
            item.setAmount(1);
            objectiveRessourceDepositUI.getInv().setItem(13, item);
        }
        else{
            item.setAmount(oldAmount);
        }

        this.objectiveRessourceDepositUI.reload();
        return false;
    }

}
