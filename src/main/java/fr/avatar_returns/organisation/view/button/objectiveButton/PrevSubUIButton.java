package fr.avatar_returns.organisation.view.button.objectiveButton;

import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveRessourceDepositUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PrevSubUIButton extends InventoryButton {

    public PrevSubUIButton(ItemStack itemToClick, InventoryUI ui){
        super(itemToClick, ui);
    }
    @Override public boolean perform(Player player) {

        InventoryUI inventoryUI = this.getUi();
        if(inventoryUI == null)return false;
        if(!(inventoryUI instanceof ObjectiveRessourceDepositUI))return false;

        ObjectiveRessourceDepositUI ui = (ObjectiveRessourceDepositUI) inventoryUI;

        if(!InventoryUI.getLstInventoryUi().contains(ui.getObjectiveUI()))InventoryUI.getLstInventoryUi().add(ui.getObjectiveUI());

        ui.getObjectiveUI().reload();
        player.openInventory(ui.getObjectiveUI().getInv());
        return true;
    }

}
