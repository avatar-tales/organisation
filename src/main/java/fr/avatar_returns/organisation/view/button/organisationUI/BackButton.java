package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class BackButton extends InventoryButton {

    public BackButton(ItemStack itemToClick, InventoryUI ui) {
        super(itemToClick, ui);
    }

    @Override public boolean perform(Player player) {

        InventoryUI linkedUI = this.getUi();
        if(linkedUI == null) return false;
        if (linkedUI.getMotherUI() == null) return false;
        if(!InventoryUI.getLstInventoryUi().contains(linkedUI.getMotherUI()))InventoryUI.getLstInventoryUi().add(linkedUI.getMotherUI());

        linkedUI.getMotherUI().reload();
        player.openInventory(linkedUI.getMotherUI().getInv());

        return true;
    }
}
