package fr.avatar_returns.organisation.view.button;

import fr.avatar_returns.organisation.view.ui.InventoryUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public abstract class InventoryButton {

    protected ItemStack itemStack;
    private InventoryUI ui;
    private UUID uuid;

    public InventoryButton(ItemStack itemToClick, InventoryUI ui){
        this.uuid = UUID.randomUUID();
        this.itemStack = itemToClick;
        this.ui = ui;
    }

    public abstract boolean perform(Player player);
    public ItemStack getItemToClick() {
        return itemStack;
    }
    public InventoryUI getUi() {
        return ui;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override public boolean equals(Object obj) {
        if(!(obj instanceof InventoryButton))return false;
        InventoryButton objInv = (InventoryButton) obj;

        if (!objInv.getUuid().equals(this.getUuid()))return false;
        if (!objInv.getItemToClick().equals(this.getItemToClick()))return false;
        if (objInv != this)return false;

        return true;
    }
}
