package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.PermissionOrganisationUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class RemoveRoleButton extends InventoryButton {

    private Role role;
    private PermissionOrganisationUI permissionOrganisationUI;

    public RemoveRoleButton(ItemStack itemToClick, PermissionOrganisationUI permissionOrganisationUI, Role role) {

        super(itemToClick, permissionOrganisationUI);
        this.role = role;
        this.permissionOrganisationUI = permissionOrganisationUI;
    }

    @Override public boolean perform(Player player) {

        if(this.getUi().getOPlayerViewer().hasRolePermission(this.role, Permission.management.removeRole)) {
            this.role.getGroup().removeRole(this.role);
        }

        if(!InventoryUI.getLstInventoryUi().contains(permissionOrganisationUI.getMotherUI()))InventoryUI.getLstInventoryUi().add(permissionOrganisationUI.getMotherUI());
        this.permissionOrganisationUI.getMotherUI().reload();
        player.openInventory(this.permissionOrganisationUI.getMotherUI().getInv());
        return true;
    }
}
