package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.GroupOrganisationUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ObjectiveButton extends InventoryButton {


    Group group;

    public ObjectiveButton(ItemStack itemToClick, Group group, InventoryUI ui) {

        super(itemToClick, ui);
        this.group = group;
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();

        if(!motherUI.getOPlayerViewer().hasPermission("organisation.admin"))return false;

        player.openInventory(new ObjectiveUI(this.group, motherUI).getInv());
        return true;
    }
}
