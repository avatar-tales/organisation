package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.PermissionOrganisationUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class KickOMemberButton extends InventoryButton {

    private OrganisationMember oMember;

    public KickOMemberButton(ItemStack itemToClick, OrganisationMember oMember, InventoryUI ui) {

        super(itemToClick, ui);
        this.oMember = oMember;
    }

    @Override public boolean perform(Player player) {

        boolean result = false;
        if(!this.getUi().getOPlayerViewer().hasRolePermission(this.oMember.getRole(), Permission.management.kick)) {
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.NoPermission", true);
        }

        Group group = this.oMember.getGroup();
        group.removeMember(this.oMember.getOrganisationPlayer());
        GeneralMethods.sendPlayerMessage(this.oMember.getOrganisationPlayer(), "Vous avez été kick de " + group.getName());
        for(OrganisationPlayer oPlayer: group.getLstGroupConnectedOrganisationPlayer()){
            GeneralMethods.sendPlayerMessage(oPlayer,this.oMember.getName()+" a été kick de "+ group.getName());
        }

        GroupRank.checkProgression(group);
        InventoryUI linkedUI = this.getUi();

        if(!InventoryUI.getLstInventoryUi().contains(linkedUI.getMotherUI()))InventoryUI.getLstInventoryUi().add(linkedUI.getMotherUI());
        linkedUI.getMotherUI().reload();
        player.openInventory(linkedUI.getMotherUI().getInv());
        result = true;


        return true;
    }
}
