package fr.avatar_returns.organisation.view.button.objectiveButton;

import fr.avatar_returns.organisation.rank.GroupObjective;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.PrevNextUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class PrevButton extends ObjectiveButton{

    private InventoryUI inventoryUI;

    public PrevButton(ItemStack itemToClick, GroupObjective groupObjective, InventoryUI inventoryUI){
        super(itemToClick, groupObjective, inventoryUI);
        this.inventoryUI = inventoryUI;
    }
    @Override public boolean perform(Player player) {
        if(this.inventoryUI instanceof PrevNextUI){
            ((PrevNextUI)this.inventoryUI).prevPage();
            this.inventoryUI.reload();
        }
        return true;
    }
}
