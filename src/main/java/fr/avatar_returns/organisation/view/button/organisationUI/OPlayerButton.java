package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstOrganisationPlayer;
import fr.avatar_returns.organisation.view.ui.organisationUI.OPlayerUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class OPlayerButton extends InventoryButton {

    OrganisationPlayer oPlayer;

    public OPlayerButton(ItemStack itemToClick, OrganisationPlayer oPlayer, InventoryUI ui) {
        super(itemToClick, ui);
        this.oPlayer = oPlayer;
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();
        player.openInventory(new OPlayerUI(this.oPlayer, motherUI).getInv());

        return true;
    }
}
