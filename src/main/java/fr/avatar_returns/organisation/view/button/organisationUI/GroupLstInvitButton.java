package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.GroupLstInvit;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstGroupMemberUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class GroupLstInvitButton extends InventoryButton {

    private Group group;
    public GroupLstInvitButton(ItemStack itemToClick, Group group, InventoryUI ui) {

        super(itemToClick, ui);
        this.group = group;
    }

    @Override public boolean perform(Player player) {
        InventoryUI motherUI = this.getUi();
        player.openInventory(new GroupLstInvit(this.group, motherUI).getInv());
        return true;
    }
}
