package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.territory.Province;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstLandUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class LargeGroupLandButton extends InventoryButton {

    private LargeGroup largeGroup;
    public LargeGroupLandButton(ItemStack itemToClick, LargeGroup largeGroup, InventoryUI ui) {

        super(itemToClick, ui);
        this.largeGroup = largeGroup;
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();
        ArrayList<VLand> lstVLand = new ArrayList<>();

        for(VLand vLand : VLand.getLstVland()){
            if(vLand.getOwnerId() == this.largeGroup.getId())lstVLand.add(vLand);
        }

        if(!lstVLand.isEmpty()) player.openInventory(new LstLandUI(lstVLand,"Land", motherUI, this.largeGroup.getItemRank()).getInv());
        return false;
    }
}
