package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.GroupOrganisationUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.PermissionOrganisationUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class RolePermissionButton extends InventoryButton {

    private Role role;
    public RolePermissionButton(ItemStack itemToClick, Role role, InventoryUI ui) {

        super(itemToClick, ui);
        this.role = role;
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();
        player.openInventory(new PermissionOrganisationUI(this.role, motherUI).getInv());

        return true;
    }
}
