package fr.avatar_returns.organisation.view.button.objectiveButton;

import fr.avatar_returns.organisation.rank.GroupObjective;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveRessourceDepositUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class RessourceObjectiveButton extends ObjectiveButton{

    public RessourceObjectiveButton(ItemStack itemToClick, GroupObjective groupObjective, InventoryUI ui) {
        super(itemToClick, groupObjective, ui);
    }

    @Override public boolean perform(Player player) {

        if(this.groupObjective.isAcomplished())return true;

        InventoryUI inventoryUI = this.getUi();
        if(inventoryUI == null)return false;
        if(!(inventoryUI instanceof ObjectiveUI)) return false;

        player.openInventory(new ObjectiveRessourceDepositUI((ObjectiveUI) inventoryUI, this.groupObjective).getInv());
        return true;
    }
}
