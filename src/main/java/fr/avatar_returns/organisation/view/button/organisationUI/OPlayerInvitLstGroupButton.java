package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.OPlayerInvitLstGroup;
import fr.avatar_returns.organisation.view.ui.organisationUI.OPlayerValidInvit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class OPlayerInvitLstGroupButton extends InventoryButton {

    private OrganisationPlayer oPlayerTarget;

    public OPlayerInvitLstGroupButton(ItemStack itemToClick, OrganisationPlayer oPlayerTarget, InventoryUI ui) {
        super(itemToClick, ui);
        this.oPlayerTarget = oPlayerTarget;
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();

        player.openInventory(new OPlayerInvitLstGroup(this.oPlayerTarget, motherUI).getInv());
        return true;
    }
}
