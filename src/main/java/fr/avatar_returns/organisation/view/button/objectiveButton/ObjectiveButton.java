package fr.avatar_returns.organisation.view.button.objectiveButton;

import fr.avatar_returns.organisation.rank.GroupObjective;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import org.bukkit.inventory.ItemStack;

public abstract class ObjectiveButton extends InventoryButton {


    protected GroupObjective groupObjective;

    ObjectiveButton(ItemStack itemToClick, GroupObjective groupObjective, InventoryUI inventoryUI){
        super(itemToClick, inventoryUI);
        this.groupObjective = groupObjective;
    }

}
