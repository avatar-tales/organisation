package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.territory.VMine;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LandUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.MineUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.RoleOrganisationUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class MineButton extends InventoryButton {

    private VMine vMine;
    public MineButton(ItemStack itemToClick, VMine vMine, InventoryUI ui) {

        super(itemToClick, ui);
        this.vMine = vMine;
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();
        player.openInventory(new MineUI(this.vMine, motherUI).getInv());

        return true;
    }
}
