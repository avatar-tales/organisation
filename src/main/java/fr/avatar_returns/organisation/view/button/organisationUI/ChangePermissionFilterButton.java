package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.PermissionOrganisationUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ChangePermissionFilterButton extends InventoryButton {

    private PermissionOrganisationUI permissionOrganisationUI;
    private String mode;

    public ChangePermissionFilterButton(ItemStack itemToClick, PermissionOrganisationUI permissionOrganisationUI, String mode) {

        super(itemToClick, permissionOrganisationUI);
        this.permissionOrganisationUI = permissionOrganisationUI;
        this.mode = mode;
    }

    @Override public boolean perform(Player player) {

        this.permissionOrganisationUI.setMode(this.mode);
        this.permissionOrganisationUI.reload();
        return true;
    }
}
