package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class InviteRemove extends InventoryButton {

    private Invitation invitation;

    public InviteRemove(ItemStack itemToClick, Invitation invitation, InventoryUI ui) {

        super(itemToClick, ui);
        this.invitation = invitation;
    }

    @Override public boolean perform(Player player) {

        if(!player.getUniqueId().equals(this.getUi().getOPlayerViewer().getUuid()))return false;
        if(!this.invitation.getOPlayerInvite().getUuid().equals(this.getUi().getOPlayerViewer().getUuid())) {
            if (!this.getUi().getOPlayerViewer().hasGroupPermission(this.invitation.getGroupHost(), Permission.management.invite)) {
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.NoPermission", true);
                return false;
            }
        }

        InviteCommand.lstInvitation.remove(this.invitation);
        DBUtils.deleteInvitation(this.invitation);

        GeneralMethods.sendPlayerMessage(player,"Vous avez bien supprimé l'invitation de " + this.invitation.getOPlayerInvite().getLastSeenName());

        InventoryUI linkedUI = this.getUi();
        if(!InventoryUI.getLstInventoryUi().contains(linkedUI.getMotherUI()))InventoryUI.getLstInventoryUi().add(linkedUI.getMotherUI());

        linkedUI.getMotherUI().reload();
        player.openInventory(linkedUI.getMotherUI().getInv());

        GeneralMethods.sendPlayerMessage(this.invitation.getOPlayerInvite(),"Commands.Invite.InvitationRemove",true);
        GeneralMethods.sendPlayerMessage(this.invitation.getOPlayerInvite(),"L'invitation vers "+this.invitation.getGroupHost().getName()+" a été suprimé par "+player.getName());

        return true;
    }
}
