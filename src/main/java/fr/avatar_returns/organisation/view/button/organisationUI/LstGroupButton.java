package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstGroupUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class LstGroupButton extends InventoryButton {

    public LstGroupButton(ItemStack itemToClick, InventoryUI ui) {
        super(itemToClick, ui);
    }

    @Override public boolean perform(Player player) {
        InventoryUI linkedUI = this.getUi();

        player.openInventory(new LstGroupUI(linkedUI).getInv());
        return true;
    }
}
