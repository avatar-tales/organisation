package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstOrganisationPlayer;
import fr.avatar_returns.organisation.view.ui.organisationUI.PermissionOrganisationUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ChangeLstOPlayerFilterButton extends InventoryButton {

    private LstOrganisationPlayer lstOrganisationPlayer;
    private String filter;

    public ChangeLstOPlayerFilterButton(ItemStack itemToClick, LstOrganisationPlayer lstOrganisationPlayer, String filter) {

        super(itemToClick, lstOrganisationPlayer);
        this.lstOrganisationPlayer = lstOrganisationPlayer;
        this.filter = filter;
    }

    @Override public boolean perform(Player player) {

        this.lstOrganisationPlayer.setFilter(this.filter);
        this.lstOrganisationPlayer.reload();
        return true;
    }
}
