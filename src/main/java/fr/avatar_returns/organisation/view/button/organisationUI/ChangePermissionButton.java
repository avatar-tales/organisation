package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.PermissionOrganisationUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ChangePermissionButton extends InventoryButton {

    private Role role;
    private PermissionOrganisationUI permissionOrganisationUI;
    private String permission;

    public ChangePermissionButton(ItemStack itemToClick, PermissionOrganisationUI permissionOrganisationUI, Role role, String permission) {

            super(itemToClick, permissionOrganisationUI);
            this.role = role;
            this.permissionOrganisationUI = permissionOrganisationUI;
            this.permission = permission;
        }

        @Override public boolean perform(Player player) {

            boolean returnValue = false;
            if(!this.getUi().getOPlayerViewer().hasRolePermission(this.role, this.permission) || !this.getUi().getOPlayerViewer().hasRolePermission(this.role, Permission.management.setPermission)) {
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.NoPermission", true);
            }
            else {

                if (this.role.hasPermission(permission)) {
                    if (this.role.getLstPermission().contains(this.permission)) {
                        this.role.getLstPermission().remove(this.permission);
                        DBUtils.deleteRolePermission(this.role, this.permission);
                    }
                } else {
                    if (!this.role.getLstPermission().contains(this.permission)) {
                        this.role.getLstPermission().add(this.permission);
                        DBUtils.addRolePermission(this.role, this.permission);
                    }
                }
                returnValue = true;
            }

            permissionOrganisationUI.reload();
            return returnValue;
        }
}
