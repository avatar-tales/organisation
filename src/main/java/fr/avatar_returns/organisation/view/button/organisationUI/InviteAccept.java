package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.JoinCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import io.lumine.mythic.bukkit.utils.lib.jooq.impl.QOM;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class InviteAccept extends InventoryButton {

    private Invitation invitation;

    public InviteAccept(ItemStack itemToClick, Invitation invitation, InventoryUI ui) {

        super(itemToClick, ui);
        this.invitation = invitation;
    }

    @Override public boolean perform(Player player) {

        Group groupJoin = this.invitation.getGroupHost();

        if(groupJoin.getLstMember().size() >= groupJoin.getMaxPlayer()){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Join.ToMutchPlayer",true);
            return false;
        }

        if(!player.hasPermission("organisation.command.join")) {
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.NoPermission",true);
            return false;
        }
        else{
            if(!groupJoin.addMember(this.invitation.getOPlayerInvite())){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.NoPermission",true);
                return false;
            }
        }

        InviteCommand.lstInvitation.remove(invitation);
        DBUtils.deleteInvitation(invitation);

        //On supprime toutes les invitation du joueur dans ce groupe.
        ArrayList<Invitation> lstInvitationToRemove =  new ArrayList<>();
        for(Invitation otherInvitation : InviteCommand.lstInvitation){
            if(otherInvitation.getOPlayerInvite().getUuid().equals(invitation.getOPlayerInvite().getUuid())
                    && otherInvitation.getGroupHost().getName().equals(invitation.getGroupHost().getName())){
                lstInvitationToRemove.add(otherInvitation);
            }
        }

        for(Invitation otherInvitation : lstInvitationToRemove){
            InviteCommand.lstInvitation.remove(otherInvitation);
            DBUtils.deleteInvitation(otherInvitation);
        }

        GeneralMethods.sendPlayerMessage(player, "Commands.Join.SuccessJoin", true);
        GroupRank.checkProgression(groupJoin);
        JoinCommand.awarePeopleJoin(groupJoin, player);

        InventoryUI linkedUI = this.getUi().getMotherUI();
        if(!InventoryUI.getLstInventoryUi().contains(linkedUI.getMotherUI()))InventoryUI.getLstInventoryUi().add(linkedUI.getMotherUI());

        linkedUI.getMotherUI().reload();
        player.openInventory(linkedUI.getMotherUI().getInv());

        return true;
    }
}
