package fr.avatar_returns.organisation.view.button.objectiveButton;

import fr.avatar_returns.organisation.rank.GroupObjective;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.rank.Objective;
import fr.avatar_returns.organisation.rank.Rank;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class BooleanObjectiveButton extends ObjectiveButton{

    ObjectiveUI objectiveUI;

    public BooleanObjectiveButton(ItemStack itemToClick, GroupObjective groupObjective, ObjectiveUI objectiveUI){
        super(itemToClick, groupObjective, objectiveUI);
        this.objectiveUI = objectiveUI;
        if(!groupObjective.getObjective().getType().equals(Objective.Type.BOOLEAN)) new Exception("You should only use BOOLEAN objective in BooleanObjectiveButton");
    }

    @Override public boolean perform(Player player) {

        if(!(player.isOp() || player.hasPermission("organisation.objective.completeBoolean")))return false;

        if(this.groupObjective.getCurrentObjectiveValue() == 0)this.groupObjective.update(1);
        else this.groupObjective.update(0);

        GroupRank.checkProgression(groupObjective.getGroup());

        objectiveUI.reload();
        return true;
    }
}
