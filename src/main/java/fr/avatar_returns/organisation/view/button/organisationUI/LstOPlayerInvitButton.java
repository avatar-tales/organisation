package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.LstOPlayerInvit;
import fr.avatar_returns.organisation.view.ui.organisationUI.MainOrganisationUI;
import fr.avatar_returns.organisation.view.ui.organisationUI.OPlayerUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class LstOPlayerInvitButton extends InventoryButton {

    private  OrganisationPlayer oTargetPlayer;
    public LstOPlayerInvitButton(ItemStack invitButton, OrganisationPlayer oTargetPlayer, MainOrganisationUI mainOrganisationUI) {
        super(invitButton, mainOrganisationUI);
        this.oTargetPlayer = oTargetPlayer;
    }

    @Override public boolean perform(Player player) {

        InventoryUI motherUI = this.getUi();
        player.openInventory(new LstOPlayerInvit(this.oTargetPlayer, motherUI).getInv());

        return true;
    }
}
