package fr.avatar_returns.organisation.view.button.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ChangeOMemberRoleButton extends InventoryButton {

    private OrganisationMember oMember;
    private Role newRole;

    public ChangeOMemberRoleButton(ItemStack itemToClick, OrganisationMember oMember, Role newRole, InventoryUI ui) {

        super(itemToClick, ui);
        this.oMember = oMember;
        this.newRole = newRole;
    }

    @Override public boolean perform(Player player) {

        InventoryUI linkedUI = this.getUi();
        if(linkedUI.getOPlayerViewer().hasRolePermission(this.newRole,  Permission.management.promote)
                && linkedUI.getOPlayerViewer().hasRolePermission(this.oMember.getRole(), Permission.management.promote)) {
            this.oMember.setRole(this.newRole);
        }

        linkedUI.reload();
        return true;
    }
}
