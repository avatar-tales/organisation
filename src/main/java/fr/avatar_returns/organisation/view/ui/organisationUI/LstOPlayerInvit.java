package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.organisationUI.*;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.PrevNextUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;

public class LstOPlayerInvit extends InventoryUI implements PrevNextUI {

    private int page;
    private final OrganisationPlayer oTargetPlayer;
    public LstOPlayerInvit(OrganisationPlayer oTargetPlayer, InventoryUI motherUI) {

        super(null, motherUI);
        this.oTargetPlayer = oTargetPlayer;
        this.page = 0;
        this.inv = this.generate();
    }

    @Override
    public Inventory generate() {

        Inventory lstPlayerInvitUI = Bukkit.createInventory(null, 27, "§8§lMes invitations");

        ItemStack backItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.FEATHER), "§6§lInvitations");
        GeneralItem.enchant(backItem, Enchantment.ARROW_FIRE);
        GeneralItem.cleanItemMeta(backItem);
        lstPlayerInvitUI.setItem(0, backItem);
        this.addButton(new BackButton(backItem, this));

        ItemStack next = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/e3fc52264d8ad9e654f415bef01a23947edbccccf649373289bea4d149541f70"), "§8§lSuivant");
        ItemStack previous = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6"), "§8§lPrécédent");

        ArrayList<Invitation> lstInvit = this.oTargetPlayer.getLstInvitation();

        int index = 0;
        for (index = 10; index <17; index++) {
            if(index - 10 + this.page * 7 >= lstInvit .size()) break;

            Invitation invit = lstInvit .get(index - 10 + this.page * 7);

            ItemStack button = invit.getInvitationItem();
            lstPlayerInvitUI.setItem(index, button);
            if(oTargetPlayer.getUuid().equals(this.getOPlayerViewer().getUuid()))this.addButton(new OPlayerValidInvitButton(button, invit, this.oTargetPlayer, this));
        }

        int maxPage = -1 + lstInvit.size()/7 + ((lstInvit.size() % 7 == 0)? 0 : 1);
        if(this.page > 0 && lstInvit .size() > 7){
            lstPlayerInvitUI.setItem(18, previous);
            this.addButton(new PrevButton(previous, null, this));
        }
        if(this.page < maxPage) {
            lstPlayerInvitUI.setItem(26, next);
            this.addButton(new NextButton(next, null, this));
        }

        return lstPlayerInvitUI;
    }

    public void nextPage(){
        int nbInvit = this.oTargetPlayer.getLstInvitation().size();
        int maxPage = -1 + nbInvit/7 + ((nbInvit % 7 == 0)? 0 : 1);
        if(this.page < maxPage) this.page += 1;
    }
    public void prevPage(){
        this.page -=1;
        if(this.page < 0)this.page = 0;
    }
}
