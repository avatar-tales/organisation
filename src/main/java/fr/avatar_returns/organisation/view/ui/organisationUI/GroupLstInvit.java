package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.organisationUI.BackButton;
import fr.avatar_returns.organisation.view.button.organisationUI.InviteRemove;
import fr.avatar_returns.organisation.view.button.organisationUI.InviteSend;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.PrevNextUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;

public class GroupLstInvit extends InventoryUI implements PrevNextUI {

    private int page;
    private final Group group;
    private ArrayList<Invitation> lstGroupInvitation;

    public GroupLstInvit(Group group, InventoryUI motherUI) {

        super(null, motherUI);
        this.group = group;
        this.lstGroupInvitation = new ArrayList<>();
        this.page = 0;
        this.inv = this.generate();
    }

    @Override
    public Inventory generate() {

        Inventory groupLstInvitUI = Bukkit.createInventory(null, 27, "§8§lMes invitations");

        this.lstGroupInvitation = this.group.getLstInvitation();

        ItemStack backItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.FEATHER), "§6§lJoueurs invités");
        GeneralItem.cleanItemMeta(backItem);
        groupLstInvitUI.setItem(0, backItem);
        this.addButton(new BackButton(backItem, this));

        ItemStack next = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/e3fc52264d8ad9e654f415bef01a23947edbccccf649373289bea4d149541f70"), "§8§lSuivant");
        ItemStack previous = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6"), "§8§lPrécédent");

        int index = 0;
        for (index = 10; index <17; index++) {
            if(index - 10 + this.page * 7 >= this.lstGroupInvitation.size()) break;

            Invitation invitation = this.lstGroupInvitation.get(index - 10 + this.page * 7);

            ItemStack button = GeneralItem.renameItemStackWithName(invitation.getOPlayerInvite().getHead(),"§6§l" + invitation.getOPlayerInvite().getName());
            GeneralItem.cleanItemMeta(button);
            GeneralItem.setLoreFromList(button, Arrays.asList(
                    "§8Invité par : §7" + invitation.getOPlayerHost().getName(),
                    "§cClique ici pour annuler l'invitation")
            );

            groupLstInvitUI.setItem(index, button);
            if(this.getOPlayerViewer().hasGroupPermission(this.group, Permission.management.invite)){
                this.addButton(new InviteRemove(button, invitation, this ));
            }
        }

        int maxPage = -1 + this.lstGroupInvitation.size()/7 + ((this.lstGroupInvitation.size() % 7 == 0)? 0 : 1);
        if(this.page > 0 && this.lstGroupInvitation.size() > 7){
            groupLstInvitUI.setItem(18, previous);
            this.addButton(new PrevButton(previous, null, this));
        }
        if(this.page < maxPage) {
            groupLstInvitUI.setItem(26, next);
            this.addButton(new NextButton(next, null, this));
        }

        return groupLstInvitUI;
    }

    public void nextPage(){
        int nbInvit = this.lstGroupInvitation.size();
        int maxPage = -1 + nbInvit/7 + ((nbInvit % 7 == 0)? 0 : 1);
        if(this.page < maxPage) this.page += 1;
    }
    public void prevPage(){
        this.page -=1;
        if(this.page < 0)this.page = 0;
    }
}
