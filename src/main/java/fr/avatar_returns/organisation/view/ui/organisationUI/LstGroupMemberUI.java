package fr.avatar_returns.organisation.view.ui.organisationUI;


import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.organisationUI.*;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.PrevNextUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class LstGroupMemberUI extends InventoryUI implements PrevNextUI {

    private Group group;
    private int page;
    private String filter;

    public LstGroupMemberUI(Group group, InventoryUI motherUI) {
        super(null, motherUI);
        this.group = group;
        this.page = 0;
        this.filter = "CONNECTED";
        this.inv = this.generate();
    }


    @Override public Inventory generate() {

        Inventory lstGroupInv = Bukkit.createInventory(null, 54, "§8§lMembre de " + this.group.getName());


        ItemStack next = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/e3fc52264d8ad9e654f415bef01a23947edbccccf649373289bea4d149541f70"), "§8§lSuivant");
        ItemStack previous = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6"), "§8§lPrécédent");

        ItemStack playerItem = new ItemStack(Material.PLAYER_HEAD);
        playerItem = GeneralItem.renameItemStackWithName(playerItem,"§8§lMembre de " + this.group.getName());
        GeneralItem.setLoreFromList(playerItem, Arrays.asList("§8Retour"));
        lstGroupInv.setItem(0, playerItem);
        this.addButton(new BackButton(playerItem, this));

        ArrayList<OrganisationMember> lstGroupMember = this.group.getLstMember();
        Collections.sort(lstGroupMember, new Comparator<OrganisationMember>() {
            @Override public int compare(OrganisationMember member1, OrganisationMember member2) {

                if(member1.getRole().getRolePower() == 0){return 1;}
                else if(member2.getRole().getRolePower() == 0){return -1;}
                else if(member1.getRole().getRolePower() == -1){return -1;}
                else if(member2.getRole().getRolePower() == -1){return 1;}

                return -Integer.compare(member1.getRole().getRolePower(), member2.getRole().getRolePower());
            }
        });
        Collections.reverse(lstGroupMember);

        if(this.filter.equals("CONNECTED")) {

            lstGroupMember = this.group.getLstGroupConnectedMember();
            Collections.sort(lstGroupMember, new Comparator<OrganisationMember>() {
                @Override public int compare(OrganisationMember member1, OrganisationMember member2) {

                    if(member1.getRole().getRolePower() == 0){return 1;}
                    else if(member2.getRole().getRolePower() == 0){return -1;}
                    else if(member1.getRole().getRolePower() == -1){return -1;}
                    else if(member2.getRole().getRolePower() == -1){return 1;}

                    return -Integer.compare(member1.getRole().getRolePower(), member2.getRole().getRolePower());
                }
            });
            Collections.reverse(lstGroupMember);

            ItemStack filterButton = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/90e23d941578285bbe7843d4510dc7b71a5a7af2e56dac2715b5d2497f6e9"), "§6§lConnecté");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour trier par rang"));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOMemberFilterButton(filterButton, this, "RANK"));
        }
        else if(this.filter.equals("RANK")) {

            Collections.sort(lstGroupMember, new Comparator<OrganisationMember>() {
                @Override public int compare(OrganisationMember member1, OrganisationMember member2) {

                    if(member1.getRole().getRolePower() == 0){return 1;}
                    else if(member2.getRole().getRolePower() == 0){return -1;}
                    else if(member1.getRole().getRolePower() == -1){return -1;}
                    else if(member2.getRole().getRolePower() == -1){return 1;}

                    return -Integer.compare(member1.getRole().getRolePower(), member2.getRole().getRolePower());
                }
            });
            Collections.reverse(lstGroupMember);

            ItemStack filterButton = GeneralItem.renameItemStackWithName(new ItemStack(Material.GOLDEN_HELMET), "§6§lRang");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour trier par temps de jeux"));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOMemberFilterButton(filterButton, this, "TIME_PLAYED"));

        }
        else if(this.filter.equals("TIME_PLAYED")) {

            Collections.sort(lstGroupMember, new Comparator<OrganisationMember>() {
                @Override
                public int compare(OrganisationMember oPlayer1, OrganisationMember oPlayer2) {
                    return Long.compare(oPlayer1.getEffectivePlayedTime(), oPlayer2.getEffectivePlayedTime());
                }
            });
            Collections.reverse(lstGroupMember);

            ItemStack filterButton = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/fcb8f06885d1daafd26cd95b3482cb525d881a67e0d247161b908d93d56d114f"), "§6§lTemps de jeux");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour trier par ordre alphabétique"));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOMemberFilterButton(filterButton, this, "LETTER"));

        }
        else if(this.filter.equals("LETTER")) {

            Collections.sort(lstGroupMember, new Comparator<OrganisationMember>() {
                @Override
                public int compare(OrganisationMember oPlayer1, OrganisationMember oPlayer2) {
                    return String.CASE_INSENSITIVE_ORDER.compare(oPlayer1.getName(), oPlayer2.getName());
                }
            });

            ItemStack filterButton = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/7746abe410028115029ce36cbf3a8ebd9c39d00d76f21c076c9360136b0e0a93"), "§6§lOrdre Alphabétique");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour trier par ancienneté"));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOMemberFilterButton(filterButton, this, "OLD"));
        }
        else if(this.filter.equals("OLD")) {

            Collections.sort(lstGroupMember, new Comparator<OrganisationMember>() {
                @Override
                public int compare(OrganisationMember oPlayer1, OrganisationMember oPlayer2) {
                    return Long.compare(oPlayer1.getFirstTimeConnexion(), oPlayer2.getFirstTimeConnexion());
                }
            });

            ItemStack filterButton = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/375919e276ee4ba7f9d751ba9f3e883f5ad788fcdfd1b3a34db012dfec2ab59"), "§6§lAncienneté");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour n'afficher que les joueurs connectés"));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOMemberFilterButton(filterButton, this, "CONNECTED"));
        }

        int index = 0;
        for (int line = 1; line < 5; line ++) {
            boolean maxReach = false;
            for (index = 1; index < 8; index++) {


                if (index -1 + (line-1)*7 + this.page * 28 >= lstGroupMember.size()) {
                    maxReach = true;
                    break;
                }

                OrganisationMember oMember = lstGroupMember.get(index-1 + (line-1)*7 + this.page * 28);
                ItemStack button = oMember.getOMemberItem();

                lstGroupInv.setItem(index + line*9, button);
                this.addButton(new GroupMemberManagementButton(button, oMember, this));
            }
            if(maxReach)break;
        }

        int maxPage = -1 + lstGroupMember.size()/28 + ((lstGroupMember.size() % 28 == 0)? 0 : 1);
        if(this.page > 0 && lstGroupMember.size() > 28){
            lstGroupInv.setItem(45, previous);
            this.addButton(new PrevButton(previous, null, this));
        }
        if(this.page < maxPage) {
            lstGroupInv.setItem(53, next);
            this.addButton(new NextButton(next, null, this));
        }


        return lstGroupInv;
    }

    public void nextPage(){
        int nbPlayer = this.group.getLstMember().size();
        int maxPage = -1 + nbPlayer/7 + ((nbPlayer % 7 == 0)? 0 : 1);
        if(this.page < maxPage) this.page += 1;
    }
    public void prevPage(){
        this.page -=1;
        if(this.page < 0)this.page = 0;
    }

    public void setFilter(String filter){this.filter = filter;}
}
