package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.rabbitMQPackets.VMinePacket;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.territory.VMine;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.organisationUI.BackButton;
import fr.avatar_returns.organisation.view.button.organisationUI.MineButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.PrevNextUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class MineUI extends InventoryUI implements PrevNextUI {

    private int page;
    private VMine vMine;
    private ArrayList<ItemStack> lstItem;

    public MineUI(VMine vMine, InventoryUI motherUI) {
        super(null, motherUI);
        this.vMine = vMine;
        this.page = 0;
        this.lstItem = getLstItem();
        this.inv = this.generate();
    }

    @Override public Inventory generate() {

        Inventory vMineUI = Bukkit.createInventory(null, 27, "§8§lMine");
        ItemStack backItem = this.vMine.getItem();
        vMineUI.setItem(0, backItem);
        this.addButton(new BackButton(backItem, this));

        ItemStack next = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/e3fc52264d8ad9e654f415bef01a23947edbccccf649373289bea4d149541f70"), "§8§lSuivant");
        ItemStack previous = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6"), "§8§lPrécédent");

        this.lstItem = getLstItem();
        int index = 0;
        for (index = 10; index <17; index++) {
            if(index - 10 + this.page * 7 >= lstItem.size()) break;

            ItemStack button = this.lstItem.get(index - 10 + this.page * 7);
            vMineUI.setItem(index, button);
        }

        int maxPage = -1 + this.lstItem.size()/7 + ((this.lstItem.size() % 7 == 0)? 0 : 1);
        if(this.page > 0 && this.lstItem .size() > 7){
            vMineUI.setItem(18, previous);
            this.addButton(new PrevButton(previous, null, this));
        }
        if(this.page < maxPage) {
            vMineUI.setItem(26, next);
            this.addButton(new NextButton(next, null, this));
        }

        return vMineUI;
    }

    private ArrayList<ItemStack> getLstItem(){

        ArrayList<ItemStack> lstItem =  new ArrayList<>();

        for(ItemStack itemStack : this.vMine.getStoredInventory().getInventory().getStorageContents()){
            if(itemStack == null)continue;
            if(itemStack.getType().equals(Material.AIR)) continue;

            lstItem.add(itemStack);
        }

        return lstItem;
    }


    public void nextPage(){
        int maxPage = -1 + this.lstItem.size()/7 + ((this.lstItem.size() % 7 == 0)? 0 : 1);
        if(this.page < maxPage) this.page += 1;
    }
    public void prevPage(){
        this.page -=1;
        if(this.page < 0)this.page = 0;
    }


}

