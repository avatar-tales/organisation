package fr.avatar_returns.organisation.view.ui;


public interface PrevNextUI {

    public void nextPage();
    public void prevPage();
}
