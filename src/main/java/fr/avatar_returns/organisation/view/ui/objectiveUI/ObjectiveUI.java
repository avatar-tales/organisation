package fr.avatar_returns.organisation.view.ui.objectiveUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.rank.GroupObjective;
import fr.avatar_returns.organisation.rank.Objective;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.BooleanObjectiveButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.RessourceObjectiveButton;
import fr.avatar_returns.organisation.view.button.organisationUI.BackButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.PrevNextUI;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ObjectiveUI extends InventoryUI implements PrevNextUI {

    private Group group;
    int page;

    public ObjectiveUI(Group group, OrganisationPlayer oPlayer){
        super(null, oPlayer);

        this.group = group;
        this.page = 0;
        this.inv = this.generate();
    }

    public ObjectiveUI(Group group, InventoryUI motherUI){
        super(null, motherUI);

        this.group = group;
        this.page = 0;
        this.inv = this.generate();
    }

    @Override public Inventory generate() {

        Inventory board =  Bukkit.createInventory(null, 27, "§8§lIntendance");
        ItemStack next = SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/e3fc52264d8ad9e654f415bef01a23947edbccccf649373289bea4d149541f70");
        ItemStack previous = SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6");
        ItemStack rank = this.group.getItemRank();

        ItemMeta prevMeta = previous.getItemMeta();
        prevMeta.setDisplayName("§8§lPrécédent");
        previous.setItemMeta(prevMeta);

        ItemMeta nextMeta = next.getItemMeta();
        nextMeta.setDisplayName("§8§lSuivant");
        next.setItemMeta(nextMeta);

        board.setItem(0, rank);
        if(this.getMotherUI() != null)this.addButton(new BackButton(rank, this));

        int index = 0;
        for (index = 10; index <17; index++) {
            if(index - 10 + this.page * 7 >= this.group.getLstGroupObjective().size()) break;

            GroupObjective groupObjective = this.group.getLstGroupObjective().get(index - 10 + this.page * 7);

            ItemStack button = groupObjective.getIcon();
            board.setItem(index, button);

            if(groupObjective.getObjective().getType().equals(Objective.Type.BOOLEAN.toString())) this.addButton(new BooleanObjectiveButton(button, groupObjective, this));
            if(groupObjective.getObjective().getType().equals(Objective.Type.RESSOURCE.toString()) && !groupObjective.isAcomplished()) this.addButton(new RessourceObjectiveButton(button, groupObjective, this));
        }

        int maxPage = -1 + this.group.getLstGroupObjective().size()/7 + ((this.group.getLstGroupObjective().size() % 7 == 0)? 0 : 1);
        if(this.page > 0 && this.group.getLstGroupObjective().size() > 7){
            board.setItem(18, previous);
            this.addButton(new PrevButton(previous, null, this));
        }

        if(this.page < maxPage) {
            board.setItem(26, next);
            this.addButton(new NextButton(next, null, this));
        }

        return board;
    }

    public void nextPage(){
        int maxPage = -1 + this.group.getLstGroupObjective().size()/7 + ((this.group.getLstGroupObjective().size() % 7 == 0)? 0 : 1);
        if(this.page < maxPage) this.page += 1;

    }
    public void prevPage(){
        this.page -=1;
        if(this.page < 0)this.page = 0;
    }
}
