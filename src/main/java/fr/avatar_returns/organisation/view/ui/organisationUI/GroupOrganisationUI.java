package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.organisationUI.*;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.organisations.Group;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class GroupOrganisationUI extends InventoryUI {

    private Group group;

    public GroupOrganisationUI( Group group, InventoryUI motherUI) {
        super(null, motherUI);
        this.group = group;
        this.inv = this.generate();
    }

    @Override
    public Inventory generate() {
        
        Inventory groupInv = Bukkit.createInventory(null, 27, "§8§l"+this.group.getName());
        ItemStack prevItem = this.group.getItemRank();
        groupInv.setItem(0, prevItem);

        this.addButton(new BackButton(prevItem, this));

        ItemStack roleItem =  GeneralItem.getItemStackWithName(Material.IRON_HELMET, "§6§lGestion des roles.");
        GeneralItem.cleanItemMeta(roleItem);

        if(this.getOPlayerViewer().hasGroupPermission(this.group, Permission.management.setPermission) ||
                this.getOPlayerViewer().hasGroupPermission(this.group, Permission.management.removeRole)
                ){
            GeneralItem.setLoreFromList(roleItem, Arrays.asList("§8Nombre de roles : §7" + this.group.getLstRole().size()));
            this.addButton(new RoleButton(roleItem, this.group, this));
            groupInv.setItem(11, roleItem);
        }

        ItemStack guildItem = GeneralItem.getItemStackWithName(Material.EMERALD,"§6§lObjectifs");
        ItemStack landItem = GeneralItem.getItemStackWithName(Material.GRASS_BLOCK,"§6§lLands");

        ItemStack housingItem = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/2a648e357a60c257f3bf3840fa532cfd6adce2f88dab9fd0c0f9303cf5aeb889"),"§6§lHousing");
        ItemStack playersItem = GeneralItem.getItemStackWithName(Material.PLAYER_HEAD,"§6§lMembres");
        ItemStack objectiveItem = GeneralItem.getItemStackWithName(Material.BOOK,"§c§l[ADMIN] §8§lObjectifs");

        if(this.getOPlayerViewer().hasGroupPermission(this.group, Permission.management.invite)) {
            int nbWaitingInvitation = this.group.getLstInvitation().size();
            if (nbWaitingInvitation > 0) {
                ItemStack invButton = GeneralItem.renameItemStackWithName(new ItemStack(Material.FEATHER), "§6§lInvitation en attente");
                groupInv.setItem(8, invButton);
                this.addButton(new GroupLstInvitButton(invButton, this.group, this));
            }
        }

        if(this.group instanceof LargeGroup){

            LargeGroup largeGroup = (LargeGroup) this.group;
            ItemStack lordItem;

            if(largeGroup.getLord() != null){
                lordItem = GeneralItem.renameItemStackWithName(largeGroup.getLord().getItemRank(),"§6§lLord : §7" + largeGroup.getLord().getName());
            }
            else{
                lordItem = GeneralItem.getItemStackWithName(Material.GRAY_BANNER,"§6§l"+this.group.getName()+" n'a pas de seigneur.");
            }

            groupInv.setItem(12, lordItem);
            GeneralItem.setLoreFromList(landItem, Arrays.asList("§8Nombre de land : §7" + largeGroup.getLstLand().size()+"/"+largeGroup.getMaxLand()));
            groupInv.setItem(13,landItem);
            this.addButton(new LargeGroupLandButton(landItem, largeGroup, this));

        }
        else{
            groupInv.setItem(12, guildItem);
            this.addButton(new ObjectiveButton(guildItem, this.group, this));
            groupInv.setItem(13,housingItem);
        }

        GeneralItem.setLoreFromList(playersItem, Arrays.asList(
                "§8Joueurs : §7" + this.group.getLstMember().size()+"/"+this.group.getMaxPlayer(),
                "§8Connecté : §7" + this.group.getLstGroupConnectedMember().size() + "/" + this.group.getLstMember().size(),
                "§8Déconnecté : §7" + (this.group.getLstMember().size() - this.group.getLstGroupConnectedMember().size()) + "/" + this.group.getLstMember().size())
        );
        groupInv.setItem(14,playersItem);
        this.addButton(new OMemberButton(playersItem, this.group, this));


        if(this.getOPlayerViewer().hasPermission("organisation.admin")) {
            groupInv.setItem(26, objectiveItem);
            this.addButton(new ObjectiveButton(objectiveItem, this.group, this));
        }

        return groupInv;
    }
}
