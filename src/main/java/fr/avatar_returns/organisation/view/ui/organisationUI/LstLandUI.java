package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.organisationUI.BackButton;
import fr.avatar_returns.organisation.view.button.organisationUI.LandButton;
import fr.avatar_returns.organisation.view.button.organisationUI.MineButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class LstLandUI extends InventoryUI {

    private ArrayList<VLand> lstVLand;
    private String  uiName;
    private ItemStack backItem;

    public LstLandUI(ArrayList<VLand> lstVLand, String uiName, InventoryUI motherUI, ItemStack backItem) {
        super(null, motherUI);
        this.uiName = uiName;
        this.lstVLand = lstVLand;
        this.backItem = backItem;
        this.inv = this.generate();
    }

    @Override public Inventory generate() {

        Inventory lstLandUI = Bukkit.createInventory(null, 36, "§8§l"+this.uiName);
        lstLandUI.setItem(0, this.backItem);
        this.addButton(new BackButton(this.backItem, this));

        int cpt = 0;
        for(VLand vLand: this.lstVLand){
            lstLandUI.setItem(cpt%5 + 11 + (cpt/5*9), vLand.getVlandItem());
            this.addButton(new LandButton(vLand.getVlandItem(), vLand, this));
            cpt++;
        }

        return lstLandUI;
    }
}


