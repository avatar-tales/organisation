package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.organisations.largeGroup.Faction;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.rank.GroupObjective;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.organisationUI.BackButton;
import fr.avatar_returns.organisation.view.button.organisationUI.GroupButton;
import fr.avatar_returns.organisation.view.button.organisationUI.LstGroupChangeViewButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.PrevNextUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class LstGroupUI extends InventoryUI implements PrevNextUI {

    private boolean isGuilde;
    private int page;

    public LstGroupUI(InventoryUI motherUI) {
        super(null, motherUI);
        this.isGuilde = false;
        this.inv = this.generate();
        this.page = 0;
    }


    @Override public Inventory generate() {

        Inventory lstGroupInv = Bukkit.createInventory(null, 27, "§8§l" +((isGuilde)? "Guildes" : "Factions"));


        ItemStack reload = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/d761ea71913137eb986ed861d1a9da1c70022ab5acd57b88499ab80cc4e0bd24"), ((this.isGuilde) ? "§6§lVue factions" : "§6§lVue guildes") );
        ItemStack next = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/e3fc52264d8ad9e654f415bef01a23947edbccccf649373289bea4d149541f70"), "§8§lSuivant");
        ItemStack previous = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6"), "§8§lPrécédent");

        lstGroupInv.setItem(8, reload);
        this.addButton(new LstGroupChangeViewButton(reload, this));

        if(this.isGuilde){
            ItemStack guildeItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.EMERALD),"§6§lGuildes");
            lstGroupInv.setItem(0, guildeItem);
            this.addButton(new BackButton(guildeItem, this));

            ArrayList<Guilde> lstDisplayGuilde = Guilde.getLstGuilde(false);
            Collections.sort(lstDisplayGuilde, new Comparator<Guilde>() {
                @Override public int compare(Guilde guilde1, Guilde guilde2) {

                    int orderComparison = Integer.compare(guilde1.getRank().getOrder(), guilde2.getRank().getOrder());
                    if (orderComparison != 0)return orderComparison;

                    else{
                        // On compare par nombre d'objectif accomplis
                        int nbObjectAcomplishGuilde1 = 0;
                        for(GroupObjective groupObjective : guilde1.getLstGroupObjective()) if(groupObjective.isAcomplished()) nbObjectAcomplishGuilde1 ++;
                        int nbObjectAcomplishGuilde2 = 0;
                        for(GroupObjective groupObjective : guilde2.getLstGroupObjective()) if(groupObjective.isAcomplished()) nbObjectAcomplishGuilde2 ++;

                        int objectiveComparison = Integer.compare(nbObjectAcomplishGuilde1, nbObjectAcomplishGuilde2);
                        if (objectiveComparison != 0)return objectiveComparison;
                        else{
                            // On compare par la taille des joueurs.
                            return Integer.compare(guilde1.getLstMember().size(), guilde2.getLstMember().size());
                        }
                    }
                }
            });
            Collections.reverse(lstDisplayGuilde);

            int index = 0;
            for (index = 10; index <17; index++) {
                if(index - 10 + this.page * 7 >= lstDisplayGuilde.size()) break;

                Guilde guilde = lstDisplayGuilde.get(index - 10 + this.page * 7);

                ItemStack button = guilde.getItemRank();
                lstGroupInv.setItem(index, button);
                this.addButton(new GroupButton(button, guilde, this));
            }

            int toDIsplay = lstDisplayGuilde.size()-(7*this.page);
            if(this.page > 0 && lstDisplayGuilde.size() > 7){
                lstGroupInv.setItem(18, previous);
                this.addButton(new PrevButton(previous, null, this));
            }
            if(toDIsplay%7 != 0 && lstDisplayGuilde.size() > 7 && toDIsplay > 7) {
                lstGroupInv.setItem(26, next);
                this.addButton(new NextButton(next, null, this));
            }
        }
        else {
            ItemStack factionItem = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/49c1832e4ef5c4ad9c519d194b1985030d257914334aaf2745c9dfd611d6d61d"), "§6§lFactions");
            lstGroupInv.setItem(0, factionItem);
            this.addButton(new BackButton(factionItem, this));

            ArrayList<Faction> lstDisplayFaction = Faction.getLstFaction(false);
            Collections.sort(lstDisplayFaction, new Comparator<Faction>() {
                @Override
                public int compare(Faction faction1, Faction faction2) {

                    int orderComparison = Integer.compare(faction1.getRank().getOrder(), faction2.getRank().getOrder());
                    if (orderComparison != 0)return orderComparison;
                    else{
                        // On compare par nombre de vassaux
                        int vassalComparison = Integer.compare(faction1.getLstVassal().size(), faction2.getLstVassal().size());
                        if (vassalComparison != 0)return vassalComparison;
                        else{
                            // On compare par nombre de land
                            int landComparison = Integer.compare(faction1.getLstVland().size(), faction2.getLstVland().size());
                            if (landComparison != 0)return landComparison;
                            else{
                                // On compare par nombre d'objectif accomplis
                                int nbObjectAcomplishFaction1 = 0;
                                for(GroupObjective groupObjective : faction1.getLstGroupObjective()) if(groupObjective.isAcomplished()) nbObjectAcomplishFaction1 ++;
                                int nbObjectAcomplishFaction2 = 0;
                                for(GroupObjective groupObjective : faction2.getLstGroupObjective()) if(groupObjective.isAcomplished()) nbObjectAcomplishFaction2 ++;

                                int objectiveComparison = Integer.compare(nbObjectAcomplishFaction1, nbObjectAcomplishFaction2);
                                if (objectiveComparison != 0)return objectiveComparison;
                                else{
                                    //Sinon on compare le nombre de joueur des factions.
                                    return Integer.compare(faction1.getLstMember().size(), faction2.getLstMember().size());
                                }
                            }
                        }
                    }
                }
            });
            Collections.reverse(lstDisplayFaction);

            int index = 0;
            for (index = 10; index <17; index++) {
                if(index - 10 + this.page * 7 >= lstDisplayFaction.size()) break;

                Faction faction = lstDisplayFaction.get(index - 10 + this.page * 7);

                ItemStack button = faction.getItemRank();
                lstGroupInv.setItem(index, button);
                this.addButton(new GroupButton(button, faction, this));
            }

            int maxPage = -1 + lstDisplayFaction.size()/7 + ((lstDisplayFaction.size() % 7 == 0)? 0 : 1);
            if(this.page > 0 && lstDisplayFaction.size() > 7){
                lstGroupInv.setItem(18, previous);
                this.addButton(new PrevButton(previous, null, this));
            }
            if(this.page < maxPage) {
                lstGroupInv.setItem(26, next);
                this.addButton(new NextButton(next, null, this));
            }
        }

        return lstGroupInv;
    }

    public void nextPage(){
        int nbGroups = ((this.isGuilde)?Guilde.getLstGuilde(false).size() : Faction.getLstFaction(false).size());
        int maxPage = -1 + nbGroups/7 + ((nbGroups % 7 == 0)? 0 : 1);
        if(this.page < maxPage) this.page += 1;
    }
    public void prevPage(){
        this.page -=1;
        if(this.page < 0)this.page = 0;
    }

    public void changeView(){
        this.page = 0;
        this.isGuilde = !this.isGuilde;
        this.inv = this.generate();
    }
}
