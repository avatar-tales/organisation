package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.organisationUI.BackButton;
import fr.avatar_returns.organisation.view.button.organisationUI.ChangeLstOPlayerFilterButton;
import fr.avatar_returns.organisation.view.button.organisationUI.OPlayerButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.PrevNextUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class LstOrganisationPlayer extends InventoryUI implements PrevNextUI {

    private int page;
    private String filter; // LETTER, CONNECTED, DEATH, KILL_PLAYER, KILL_MOBS, TIME_PLAYED, OLD
    private ArrayList<OrganisationPlayer> lstOPlayer = new ArrayList<>();

    public LstOrganisationPlayer(InventoryUI motherUI) {
        super(null, motherUI);
        this.page = 0;
        this.lstOPlayer = OrganisationPlayer.getLstOrganisationPlayer();
        this.filter = "CONNECTED";
        this.inv = this.generate();
    }


    @Override public Inventory generate() {

        Inventory lstGroupInv = Bukkit.createInventory(null, 54, "§8§lJoueurs du conquete");

        ItemStack next = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/e3fc52264d8ad9e654f415bef01a23947edbccccf649373289bea4d149541f70"), "§8§lSuivant");
        ItemStack previous = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6"), "§8§lPrécédent");

        ItemStack playerItem = GeneralItem.renameItemStackWithName(this.getOPlayerViewer().getHead(), "§6§lJoueurs");
        GeneralItem.setLoreFromList(playerItem, Arrays.asList("§8§8Retour"));
        lstGroupInv.setItem(0, playerItem);
        this.addButton(new BackButton(playerItem, this));

        if(this.filter.equals("CONNECTED")) {

            this.lstOPlayer = OrganisationPlayer.getLstConnectedOrganisationPlayer();
            Collections.sort(this.lstOPlayer, new Comparator<OrganisationPlayer>() {
                @Override
                public int compare(OrganisationPlayer oPlayer1, OrganisationPlayer oPlayer2) {
                    return Long.compare(oPlayer1.getFirstTimeConnexion(), oPlayer2.getFirstTimeConnexion());
                }
            });
            Collections.reverse(this.lstOPlayer);

            ItemStack filterButton = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/90e23d941578285bbe7843d4510dc7b71a5a7af2e56dac2715b5d2497f6e9"), "§6§lConnecté");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour trier par temps de jeux sur le serveur"));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOPlayerFilterButton(filterButton, this, "TIME_PLAYED"));
        }
        else if(this.filter.equals("TIME_PLAYED")) {

            this.lstOPlayer = OrganisationPlayer.getLstOrganisationPlayer();
            Collections.sort(this.lstOPlayer, new Comparator<OrganisationPlayer>() {
                @Override
                public int compare(OrganisationPlayer oPlayer1, OrganisationPlayer oPlayer2) {
                    return Long.compare(oPlayer1.getEffectivePlayedTime(), oPlayer2.getEffectivePlayedTime());
                }
            });
            Collections.reverse(this.lstOPlayer);

            ItemStack filterButton = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/fcb8f06885d1daafd26cd95b3482cb525d881a67e0d247161b908d93d56d114f"), "§6§lTemps de jeux");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour trier par nombre de monstres tués."));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOPlayerFilterButton(filterButton, this, "KILL_MOBS"));

        }
        else if(this.filter.equals("KILL_MOBS")) {

            this.lstOPlayer = OrganisationPlayer.getLstOrganisationPlayer();
            Collections.sort(this.lstOPlayer, new Comparator<OrganisationPlayer>() {
                @Override
                public int compare(OrganisationPlayer oPlayer1, OrganisationPlayer oPlayer2) {
                    return Integer.compare(oPlayer1.getMobsKill(), oPlayer2.getMobsKill());
                }
            });
            Collections.reverse(this.lstOPlayer);

            ItemStack filterButton = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/ac6aeb9d78f4bdb58a5e2c37002167797c960a6bb4a90ccc4a819349931db36e"), "§6§lMonstre tués");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour trier par nombre de joueurs tués"));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOPlayerFilterButton(filterButton, this, "KILL_PLAYER"));
        }
        else if(this.filter.equals("KILL_PLAYER")) {

            this.lstOPlayer = OrganisationPlayer.getLstOrganisationPlayer();
            Collections.sort(this.lstOPlayer, new Comparator<OrganisationPlayer>() {
                @Override
                public int compare(OrganisationPlayer oPlayer1, OrganisationPlayer oPlayer2) {
                    return Integer.compare(oPlayer1.getPlayerKill(), oPlayer2.getPlayerKill());
                }
            });
            Collections.reverse(this.lstOPlayer);

            ItemStack filterButton = GeneralItem.renameItemStackWithName(new ItemStack(Material.IRON_SWORD), "§6§lJoueurs tués");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour trier par nombre morts"));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOPlayerFilterButton(filterButton, this, "DEATH"));
        }
        else if(this.filter.equals("DEATH")) {

            this.lstOPlayer = OrganisationPlayer.getLstOrganisationPlayer();
            Collections.sort(this.lstOPlayer, new Comparator<OrganisationPlayer>() {
                @Override
                public int compare(OrganisationPlayer oPlayer1, OrganisationPlayer oPlayer2) {
                    return Integer.compare(oPlayer1.getNbDeath(), oPlayer2.getNbDeath());
                }
            });
            Collections.reverse(this.lstOPlayer);

            ItemStack filterButton = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/8983065ed697c84a8602b46d7b8ebff3cda346112fd6903b91296eb38ef19e4e"), "§6§lMorts");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour trier par ordre alphabétique"));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOPlayerFilterButton(filterButton, this, "LETTER"));
        }
        else if(this.filter.equals("LETTER")) {

            this.lstOPlayer = OrganisationPlayer.getLstOrganisationPlayer();
            Collections.sort(this.lstOPlayer, new Comparator<OrganisationPlayer>() {
                @Override
                public int compare(OrganisationPlayer oPlayer1, OrganisationPlayer oPlayer2) {
                    return String.CASE_INSENSITIVE_ORDER.compare(oPlayer1.getLastSeenName(), oPlayer2.getLastSeenName());
                }
            });

            ItemStack filterButton = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/7746abe410028115029ce36cbf3a8ebd9c39d00d76f21c076c9360136b0e0a93"), "§6§lOrdre Alphabétique");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour trier par ancienneté sur le serveur"));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOPlayerFilterButton(filterButton, this, "OLD"));
        }
        else if(this.filter.equals("OLD")) {

            this.lstOPlayer = OrganisationPlayer.getLstOrganisationPlayer();
            Collections.sort(this.lstOPlayer, new Comparator<OrganisationPlayer>() {
                @Override
                public int compare(OrganisationPlayer oPlayer1, OrganisationPlayer oPlayer2) {
                    return Long.compare(oPlayer1.getFirstTimeConnexion(), oPlayer2.getFirstTimeConnexion());
                }
            });

            ItemStack filterButton = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/375919e276ee4ba7f9d751ba9f3e883f5ad788fcdfd1b3a34db012dfec2ab59"), "§6§lAncienneté");
            GeneralItem.cleanItemMeta(filterButton);
            GeneralItem.setLoreFromList(filterButton, Arrays.asList("§8Clique pour n'afficher que les joueurs connectés"));
            lstGroupInv.setItem(4, filterButton);
            this.addButton(new ChangeLstOPlayerFilterButton(filterButton, this, "CONNECTED"));
        }

        int index = 0;
        for (int line = 1; line < 5; line ++) {
            for (index = 0; index < 7; index++) {
                if (index + (line-1)*7 + this.page * 28 >= this.lstOPlayer.size()) break;

                OrganisationPlayer oPlayer = this.lstOPlayer.get(index + (line-1)*7 + this.page * 28);

                ItemStack button = oPlayer.getFullHeadItem();
                lstGroupInv.setItem(index + 1 + line*9, button);
                if((!this.getOPlayerViewer().getLstGroup().isEmpty() || !oPlayer.getLstGroup().isEmpty())
                        && !oPlayer.getUuid().equals(this.getOPlayerViewer().getUuid())) this.addButton(new OPlayerButton(button, oPlayer, this));
            }
        }

        int maxPage = -1 + this.lstOPlayer.size()/28 + ((this.lstOPlayer.size() % 28 == 0)? 0 : 1);
        if(this.page > 0 && this.lstOPlayer.size() > 28){
            lstGroupInv.setItem(45, previous);
            this.addButton(new PrevButton(previous, null, this));
        }
        if(this.page < maxPage) {
            lstGroupInv.setItem(53, next);
            this.addButton(new NextButton(next, null, this));
        }


        return lstGroupInv;
    }

    public void nextPage(){
        int nbPlayer = this.lstOPlayer.size();
        int maxPage = -1 + nbPlayer/28 + ((nbPlayer % 28 == 0)? 0 : 1);
        if(this.page < maxPage) this.page += 1;
    }
    public void prevPage(){
        this.page -=1;
        if(this.page < 0)this.page = 0;
    }

    public void setFilter(String filter){
        this.page = 0;
        this.filter = filter;
    }
}
