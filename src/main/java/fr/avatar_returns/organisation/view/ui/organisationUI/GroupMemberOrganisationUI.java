package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.organisationUI.BackButton;
import fr.avatar_returns.organisation.view.button.organisationUI.ChangeOMemberRoleButton;
import fr.avatar_returns.organisation.view.button.organisationUI.KickOMemberButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.PrevNextUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GroupMemberOrganisationUI extends InventoryUI implements PrevNextUI {

    private int page;
    private OrganisationMember oMember;

    public GroupMemberOrganisationUI(OrganisationMember oMember, InventoryUI motherUI) {
        super(null, motherUI);
        this.oMember = oMember;
        this.inv = this.generate();
    }

    @Override public Inventory generate() {

        Inventory lstRolesInv = Bukkit.createInventory(null, 27, "§8§l");

        ItemStack next = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/e3fc52264d8ad9e654f415bef01a23947edbccccf649373289bea4d149541f70"), "§8§lSuivant");
        ItemStack previous = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6"), "§8§lPrécédent");

        ItemStack oMemberItem = this.oMember.getOMemberItem();
        lstRolesInv.setItem(0, oMemberItem);
        this.addButton(new BackButton(oMemberItem, this));

        if(this.getOPlayerViewer().hasRolePermission(this.oMember.getRole(),  Permission.management.kick)) {
            ItemStack kickItem = GeneralItem.getItemStackWithName(Material.SPRUCE_DOOR, "§c§lKick " + this.oMember.getName());
            lstRolesInv.setItem(8, kickItem);
            this.addButton(new KickOMemberButton(kickItem, this.oMember, this));
        }

        ArrayList<Role> lstRole = this.oMember.getGroup().getLstRole();
        Collections.sort(lstRole, new Comparator<Role>() {
            @Override
            public int compare(Role role1, Role role2) {

                if(role1.getRolePower() == 0){
                    return 1;
                }
                else if(role2.getRolePower() == 0){
                    return -1;
                }
                else if(role1.getRolePower() == -1){
                    return -1;
                }
                else if(role2.getRolePower() == -1){
                    return 1;
                }

                return -Integer.compare(role1.getRolePower(), role2.getRolePower());
            }
        });
        Collections.reverse(lstRole);

        int index = 0;
        for (index = 10; index <17; index++) {
            if(index - 10 + this.page * 7 >= lstRole.size()) break;

           Role role = lstRole.get(index - 10 + this.page * 7);

            ItemStack button = role.getRoleItem();
            if(role.getName() == this.oMember.getRole().getName()){
                button.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL,1);
                GeneralItem.cleanItemMeta(button);
            }

            lstRolesInv.setItem(index, button);
            if(this.getOPlayerViewer().hasRolePermission(role,  Permission.management.promote) && this.getOPlayerViewer().hasRolePermission(this.oMember.getRole(), Permission.management.promote)) {
                this.addButton(new ChangeOMemberRoleButton(button, this.oMember, role, this));
            }
        }

        int maxPage = -1 + lstRole.size()/7 + ((lstRole.size() % 7 == 0)? 0 : 1);
        if(this.page > 0 && lstRole.size() > 7){
            lstRolesInv.setItem(18, previous);
            this.addButton(new PrevButton(previous, null, this));
        }
        if(this.page < maxPage) {
            lstRolesInv.setItem(26, next);
            this.addButton(new NextButton(next, null, this));
        }

        return lstRolesInv;
    }

    public void nextPage(){
        int nbRoles = this.oMember.getGroup().getLstRole().size();
        int maxPage = -1 + nbRoles/7 + ((nbRoles % 7 == 0)? 0 : 1);
        if(this.page < maxPage) this.page += 1;
    }
    public void prevPage(){
        this.page -=1;
        if(this.page < 0)this.page = 0;
    }
}
