package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.organisationUI.BackButton;
import fr.avatar_returns.organisation.view.button.organisationUI.InviteRemove;
import fr.avatar_returns.organisation.view.button.organisationUI.InviteSend;
import fr.avatar_returns.organisation.view.button.organisationUI.OPlayerValidInvitButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.PrevNextUI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;

public class OPlayerInvitLstGroup extends InventoryUI implements PrevNextUI {

    private int page;
    private final OrganisationPlayer oPlayer;
    private ArrayList<Group> lstGroupAvailableForInvit;

    public OPlayerInvitLstGroup(OrganisationPlayer oPlayer, InventoryUI motherUI) {

        super(null, motherUI);
        this.oPlayer = oPlayer;
        this.page = 0;
        this.lstGroupAvailableForInvit = new ArrayList<>();
        this.inv = this.generate();
    }

    @Override
    public Inventory generate() {

        Inventory oPlayerInvitLstGroup = Bukkit.createInventory(null, 27, "§8§lMes invitations");

        ArrayList<Invitation> lstPlayerInvit = oPlayer.getLstInvitation();
        ItemStack backItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.FEATHER, Math.max(lstPlayerInvit.size(), 1)), "§6§lInvitations");
        GeneralItem.cleanItemMeta(backItem);
        oPlayerInvitLstGroup.setItem(0, backItem);
        this.addButton(new BackButton(backItem, this));

        ItemStack next = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/e3fc52264d8ad9e654f415bef01a23947edbccccf649373289bea4d149541f70"), "§8§lSuivant");
        ItemStack previous = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6"), "§8§lPrécédent");

        this.lstGroupAvailableForInvit = new ArrayList<>();
        for(Group group : this.getOPlayerViewer().getLstGroup()){
            if(this.getOPlayerViewer().hasGroupPermission(group, Permission.management.invite))lstGroupAvailableForInvit.add(group);
        }


        int index = 0;
        for (index = 10; index <17; index++) {
            if(index - 10 + this.page * 7 >= lstGroupAvailableForInvit.size()) break;

            Group group = lstGroupAvailableForInvit.get(index - 10 + this.page * 7);

            ItemStack button = (group.getFlag() != null)? group.getFlag() : new ItemStack(Material.RED_BANNER);
            button = GeneralItem.renameItemStackWithName(button, "§6§l"+group.getName());
            GeneralItem.cleanItemMeta(button);

            boolean inviteFind = false;
            for(Invitation invitation: InviteCommand.getAllInvitation(this.oPlayer.getOfflinePlayer())){
                if(invitation.getOPlayerHost().getUuid().equals(this.getOPlayerViewer().getUuid())
                && invitation.getGroupHost().getName().equals(group.getName())) {
                    GeneralItem.setLoreFromList(button, Arrays.asList("§cClique pour supprimer ton invitation"));
                    GeneralItem.enchant(button, Enchantment.ARROW_FIRE);
                    inviteFind = true;
                    this.addButton(new InviteRemove(button,invitation, this));
                    break;
                }
            }

            if(!inviteFind) {
                GeneralItem.setLoreFromList(button, Arrays.asList("§aClique pour inviter"));
                this.addButton(new InviteSend(button,this.getOPlayerViewer(), this.oPlayer, group, this));
            }
            oPlayerInvitLstGroup.setItem(index, button);
        }

        int maxPage = -1 + lstGroupAvailableForInvit.size()/7 + ((lstGroupAvailableForInvit.size() % 7 == 0)? 0 : 1);
        if(this.page > 0 && lstGroupAvailableForInvit.size() > 7){
            oPlayerInvitLstGroup.setItem(18, previous);
            this.addButton(new PrevButton(previous, null, this));
        }
        if(this.page < maxPage) {
            oPlayerInvitLstGroup.setItem(26, next);
            this.addButton(new NextButton(next, null, this));
        }

        return oPlayerInvitLstGroup;
    }

    public void nextPage(){
        int nbInvit = this.lstGroupAvailableForInvit.size();
        int maxPage = -1 + nbInvit/7 + ((nbInvit % 7 == 0)? 0 : 1);
        if(this.page < maxPage) this.page += 1;
    }
    public void prevPage(){
        this.page -=1;
        if(this.page < 0)this.page = 0;
    }
}
