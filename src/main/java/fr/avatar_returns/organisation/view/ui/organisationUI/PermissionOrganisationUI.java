package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.organisationUI.*;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import fr.avatar_returns.organisation.view.ui.PrevNextUI;
import net.md_5.bungee.api.chat.hover.content.Item;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;

public class PermissionOrganisationUI extends InventoryUI implements PrevNextUI {

    private Role role;
    private int page;
    private String mode; //ROLE, MANAGEMENT, BUILD, LAND, DIPLOMATIE, INTERACT, ALL
    private ArrayList<String> lstPermission;
    public PermissionOrganisationUI(Role role, InventoryUI motherUI) {

        super(null, motherUI);
        this.role = role;
        this.page = 0;
        this.mode = "ROLE";
        this.lstPermission = role.getLstPermission();

        this.inv = this.generate();
    }

    @Override
    public Inventory generate() {

        Inventory lstPermInv = Bukkit.createInventory(null, 27, "§8§lPermissions");

        if(this.mode.equalsIgnoreCase("ROLE")) {
            ItemStack rolePerm = GeneralItem.renameItemStackWithName(GeneralItem.resetLore(role.getRoleItem()), "§7Permission du role " + this.role.getName());
            GeneralItem.setLoreFromList(rolePerm, Arrays.asList("§8Clique pour voir les permission de gestion de ton organisation"));
            lstPermInv.setItem(4, rolePerm);
            this.addButton(new ChangePermissionFilterButton(rolePerm,this, "MANAGEMENT"));
            this.lstPermission = role.getLstPermission();
        }
        else if(this.mode.equalsIgnoreCase("MANAGEMENT")){
            ItemStack managementPerm = GeneralItem.renameItemStackWithName(new ItemStack(Material.SPYGLASS), "§7Gestion");
            GeneralItem.setLoreFromList(managementPerm, Arrays.asList("§8Clique pour voir les permission de gestion de build"));
            lstPermInv.setItem(4, managementPerm);
            this.addButton(new ChangePermissionFilterButton(managementPerm,this, "BUILD"));
            this.lstPermission = Permission.getManagementPermissions();
        }
        else if(this.mode.equalsIgnoreCase("BUILD")){
            this.lstPermission = Permission.getBuildPermissions();
            ItemStack buildPerm = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/65390e3cc1f07441e9de139840d08217f861857ff98f4211c79f3c0b4860d80e"), "§7Build");
            GeneralItem.setLoreFromList(buildPerm, Arrays.asList("§8Clique pour voir les permission de gestion de gestion des lands"));
            lstPermInv.setItem(4, buildPerm);
            this.addButton(new ChangePermissionFilterButton(buildPerm,this, "LAND"));
        }
        else if(this.mode.equalsIgnoreCase("LAND")) {
            this.lstPermission = Permission.getLandPermissions();
            ItemStack landPerm = GeneralItem.renameItemStackWithName(new ItemStack(Material.GRASS_BLOCK), "§7Land");
            GeneralItem.setLoreFromList(landPerm, Arrays.asList("§8Clique pour voir les permission de gestion de diplomatie"));
            lstPermInv.setItem(4, landPerm);
            this.addButton(new ChangePermissionFilterButton(landPerm,this, "DIPLOMATIE"));
        }
        else if(this.mode.equalsIgnoreCase("DIPLOMATIE")){
            this.lstPermission = Permission.getDiplomatiePermissions();
            ItemStack diplomatiePerm = GeneralItem.renameItemStackWithName(new ItemStack(Material.WRITABLE_BOOK), "§7Diplomatie");
            GeneralItem.setLoreFromList(diplomatiePerm, Arrays.asList("§8Clique pour voir les permission d'interaction"));
            lstPermInv.setItem(4, diplomatiePerm);
            this.addButton(new ChangePermissionFilterButton(diplomatiePerm,this, "INTERACT"));
        }
        else if(this.mode.equalsIgnoreCase("INTERACT")){
            this.lstPermission = Permission.getInteractPermissions();
            ItemStack interactPerm = GeneralItem.renameItemStackWithName(new ItemStack(Material.LEVER), "§7Interactions");
            GeneralItem.setLoreFromList(interactPerm, Arrays.asList("§8Clique pour voir les permission du role"));
            lstPermInv.setItem(4, interactPerm);
            this.addButton(new ChangePermissionFilterButton(interactPerm,this, "ROLE"));
        }


        ItemStack backButton = role.getRoleItem();
        lstPermInv.setItem(0, backButton);
        this.addButton(new BackButton(backButton, this));


        ItemStack next = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/e3fc52264d8ad9e654f415bef01a23947edbccccf649373289bea4d149541f70"), "§8§lSuivant");
        ItemStack previous = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6"), "§8§lPrécédent");

        if(!(role.isRecrue() || role.isLeader())) {
            if(this.getOPlayerViewer().hasRolePermission(this.role, Permission.management.removeRole)) {
                ItemStack remove = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/441f4edbc68c9061355242bd73effc9299a3252b9f11e82b5f1ec7b3b6ac0"), "§c§lSupprimer le rôle " + this.role.getName());
                lstPermInv.setItem(8, remove);
                this.addButton(new RemoveRoleButton(remove, this, role));
            }
        }

        int index = 0;
        for (index = 10; index <17; index++) {
            if(index - 10 + this.page * 7 >= this.lstPermission .size()) break;

            String perm = this.lstPermission .get(index - 10 + this.page * 7);

            ItemStack button = Permission.getPermissionItem(this.role, perm);
            lstPermInv.setItem(index, button);
            this.addButton(new ChangePermissionButton(button, this, role, perm));
        }

        int maxPage = -1 + this.lstPermission.size()/7 + ((this.lstPermission.size() % 7 == 0)? 0 : 1);
        if(this.page > 0 && this.lstPermission .size() > 7){
            lstPermInv.setItem(18, previous);
            this.addButton(new PrevButton(previous, null, this));
        }
        if(this.page < maxPage) {
            lstPermInv.setItem(26, next);
            this.addButton(new NextButton(next, null, this));
        }

        return lstPermInv;
    }

    public void setMode(String mode){
        this.page = 0;
        this.mode = mode;
    }

    public void nextPage(){
        int nbPerm = this.lstPermission.size();
        int maxPage = -1 + nbPerm/7 + ((nbPerm % 7 == 0)? 0 : 1);
        if(this.page < maxPage) this.page += 1;
    }
    public void prevPage(){
        this.page -=1;
        if(this.page < 0)this.page = 0;
    }
}
