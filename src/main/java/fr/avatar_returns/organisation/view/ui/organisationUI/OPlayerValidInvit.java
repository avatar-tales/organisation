package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.organisationUI.BackButton;
import fr.avatar_returns.organisation.view.button.organisationUI.InviteAccept;
import fr.avatar_returns.organisation.view.button.organisationUI.InviteRemove;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class OPlayerValidInvit extends InventoryUI {

    private final Invitation invitation;

    public OPlayerValidInvit(Invitation invitation, InventoryUI motherUI) {

        super(null, motherUI);
        this.invitation = invitation;
        this.inv = this.generate();

    }

    @Override public Inventory generate() {

        Inventory oPlayerValidInvitUI = Bukkit.createInventory(null, 27, "§8§lMes invitations");

        ItemStack backItem = this.invitation.getInvitationItem();
        oPlayerValidInvitUI.setItem(0, backItem);

        this.addButton(new BackButton(backItem, this));

        ItemStack valid = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/a79a5c95ee17abfef45c8dc224189964944d560f19a44f19f8a46aef3fee4756"), "§a§lAccepter");
        ItemStack deny = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/27548362a24c0fa8453e4d93e68c5969ddbde57bf6666c0319c1ed1e84d89065"), "§c§lRefuser");

        if((this.getOPlayerViewer().getLstLargeGroup().isEmpty() && this.invitation.getGroupHost() instanceof LargeGroup)
        || this.getOPlayerViewer().getLstGuilde().isEmpty() && this.invitation.getGroupHost() instanceof Guilde) {
            oPlayerValidInvitUI.setItem(12, valid);
            oPlayerValidInvitUI.setItem(14, deny);
            this.addButton(new InviteAccept(valid, this.invitation, this));
        }
        else{
            oPlayerValidInvitUI.setItem(13, deny);
        }

        this.addButton(new InviteRemove(deny, this.invitation, this ));

        return oPlayerValidInvitUI;
    }
}
