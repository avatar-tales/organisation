package fr.avatar_returns.organisation.view.ui.objectiveUI;

import fr.avatar_returns.organisation.rank.GroupObjective;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.button.objectiveButton.CheckButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevSubUIButton;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ObjectiveRessourceDepositUI extends InventoryUI {

    GroupObjective groupObjective;

    public ObjectiveRessourceDepositUI(ObjectiveUI objectiveUI, GroupObjective groupObjective){

        super(null, objectiveUI);
        this.groupObjective = groupObjective;
        this.inv = this.generate();
    }

    public ObjectiveUI getObjectiveUI() {return (ObjectiveUI) this.getMotherUI();}
    public GroupObjective getGroupObjective() {return this.groupObjective;}

    @Override public Inventory generate() {

        Inventory depositInventory =  Bukkit.createInventory(null, 27, "§8§lDepot de "+this.getGroupObjective().getObjective().getName().replace("_", " "));
        ItemStack itemToDeposit = this.getGroupObjective().getObjective().getItem();
        ItemStack check = SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/a79a5c95ee17abfef45c8dc224189964944d560f19a44f19f8a46aef3fee4756");
        ItemStack previous = SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6");
        ItemStack blackStainedButton = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);

        ItemMeta prevMeta = previous.getItemMeta();
        prevMeta.setDisplayName("§8§lRetour au menu");
        previous.setItemMeta(prevMeta);

        ItemMeta blackMeta = blackStainedButton.getItemMeta();
        blackMeta.setDisplayName(" ");
        blackStainedButton.setItemMeta(blackMeta);

        ItemMeta checkMeta = check.getItemMeta();
        ArrayList<Component> checkLore = new ArrayList<>();
        checkLore.add(Component.text("§8Etat : §7" + this.getGroupObjective().getCurrentObjectiveValue() + "/" + this.getGroupObjective().getObjective().getObjectifValue()));
        checkMeta.lore(checkLore);
        checkMeta.setDisplayName("§aValider");
        check.setItemMeta(checkMeta);

        for(int i = 0; i< depositInventory.getSize(); i++)depositInventory.setItem(i, blackStainedButton);

        depositInventory.setItem(13, itemToDeposit);
        depositInventory.setItem(18, previous);
        this.addButton( new PrevSubUIButton(previous, this));

        if(!this.getGroupObjective().isAcomplished()) {
            depositInventory.setItem(22, check);
            this.addButton(new CheckButton(check, this));
        }
        else{
            GroupRank.checkProgression(this.groupObjective.getGroup());
        }

        return depositInventory;
    }
}
