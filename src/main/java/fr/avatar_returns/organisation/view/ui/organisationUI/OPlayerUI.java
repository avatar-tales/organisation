package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.territory.Province;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.territory.VMine;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.ItemUtils;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.organisationUI.*;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class OPlayerUI extends InventoryUI{

    private OrganisationPlayer oPlayer;

    public OPlayerUI(OrganisationPlayer oPlayer, InventoryUI motherUI) {
        super(null, motherUI);

        this.oPlayer = oPlayer;
        this.inv = this.generate();
    }

    @Override public Inventory generate() {

        Inventory oPlayerUI = Bukkit.createInventory(null, 27, "§8§l"+this.oPlayer.getLastSeenName());
        ItemStack backItem = GeneralItem.renameItemStackWithName(this.oPlayer.getFullHeadItem(), "§6§l" + this.oPlayer.getLastSeenName());
        oPlayerUI.setItem(0, backItem);
        this.addButton(new BackButton(backItem, this));

        ItemStack factionItem = null;
        ItemStack guildItem = null;
        LargeGroup largeGroup = null;
        Guilde guilde = null;

        for(Group tmpGroup: this.oPlayer.getLstGroup()){
            if(tmpGroup instanceof LargeGroup){
                largeGroup = (LargeGroup) tmpGroup;
                factionItem = tmpGroup.getItemRank();
            }
            else if(tmpGroup instanceof Guilde){
                guilde = (Guilde) tmpGroup;
                guildItem =guilde.getItemRank();
            }
        }

        if(largeGroup != null) {
            oPlayerUI.setItem(8, factionItem);
            this.addButton(new GroupButton(factionItem, largeGroup, this));
        }
        if(guilde != null) {
            oPlayerUI.setItem(7, guildItem);
            this.addButton(new GroupButton(guildItem, guilde, this));
        }

        int nbInvitAutorisation = 0;
        Group lastGroupInvite = null;
        for(Group group: this.getOPlayerViewer().getLstGroup()){
            if(group.getLstGroupOrganisationPlayer().contains(this.oPlayer))continue; // Si le groupe contient déja le joueur, on ne peut pas l'inviter.
            if(this.getOPlayerViewer().hasGroupPermission(group, Permission.management.invite)){
                nbInvitAutorisation ++;
                lastGroupInvite = group;
            }
        }

        if(nbInvitAutorisation > 0) {
            ItemStack invitItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.FEATHER), "§6§lInviter");

            if(nbInvitAutorisation == 1){

                boolean inviteFind = false;
                for(Invitation invitation: InviteCommand.getAllInvitation(this.oPlayer.getOfflinePlayer())){
                    if(invitation.getOPlayerHost().getUuid().equals(this.getOPlayerViewer().getUuid())) {
                        GeneralItem.setLoreFromList(invitItem, Arrays.asList("§cClique pour supprimer ton invitation"));
                        GeneralItem.enchant(invitItem, Enchantment.ARROW_FIRE);
                        inviteFind = true;
                        this.addButton(new InviteRemove(invitItem,invitation, this));
                        break;
                    }
                }
                if(!inviteFind) {
                    GeneralItem.setLoreFromList(invitItem, Arrays.asList("§8Inviter a rejoindre " + lastGroupInvite.getName()));
                    this.addButton(new InviteSend(invitItem, this.getOPlayerViewer(), oPlayer, lastGroupInvite, this));
                }

            }
            else{
                this.addButton(new OPlayerInvitLstGroupButton(invitItem, this.oPlayer, this));
            }
            oPlayerUI.setItem(13, invitItem);
        }

        if(this.oPlayer.isInFightMod()){
            ItemStack fightModItem = new ItemStack(Material.STONE_SWORD);
            ItemMeta tmpMeta = fightModItem.getItemMeta();
            tmpMeta.setDisplayName("§c§lMode combat");
            ArrayList<Component> fightDesc = new ArrayList<>();
            long timeLeft = this.oPlayer.getLastTimeFightMod()/1000 + GeneralMethods.getIntConf("Organisation.FightMode.Duration") - (System.currentTimeMillis()/1000);
            fightDesc.add(Component.text("§8Temps restant : §7" + timeLeft));
            tmpMeta.lore(fightDesc);
            fightModItem.setItemMeta(tmpMeta);
            GeneralItem.cleanItemMeta(fightModItem);
            oPlayerUI.setItem(26, fightModItem);
        }

        return oPlayerUI;
    }


}
