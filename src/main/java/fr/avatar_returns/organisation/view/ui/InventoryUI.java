package fr.avatar_returns.organisation.view.ui;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.view.button.InventoryButton;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveRessourceDepositUI;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import io.lumine.mythic.bukkit.utils.lib.jooq.impl.QOM;
import io.papermc.paper.event.world.StructuresLocateEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.Nullable;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class InventoryUI {

    protected Inventory inv;
    private ArrayList<InventoryButton> lstButton;
    private static ArrayList<InventoryUI> lstInventoryUi = new ArrayList<>();
    private InventoryUI motherUI;
    private OrganisationPlayer oPlayerViewer;
    private static HashMap<Player, Long> mapTimeToClick = new HashMap<>();

    public InventoryUI(Inventory inventory, OrganisationPlayer oPlayerViewer){
        this.inv = inventory;
        this.motherUI = null;
        this.lstButton = new ArrayList<>();
        this.oPlayerViewer = oPlayerViewer;
        InventoryUI.lstInventoryUi.add(this);
    }
    public InventoryUI(Inventory inventory, InventoryUI motherUI){
        this.inv = inventory;
        this.motherUI = motherUI;
        this.oPlayerViewer = motherUI.getOPlayerViewer();
        this.lstButton = new ArrayList<>();
        InventoryUI.lstInventoryUi.add(this);
    }

    public abstract Inventory generate();

    public void clear(){
        this.lstButton = new ArrayList<>();
        if(this.inv != null) this.inv.clear();
    }
    public void reload(){

        if(this.inv == null)return;
        this.clear();

        Inventory generate = this.generate();
        for(int i =0;  i <this.getInv().getSize(); i++){
            this.inv.setItem(i, generate.getItem(i));
        }
    }
    public void addButton(InventoryButton inventoryButton){
        this.lstButton.add(inventoryButton);
    }
    public static void remove(Inventory inventory) {

        InventoryUI inventoryUI = InventoryUI.getInventoryUiFromInventory(inventory);
        if(inventoryUI != null)inventoryUI.remove();

    }

    public void remove(){
        this.clear();
        if(InventoryUI.lstInventoryUi.contains(this)) InventoryUI.lstInventoryUi.remove(this);
    }
    public boolean isButton(ItemStack itemStack){return this.getButtonFromItemStack(itemStack) != null;}
    public static boolean isUIButton(ItemStack itemStack){

        if(itemStack == null)return false;

        for(InventoryUI inventoryUI: InventoryUI.lstInventoryUi){
            if(inventoryUI.isButton(itemStack))return true;
        }
        return false;
    }

    public OrganisationPlayer getOPlayerViewer() {
        if(this.oPlayerViewer != null)return this.oPlayerViewer;
        if(this.motherUI != null)return this.getMotherUI().getOPlayerViewer();
        System.out.println("WTF -------------> THEORIQUEMENT IMPOSSIBLE");
        return this.oPlayerViewer;
    }

    @Nullable public InventoryUI getMotherUI() {return motherUI;}
    public ArrayList<InventoryUI> getLstSubUI(){
        ArrayList<InventoryUI> lstSubUI = new ArrayList<>();

        for(InventoryUI inventoryUI : InventoryUI.getLstInventoryUi()){
            if(inventoryUI.motherUI == null)continue;
            if(inventoryUI.equals(this))continue;
            if(inventoryUI.motherUI.equals(this))lstSubUI.add(inventoryUI);
        }

        return lstSubUI;
    }
    public static ArrayList<InventoryUI> getLstInventoryUi() {
        return lstInventoryUi;
    }
    public ArrayList<InventoryButton> getLstButton() {
        return lstButton;
    }
    public Inventory getInv() {
        return inv;
    }

    @Nullable public static InventoryButton getButtonFromItem(ItemStack itemStack, Inventory inventory, Player player){

        InventoryUI inventoryUI = InventoryUI.getInventoryUiFromInventory(inventory, player);
        if(inventoryUI == null)return null;
        if(itemStack == null)return null;

        for(InventoryButton inventoryButton: inventoryUI.getLstButton()){
            if(inventoryButton.getItemToClick() == null)continue;
            if(inventoryButton.getItemToClick().equals(itemStack)){
                return inventoryButton;
            }
        }
        return null;
    }
    @Nullable public static InventoryUI getInventoryUiFromItem(ItemStack itemStack){

        if(itemStack == null)return null;
        for(InventoryUI inventoryUI: InventoryUI.lstInventoryUi){
            for(InventoryButton inventoryButton: inventoryUI.getLstButton()){
                if(inventoryButton.getItemToClick() == null || itemStack == null)continue;
                if(inventoryButton.getItemToClick().equals(itemStack))return inventoryUI;
            }
        }
        return null;
    }
//    @Nullable public static InventoryUI getInventoryUiFromButton(InventoryButton inventoryButton, Player player){
//
//        if(inventoryButton == null)return null;
//        if(player == null) return null;
//
//        for(InventoryUI inventoryUI: InventoryUI.lstInventoryUi){
//            if(inventoryUI.getLstButton().contains(inventoryButton) && inventoryUI.getOPlayerViewer().getUuid().equals(player.getUniqueId()))return inventoryUI;
//        }
//        return null;
//    }
    @Nullable public static InventoryUI getInventoryUiFromInventory(Inventory inventory, Player player){

    if(inventory == null)return null;
    for(InventoryUI inventoryUI : InventoryUI.getLstInventoryUi()){
        if(inventoryUI.getInv() == inventory && inventoryUI.getOPlayerViewer().getUuid().equals(player.getUniqueId()))return inventoryUI;
    }
    return null;
}

    @Nullable public static InventoryUI getInventoryUiFromInventory(Inventory inventory){

        if(inventory == null)return null;
        for(InventoryUI inventoryUI : InventoryUI.getLstInventoryUi()){
            if(inventoryUI.getInv() == inventory)return inventoryUI;
        }
        return null;
    }
    @Nullable InventoryButton getButtonFromItemStack(ItemStack itemStack){

        if(itemStack == null)return null;

        for(InventoryButton inventoryButton : this.lstButton){
            if(inventoryButton.getItemToClick() == null || itemStack == null)continue;
            if(inventoryButton.getItemToClick().equals(itemStack))return inventoryButton;
        }
        return null;
    }

    public static boolean manageEvent(InventoryEvent e){

        Inventory inventory = e.getInventory();
        InventoryUI ui = InventoryUI.getInventoryUiFromInventory(inventory);


        if(ui == null)return false;
        if (e instanceof InventoryDragEvent){

            InventoryDragEvent event = (InventoryDragEvent)e;
            if (!(ui instanceof ObjectiveRessourceDepositUI)) return true;

            ObjectiveRessourceDepositUI depositUi = (ObjectiveRessourceDepositUI) ui;
            if(event.getNewItems() == null || depositUi.getGroupObjective().getObjective().getItem() == null)return GeneralMethods.cancelEvent(event);
            if(!event.getNewItems().equals(depositUi.getGroupObjective().getObjective().getItem()))return GeneralMethods.cancelEvent(event);

            return true;
        }
        else if(e instanceof InventoryClickEvent){

            InventoryClickEvent event = (InventoryClickEvent)e;
            if(!(event.getWhoClicked() instanceof Player)) return GeneralMethods.cancelEvent(event);
            Player player = (Player) event.getWhoClicked();

            InventoryButton inventoryButton = InventoryUI.getButtonFromItem(event.getCurrentItem(), event.getClickedInventory(), player);
            if (inventoryButton != null) {

                if (inventoryButton.getUi().getOPlayerViewer().getUuid().equals(player.getUniqueId())) {
                    long nextAllowedTime = mapTimeToClick.getOrDefault(player, 0L);
                    if (nextAllowedTime < System.currentTimeMillis()) {
                        try {
                            inventoryButton.perform(player);
                        }
                        catch (Exception | Error exception){
                            Organisation.log.severe(exception.getMessage());
                            Organisation.log.severe(exception.getStackTrace().toString());
                        }
                    }

                }
                else{
                    Organisation.log.severe("DUPLICATE BUTTON IN MULTIPLE UI for " + player.getName() + " with item " + event.getCurrentItem().toString());
                    GeneralMethods.sendPlayerErrorMessage(player,"Vous rencontrez un bug de bouton dupliqué entre plusieurs UI.\nMerci d'ouvrir un ticket a l'administration");

                }
            }

            if(!(event.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)
                    || event.getAction().toString().contains("PICKUP")
                    || event.getAction().toString().contains("PLACE"))
            ){
                if(event.getAction().equals(InventoryAction.CLONE_STACK) && (player.isOp() || player.hasPermission("organisation.board.cloneItem")))return true;
                return GeneralMethods.cancelEvent(event);
            }

            if(event.getClickedInventory() != null){
                InventoryUI clickedUI = InventoryUI.getInventoryUiFromInventory(((InventoryClickEvent) e).getClickedInventory());
                if(clickedUI == null) {
                    if(event.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
                        if (!(ui instanceof ObjectiveRessourceDepositUI)) return GeneralMethods.cancelEvent(event);
                    }
                    return true;
                }
                if (!(clickedUI instanceof ObjectiveRessourceDepositUI)) return GeneralMethods.cancelEvent(event);
                if(event.getSlot() != 13)return GeneralMethods.cancelEvent(event);

                ObjectiveRessourceDepositUI depositUi = (ObjectiveRessourceDepositUI) clickedUI;
                if(event.getCursor() == null || depositUi.getGroupObjective().getObjective().getItem() == null)return GeneralMethods.cancelEvent(event);

                ItemStack itemToPut = event.getCursor();
                int oldAmount = itemToPut.getAmount();
                itemToPut.setAmount(1);

                if(!itemToPut.equals(depositUi.getGroupObjective().getObjective().getItem()))return GeneralMethods.cancelEvent(event);

                itemToPut.setAmount(oldAmount);
            }

            return true;
        }
        else if(e instanceof InventoryCloseEvent){
            ui.remove();
            return true;
        }

        return false;
    }

}

