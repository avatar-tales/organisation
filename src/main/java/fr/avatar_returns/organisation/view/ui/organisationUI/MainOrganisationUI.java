package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.Province;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.organisationUI.*;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class MainOrganisationUI extends InventoryUI {

    OrganisationPlayer oTargetPlayer;
    OrganisationPlayer oViewerPlayer;

    public MainOrganisationUI(OrganisationPlayer oTargetPlayer) {
        super(null, oTargetPlayer);
        this.oTargetPlayer = oTargetPlayer;
        this.oViewerPlayer = oTargetPlayer;
        this.inv = this.generate();
    }

    public MainOrganisationUI(OrganisationPlayer oTargetPlayer, OrganisationPlayer oViewerPlayer) {
        super(null, oViewerPlayer);
        this.oTargetPlayer = oTargetPlayer;
        this.oViewerPlayer = oViewerPlayer;
        this.inv = this.generate();
    }

    @Override public Inventory generate() {

        Inventory management = Bukkit.createInventory(null, 45, "§8§lGestion");
        
        ItemStack factionItem = null;
        ItemStack guildItem = null;
        LargeGroup largeGroup = null;
        Guilde guilde = null;

        for(Group tmpGroup: oTargetPlayer.getLstGroup()){
            if(tmpGroup instanceof LargeGroup){
                largeGroup = (LargeGroup) tmpGroup;
                factionItem = tmpGroup.getItemRank();
            }
            else if(tmpGroup instanceof Guilde){
                guilde = (Guilde) tmpGroup;
                guildItem =guilde.getItemRank();
            }
        }

        ItemStack groupsItem = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/49c1832e4ef5c4ad9c519d194b1985030d257914334aaf2745c9dfd611d6d61d"),"§6§lOrganisations");
        management.setItem(4, groupsItem);
        this.addButton(new LstGroupButton(groupsItem, this));

        management.setItem( 0, oTargetPlayer.getFullHeadItem());

        ItemStack chatLockedItem = oTargetPlayer.getChatLockedItem();
        management.setItem( 1, chatLockedItem);
        if(oTargetPlayer.equals(oViewerPlayer))this.addButton(new ChatLockedButton(chatLockedItem, this, this.oTargetPlayer));

        ArrayList<Invitation> lstPlayerInvit = oTargetPlayer.getLstInvitation();
        if(!lstPlayerInvit.isEmpty()){
            ItemStack invitButton = GeneralItem.renameItemStackWithName(new ItemStack(Material.FEATHER,lstPlayerInvit.size()), "§6§lInvitations");
            GeneralItem.enchant(invitButton, Enchantment.ARROW_FIRE);
            GeneralItem.cleanItemMeta(invitButton);
            management.setItem(9, invitButton);
            this.addButton(new LstOPlayerInvitButton(invitButton, this.oTargetPlayer, this));
        }

        if(largeGroup != null) {
            management.setItem(8, factionItem);
            this.addButton(new GroupButton(factionItem, largeGroup, this));
        }
        if(guilde != null) {
            management.setItem(17, guildItem);
            this.addButton(new GroupButton(guildItem, guilde, this));
        }


        int cpt = 0;
        for(Province province: Province.getLstProvince()){
            ItemStack tmpProvinceItem = province.getProvinceItem();
            management.setItem(cpt%5 + 20 + (cpt/5*9), tmpProvinceItem);
            this.addButton(new ProvinceButton(tmpProvinceItem, province, this));
            cpt++;
        }

        try {
            if(!oTargetPlayer.isInPrespawnMod()) {
                ItemStack localisation = new ItemStack(Material.COMPASS);
                ItemMeta locMeta = localisation.getItemMeta();
                locMeta.setDisplayName("§6§lLocalisation");

                Location location = oTargetPlayer.getOfflinePlayer().getPlayer().getLocation();

                LandSubTerritory sub = LandSubTerritory.getLandSubTerritoryByLocation(location);
                Land land = null;
                if (sub == null) land = Land.getLandByLocation(location);
                else land = sub.getLand();

                ArrayList<Component> locationDesc = new ArrayList<>();
                if(land != null) locationDesc.add(Component.text("§8Province : §7" + land.getProvince().getName().replace("_", " ")));
                if(land != null) locationDesc.add(Component.text("§8Land : §7" + land.getName().replace("_", " ")));
                if(sub != null) locationDesc.add(Component.text("§8Zone : §7" + sub.getName().replace("_", " ")));

                locMeta.lore(locationDesc);
                localisation.setItemMeta(locMeta);
                if(land != null) management.setItem(36, localisation);
            }
        }
        catch (NullPointerException e){};

        if(oTargetPlayer.isInFightMod()){
            ItemStack fightModItem = new ItemStack(Material.STONE_SWORD);
            ItemMeta tmpMeta = fightModItem.getItemMeta();
            tmpMeta.setDisplayName("§c§lMode combat");
            ArrayList<Component> fightDesc = new ArrayList<>();
            long timeLeft = oTargetPlayer.getLastTimeFightMod()/1000 + GeneralMethods.getIntConf("Organisation.FightMode.Duration") - (System.currentTimeMillis()/1000);
            fightDesc.add(Component.text("§8Temps restant : §7" + timeLeft));
            tmpMeta.lore(fightDesc);
            fightModItem.setItemMeta(tmpMeta);
            GeneralItem.cleanItemMeta(fightModItem);
            //management.setItem(44, fightModItem);
            management.setItem(40, fightModItem);
        }

        ItemStack lstPlayerItem = new ItemStack(Material.PLAYER_HEAD);
        lstPlayerItem = GeneralItem.renameItemStackWithName(lstPlayerItem, "§6§lJoueurs");
        GeneralItem.cleanItemMeta(lstPlayerItem);
        management.setItem(44, lstPlayerItem);
        this.addButton(new LstOPlayerButton(lstPlayerItem, this));

        this.inv = management;
        return management;
    }
}
