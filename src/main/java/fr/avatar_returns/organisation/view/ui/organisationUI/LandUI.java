package fr.avatar_returns.organisation.view.ui.organisationUI;

import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.territory.Province;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.territory.VMine;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.button.objectiveButton.NextButton;
import fr.avatar_returns.organisation.view.button.objectiveButton.PrevButton;
import fr.avatar_returns.organisation.view.button.organisationUI.*;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;

public class LandUI extends InventoryUI{

    private VLand vLand;
    private ItemStack backItem;
    private  int page;
    private ArrayList<VMine> lstVMine;

    public LandUI(VLand vLand, InventoryUI motherUI, ItemStack backItem) {
        super(null, motherUI);
        this.vLand = vLand;
        this.page = 0;
        this.backItem = backItem;
        this.lstVMine = new ArrayList<>();

        for (VMine vMine : VMine.getLstVMine()){ if (vMine.getVLand().getId() == this.vLand.getId() && vMine.getVLand().getServerId().equals(this.vLand.getServerId())) this.lstVMine.add(vMine);}

        this.inv = this.generate();
    }

    @Override public Inventory generate() {

        Inventory landUI = Bukkit.createInventory(null, 27, "§8§l"+this.vLand.getName());
        landUI.setItem(0, this.backItem);
        this.addButton(new BackButton(this.backItem, this));

        ItemStack next = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/e3fc52264d8ad9e654f415bef01a23947edbccccf649373289bea4d149541f70"), "§8§lSuivant");
        ItemStack previous = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/5f133e91919db0acefdc272d67fd87b4be88dc44a958958824474e21e06d53e6"), "§8§lPrécédent");

        for(Province province : Province.getLstProvince()){
            if(province.getName().equals(this.vLand.getProvinceName())){
                ItemStack tmpProvinceItem = province.getProvinceItem();
                landUI.setItem( 8, tmpProvinceItem);
                this.addButton(new ProvinceButton(tmpProvinceItem, province, this));
                break;
            }
        }

        Group group = Group.getGroupById(vLand.getOwnerId());
        if(group != null) {

            ItemStack factionItem = group.getItemRank().clone();
            landUI.setItem( 7, factionItem);
            this.addButton(new GroupButton(factionItem, group, this));

            if (this.vLand.isMainLand()) {
                ItemStack isMainLand = GeneralItem.renameItemStackWithName(SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/639fd137e995da264df2448ff9f635728b48ca10eac7a6147f31e7c1901750f9"), "§6§lLand principale");
                landUI.setItem(6, isMainLand);
            }
        }

        this.lstVMine = new ArrayList<>();
        for (VMine vMine : VMine.getLstVMine()){ if (vMine.getVLand().getId() == this.vLand.getId() && vMine.getVLand().getServerId().equals(this.vLand.getServerId())) this.lstVMine.add(vMine);}

        int index = 0;
        for (index = 10; index <17; index++) {
            if(index - 10 + this.page * 7 >= lstVMine.size()) break;

            VMine vMine = this.lstVMine.get(index - 10 + this.page * 7);

            ItemStack button = vMine.getItem();
            if(getOPlayerViewer().hasPermission("organisation.admin")){
                GeneralItem.setLoreFromList(button, Arrays.asList("§c§l[ADMIN] §8Id inventory : ","§7" + vMine.getStoredInventory().getName()));
            }
            landUI.setItem(index, button);
            this.addButton(new MineButton(button, vMine, this));
        }

        int maxPage = -1 + this.lstVMine.size()/7 + ((this.lstVMine.size() % 7 == 0)? 0 : 1);
        if(this.page > 0 && this.lstVMine .size() > 7){
            landUI.setItem(18, previous);
            this.addButton(new PrevButton(previous, null, this));
        }
        if(this.page < maxPage) {
            landUI.setItem(26, next);
            this.addButton(new NextButton(next, null, this));
        }

        return landUI;
    }


    public void nextPage(){
        int nbPerm = this.lstVMine.size();
        int maxPage = -1 + nbPerm/7 + ((nbPerm % 7 == 0)? 0 : 1);
        if(this.page < maxPage) this.page += 1;
    }
    public void prevPage(){
        this.page -=1;
        if(this.page < 0)this.page = 0;
    }
}
