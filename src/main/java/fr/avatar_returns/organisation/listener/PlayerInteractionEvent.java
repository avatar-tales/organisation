package fr.avatar_returns.organisation.listener;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.hook.ItemAdderHook;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.storedInventory.MineUtils;
import fr.avatar_returns.organisation.storedInventory.StoredInventory;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.territory.Territory;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.groupSubTerritory.GroupSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.RPSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Totem;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import net.citizensnpcs.api.CitizensAPI;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityPotionEffectEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.inventory.Inventory;
import java.util.ArrayList;

public class PlayerInteractionEvent implements Listener {

    @EventHandler public void onPlayerBlockBreakEvent(BlockBreakEvent e){

        if(!Organisation.isBuildProtectionEnable)return;

        Block breakBlock = e.getBlock();
        Player player  = e.getPlayer();
        Location locationBreakBlock = breakBlock.getLocation();

        if(ItemAdderHook.isFactionBoard(breakBlock)){
            if(!(player.isOp() || player.hasPermission("organisation.admin"))){
                e.setCancelled(true);
                return;
            }
        }

        Territory territory = LandSubTerritory.getLandSubTerritoryByLocation(locationBreakBlock);

        if(territory == null)territory = Land.getLandByLocation(locationBreakBlock);
        if(territory == null && !(player.isOp() || player.hasPermission("organisation.widl.allowBreakBlock"))) {
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.CantBreakHere", true);
            e.setCancelled(true);
        }

        if(territory instanceof LandSubTerritory && !(player.isOp() || player.hasPermission("organisation.landSubTerritory.allowBreakBlock"))){

            if(territory instanceof RPSubTerritory || territory instanceof Mine || territory instanceof Totem){
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.CantBreakHere", true);
                e.setCancelled(true);
            }
            else if(territory instanceof GroupSubTerritory){
                //TODO CHECK IF PLAYER ROLE IS OK TO EDIT this group subterritory
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.CantBreakHere", true);
                e.setCancelled(true);
            }
        }
        else if(territory instanceof Land && !(player.isOp() || player.hasPermission("organisation.land.allowBreakBlock"))){

            Land land = (Land)territory;
            OrganisationPlayer organisationPlayer;

            try {
                organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
            } catch (OrganisationPlayerException ex) {
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.CantBreakOtherLand", true);
                e.setCancelled(true);
            }

            if(land.getOwner() == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.CantBreakOtherLand", true);
                e.setCancelled(true);
                return;
            }
            if(!land.isMainLand()){
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.CantBreakOtherLand", true);
                e.setCancelled(true);
                return;
            }
            if(!land.getOwner().getLocalPlayers().contains(player)) {
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.CantBreakOtherLand", true);
                e.setCancelled(true);
                return;
            }
            if(!land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.build.breakBlock)){

                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.NoPermission", true);
                e.setCancelled(true);
                return;

            }

            if(e.getBlock().getType().equals(Material.CHEST) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.openChest)
               || isWoodTrapDoor(e.getBlock().getType()) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.openWoodTrap)
               || e.getBlock().getType().equals(Material.IRON_TRAPDOOR) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.openIronTrap)
               || isWoodDoor(e.getBlock().getType()) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.openWoodDor)
               || e.getBlock().getType().equals(Material.IRON_DOOR) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.openIronDor)
               || e.getBlock().getType().equals(Material.LEVER) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.userLever)
               || isWoodButton(e.getBlock().getType()) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.useWoodButton)
               || e.getBlock().getType().equals(Material.STONE_BUTTON) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.useStoneButton)){

                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.NoPermission", true);
                e.setCancelled(true);
                return;

            }
        }

        PositionPointInterest positionPointInterest =  PositionPointInterest.getByLocation(locationBreakBlock);
        if(positionPointInterest != null){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.CantBreakPositionPointOfInterestBlock", true);
            player.sendMessage("§cPour casser ce bock effectuer d'abord la commande : \n§6/o position remove "+positionPointInterest.getId());
            e.setCancelled(true);
        }
    }
    @EventHandler public void onPlayerBlockExplosionEvent(BlockExplodeEvent e){


        if(!ConfigManager.getConfig().getBoolean("Organisation.Player.CanExplodeBlock")){
            e.setCancelled(true);
            return;
        }
        if(!Organisation.isBuildProtectionEnable)return;

        ArrayList<Block> lstBlockToKeep = new ArrayList<>();
        for(Block block : e.blockList()) {

            PositionPointInterest pointInterest = PositionPointInterest.getByLocation(block.getLocation());
            if(pointInterest != null){
                lstBlockToKeep.add(block);
                continue;
            }

            LandSubTerritory landSubTerritory = LandSubTerritory.getLandSubTerritoryByLocation(block.getLocation());
            if(landSubTerritory == null){
                lstBlockToKeep.add(block);
            }
            if(landSubTerritory instanceof RPSubTerritory ) {
                //|| landSubTerritory instanceof Mine || landSubTerritory instanceof Totem
                lstBlockToKeep.add(block);
            }
        }
    }
    @EventHandler public void onPlayerInteractEvent(PlayerInteractEvent e){


        if(e.getClickedBlock() == null || !Organisation.isInteractionProtectionEnable)return;
        Player player = e.getPlayer();

        Location location = e.getClickedBlock().getLocation();
        Land land = Land.getLandByLocation(location);
        LandSubTerritory landSubTerritory = LandSubTerritory.getLandSubTerritoryByLocation(location);

        //Bed respawn management
        if(e.getClickedBlock().getType().toString().endsWith("_BED") && e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
            e.getPlayer().setBedSpawnLocation(e.getClickedBlock().getLocation());
            GeneralMethods.sendPlayerMessage(e.getPlayer(),"Vous réapparaîtrez désormais dans ce lit.");
            e.setCancelled(true);
            return;
        }


        if(!(player.hasPermission("organisation.interact") || player.isOp())) {

            boolean isInteractableMaterial = isInteractableMaterial(e.getClickedBlock().getType()) || e.getClickedBlock().getType().equals(Material.PAINTING);

            if (land == null) {
                if(isInteractableMaterial){
                    GeneralMethods.sendPlayerErrorMessage(player,"Organisation.InteractEvent.NoPermission", true);
                    e.setCancelled(true);
                }
                return;
            }
            if (land.getOwner() == null) {
                if(isInteractableMaterial){
                    GeneralMethods.sendPlayerErrorMessage(player,"Organisation.InteractEvent.NoPermission", true);
                    e.setCancelled(true);
                }
                return;
            }

            if (!land.getOwner().getLocalPlayers().contains(player)) {
                if(isInteractableMaterial(e.getClickedBlock().getType())) {
                    GeneralMethods.sendPlayerErrorMessage(player,"Organisation.InteractEvent.NoPermission", true);
                    e.setCancelled(true);
                }
                return;
            }
        }

        // Mine management
        if(e.getClickedBlock().getType().equals(Material.CHEST)){

            PositionPointInterest positionPointInterest = PositionPointInterest.getByLocation(e.getInteractionPoint());
            if(positionPointInterest != null) {

                if (positionPointInterest.getContext().equals(PositionPointInterest.Type.MINE)
                        || positionPointInterest.getContext().equals(PositionPointInterest.Type.MINE_FIXE)) {

                    Inventory inventory = MineUtils.getInventoryOfMinePosition(positionPointInterest);
                    if (inventory == null) return;
                    if(!(player.hasPermission("organisation.interact") || player.isOp())){
                        if (land != null) {
                            if (land.getOwner() != null) {
                                if (!land.getOwner().getLocalPlayers().contains(player)) {
                                    GeneralMethods.sendPlayerErrorMessage(player,"Organisation.InteractEvent.NoPermission", true);
                                    e.setCancelled(true);
                                    return;
                                }
                                if (!land.getOwner().hasPermission(player, Permission.interact.getItemInMine)) {
                                    GeneralMethods.sendPlayerErrorMessage(player,"Organisation.InteractEvent.NoPermission", true);
                                    e.setCancelled(true);
                                    return;
                                }
                            }
                        }
                    }

                    e.setCancelled(true);
                    player.openInventory(inventory);
                    return;
                }
            }
        }

        if(landSubTerritory != null){
            return;
        }

        //Custom Door Management
        if(ItemAdderHook.isDoor(e.getClickedBlock())){
            ItemAdderHook.openDoor(e.getClickedBlock());
            return;
        }
        else if(e.getInteractionPoint() != null){

            Block block = e.getInteractionPoint().getBlock();
            if(ItemAdderHook.isDoor(block)){
                ItemAdderHook.openDoor(block);
                return;
            }
        }

        //Board management
        if(land != null && land.getOwner() != null && landSubTerritory == null && ItemAdderHook.isFactionBoard(e.getClickedBlock())){

            if(!(player.isOp() || player.hasPermission("organisation.board.canSeeOther")) && !GeneralMethods.getBooleanConf("Organisation.board.canSeeOther") && !land.getOwner().getLocalPlayers().contains(player)){
                e.setCancelled(true);
                return;
            }
            if(!(player.isOp() || player.hasPermission("organisation.widl.allowBreakBlock")) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){

                try {
                    OrganisationPlayer oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
                    e.getPlayer().openInventory(land.getOwner().getBoard(oPlayer));
                }
                catch (OrganisationPlayerException exception){};
                e.setCancelled(true);
            }
            return;

        }


       //Other interaction management
       if(e.getAction().equals(Action.LEFT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_AIR))return;

       if(landSubTerritory != null ){
           if(landSubTerritory instanceof RPSubTerritory){
               if(!isInteractableMaterial(e.getClickedBlock().getType()))return;
               if(e.getPlayer().hasPermission("organisation.interact") || e.getPlayer().isOp())return;
               if(isWoodDoor(e.getClickedBlock().getType()))return;
               e.setCancelled(true);
               GeneralMethods.sendPlayerErrorMessage(player, "Commands.NoPermission", true);
           }
       }

       if(!isInteractableMaterial(e.getClickedBlock().getType()))return;
       if(e.getPlayer().hasPermission("organisation.interact") || e.getPlayer().isOp())return;
       if( isChest(e.getClickedBlock().getType()) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.openChest)
                || isFurnace(e.getClickedBlock().getType()) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.openFurnace)
                || isWoodTrapDoor(e.getClickedBlock().getType()) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.openWoodTrap)
                || e.getClickedBlock().getType().equals(Material.IRON_TRAPDOOR) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.openIronTrap)
                || isWoodDoor(e.getClickedBlock().getType()) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.openWoodDor)
                || e.getClickedBlock().getType().equals(Material.IRON_DOOR) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.openIronDor)
                || e.getClickedBlock().getType().equals(Material.LEVER) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.userLever)
                || isWoodButton(e.getClickedBlock().getType()) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.useWoodButton)
                || e.getClickedBlock().getType().equals(Material.STONE_BUTTON) && !land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.useStoneButton)){

            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.InteractEvent.NoPermission", true);
            e.setCancelled(true);
            return;
       }
    }

    @EventHandler public void onPlayerInventoryCloseEvent(InventoryCloseEvent e){

        //Gestion des UI
        if(InventoryUI.manageEvent(e))return;

        // Sauvegarde des inventaires (mode éditions)
        if(!e.getPlayer().hasPermission("organisation.command.inventory"))return;
        if(!e.getReason().equals(InventoryCloseEvent.Reason.PLAYER) && !e.getReason().equals(InventoryCloseEvent.Reason.OPEN_NEW))return;

        Inventory inventory = e.getInventory();
        StoredInventory storedInventory = StoredInventory.getStoredInventoryByInventory(inventory);

        if(storedInventory == null)return;

        storedInventory.save();
        if(e.getPlayer() instanceof OfflinePlayer) GeneralMethods.sendPlayerMessage((OfflinePlayer) e.getPlayer(),"Commands.Inventory.Save",true);
    }
    @EventHandler public void onPlayerInventoryClickEvent(InventoryClickEvent e){

        //Gestion des UI
        if(InventoryUI.manageEvent(e))return;

        //Vérification des mines
        Land land = MineUtils.getLandFromInventory(e.getClickedInventory());
        if(land == null)return;

        if(e.getWhoClicked().isOp() || e.getWhoClicked().hasPermission("organisation.land.byPassMineProtection"))return;
        if(!(e.getWhoClicked() instanceof Player))return;

        Player player = ((Player) e.getWhoClicked());
        OrganisationPlayer organisationPlayer;

        try {
            organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException ex) {
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.CantBreakOtherLand", true);
            e.setCancelled(true);
        }

        if(land.getOwner() == null){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.Mine.CantRecupItem", true);
            e.setCancelled(true);
            return;
        }
        if(!land.getOwner().getLocalPlayers().contains(player)){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.Mine.CantInteract", true);
            e.setCancelled(true);
            return;
        }
        if(!land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.getItemInMine) && ConfigManager.getConfig().getBoolean("Organisation.Player.CanDefineIfGroupMemberCantOpenMine")){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.Mine.NoPermission", true);
            e.setCancelled(true);
        }
    }
    @EventHandler public void onPlayerInventoryDragEvent(InventoryDragEvent e){

        //Gestion des UI
        if(InventoryUI.manageEvent(e))return;

        //Verification des mines
        Land land = MineUtils.getLandFromInventory(e.getInventory());
        if(land == null)return;
        if(e.getWhoClicked().isOp() || e.getWhoClicked().hasPermission("organisation.mine.canPlaceItem")){
            return;
        }

        Player player = ((Player) e.getWhoClicked());
        OrganisationPlayer organisationPlayer;

        try {
            organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException ex) {
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.CantBreakOtherLand", true);
            e.setCancelled(true);
        }

        if(land.getOwner() == null){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.Mine.CantRecupItem", true);
            e.setCancelled(true);
            return;
        }
        if(!land.isMainLand()){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.Mine.CantRecupItem", true);
            e.setCancelled(true);
            return;
        }
        if(!land.getOwner().getLocalPlayers().contains(player)){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.Mine.CantInteract", true);
            e.setCancelled(true);
            return;
        }
        if(!land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.interact.getItemInMine)){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.Mine.NoPermission", true);
            e.setCancelled(true);
        }
    }

    @EventHandler public void onPlayerCreatePortalEvent(PortalCreateEvent e){

        if(!(e.getEntity() instanceof Player) || ConfigManager.getConfig().getBoolean("Organisation.Player.CanCreatePortal"))return;

        Player player = (Player) e.getEntity();
        if(player.isOp() || player.hasPermission("organisation.canPlayerCreatePortal"))return;

        e.setCancelled(true);
    }
    @EventHandler public void onPlayerPlaceBlockEvent(BlockPlaceEvent e){

        if(!Organisation.isBuildProtectionEnable)return;

        Player player = e.getPlayer();
        Block placeBlock = e.getBlock();
        Location locationPlaceBlock = placeBlock.getLocation();

        Territory territory = LandSubTerritory.getLandSubTerritoryByLocation(locationPlaceBlock);

        if(territory == null)territory = Land.getLandByLocation(locationPlaceBlock);
        if(territory == null && !(player.isOp() || player.hasPermission("organisation.widl.allowPlaceBlock"))) {
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.PlaceBlockEvent.CantPlaceHere", true);
            e.setCancelled(true);
        }

        if(territory instanceof LandSubTerritory && !(player.isOp() || player.hasPermission("organisation.landSubTerritory.allowPlaceBlock"))){

            if(territory instanceof RPSubTerritory || territory instanceof Mine || territory instanceof Totem){
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.PlaceBlockEvent.CantPlaceHere", true);
                e.setCancelled(true);
            }
            else if(territory instanceof GroupSubTerritory){
                //TODO CHECK IF PLAYER ROLE IS OK TO EDIT this group subterritory
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.PlaceBlockEvent.CantPlaceHere", true);
                e.setCancelled(true);
            }
        }
        else if(territory instanceof Land && !(player.isOp() || player.hasPermission("organisation.land.allowPlaceBlock"))){

            Land land = (Land)territory;
            OrganisationPlayer organisationPlayer;

            try {
                organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
            } catch (OrganisationPlayerException ex) {
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.PlaceBlockEvent.CantPlaceOtherLand", true);
                e.setCancelled(true);
            }

            if(land.getOwner() == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.PlaceBlockEvent.CantPlaceOtherLand", true);
                e.setCancelled(true);
                return;
            }
            if(!land.isMainLand()){
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.PlaceBlockEvent.CantPlaceOtherLand", true);
                e.setCancelled(true);
                return;
            }
            if(!land.getOwner().getLocalPlayers().contains(player)) {
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.PlaceBlockEvent.CantPlaceOtherLand", true);
                e.setCancelled(true);
                return;
            }
            if(!land.getOwner().getOrganisationMember(player).getRole().hasPermission(Permission.build.placeBlock)){

                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.PlaceBlockEvent.NoPermission", true);
                e.setCancelled(true);
                return;

            }
        }

        PositionPointInterest positionPointInterest =  PositionPointInterest.getByLocation(locationPlaceBlock);
        if(positionPointInterest != null){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.BreakBlockEvent.CantBreakPositionPointOfInterestBlock", true);
            player.sendMessage("§cPour casser ce bock effectuer d'abord la commande : \n§6/o position remove "+positionPointInterest.getId());
            e.setCancelled(true);
        }
    }

    @EventHandler public void onPlayerDropItemEvent(PlayerDropItemEvent e){

        OrganisationPlayer oPlayer;
        Player player = e.getPlayer();
        if(CitizensAPI.getNPCRegistry().getNPC(player) != null)return;

        try {
            oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException exception) {
            return;
        }

        if(oPlayer.isInPrespawnMod() && !(player.isOp() || player.hasPermission("organisation.prespawn.admin"))){
            e.setCancelled(true);
            return;
        }
    }
    @EventHandler public void onFoodLevelChangeEvent(FoodLevelChangeEvent e){

        Player player;
        OrganisationPlayer oPlayer;

        if(!(e.getEntity() instanceof Player))return;
        player = (Player) e.getEntity();

        if(CitizensAPI.getNPCRegistry().getNPC(player) != null)return;
        try {
            oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException exception) {
            return;
        }

        if(oPlayer.isInPrespawnMod()){
            // Au prespawn, on ne perd pas de nouriture
            e.setCancelled(true);
            return;
        }
    }
    @EventHandler public void onPlayerPotionEvent(EntityPotionEffectEvent e) {

        if (!(e.getEntity() instanceof Player)) return;

        Player player;
        OrganisationPlayer oPlayer;
        player = (Player) e.getEntity();

        if(CitizensAPI.getNPCRegistry().getNPC(player) != null)return;
        try {
            oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException exception) {
            return;
        }
    }

    private boolean isInteractableMaterial(Material material){

        return  isChest(material) ||
                isFurnace(material) ||
                isWoodDoor(material) ||
                isWoodButton(material) ||
                isWoodTrapDoor(material) ||
                material.equals(Material.LEVER) ||
                material.equals(Material.IRON_DOOR) ||
                material.equals(Material.STONE_BUTTON) ||
                material.equals(Material.IRON_TRAPDOOR);
    }
    private boolean isWoodButton(Material material){
        if(material.equals(Material.STONE_BUTTON))return false;
        if(material.name().toUpperCase().contains("_BUTTON"))return true;
        return false;
    }
    private boolean isWoodDoor(Material material){
        if(material.equals(Material.IRON_DOOR))return false;
        if(material.name().toUpperCase().contains("_DOOR"))return true;
        return false;
    }
    private boolean isWoodTrapDoor(Material material){

        if(material.equals(Material.IRON_TRAPDOOR))return false;
        if(material.name().toUpperCase().contains("_TRAPDOOR"))return true;
        return false;
    }
    private boolean isChest(Material material){

        if(material.equals(Material.CHEST))return true;
        else if(material.equals(Material.CHEST_MINECART))return true;
        else if(material.equals(Material.TRAPPED_CHEST))return true;
        else if(material.equals(Material.BARREL))return true;
        else if(material.equals(Material.SHULKER_BOX))return true;
        else if(material.name().toUpperCase().contains("_SHULKER_BOX"))return true;

        return false;
    }

    private boolean isFurnace(Material material){
        if(material.equals(Material.FURNACE))return true;
        else if(material.equals(Material.FURNACE_MINECART))return true;
        else if(material.equals(Material.BLAST_FURNACE))return true;
        return false;
    }
}
