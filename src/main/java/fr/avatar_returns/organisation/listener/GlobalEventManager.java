package fr.avatar_returns.organisation.listener;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.hook.AvatarRoadsHook;
import fr.avatar_returns.organisation.manager.SecondeTaskManager;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.relation.Raid;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import net.citizensnpcs.api.CitizensAPI;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import fr.avatar_returns.organisation.rabbitMQPackets.player.SimpleChatPacket;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class GlobalEventManager implements Listener {

    public static ConcurrentHashMap<OrganisationPlayer, Long> mapOPlayerLeaving = new ConcurrentHashMap<>();

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true) public void onJoin(final  PlayerJoinEvent event){

        Player player = event.getPlayer();
        if(CitizensAPI.getNPCRegistry().getNPC(player) != null)return;

        OrganisationPlayer organisationPlayer;
        final long currentTime = System.currentTimeMillis();

       try{
           organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
           organisationPlayer.setHead(SkullCreator.getPlayerHeadItem(organisationPlayer.getUuid()));
           organisationPlayer.setLastTimeConnexion(currentTime);
           organisationPlayer.setLastSeenName(player.getName());
           organisationPlayer.setIsConnected(true);

           if(organisationPlayer.isInFightMod() && AvatarRoadsHook.isRoadEnableForPlayer(player)){
               if(Organisation.getHook(AvatarRoadsHook.class).isPresent()) {
                   Organisation.getHook(AvatarRoadsHook.class).get().disableRoadsForPlayer(player);
               }
           } else if (!organisationPlayer.isInFightMod()) {
               if(Organisation.getHook(AvatarRoadsHook.class).isPresent()) {
                   Organisation.getHook(AvatarRoadsHook.class).get().enableRoadsForPlayer(player);
               }
               else{
                   Organisation.log.severe("---> NO HOOK FOR ROAD");
               }
           }

           if(!OrganisationPlayer.getOrganisationPlayerByName(player.getName()).equals(organisationPlayer)){
               // Si un ancien joueur avait ce nom. Alors, on le modifie et on le log
               OrganisationPlayer.getOrganisationPlayerByName(player.getName()).setLastSeenName("*");
               Organisation.log.warning("Le pseudo " + player.getName() + " est enregistré comme le nom d'un ancien joueur... Modification du dernier pseudo pu pour l'ancien joueur");
           }
       }
       catch (OrganisationPlayerException e){
            organisationPlayer = new OrganisationPlayer(player);
            Organisation.log.info("Le joueur "+player.getName()+" a été ajouté a la liste des organisationPlayers");
       }

       organisationPlayer.manageVisiblePlayer();

        ArrayList<Invitation> lstInvitation = InviteCommand.getAllInvitation(organisationPlayer.getOfflinePlayer());
        if(!lstInvitation.isEmpty()){
            GeneralMethods.sendPlayerMessage(player,"Vous avez "+lstInvitation.size()+" invitation(s) en attente");
            for(Invitation invitation: lstInvitation){
                player.sendMessage(" - "+invitation.getGroupHost().getName());
            }
            player.sendMessage("");
        }

        for(Group group : organisationPlayer.getLstGroup()){
            String prefix = (group instanceof LargeGroup)?"§2" + group.getName() + "§f : " : "§3"+group.getName()+"§f ";
            String groupConnexionMessage = group.getConnexionMessage();
            if(!groupConnexionMessage.isEmpty())GeneralMethods.sendPlayerMessage(player, prefix+groupConnexionMessage);

            OrganisationMember organisationMember = group.getOrganisationMember(organisationPlayer);
            if(organisationMember != null)organisationMember.setLastConnexionTime();
        }

        if(organisationPlayer.getLstGroup().isEmpty())Organisation.getDefaultTeam().addPlayer(player);
//        if(Organisation.getHook(ProjectKorraHook.class).isPresent()){
//            for (Raid raid: Raid.getLstRunningRaid()){
//                if(raid.getTarget().getLstGroupConnectedOrganisationPlayer().contains(organisationPlayer)){
//                    Organisation.getHook(ProjectKorraHook.class).get().disableActionBar(organisationPlayer);
//                    break;
//                }
//            }
//        }
        LandPlayerEventManager.updatePlayerBossBar(player);

        for(Group group : Group.getLstGroup()){
            if(group.getLocalPlayers().contains(player) && !group.getTeam().hasPlayer(player)){
                group.getTeam().addEntity(player);
            }
        }

        if(organisationPlayer.getLstGroup().isEmpty()){
            if(Organisation.getDefaultTeam().hasPlayer(player)) Organisation.getDefaultTeam().addPlayer(player);
        }
    }
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true) public void onLeave(final PlayerQuitEvent event){

        Player player = event.getPlayer();
        OrganisationPlayer organisationPlayer;
        try{
            organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
            DBUtils.setOrganisationPlayerEffectivePlayedTime(organisationPlayer);
            organisationPlayer.setLastServerSeenName(GeneralMethods.getServerId());
            mapOPlayerLeaving.put(organisationPlayer, System.currentTimeMillis() + 15000);
            //organisationPlayer.updateLastStrElementSeen();

            // On autorise de nouveau le joueur a voir les actions bar.
            // if(Organisation.getHook(ProjectKorraHook.class).isPresent()) Organisation.getHook(ProjectKorraHook.class).get().enableActionBar(organisationPlayer);

            for(Group group: organisationPlayer.getLstGroup()){
                OrganisationMember organisationMember = group.getOrganisationMember(organisationPlayer.getOfflinePlayer());
                DBUtils.setGroupMemberEffectivePlayedTime(organisationMember);
            }

            //On retire les demandes local de téléportations
            if(SecondeTaskManager.mapPlayerToTPFHome.containsKey(organisationPlayer)){
                SecondeTaskManager.mapPlayerToTPFHome.remove(organisationPlayer);
                SecondeTaskManager.mapPlayerToTPFHomeVland.remove(organisationPlayer);
            }
            if(SecondeTaskManager.mapPlayerToTPSpawn.containsKey(organisationPlayer)){
                SecondeTaskManager.mapPlayerToTPSpawn.remove(organisationPlayer);
            }

            if(organisationPlayer.isInFightMod()){
                // Si le joueur est en mode combat, on l'achève
                organisationPlayer.setLastTimeFightMod(-1);
                if(event.getReason().equals(PlayerQuitEvent.QuitReason.DISCONNECTED) || event.getReason().equals(PlayerQuitEvent.QuitReason.TIMED_OUT)) {
                    player.setHealth(0);
                }
            }
            DBUtils.setOrganisationLastTimeFightMod(organisationPlayer);
        }
        catch (OrganisationPlayerException e){
        }

        // On retire la boss bar du joueur
        if (LandPlayerEventManager.mapPlayerBossBar.containsKey(player)){
            LandPlayerEventManager.mapPlayerBossBar.get(player).removeAll();
            LandPlayerEventManager.mapPlayerBossBar.remove(player);
        }
        if (LandPlayerEventManager.mapPlayerPriorBossBarMessage.containsKey(player)){
            LandPlayerEventManager.mapPlayerPriorBossBarMessage.remove(player);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true) public void onPlayerDeathEvent(PlayerDeathEvent e){

        String deathMessage = e.getDeathMessage();
        for(OrganisationPlayer oPlayer: OrganisationPlayer.getLstConnectedOrganisationPlayer()){
            GeneralMethods.sendPacket(new SimpleChatPacket(oPlayer, deathMessage));
        }
        try {

            OrganisationPlayer oPlayer = OrganisationPlayer.getOrganisationPlayer(e.getPlayer());
            oPlayer.incNbDeath();
        }
        catch (OrganisationPlayerException exception){};
    }
    @EventHandler public void onPlayerKillOtherEvent(EntityDeathEvent e){

        Player killer = e.getEntity().getKiller();
        LivingEntity mobs = e.getEntity();

        if(killer == null)return;
        try {

            OrganisationPlayer oKiller = OrganisationPlayer.getOrganisationPlayer(killer);

            if(mobs instanceof Player){
                if(CitizensAPI.getNPCRegistry().getNPC(killer) != null)return;
                oKiller.incPlayerKill();
            }
            else if(mobs instanceof Mob){ oKiller.incMobsKill();}
        }
        catch (OrganisationPlayerException exception){};
    }
    @EventHandler public void onPlayerDeathEvent(PlayerRespawnEvent e){

        Player player = e.getPlayer();

        for(Raid raid : Raid.getLstRunningRaid()){
            if(raid.canPlayerRespawnInLand(player)){
                Location location = raid.getBestPlayerLocationPoint(player);
                if(location != null)e.setRespawnLocation(location);
            }
        }
        try {
            OrganisationPlayer oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
            oPlayer.setLastTimeFightMod(-1);
        }
        catch (OrganisationPlayerException exception){};
    }
    @EventHandler public void onPlayerRespawnEvent(PlayerRespawnEvent e){

        if(GeneralMethods.getBooleanConf("Organisation.Spawn.Config.EnableRespawnOnOtherServer")) {
            if (e.getPlayer().getBedSpawnLocation() == null) {
                GeneralMethods.teleportSpawnServer(e.getPlayer());
            }
        }
    }
}
