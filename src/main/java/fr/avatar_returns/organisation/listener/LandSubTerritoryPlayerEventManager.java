package fr.avatar_returns.organisation.listener;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Contest;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.RP;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Totem;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.messages.Title;
import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;

public class LandSubTerritoryPlayerEventManager implements Listener {

    private HashMap<OfflinePlayer, HashMap<LandSubTerritory, Long>> mapLastDisplayClaimNamePlayer= new HashMap<>();

    @EventHandler public void onPlayerChangePositioneEvent(PlayerMoveEvent e) {

        if (!e.hasChangedPosition()) return;

        Player player = e.getPlayer();
        LandSubTerritory currentPlayerLandSubTerritory = LandSubTerritory.getLandSubTerritoryByLocation(e.getTo());

        if (currentPlayerLandSubTerritory == null) return;

        if(Organisation.isLandDisplay) {
            if (!mapLastDisplayClaimNamePlayer.containsKey(player)) {
                mapLastDisplayClaimNamePlayer.put(player, new HashMap<LandSubTerritory, Long>());
            }

            long currentTimeStamp = System.currentTimeMillis();


            if (!mapLastDisplayClaimNamePlayer.get(player).containsKey(currentPlayerLandSubTerritory)) {
                mapLastDisplayClaimNamePlayer.get(player).put(currentPlayerLandSubTerritory, currentTimeStamp);
            }

            long previousTimeStamp = mapLastDisplayClaimNamePlayer.get(player).get(currentPlayerLandSubTerritory);

            long lastTimeMoveInLand = currentTimeStamp - previousTimeStamp;
            if (lastTimeMoveInLand > (30 * 1000) || lastTimeMoveInLand == 0L) {

                String landSubTerritoryName = currentPlayerLandSubTerritory.getName();
                if (landSubTerritoryName.equalsIgnoreCase("")) {

                    if (currentPlayerLandSubTerritory instanceof Mine) landSubTerritoryName = "Mine";
                    else if (currentPlayerLandSubTerritory instanceof Totem) landSubTerritoryName = "Totem";

                }

                if (!landSubTerritoryName.equalsIgnoreCase("")) {
                    String canOtherGoIn = (currentPlayerLandSubTerritory.canOtherGoIn()) ? "§2(Ouvert)" : "§c(Fermé)";
                    String color = (currentPlayerLandSubTerritory instanceof Contest)? "§c" : "§a";
                    Title.sendFullTitle(player, 10, 50, 10, color + landSubTerritoryName, canOtherGoIn);
                }
            }

            mapLastDisplayClaimNamePlayer.get(player).put(currentPlayerLandSubTerritory, currentTimeStamp - 2);
        }
        if(player.isOp() || player.hasPermission("organisation.admin"))return;

        LargeGroup largeGroup = currentPlayerLandSubTerritory.getLand().getOwner();
        if (largeGroup == null) {
            if(!(player.isOp() || player.hasPermission("organisation.land.netherChangeGameMode") || GeneralMethods.getBooleanConf("Organisation.Disable.GamemodeAutoChange")))player.setGameMode(GameMode.ADVENTURE);
        }
        else if(!(player.isOp() || player.hasPermission("organisation.land.netherChangeGameMode") || GeneralMethods.getBooleanConf("Organisation.Disable.GamemodeAutoChange"))) {
            if (!largeGroup.getLocalPlayers().contains(player)) player.setGameMode(GameMode.ADVENTURE);
            else player.setGameMode(GameMode.SURVIVAL);
        }

        if(!currentPlayerLandSubTerritory.canOtherGoIn() && !(player.isOp() || player.hasPermission("organisation.landSubTerritory.bypassCanOtherGoIn"))){
            if(currentPlayerLandSubTerritory.getLand().getOwner() != null){
                if(!currentPlayerLandSubTerritory.getLand().getOwner().getLocalPlayers().contains(player)){
                    e.setCancelled(true);
                    GeneralMethods.sendPlayerErrorMessage(player, "Vous ne pouvez pas entrer dans cette zone.");
                }
            }
            else{
                e.setCancelled(true);
                GeneralMethods.sendPlayerErrorMessage(player, "Vous ne pouvez pas entrer dans cette zone.");
            }
        }

        if(currentPlayerLandSubTerritory instanceof RP || currentPlayerLandSubTerritory instanceof Mine){

            OrganisationPlayer oPlayer = null;
            try {
                oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
            } catch (OrganisationPlayerException exception) {
                return;
            }

            if(oPlayer.isInFightMod() && !(oPlayer.hasPermission("organisation.fightmode.skip"))){
                e.setCancelled(true);
                GeneralMethods.sendPlayerErrorMessage(player, "Vous ne pouvez pas entrer dans cette zone pendant un combat.");
            }
        }
    }
}
