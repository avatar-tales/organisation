package fr.avatar_returns.organisation.listener;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.manager.SecondeTaskManager;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.relation.Raid;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.Province;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Contest;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.RP;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Totem;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.messages.Title;
import net.citizensnpcs.api.CitizensAPI;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.HashMap;

public class LandPlayerEventManager implements Listener {

    public static HashMap<Player, BossBar> mapPlayerBossBar = new HashMap<>();
    public static HashMap<Player, String> mapPlayerPriorBossBarMessage = new HashMap<>();

    private HashMap<OfflinePlayer,HashMap<Land, Long>> mapLastDisplayClaimNamePlayer= new HashMap<>();
    private HashMap<OfflinePlayer,Province> mapPreviousProvince = new HashMap<>();

    @EventHandler public void onPlayerChangePositioneEvent(PlayerMoveEvent e){

        if (!e.hasChangedPosition()) return;


        Player player = e.getPlayer();
        Land currentPlayerLand = Land.getLandByLocation(e.getTo());
        Province currentProvince = (currentPlayerLand != null) ? currentPlayerLand.getProvince() : null;

        OrganisationPlayer oPlayer = null;
        try{
            oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
            if(!oPlayer.isConnected())oPlayer.setIsConnected(true); // Un joueur ne peut pas bouger s'il est déconnecté...
        }
        catch (OrganisationPlayerException exception){
            return;
        }

        if(Organisation.isLandDisplay) {
            if (!mapLastDisplayClaimNamePlayer.containsKey(player)) {
                mapLastDisplayClaimNamePlayer.put(player, new HashMap<Land, Long>());
            }

            long currentTimeStamp = System.currentTimeMillis();

            if (!mapLastDisplayClaimNamePlayer.get(player).containsKey(currentPlayerLand)) {
                mapLastDisplayClaimNamePlayer.get(player).put(currentPlayerLand, currentTimeStamp);
            }
            if (!mapPreviousProvince.containsKey(player)) mapPreviousProvince.put(player, null);

            long previousTimeStamp = mapLastDisplayClaimNamePlayer.get(player).get(currentPlayerLand);

            long lastTimeMoveInLand = currentTimeStamp - previousTimeStamp;
            if (lastTimeMoveInLand > (30 * 1000) || lastTimeMoveInLand == 0L) {

                if (currentProvince != mapPreviousProvince.get(player)) {

                    String title = (currentProvince == null) ? "§2Monde" : currentProvince.getName();
                    String subTitle = (currentProvince == null) ? "§2Une terre inconnue..." : "";

                    if (currentPlayerLand != null) {
                        String available = (currentPlayerLand.isLandReady()) ? "§2(Disponible)" : "§c(Travaux)";
                        String owner = (currentPlayerLand.getOwner() == null) ? available : "§7(" + currentPlayerLand.getOwner().getName() + ")";
                        if (currentPlayerLand.isMainLand()) {
                            owner = "§7(§6۩ §7" + currentPlayerLand.getOwner().getName() + ")";
                        }
                        subTitle = currentPlayerLand.getName() + " " + owner;
                    }

                    if (currentProvince != null || GeneralMethods.getBooleanConf("Organisation.Player.DisplayWorldWideTitle")) {
                        Title.sendFullTitle(player, 10, 50, 10, "§6" + title.replace("_", " "), "§e" + subTitle.replace("_", " "));
                    }

                } else if (currentPlayerLand != null) {
                    String landName = (currentPlayerLand.getName().length() > 0) ? currentPlayerLand.getName() : currentPlayerLand.getWorldGuardRegion().getId();
                    String available = (currentPlayerLand.isLandReady()) ? "§2(Disponible)" : "§c(Travaux)";
                    String owner = (currentPlayerLand.getOwner() == null) ? available : "§7(" + currentPlayerLand.getOwner().getName() + ")";

                    if (currentPlayerLand.isMainLand()) {
                        owner = "§7(§6۩ §7" + currentPlayerLand.getOwner().getName() + ")";
                    }

                    Title.sendFullTitle(player, 10, 50, 10, "§e" + landName.replace("_", " "), owner.replace("_", " "));
                }
                String province = (currentProvince == null) ? "" : "Province : " + currentProvince.getName().replace("_", " ") + "\n";
                String land = (currentPlayerLand == null) ? "" : "§eLand : " + currentPlayerLand.getName().replace("_", " ") + "\n";
                String available = (currentPlayerLand == null) ? "" : (currentPlayerLand.isLandReady()) ? "§2(Disponible)" : "§c(Travaux)";
                String owner = (currentPlayerLand == null) ? "" : (currentPlayerLand.getOwner() == null) ? "§7Proprietaire : " + available : "§7Proprietaire : " + currentPlayerLand.getOwner().getName().replace("_", " ") + "\n";

                if (currentPlayerLand != null) {
                    if (currentPlayerLand.isMainLand()) {
                        owner = "§7Proprietaire : §6۩ §7" + currentPlayerLand.getOwner().getName().replace("_", " ") + "\n";
                    }
                }

                if ((province + land + owner).length() > 0)
                    GeneralMethods.sendPlayerMessage(player, "\n" + province + land + owner);
            }

            mapLastDisplayClaimNamePlayer.get(player).put(currentPlayerLand, currentTimeStamp - 2);
            mapPreviousProvince.put(player, currentProvince);

            LandPlayerEventManager.updatePlayerBossBar(player);
        }

        if(SecondeTaskManager.mapPlayerToTPSpawn.containsKey(oPlayer) && !(player.isOp() || player.hasPermission("organisation.command.spawn.admin"))){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Spawn.Teleportation.MoveCancel", true);
            SecondeTaskManager.mapPlayerToTPSpawn.remove(oPlayer);
        }
        if(SecondeTaskManager.mapPlayerToTPFHome.containsKey(oPlayer) && !(player.isOp() || player.hasPermission("organisaton.command.home.admin"))){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Home.Teleportation.MoveCancel", true);
            SecondeTaskManager.mapPlayerToTPFHome.remove(oPlayer);
            SecondeTaskManager.mapPlayerToTPFHomeVland.remove(oPlayer);
        }

        if (currentPlayerLand == null || currentProvince == null) {
            if(!(player.isOp() || player.hasPermission("organisation.land.netherChangeGameMode") || GeneralMethods.getBooleanConf("Organisation.Disable.GamemodeAutoChange"))){
                player.setGameMode(GameMode.ADVENTURE);
            }
        }
        else {

            if(!currentPlayerLand.canOtherGoIn() && !(player.isOp() || player.hasPermission("organisation.land.bypassCanOtherGoIn"))){
                if(currentPlayerLand.getOwner() != null){
                    if(!currentPlayerLand.getOwner().getLocalPlayers().contains(player)){
                        e.setCancelled(true);
                        GeneralMethods.sendPlayerErrorMessage(player, "Vous ne pouvez pas entrer dans ce Land.");
                    }
                }
                else if(!currentPlayerLand.isLandReady() || !currentPlayerLand.canOtherGoIn()){
                    e.setCancelled(true);
                    GeneralMethods.sendPlayerErrorMessage(player, "Vous ne pouvez pas entrer dans ce Land (il est en construction)");
                }
            }

            LargeGroup largeGroup = currentPlayerLand.getOwner();
            if (largeGroup == null) {
                if (!(player.isOp() || player.hasPermission("organisation.land.netherChangeGameMode") || GeneralMethods.getBooleanConf("Organisation.Disable.GamemodeAutoChange"))) {
                    player.setGameMode(GameMode.ADVENTURE);
                }
            }
            else {

                if (!largeGroup.getLocalPlayers().contains(player)) {
                    if (!(player.isOp() || player.hasPermission("organisation.land.netherChangeGameMode") || GeneralMethods.getBooleanConf("Organisation.Disable.GamemodeAutoChange")))
                        player.setGameMode(GameMode.ADVENTURE);
                } else if (!(player.isOp() || player.hasPermission("organisation.land.netherChangeGameMode") || GeneralMethods.getBooleanConf("Organisation.Disable.GamemodeAutoChange")))
                    player.setGameMode(GameMode.SURVIVAL);
            }
        }
    }
    @EventHandler public void onPlayerDammageByPlayerEvent(EntityDamageByEntityEvent e){


        Player sourcePlayer = null;
        Player targetPlayer = null;
        Entity sourceEntity = null;
        if(e.getDamager() instanceof Arrow) sourceEntity = (Entity) ((Arrow) e.getDamager()).getShooter();
        else sourceEntity = e.getDamager();

        if(sourceEntity instanceof Player)sourcePlayer = (Player) sourceEntity;
        else return;

        if(e.getEntity() instanceof Player) targetPlayer = (Player) e.getEntity();
        else return;

        Land currentTargetLand = Land.getLandByLocation(targetPlayer.getLocation());
        LandSubTerritory landSubTerritory = LandSubTerritory.getLandSubTerritoryByLocation(targetPlayer.getLocation());
        boolean isInContestArea = false;

        if(landSubTerritory != null){
            if(landSubTerritory instanceof Contest)isInContestArea = true;
            else if(landSubTerritory instanceof Mine || landSubTerritory instanceof RP){
                // Si le joueur visé est dans une safe zone, aucun dégat ne lui ai appliqué.
                e.setCancelled(true);
                return;
            }
        }

        // On récupère les joueurs organisations.
        OrganisationPlayer sourceOPlayer;
        OrganisationPlayer targetOPlayer;

        try {
            sourceOPlayer = OrganisationPlayer.getOrganisationPlayer(sourcePlayer);
        } catch (OrganisationPlayerException exception) {
            return;
        }
        try {
            targetOPlayer = OrganisationPlayer.getOrganisationPlayer(targetPlayer);
        } catch (OrganisationPlayerException exception) {
            return;
        }

        // Cancel damage if in prespawn mod
        if(targetOPlayer.isInPrespawnMod()){
            // Au prespawn, on ne prend pas de dégats
            e.setCancelled(true);
            return;
        }
        if (!GeneralMethods.getBooleanConf("Organisation.Faction.AllowFriendlyFire")) {
            for(LargeGroup largeGroup : sourceOPlayer.getLstLargeGroup()){
                if(largeGroup.getLocalPlayers().contains(targetPlayer)){
                    e.setCancelled(true);
                    return;
                }
            }
        }

        // Update FightMode END
        if(GeneralMethods.getBooleanConf("Organisation.FightMode.IsEnabled")) {
            if (!sourceOPlayer.isInFightMod() && !(sourcePlayer.isOp() || sourcePlayer.hasPermission("organisation.fightmode.skip"))) {
                GeneralMethods.sendPlayerMessage(sourceOPlayer, "Organisation.FightMod.Alert.Start", true);
                SecondeTaskManager.playerInFightMode.add(sourceOPlayer);

            }
            if (!targetOPlayer.isInFightMod() && !(targetPlayer.isOp() || targetPlayer.hasPermission("organisation.fightmode.skip"))) {
                GeneralMethods.sendPlayerMessage(targetOPlayer, "Organisation.FightMod.Alert.Start", true);
                SecondeTaskManager.playerInFightMode.add(targetOPlayer);
            }

            sourceOPlayer.setLastTimeFightMod(System.currentTimeMillis());
            targetOPlayer.setLastTimeFightMod(System.currentTimeMillis());
        }

        if(currentTargetLand == null) return;
        if(currentTargetLand.getOwner() == null)  return;
        else if(sourceOPlayer.getLstLargeGroup().isEmpty() && currentTargetLand.isMainLand()){
            // Player without LargeGroup/Faction can't fight in mainLand
            e.setCancelled(true);
            return;
        }
        else if (!currentTargetLand.getOwner().getLocalPlayers().contains(targetPlayer)) return;

       double reduction = GeneralMethods.getDoubleConf("Organisation.LargeGroup.DamageReduction.value");
       if (reduction > 1D) {
            e.setCancelled(true);
            return;
        }

       if(!isInContestArea) {
           // Si le joueur target est dans une zone contest (Zone avec des mobs, des auberges, ...) alors aucune réduction de dégats n'est appliquée
           e.setDamage(e.getDamage() * (1 - reduction));
           GeneralMethods.sendPlayerMessage(targetPlayer, "Organisation.LargeGroup.DamageReduction.message", true);
       }
    }

    public static void initPlayerBossBar(Player player){

        if (!LandPlayerEventManager.mapPlayerBossBar.containsKey(player)){

            BossBar bossBar = Bukkit.createBossBar(LandPlayerEventManager.getPlayerLandBossBarString(player), BarColor.YELLOW, BarStyle.SOLID);
            bossBar.addPlayer(player);
            bossBar.setVisible(true);

            LandPlayerEventManager.mapPlayerBossBar.put(player, bossBar);
        }

        if(!LandPlayerEventManager.mapPlayerPriorBossBarMessage.containsKey(player)){
            LandPlayerEventManager.mapPlayerPriorBossBarMessage.put(player, "");
        }
    }
    public static void updatePlayerBossBar(Player player){

        if(!Organisation.isLandDisplay)return;

        LandPlayerEventManager.initPlayerBossBar(player);

        BossBar bossBar = LandPlayerEventManager.mapPlayerBossBar.get(player);
        String barTxt = LandPlayerEventManager.mapPlayerPriorBossBarMessage.get(player);
        if (barTxt == "") barTxt = LandPlayerEventManager.getPlayerLandBossBarString(player);

        if(barTxt.equals("§2Monde"))bossBar.setColor(BarColor.GREEN);
        else if(barTxt.contains("§c"))bossBar.setColor(BarColor.RED);

        else bossBar.setColor(BarColor.YELLOW);

        bossBar.setTitle(barTxt);
    }
    public static String getPlayerLandBossBarString(Player player){

        Location location = player.getLocation();
        Land land = Land.getLandByLocation(location);
        if(land == null)return "§2Monde";

        Raid raid = Raid.getRaidByTargetLand(land);
        boolean isRaid = false;

        OrganisationPlayer oPlayer = null;
        try{oPlayer = OrganisationPlayer.getOrganisationPlayer(player);}
        catch (OrganisationPlayerException e) {};

        if(raid != null){
            // Si le land est subit un raid.
            isRaid = true;

            String raidDisclaimer = "§cCe land vas subir un RAID : Quittez le imédiatement.";

            if(oPlayer == null)return raidDisclaimer;
            if(!raid.getLstAttacker().contains(oPlayer) && !land.getOwner().getLocalPlayers().contains(player)){
                return raidDisclaimer;
            }
        }

        LandSubTerritory landSubTerritory = LandSubTerritory.getLandSubTerritoryByLocation(location);
        String provinceName = (land.getProvince() != null) ?"§6"+land.getProvince().getName().replace("_"," ") + " " : "";
        String landName = ((isRaid)?"§c":"§e") +land.getName().replace("_"," ") + " ";
        String end = "";

        if(landSubTerritory == null){

            LargeGroup largeGroup = land.getOwner();
            if(largeGroup == null){
                if (land.isLandReady()) end += "§2(Disponible)";
            }
            else{

                String owner = (largeGroup.getLocalPlayers().contains(player))? "§2": "§7";
                if (isRaid) owner = "§c";
                end = owner + "(";

                if (land.isMainLand()){
                    owner += "§6۩" + owner;
                }

                owner += largeGroup.getName().replace("_"," ") + ")";
                end += owner;
            }
        }
        else{

            String subName =  landSubTerritory.getName();
            if(subName.length() > 0) subName = " " +subName;
            if(landSubTerritory instanceof Mine){
                end += ((isRaid)?"§c":"§a")+"(Mine" + subName + ")";
            }
            else if(landSubTerritory instanceof RP){
                end += ((isRaid)?"§c":"§a")+"("+ subName + ")";
            }
            else if(landSubTerritory instanceof Contest){
                end += ((isRaid)?"§c":"§c")+"("+ subName + ")";
            }
            else if(landSubTerritory instanceof Totem){
                end += ((isRaid)?"§c":"§a")+"(Totem" + subName + ")";
            }
        }

        return provinceName + landName + end;
    }

}
