package fr.avatar_returns.organisation.listener;

import dev.lone.itemsadder.api.ItemsAdder;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Contest;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.RP;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.RPSubTerritory;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import io.lumine.mythic.bukkit.events.MythicMobSpawnEvent;
import io.lumine.mythic.bukkit.events.MythicProjectileHitEvent;
import io.lumine.mythic.core.mobs.ActiveMob;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;


public class MythcisMobsEvent implements Listener {

    @EventHandler public boolean onMythicsMobsDamageEvent(MythicProjectileHitEvent e){

        Location location = e.getEntity().getBukkitEntity().getLocation();
        LandSubTerritory subTerritory = LandSubTerritory.getLandSubTerritoryByLocation(location);

        if(subTerritory == null)return true;
        if(subTerritory instanceof Mine || subTerritory instanceof RP){
            e.setCancelled(true);
            return true;
        }

        return true;
    }

}
