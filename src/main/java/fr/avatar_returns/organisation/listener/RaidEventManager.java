package fr.avatar_returns.organisation.listener;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.relation.Raid;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public class RaidEventManager implements Listener {

    @EventHandler public void onPlayerMoveEvent(PlayerMoveEvent e){

        if (!e.hasChangedPosition()) return;
        if(Raid.getTargetLandRaid().size() == 0)return;

        Land landFrom = Land.getLandByLocation(e.getFrom());
        Land land = Land.getLandByLocation(e.getTo());
        if(land == null) return;

        Raid raid = Raid.getRaidByTargetLand(land);
        if(raid == null)return;

        boolean alreadyInRaidLand = (land == landFrom);
        Player player = e.getPlayer();
        OrganisationPlayer oPlayer = null;
        try {
            oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        }catch (OrganisationPlayerException exception){
            e.setCancelled(true);
            return;
        }

        if(!(land.getOwner().getLocalPlayers().contains(player) || raid.getLstAttacker().contains(oPlayer)) && !raid.getPhase().equals("RAID")){
            if(!alreadyInRaidLand){
                e.setCancelled(true);
            }
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.Raid.CantJoinRaidLand", true);
        }
        else if(!(land.getOwner().getLocalPlayers().contains(player) || raid.getLstAttacker().contains(oPlayer) || player.isOp() || player.hasPermission("organisation.admin"))){
            if(!alreadyInRaidLand){
                e.setCancelled(true);
                GeneralMethods.sendPlayerErrorMessage(player, "Vous ne pouvez pas rentrer dans un land qui subit un raid.");
                return;
            }
            GeneralMethods.teleportSpawnServer(e.getPlayer());
            GeneralMethods.sendPlayerErrorMessage(player, "Vous ne pouvez pas rester sur le land pendant un raid.");
        }

        if(!raid.getPhase().equals("MATCHMAKING"))return;

        PositionPointInterest position = PositionPointInterest.getByLocation(player.getLocation());
        if(position == null)return;
        if(!(position.getContext().equals(PositionPointInterest.Type.TOTEM)))return;

        int captureSecond = raid.getCapturedSecond(player, position);
        if(captureSecond == -1)return;

        GeneralMethods.sendPlayerMessage(player, ""+captureSecond);
    }
    @EventHandler public void onPlayerTeleportEvent(PlayerTeleportEvent e){

        if(Raid.getTargetLandRaid().isEmpty())return;

        Land land = Land.getLandByLocation(e.getTo());
        if(land == null) return;

        Raid raid = Raid.getRaidByTargetLand(land);
        if(raid == null)return;

        Player player = e.getPlayer();
        OrganisationPlayer oPlayer = null;
        try {
            oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
            if(oPlayer.hasPermission("organisation.admin"))return;
        }catch (OrganisationPlayerException exception){
            e.setCancelled(true);
            return;
        }


        if(!(land.getOwner().getLocalPlayers().contains(player) || raid.getLstAttacker().contains(oPlayer))){
            e.setCancelled(true);
            player.teleport(e.getFrom());
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.Raid.CantJoinRaidLand", true);
        }
    }
    @EventHandler public void onPlayerDamagePlayer(EntityDamageByEntityEvent e){

        if(!(e.getDamager() instanceof Player && e.getEntity() instanceof Player))return;
        if(Raid.getTargetLandRaid().size() == 0)return;

        Land land = Land.getLandByLocation(e.getEntity().getLocation());
        if(land == null) return;

        Raid raid = Raid.getRaidByTargetLand(land);
        if(raid == null)return;

        Player damager = (Player) e.getDamager();
        Player target = (Player) e.getEntity();
        OrganisationPlayer oDamager = null;
        OrganisationPlayer oTarget = null;
        try {
            oDamager = OrganisationPlayer.getOrganisationPlayer(damager);
            oTarget = OrganisationPlayer.getOrganisationPlayer(target);
        }catch (OrganisationPlayerException exception){
            GeneralMethods.sendPlayerErrorMessage(damager,"Organisation.Raid.CantDamage", true);
            e.setCancelled(true);
            return;
        }

        if(!((land.getOwner().getLocalPlayers().contains(target) && raid.getLstAttacker().contains(oDamager)) ||
                (land.getOwner().getLocalPlayers().contains(target) && land.getOwner().getLocalPlayers().contains(target)) ||
                (raid.getLstAttacker().contains(oDamager) && raid.getLstAttacker().contains(oDamager)) ||
                (land.getOwner().getLocalPlayers().contains(damager) && raid.getLstAttacker().contains(oTarget))
        )){
            e.setCancelled(true);
            GeneralMethods.sendPlayerErrorMessage(damager,"Organisation.Raid.CantDamage", true);
        }
    }
}
