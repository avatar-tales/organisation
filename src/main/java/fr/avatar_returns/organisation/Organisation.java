package fr.avatar_returns.organisation;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import fr.alessevan.api.AvatarAPI;
import fr.alessevan.api.connections.messaging.Messaging;
import fr.alessevan.api.connections.messaging.rabbitmq.RabbitMessaging;
import fr.alessevan.api.module.AvatarModule;
import fr.avatar_returns.organisation.commands.*;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.hook.*;
import fr.avatar_returns.organisation.listener.*;
import fr.avatar_returns.organisation.manager.DailyTaskManager;
import fr.avatar_returns.organisation.manager.MinutesTaskManager;
import fr.avatar_returns.organisation.manager.SecondeTaskManager;
import fr.avatar_returns.organisation.modules.chat.listeners.ChatHandler;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.rabbitMQPackets.*;
import fr.avatar_returns.organisation.rabbitMQPackets.group.*;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.GroupObjectivePacket;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.GroupRankPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.ObjectivePacket;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.PlayerRankPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.player.ActionBarPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.player.BossBarPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.player.SimpleChatPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.player.TitlePacket;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.territory.renderland.commands.CommandRender;
import fr.avatar_returns.organisation.storage.DBConnection;
import fr.avatar_returns.organisation.manager.RaidManager;
import fr.avatar_returns.organisation.utils.teleportation.MultiServerTPMethodimplements;
import net.citizensnpcs.api.CitizensAPI;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import static fr.avatar_returns.organisation.listener.GlobalEventManager.mapOPlayerLeaving;

public class Organisation extends JavaPlugin {

    public static Organisation plugin;
    public static Logger log;
    public static Scoreboard board;
    public static RegionManager worldRgManager;
    private static Team defaultTeam;
    private static ArrayList<OfflinePlayer> lstAdminSeePseudo;
    private Messaging messaging;

    public static long lastDayRunningTask = 0;
    public static boolean isSpawnServer = false;
    public static boolean  isBuildProtectionEnable;
    public static boolean  isInteractionProtectionEnable;
    public static boolean  isLandDisplay;
    final List<AvatarModule<? extends JavaPlugin>> modules =  new ArrayList<>();

    private static List<Hook<? extends JavaPlugin>> hooks;

    @Override public void onEnable() {
        super.onEnable();
        plugin = this;
        log = this.getLogger();
        hooks = new ArrayList<>();

        new ConfigManager();
        new GeneralMethods();

        Plugin plugin = getServer().getPluginManager().getPlugin("WorldGuard");
        if(plugin == null) {
            log.severe("WorldGuard is mandatory to use Organisation. Organisation is now disabled");
            GeneralMethods.stopPlugin();
        }

        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        worldRgManager = container.get(BukkitAdapter.adapt(this.getServer().getWorld(GeneralMethods.getStringConf("Default.OrganisationWorld"))));
        if(worldRgManager == null) {
            log.severe("Unable to get world throught WorldGuard. Organisation is now disabled");
            GeneralMethods.stopPlugin();
        }

        if (this.getServer().getPluginManager().isPluginEnabled("AvatarRoads")) hooks.add(new AvatarRoadsHook(this.getServer().getPluginManager().getPlugin("AvatarRoads")));
        if (this.getServer().getPluginManager().isPluginEnabled("MythicMobs")) hooks.add(new MythicsMobsHook(this.getServer().getPluginManager().getPlugin("MythicMobs")));

        this.registerStringHooks();

        new Commands(this);
        new OrganisationChatCommand(this);
        new OrganisationSpawnCommand(this);
        new OrganisationHomeCommand(this);
        new OrganisationTPMultiServerCommand(this);

        getServer().getMessenger().registerIncomingPluginChannel((Plugin)this, "tp:velocity", new MultiServerTPMethodimplements());
        getServer().getMessenger().registerOutgoingPluginChannel((Plugin)this, "tp:velocitypaper");

        AvatarAPI.getAPI().registerCommand(this, CommandRender.class);

        Organisation.isBuildProtectionEnable =  !GeneralMethods.getBooleanConf("Organisation.Disable.Protection.Build");
        Organisation.isInteractionProtectionEnable = !GeneralMethods.getBooleanConf("Organisation.Disable.Protection.Interaction");
        Organisation.isLandDisplay = !GeneralMethods.getBooleanConf("Organisation.Disable.LandDisplay");

        this.getServer().getPluginManager().registerEvents(new GlobalEventManager(), this);
        this.getServer().getPluginManager().registerEvents(new LandPlayerEventManager(), this);
        this.getServer().getPluginManager().registerEvents(new PlayerInteractionEvent(), this);
        this.getServer().getPluginManager().registerEvents(new LandSubTerritoryPlayerEventManager(), this);
        this.getServer().getPluginManager().registerEvents(new RaidEventManager(), this);
        this.getServer().getPluginManager().registerEvents(new ChatHandler(), this);

        if(Organisation.getHook(MythicsMobsHook.class).isPresent())this.getServer().getPluginManager().registerEvents(new MythcisMobsEvent(), this);

        if ("rabbitmq".equalsIgnoreCase(GeneralMethods.getStringConf("Messaging.engine"))){

            this.messaging = AvatarAPI.getAPI().createRabbitMQMessaging(
                    ConfigManager.getServerId(),
                    GeneralMethods.getStringConf("Messaging.host"),
                    GeneralMethods.getIntConf("Messaging.port"),
                    GeneralMethods.getStringConf("Messaging.user"),
                    GeneralMethods.getStringConf("Messaging.pass"));

            ((RabbitMessaging) this.messaging).connect();
            ((RabbitMessaging) this.messaging).addExchangeKey("organisation"); //On publie des message dans le canal organisation

        }
        else{
            this.messaging = AvatarAPI.getAPI().createInternalMessaging();
        }

        this.messaging.addCodec(PlayerRankPacket.getCodec(this));
        this.messaging.addCodec(GroupRankPacket.getCodec(this));
        this.messaging.addCodec(OrganisationPlayerPacket.getCodec(this));
        this.messaging.addCodec(VMinePacket.getCodec(this));
        this.messaging.addCodec(fr.avatar_returns.organisation.modules.chat.messaging.packets.ChatPacket.getCodec(this));
        this.messaging.addCodec(SimpleChatPacket.getCodec(this));
        this.messaging.addCodec(FactionPacket.getCodec(this));
        this.messaging.addCodec(GuildePacket.getCodec(this));
        this.messaging.addCodec(TitlePacket.getCodec(this));
        this.messaging.addCodec(BossBarPacket.getCodec(this));
        this.messaging.addCodec(ActionBarPacket.getCodec(this));
        this.messaging.addCodec(RolePacket.getCodec(this));
        this.messaging.addCodec(PermissionPacket.getCodec(this));
        this.messaging.addCodec(ObjectivePacket.getCodec(this));
        this.messaging.addCodec(GroupObjectivePacket.getCodec(this));
        this.messaging.addCodec(GroupMemberPacket.getCodec(this));
        this.messaging.addCodec(InvitationPacket.getCodec(this));
        this.messaging.addCodec(StoredInventoryPacket.getCodec(this));
        this.messaging.addCodec(VLandPacket.getCodec(this));


        if (!DBConnection.init()) {
            GeneralMethods.stopPlugin();
            return;
        }

        this.board = Bukkit.getScoreboardManager().getMainScoreboard();

        Organisation.defaultTeam = this.board.getTeam("wild");
        if (Organisation.defaultTeam == null) Organisation.defaultTeam = this.board.registerNewTeam("wild");
        Organisation.defaultTeam.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.NEVER);

        GeneralMethods.restoreFromDb();
        for(Player player :Bukkit.getOnlinePlayers()){

            if(CitizensAPI.getNPCRegistry().getNPC(player) != null)continue;
            OrganisationPlayer organisationPlayer = null;
            try {
                organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
                if(!OrganisationPlayer.getOrganisationPlayerByName(player.getName()).equals(organisationPlayer)){
                    // Si un ancien joueur avait ce nom. Alors, on le modifie et on le log
                    OrganisationPlayer.getOrganisationPlayerByName(player.getName()).setLastSeenName("*");
                    Organisation.log.warning("Le pseudo " + player.getName() + " est enregistré comme le nom d'un ancien joueur... Modification du dernier pseudo pu pour l'ancien joueur");
                }
                organisationPlayer.setIsConnected(true);
            } catch (OrganisationPlayerException e) {
                organisationPlayer = new OrganisationPlayer(player);
            }

            if(organisationPlayer.getLstGroup().isEmpty())Organisation.getDefaultTeam().addPlayer(player);
        }
        lstAdminSeePseudo = new ArrayList<>();

        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new RaidManager(),0 ,1);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new DailyTaskManager(),0 ,1);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new MinutesTaskManager(),0 ,1);
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new SecondeTaskManager(),0 ,1);

        if(!ItemAdderHook.isItemAdderHooked())Organisation.log.severe("Unable to hook ItemAdder, some feature will be disabled.");

        System.out.println("DEBUG HASH : " + this.hashCode() + " SIZE : " + Organisation.getOrganisationPlayer().size());
        Organisation.log.info("[Organisation] On");

        for(OrganisationPlayer oPlayer : OrganisationPlayer.getLstOrganisationPlayer()){
            oPlayer.manageVisiblePlayer();
        }
        for(Player player: Organisation.plugin.getServer().getOnlinePlayers()){
            boolean findGroup = false;
            for(LargeGroup largeGroup: LargeGroup.getLstLargeGroup()){
                if(largeGroup.getLocalPlayers().contains(player)){
                    findGroup = true;
                    if(!largeGroup.getTeam().hasPlayer(player)){
                        largeGroup.getTeam().addEntry(player.getName());
                    }
                }
            }

            if(!findGroup && !Organisation.getDefaultTeam().hasPlayer(player)){
                Organisation.getDefaultTeam().addPlayer(player);
            }
        }
    }

    @Override public void onDisable() {

        GeneralMethods.stopMine();
        for(OrganisationPlayer organisationPlayer : OrganisationPlayer.getLstOrganisationPlayer()){
            if(organisationPlayer.getOfflinePlayer() == null)continue;
            if(organisationPlayer.getOfflinePlayer().isOnline()){
                DBUtils.setOrganisationPlayerEffectivePlayedTime(organisationPlayer);
                organisationPlayer.setIsConnected(false);

                for(Group group: organisationPlayer.getLstGroup()){
                    OrganisationMember organisationMember = group.getOrganisationMember(organisationPlayer.getOfflinePlayer());
                    DBUtils.setGroupMemberEffectivePlayedTime(organisationMember);
                }
            }
        }

        // On retire les boss bar.
        for (Player player : LandPlayerEventManager.mapPlayerBossBar.keySet()){
            LandPlayerEventManager.mapPlayerBossBar.get(player).removeAll();
        }

        if (DBConnection.sql != null) DBConnection.sql.close();
        if (this.messaging != null)((RabbitMessaging)this.messaging).disconnect();

        this.modules.forEach(AvatarModule::onDisable);

        super.onDisable();
        Organisation.log.info("[Organisation] Off");
    }

    // TODO : complete method
    private void registerStringHooks() {
        Pattern UUIDPattern = Pattern.compile("\\{COMPLETE_(\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12})}", Pattern.CASE_INSENSITIVE);
        AvatarAPI.getAPI().registerStringHook(UUIDPattern, message -> {
            final Matcher matcher = UUIDPattern.matcher(message);
            final UUID uuid = UUID.fromString(matcher.results().toList().get(0).group(1));

            return "";
        });

        Pattern namePattern = Pattern.compile("\\{COMPLETE_(\\w{2,16})}", Pattern.CASE_INSENSITIVE);
        AvatarAPI.getAPI().registerStringHook(namePattern, message -> {
            final Matcher matcher = namePattern.matcher(message);
            final String name = matcher.results().toList().get(0).group(1);

            return "";
        });
    }

    public static Team getDefaultTeam(){
        return Organisation.defaultTeam;
    }

    public static ArrayList<OfflinePlayer> getAdminTeam(){
        return Organisation.lstAdminSeePseudo;
    }

    public Messaging getMessaging() {
        return this.messaging;
    }

    public static <T> Optional<T> getHook(Class<T> clazz) {
        return hooks.stream()
                .filter(hook -> hook.getClass().equals(clazz))
                .map(hook -> (T) hook)
                .findFirst();
    }

    public static ArrayList<OrganisationPlayer> getOrganisationPlayer(){
        System.out.println("DEBUG HASH LST : " + OrganisationPlayer.getLstOrganisationPlayer().hashCode() + " SIZE : " + OrganisationPlayer.getLstOrganisationPlayer().size());
        return OrganisationPlayer.getLstOrganisationPlayer();
    }

    public ArrayList<OrganisationPlayer> getOrganisationPlayers(){
        return OrganisationPlayer.getLstOrganisationPlayer();
    }
}
