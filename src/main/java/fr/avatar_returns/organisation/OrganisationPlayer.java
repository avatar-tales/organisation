package fr.avatar_returns.organisation;

import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPlayerPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.player.ActionBarPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.player.PotionEffectPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.player.SoundPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.player.TitlePacket;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.RP;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.ItemUtils;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import fr.avatar_returns.organisation.utils.messages.Title;
import fr.avatar_returns.organisation.view.GeneralItem;
import net.kyori.adventure.text.Component;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.jetbrains.annotations.Nullable;

import java.text.SimpleDateFormat;
import java.util.*;

public class OrganisationPlayer {

    private static ArrayList<OrganisationPlayer> lstOrganisationPlayer = new ArrayList<>();

    private static int maxGuilde = 1;
    private static int maxFaction = 1;
    private final long firstTimeConnexion;
    private long lastEffectivePlayedTime;
    private long lastTimeConnexion;
    private int rank;
    private boolean isConnected;
    private String lastSeenName;
    private long lastTimeFightMod;
    private boolean isInPrespawnMod;
    protected UUID uuid;
    private String lastServerSeenName;
    private int playerKill;
    private int mobsKill;
    private int nbDeath;
    private String stringPlayerHead;
    private String lastStrElementSeen;
    private String nickName;
    private String chatLocked; //GroupName of locked chat, "" if unlocked

    public OrganisationPlayer(OfflinePlayer player){

        this.setPlayer(player);

        this.firstTimeConnexion = System.currentTimeMillis();
        this.lastEffectivePlayedTime = 0;
        this.lastTimeConnexion = System.currentTimeMillis();
        this.rank = 0;
        this.lastSeenName = player.getName();
        this.isConnected = true;
        this.isInPrespawnMod = true;
        this.lastTimeFightMod = -1;
        this.mobsKill = 0;
        this.playerKill = 0;
        this.nbDeath = 0;
        this.lastServerSeenName = ConfigManager.getServerId();
        this.stringPlayerHead = ItemUtils.itemStackToBase64(SkullCreator.getPlayerHeadItem(this.getUuid()));
        this.lastStrElementSeen = "";
        this.nickName = "";
        this.chatLocked = "GLOBAL";

        // We set local lastStrElementSeen
        //this.updateLastStrElementSeen();

        for(OrganisationPlayer connectedOPlayer : OrganisationPlayer.getLstConnectedOrganisationPlayer()){
            GeneralMethods.sendPlayerMessage(connectedOPlayer, "§l§6Bienvenue §bà " + player.getName() + " dans le monde d'§6Avatar-Returns !");
            connectedOPlayer.sendSound(Sound.ENTITY_FIREWORK_ROCKET_BLAST);
        }

        this.sendFullTitle(10,75,10,"§6Bienvenue","§b" +player.getName());
        lstOrganisationPlayer.add(this);
        DBUtils.addOrganisationPlayer(this);
    }
    public OrganisationPlayer(String uuid,
                              long firstTimeConnexion,
                              long lastTotalPlayedTime,
                              int rank,
                              String lastSeenName,
                              boolean isConnected,
                              boolean isInPrespawnMod,
                              long lastTimeFightMod,
                              String lastServerSeenName,
                              String stringPlayerHead,
                              int mobsKill,
                              int playerKill,
                              int nbDeath,
                              String lastStrElementSeen,
                              String chatLocked,
                              String nickName
    ){

        // This method should be only used to import OrganisationPlayer at startup
        OfflinePlayer offlinePlayer = Organisation.plugin.getServer().getOfflinePlayer(UUID.fromString(uuid));
        this.setPlayer(offlinePlayer);

        this.firstTimeConnexion = firstTimeConnexion;
        this.lastEffectivePlayedTime = lastTotalPlayedTime;
        this.lastTimeConnexion = System.currentTimeMillis();
        this.rank = rank;
        this.lastSeenName = lastSeenName;
        this.isConnected = isConnected;
        this.isInPrespawnMod = isInPrespawnMod;
        this.lastTimeFightMod = lastTimeFightMod;
        this.lastServerSeenName = lastServerSeenName;
        this.stringPlayerHead = stringPlayerHead;
        this.mobsKill = mobsKill;
        this.playerKill = playerKill;
        this.nbDeath = nbDeath;
        this.lastStrElementSeen = lastStrElementSeen;
        this.chatLocked = chatLocked;
        this.nickName = nickName;

        lstOrganisationPlayer.add(this);
    }

    public boolean isInGroup(Group group){
        return this.getLstGroup().contains(group);
    }
    public boolean canJoinOrganisation(Group targetGroup){

        if (getOfflinePlayer().isOp() || getOfflinePlayer().getPlayer().hasPermission("organisation.command.join.force")) return true;
        if( !getOfflinePlayer().getPlayer().hasPermission("organisation.command.join")) return false;

        int nbGuilde = 0 ,nbFaction = 0;
        for(Group group : this.getLstGroup()){
            if(group instanceof LargeGroup)nbFaction++;
            else if(group instanceof Guilde) nbGuilde++;
        }

        //Organisation.log.severe(nbFaction+" < "+maxFaction+" && "+nbGuilde+" < "+maxGuilde);
        if(targetGroup instanceof LargeGroup)return nbFaction<1;
        else return nbGuilde < maxGuilde;

    }

    //Getter
    public UUID getUuid() {
        return this.uuid;
    }
    @Nullable public OfflinePlayer getOfflinePlayer() {
        return GeneralMethods.getOfflinePlayerFromUUID(this.uuid);
    }

    public static OrganisationPlayer getOrganisationPlayerByName(String playerName) throws OrganisationPlayerException {

        for(OrganisationPlayer organisationPlayer : OrganisationPlayer.getLstOrganisationPlayer()){
            if(organisationPlayer.getOfflinePlayer() != null) {
                if(organisationPlayer.getOfflinePlayer().getName() != null){
                    if(organisationPlayer.getOfflinePlayer().getName().equals(playerName)) return organisationPlayer;
                }
                else if(organisationPlayer.getLastSeenName().equals(playerName))return organisationPlayer;
            }
            else if(organisationPlayer.getLastSeenName().equals(playerName))return organisationPlayer;
        }

        //If the player is not found, start Exception
        throw new OrganisationPlayerException(playerName);
    }
    public static OrganisationPlayer getOrganisationPlayerByUUID(UUID uuid) throws OrganisationPlayerException {

        for(OrganisationPlayer organisationPlayer : OrganisationPlayer.getLstOrganisationPlayer()){
            if(organisationPlayer.getUuid().equals(uuid))return organisationPlayer;
        }

        //If the player is not found, start Exception
        throw new OrganisationPlayerException(uuid.toString());
    }
    public static OrganisationPlayer getOrganisationPlayer(Player player) throws OrganisationPlayerException {

        for(OrganisationPlayer organisationPlayer : OrganisationPlayer.getLstOrganisationPlayer()){
            if(organisationPlayer.getUuid().equals(player.getUniqueId())){
                return organisationPlayer;
            }
        }

        //If the player is not found, start Exception
        throw new OrganisationPlayerException(player);
    }
    public static OrganisationPlayer getOrganisationPlayer(OfflinePlayer player) throws OrganisationPlayerException {

        for(OrganisationPlayer organisationPlayer : OrganisationPlayer.getLstOrganisationPlayer()){
            if(organisationPlayer.getOfflinePlayer().getUniqueId().equals(player.getUniqueId()))return organisationPlayer;
        }

        //If the player is not found, start Exception
        throw new OrganisationPlayerException(player);
    }
    public ArrayList<Group> getLstGroup() {
        ArrayList<Group> lstGroup = new ArrayList<>();
        for (Group group: Group.getLstGroup()){
            if(group.getLstGroupOrganisationPlayer().contains(this))lstGroup.add(group);
        }
        return lstGroup;
    }
    public ArrayList<LargeGroup> getLstLargeGroup() {
        ArrayList<LargeGroup> lstGroup = new ArrayList<>();
        for (Group group: Group.getLstGroup()){
            if(!(group instanceof LargeGroup))continue;
            if(group.getLstGroupOrganisationPlayer().contains(this))lstGroup.add((LargeGroup) group);
        }
        return lstGroup;
    }

    public ArrayList<Guilde> getLstGuilde() {
        ArrayList<Guilde> lstGuilde = new ArrayList<>();
        for (Group group: Group.getLstGroup()){
            if(!(group instanceof Guilde))continue;
            if(group.getLstGroupOrganisationPlayer().contains(this))lstGuilde.add((Guilde) group);
        }
        return lstGuilde;
    }

    public static ArrayList<OrganisationPlayer> getLstOrganisationPlayer() {
        return lstOrganisationPlayer;
    }
    public static ArrayList<OrganisationPlayer> getLstPrespawnModOrganisationPlayer() {
        ArrayList<OrganisationPlayer> lstPrespawnModOrganisationPlayer = new ArrayList<>();
        for(OrganisationPlayer oPlayer : OrganisationPlayer.getLstOrganisationPlayer()){
            if(oPlayer.isInPrespawnMod())lstPrespawnModOrganisationPlayer.add(oPlayer);
        }
        return lstPrespawnModOrganisationPlayer;
    }
    public static ArrayList<OrganisationPlayer> getLstConnectedOrganisationPlayer() {
        ArrayList<OrganisationPlayer> lstConnectedOrganisationPlayer = new ArrayList<>();
        for(OrganisationPlayer oPlayer : OrganisationPlayer.getLstOrganisationPlayer()){
            if(oPlayer.isConnected())lstConnectedOrganisationPlayer.add(oPlayer);
        }
        return lstConnectedOrganisationPlayer;
    }
    
    public void setPlayer(OfflinePlayer player) {
        this.uuid = player.getUniqueId();
    }
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
    public static void setLstOrganisationPlayer(ArrayList<OrganisationPlayer> lstOrganisationPlayer) {
        OrganisationPlayer.lstOrganisationPlayer = lstOrganisationPlayer;
    }

    public ArrayList<Invitation> getLstInvitation(){
        ArrayList<Invitation> lstPlayerInvit = new ArrayList<>();
        for(Invitation invitation : InviteCommand.lstInvitation){
            if(invitation.getOPlayerInvite().getUuid().equals(this.getUuid())){
                lstPlayerInvit.add(invitation);
            }
        }
        return lstPlayerInvit;
    }

    public final long getFirstTimeConnexion(){return this.firstTimeConnexion;}
    public final long getLastEffectivePlayedTime(){return this.lastEffectivePlayedTime;}

    public void setLastTimeConnexion(long setLastTimeConnexion){this.setLastTimeConnexion(lastTimeConnexion, true);}
    public void setLastTimeConnexion(long setLastTimeConnexion, boolean storeDb) {

        this.lastTimeConnexion = lastTimeConnexion;
        if(storeDb)DBUtils.newOrganisationPlayerConnexion(this);
    }
    public final long getLastTimeConnexion(){return this.lastTimeConnexion;}

    public long getEffectivePlayedTime() {
        long currentTime = System.currentTimeMillis();
        this.lastEffectivePlayedTime += (currentTime - this.getLastTimeConnexion());

        //Secure way to be sure an past time will not be count two times.
        this.lastTimeConnexion = currentTime;
        return  this.lastEffectivePlayedTime;
    }
    public boolean isYoung() {
        return this.getLastEffectivePlayedTime() < GeneralMethods.getLongConf("Organisation.Player.timeToBeExperimentedAsPlayer");
    }

    public int getRank() {return rank;}
    public void setRank(int rank) {this.setRank(rank, true);}
    public void setRank(int rank, boolean storeDb) {
        this.rank = rank;
        if(storeDb) DBUtils.setOrganisationPlayerRank(this);
    }

    public void setHead(ItemStack playerHead){ this.setHead(playerHead,true);}
    public void setHead(ItemStack playerHead, boolean storeDb){
        this.setLastHeadSeen(ItemUtils.itemStackToBase64(playerHead), storeDb);
    }

    public void setLastHeadSeen(String stringPlayerHead,  boolean storeDb) {
        this.stringPlayerHead = stringPlayerHead;
        if(storeDb) DBUtils.setOrganisationLastPlayerHeadSeen(this) ;
    }
    public String getLastHeadSeen() {return this.stringPlayerHead;}
    public ItemStack getHead() {
        try{
            ItemStack playerHead = ItemUtils.itemStackFromBase64(this.stringPlayerHead);
            if(playerHead != null)return playerHead;
        }
        catch (Exception e){
            Organisation.log.warning("Unable to get player head of " + this.getName() + " for data \n ->" + this.stringPlayerHead);
            return new ItemStack(Material.PLAYER_HEAD, 1);
        }
        return new ItemStack(Material.PLAYER_HEAD, 1);
    }
    public ItemStack getFullHeadItem() {

        ItemStack head = this.getHead().clone();
        ItemMeta headMeta = head.getItemMeta();
        headMeta.setDisplayName("§6§l"+this.getName());
        ArrayList<Component> headDesc = new ArrayList<>();
        String playerPlayedTime = "";
        long effectivePlayedTime = this.getEffectivePlayedTime();
        long seconds = effectivePlayedTime / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        long remainingDays = days % 365;
        long remainingHours = hours % 24;
        long remainingMinutes = minutes % 60;
        long remainingSeconds = seconds % 60;

        String min_str = (remainingMinutes < 9)? "0"+remainingMinutes : ""+remainingMinutes;
        String sec_str = (remainingSeconds < 9)? "0"+remainingSeconds : ""+remainingSeconds;

        playerPlayedTime = remainingHours+"h" + min_str + ":" + sec_str;
        if(remainingDays > 0) playerPlayedTime = remainingDays + " jours, " + playerPlayedTime;

        headDesc.add(Component.text("§8Joueurs tués : §7" + this.getPlayerKill()));
        headDesc.add(Component.text("§8Monstres tués : §7" + this.getMobsKill()));
        headDesc.add(Component.text("§8Morts: §7" + this.getNbDeath()));
        headDesc.add(Component.text("§8Temps de jeux : §7" + playerPlayedTime));

        Date date = new Date(this.getFirstTimeConnexion());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy  HH:mm:ss");
        String formattedDate = sdf.format(date);

        headDesc.add(Component.text("§8Arrivé : §7" + formattedDate));
        headMeta.lore(headDesc);
        head.setItemMeta(headMeta);

        return head;
    }

    public String getNickName(){
        return (this.nickName == null)? "" : this.nickName;
    }
    public void setNickName(String nickName) {
        this.setNickName(nickName, true);
    }
    public void setNickName(String nickName, boolean storeDB) {
        this.nickName = nickName;
        if(storeDB) DBUtils.setOrganisationPlayerNickName(this);
    }

    public String getName(){
        if(!this.getNickName().isBlank())return this.getNickName();
        if(this.getOfflinePlayer() == null)return this.getLastSeenName();
        if(!this.getOfflinePlayer().isOnline())return this.getLastSeenName();
        return this.getOfflinePlayer().getName();
    }
    public String getLastSeenName() {
        return this.lastSeenName;
    }
    public void setLastSeenName(String lastSeenName){this.setLastSeenName(lastSeenName, true);}
    public void setLastSeenName(String lastSeenName, boolean storeDB) {
        this.lastSeenName = lastSeenName;
        if(storeDB) DBUtils.setOrganisationPlayerLastSeenName(this);
    }

    public boolean isConnected(){
        return this.isConnected;
    }
    public void setIsConnected(boolean isConnected){ this.setIsConnected(isConnected, true);}
    public void setIsConnected(boolean isConnected, boolean storeDB){
        this.isConnected = isConnected;
        if(storeDB)DBUtils.setOrganisationPlayerIsConnected(this);
    }

    public boolean isInPrespawnMod() {return isInPrespawnMod;}
    public void setIsInPrespawnMod(boolean isInPrespawnMod){this.setIsInPrespawnMod(isInPrespawnMod, true);}
    public void setIsInPrespawnMod(boolean isInPrespawnMod, boolean storeDB){

        this.isInPrespawnMod = isInPrespawnMod;
        this.manageVisiblePlayer();
        if(storeDB) DBUtils.setOrganisationIsInPrespawnMod(this);
    }

    public void setLastServerSeenName(String lastServerSeenName) {
        this.setLastServerSeenName(lastServerSeenName, true);
    }
    public void setLastServerSeenName(String lastServerSeenName, boolean storeDB) {
        this.lastServerSeenName = lastServerSeenName;
        if(storeDB) GeneralMethods.sendPacket(new OrganisationPlayerPacket(this, OrganisationPacket.Action.UPDATE));
    }
    public String getLastServerSeenName(){return this.lastServerSeenName;}

    public int getMobsKill() {return mobsKill;}
    public void incMobsKill(){ this.incMobsKill(true);}
    public void incMobsKill(boolean storeDB){
        this.mobsKill++;
        if(storeDB) DBUtils.setOrganisationPlayerMobsKill(this);
    }
    public void setMobsKill(int mobsKill, boolean storeDB){
        this.mobsKill = mobsKill;
        if(storeDB) DBUtils.setOrganisationPlayerMobsKill(this);
    }

    public int getNbDeath() {return nbDeath;}
    public void incNbDeath(){ this.incNbDeath(true);}
    public void incNbDeath(boolean storeDB){
        this.nbDeath++;
        if(storeDB) DBUtils.setOrganisationPlayerNbDeath(this);
    }
    public void setNbDeath(int nbDeath, boolean storeDB){
        this.nbDeath = nbDeath;
        if(storeDB) DBUtils.setOrganisationPlayerNbDeath(this);
    }

    public int getPlayerKill() {return playerKill;}
    public void incPlayerKill(){ this.incPlayerKill(true);}
    public void incPlayerKill(boolean storeDB){
        this.playerKill++;
        if(storeDB) DBUtils.setOrganisationPlayerKill(this);
    }
    public void setPlayerKill(int playerKill, boolean storeDB){
        this.playerKill = playerKill;
        if(storeDB) DBUtils.setOrganisationPlayerKill(this);
    }

    public void manageVisiblePlayer(){

        if(this.getOfflinePlayer().getPlayer() != null){

            Player player = this.getOfflinePlayer().getPlayer();

            for(OrganisationPlayer oConnectedPlayer : OrganisationPlayer.getLstConnectedOrganisationPlayer()){

                if(oConnectedPlayer.getUuid() == this.getUuid())continue;
                if(oConnectedPlayer.getOfflinePlayer() == null)continue;
                if(oConnectedPlayer.getOfflinePlayer().getPlayer() == null)continue;

                Player connectedPlayer = oConnectedPlayer.getOfflinePlayer().getPlayer();

                if(connectedPlayer.isOp() || connectedPlayer.hasPermission("organisation.prespawn.admin") || player.isOp() || player.hasPermission("organisation.prespawn.admin")){
                    connectedPlayer.showPlayer(Organisation.plugin, player);
                    player.showPlayer(Organisation.plugin, connectedPlayer);
                }
                else if(this.isInPrespawnMod()) {
                    connectedPlayer.hidePlayer(Organisation.plugin, player);
                    player.hidePlayer(Organisation.plugin, connectedPlayer);
                }
                else {
                    connectedPlayer.showPlayer(Organisation.plugin, player);
                    player.showPlayer(Organisation.plugin, connectedPlayer);
                }
            }
        }
    }

    public boolean isInFightMod() {

        boolean isInFightMod = true;

        if(this.getOfflinePlayer() != null) {
            if (this.getOfflinePlayer().getPlayer() != null) {
                if(this.getOfflinePlayer().getPlayer().isOp() || this.getOfflinePlayer().getPlayer().hasPermission("organisation.fightmode.skip"))isInFightMod = false;
            }
        }
        if(this.lastTimeFightMod <= -1)isInFightMod = false;
        if (isInFightMod) {
            isInFightMod = (this.lastTimeFightMod / 1000 + GeneralMethods.getIntConf("Organisation.FightMode.Duration")) > (System.currentTimeMillis() / 1000);
        }

        return isInFightMod;
    }
    public long getLastTimeFightMod() {return lastTimeFightMod;}
    public void setLastTimeFightMod(long lastTimeFightMod) {
        this.setLastTimeFightMod(lastTimeFightMod, true);
    }
    public void setLastTimeFightMod(long lastTimeFightMod, boolean storeDB) {
        this.lastTimeFightMod = lastTimeFightMod;
    }

//    public String getLastStrElementSeen(){
//        if(this.lastStrElementSeen == null) this.setLastStrElementSeen("");
//        return this.lastStrElementSeen;
//    }

//    public void updateLastStrElementSeen(){
//        try {
//            if (this.getOfflinePlayer() != null) {
//                if (this.getOfflinePlayer().getPlayer() != null) {
//                    this.setLastStrElementSeen((Organisation.getHook(ProjectKorraHook.class).isPresent()) ? Organisation.getHook(ProjectKorraHook.class).get().getBenderColor(this.getLastSeenName()) : "");
//                }
//            }
//        }
//        catch (Exception e){
//            Organisation.log.severe(e.getMessage());
//        }
//    }

//    public void setLastStrElementSeen(String lastStrElementSeen){
//        this.setLastStrElementSeen(lastStrElementSeen, true);
//    }

//    public void setLastStrElementSeen(String lastStrElementSeen, boolean storeDB){
//
//        boolean changed = !this.lastStrElementSeen.equals(lastStrElementSeen);
//        this.lastStrElementSeen = lastStrElementSeen;
//
//        if(changed && storeDB)DBUtils.setOrganisationPlayerLastStrElementSeen(this);
//    }

    public ItemStack getChatLockedItem(){

        ItemStack itemStack = null;
        List<String> lst_desc = null;
        if(this.getChatLocked().equals("GUILDE")) {

            itemStack = new ItemStack(Material.EMERALD);
            lst_desc = Arrays.asList("§8" + this.getChatLocked().toUpperCase()+ ":§7 Vous ne parlez que dans votre guilde", "", "§8Cliquez pour changer de mode");
        }
        else if(this.getChatLocked().equals("FACTION")){
            itemStack = new ItemStack(Material.RED_BANNER);
            lst_desc = Arrays.asList("§8" + this.getChatLocked().toUpperCase()+ ":§7 Vous ne parlez que dans votre faction", "", "§8Cliquez pour changer de mode");
        }
        else if(this.getChatLocked().equals("GLOBAL")){
            itemStack = SkullCreator.itemFromUrl("https://textures.minecraft.net/texture/b1dd4fe4a429abd665dfdb3e21321d6efa6a6b5e7b956db9c5d59c9efab25");
            lst_desc = Arrays.asList("§8" + this.getChatLocked().toUpperCase()+ ":§7 Vous ne parlez que dans le chat GLOBAL", "§8Cliquez pour changer de mode");
        }
        else if(this.getChatLocked().equals("MUTE")){
            itemStack = new ItemStack(Material.BARRIER);
            lst_desc = Arrays.asList("§8" + this.getChatLocked().toUpperCase()+ ":§7 Vous ne parlez ni ne pouvez entendre.", "", "§8Cliquez pour changer de mode");
        }
        else {
            itemStack = new ItemStack(Material.TRIPWIRE_HOOK);
            lst_desc = Arrays.asList("§8" + this.getChatLocked().toUpperCase()+ ":§7 Mode par défaut. <<!>> pour parler en global.", "","§8Suivant : §7GLOBAL");
        }

        GeneralItem.cleanItemMeta(itemStack);
        itemStack = GeneralItem.renameItemStackWithName(itemStack, "§6§lMode discussion");
        GeneralItem.setLoreFromList(itemStack, lst_desc);

        return itemStack;
    }

    public String getChatLocked(){
        return this.chatLocked;
    }
    public void setChatLocked(String chatLocked){ this.setChatLocked(chatLocked, true);}
    public void setChatLocked(String chatLocked, boolean storeDB){
        this.chatLocked = chatLocked;
        if(storeDB)DBUtils.setOrganisationPlayerChatLocked(this);
    }
    public boolean isMuted(){
        return this.getChatLocked().equals("MUTE");
    }

    public void sendActionBar(String msg){
        if(this.getOfflinePlayer() == null) GeneralMethods.sendPacket(new ActionBarPacket(this, msg));
        else if(this.getOfflinePlayer().getPlayer() == null) GeneralMethods.sendPacket(new ActionBarPacket(this, msg));
        else this.getOfflinePlayer().getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(msg));
    }
    public void sendFullTitle(int fadeIn, int stay, int fadeOut, String title, String subTitle){

        if(this.getOfflinePlayer() == null) GeneralMethods.sendPacket(new TitlePacket(this, fadeIn, stay, fadeOut, title, subTitle));
        else if(this.getOfflinePlayer().getPlayer() == null) GeneralMethods.sendPacket(new TitlePacket(this, fadeIn, stay, fadeOut, title, subTitle));
        else Title.sendFullTitle(this.getOfflinePlayer().getPlayer(), fadeIn, stay, fadeOut, title, subTitle);
    }

    public void sendSound(Sound sound){
        if(this.getOfflinePlayer() == null) GeneralMethods.sendPacket(new SoundPacket(this, sound));
        else if(this.getOfflinePlayer().getPlayer() == null) GeneralMethods.sendPacket(new SoundPacket(this, sound));
        else this.getOfflinePlayer().getPlayer().playSound(this.getOfflinePlayer().getPlayer(), sound, 1.0f, 1.0f);
    }
    public void addPotionEffect(PotionEffect potionEffect){
        if(this.getOfflinePlayer() == null) GeneralMethods.sendPacket(new PotionEffectPacket(this, potionEffect));
        else if(this.getOfflinePlayer().getPlayer() == null) GeneralMethods.sendPacket(new PotionEffectPacket(this,potionEffect));
        else this.getOfflinePlayer().getPlayer().addPotionEffect(potionEffect);
    }

    public boolean hasGroupPermission(Group group, Enum perm){
        return hasGroupPermission(group,perm.toString());
    }
    public boolean hasGroupPermission(Group group, String permission){

        if(this.hasPermission("organisation.admin"))return true;
        if(this.getOfflinePlayer() == null)return false;

        return group.hasPermission(this.getOfflinePlayer(), permission);
    }

    public boolean hasRolePermission(Role role, Enum perm){
        return hasRolePermission(role, perm.toString());
    }
    public boolean hasRolePermission(Role role, String permission){
        if(this.hasPermission("organisation.admin"))return true;
        return this.hasGroupPermission(role.getGroup(), permission) && Role.canExecuteOnOtherRole(this, role);
    }

    public boolean hasPermission(String permission){
        if(this.getOfflinePlayer() == null)return false;
        if(this.getOfflinePlayer().isOp()) return true;
        if(this.getOfflinePlayer().getPlayer() == null) return false;

        return this.getOfflinePlayer().getPlayer().hasPermission(permission);
    }

    public boolean isLocalyInSafeZone(){
        if(this.getOfflinePlayer() == null)return false;
        if(this.getOfflinePlayer().getPlayer() == null)return false;

        Location location = this.getOfflinePlayer().getPlayer().getLocation();
        LandSubTerritory subTerritory = LandSubTerritory.getLandSubTerritoryByLocation(location);

        if(subTerritory == null)return false;
        return subTerritory instanceof Mine || subTerritory instanceof RP;
    }

    @Override public boolean equals(Object o){
        if(!(o instanceof OrganisationPlayer))return false;
        return ((OrganisationPlayer) o).getUuid().equals(this.getUuid());
    }
}
