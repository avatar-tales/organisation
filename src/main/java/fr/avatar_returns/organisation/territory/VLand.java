package fr.avatar_returns.organisation.territory;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.VLandPacket;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public class VLand {

    private static ArrayList<VLand> lstVland = new ArrayList<>();

    private int id;
    private String serverId;
    private String wgRegionName;
    private int ownerId;
    private boolean isActive;
    private boolean isMainLand;
    private long ownedTime;
    private boolean canOtherGoIn;
    private String provinceName;
    private String name;
    private String fhomeCoordonate;

    public VLand(int id,
                 String serverId,
                 String wgRegionName,
                 int ownerId,
                 boolean isActive,
                 boolean isMainLand,
                 long ownedTime,
                 boolean canOtherGoIn,
                 String name,
                 String provinceName){

        this.id = id;
        this.serverId = serverId;
        this.wgRegionName = wgRegionName;
        this.ownerId = ownerId;
        this.isActive = isActive;
        this.isMainLand = isMainLand;
        this.ownedTime = ownedTime;
        this.canOtherGoIn = canOtherGoIn;
        this.name = name;
        this.provinceName = provinceName;
        this.fhomeCoordonate = "";

        VLand.lstVland.add(this);
    }

    public int getId() {return id;}
    public String getServerId() {return serverId;}
    public int getOwnerId() {return ownerId;}
    public long getOwnedTime() {return ownedTime;}
    public String getWgRegionName() {return wgRegionName;}
    public String getName() {return name;}
    public String getProvinceName(){return this.provinceName;}
    public boolean isActive() {return isActive;}
    public boolean isMainLand() {return isMainLand;}
    public boolean canOtherGoIn() {return canOtherGoIn;}

    public void setCanOtherGoIn(boolean canOtherGoIn) {this.canOtherGoIn = canOtherGoIn;}
    public void setName(String name) {this.name = name;}
    public void setProvinceName(String provinceName) {this.provinceName = provinceName;}
    public void setActive(boolean active) {isActive = active;}
    public void setMainLand(boolean mainLand) {isMainLand = mainLand;}
    public void setOwnedTime(long ownedTime) {this.ownedTime = ownedTime;}
    public void setOwnerId(int ownerId) {this.ownerId = ownerId;}

    public void setFhome(String fhomeCoordonate){this.setFhome(fhomeCoordonate, true);}
    public void setFhome(String fhomeCoordonate, boolean sync){
        this.fhomeCoordonate = fhomeCoordonate;
        //if(fhomeCoordonate.contains(":"))Organisation.log.info("RECEPTION VLAND FHOME pour "+ this.getName() + " à ->"+ fhomeCoordonate);
        if(sync)GeneralMethods.sendPacket(new VLandPacket(this, OrganisationPacket.Action.UPDATE));
    }
    public String getFhome(){
        // La fonction getFhome retourne "" quand le land n'a pas de fhome
        return this.fhomeCoordonate;
    }

    public static ArrayList<VLand> getLstVland() {return lstVland;}
    @Nullable public static VLand getVlandById(int landId){
        for(VLand vLand : VLand.getLstVland()){
            if(vLand.getId() == landId)return vLand;
        }
        return null;
    }

    public void remove(){
        if(VLand.getLstVland().contains(this))VLand.getLstVland().remove(this);
    }

    public ItemStack getVlandItem(){

        ItemStack itemStack = new ItemStack(Material.GRASS_BLOCK);

        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName("§6§l"+this.getName().replace("_"," "));

        ArrayList<Component> groupDesc = new ArrayList<>();
        if(this.isActive) {
            if(this.getOwnerId() != -1 ) {
                Group group = Group.getGroupById(this.getOwnerId());
                if(group != null) groupDesc.add(Component.text("§8Propriétaire : §7" +group.getName()));

                String capturedTime = "";
                long effectivePlayedTime = System.currentTimeMillis() - this.getOwnedTime();
                long seconds = effectivePlayedTime / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                long days = hours / 24;

                long remainingDays = days % 365;
                long remainingHours = hours % 24;
                long remainingMinutes = minutes % 60;
                long remainingSeconds = seconds % 60;

                capturedTime = remainingHours+" heures, " + remainingMinutes + " minutes et " + remainingSeconds + " secondes";
                if(remainingDays > 0) capturedTime = remainingDays + " jours, " + capturedTime;

                groupDesc.add(Component.text("§8Durée : §7" + capturedTime));
            }
            else{
                groupDesc.add(Component.text("§8Etat : §aDisponible"));
            }
            groupDesc.add(Component.text("§8Ouvert : " + ((this.canOtherGoIn)?"§aOui": "§cNon")));
        }
        else{
            groupDesc.add(Component.text("§8Etat : §cEn travaux"));
        }

        itemMeta.lore(groupDesc);
        itemMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        itemMeta.addItemFlags(ItemFlag.HIDE_DESTROYS);
        itemMeta.addItemFlags(ItemFlag.HIDE_DYE);
        itemMeta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
        itemMeta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);

        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

}
