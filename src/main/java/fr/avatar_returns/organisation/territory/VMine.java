package fr.avatar_returns.organisation.territory;

import fr.avatar_returns.organisation.storedInventory.StoredInventory;
import fr.avatar_returns.organisation.view.GeneralItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.ArrayList;

public class VMine {

    private final String serverId;
    private final VLand vLand;
    private final PositionPointInterest mine;
    private final static ArrayList<VMine> lstVMine = new ArrayList<>();

    public VMine(String serverId, VLand vLand, PositionPointInterest mine){
        this.serverId = serverId;
        this.vLand = vLand;
        this.mine = mine;
        lstVMine.add(this);
    }

    public int getMinRankPlayer() {
        return mine.getMinRankPlayer();
    }
    public int getMaxRankPlayer() {
        return mine.getMaxRankPlayer();
    }
    public int getMinRankGroup() {
        return mine.getMinRankPlayer();
    }
    public int getMaxRankGroup() {
        return mine.getMaxRankGroup();
    }

    public String getServerId() {return serverId;}
    public PositionPointInterest getMine() {return mine;}
    public VLand getVLand() {
        return this.vLand;
    }
    public String getStoredInventoryId() {return this.mine.getOptionalValueOne();}

    @Nullable public StoredInventory getStoredInventory(){
        try {
            return StoredInventory.getStoredInventoryById(Integer.parseInt(this.getStoredInventoryId()));
        }
        catch (NumberFormatException e){
            return null;
        }
    }
    public  String getOptionalValueTwo(){
        return this.mine.getOptionalValueTwo();
    }

    public void setOptionalValueOne(String optionalValueOne) {
        this.setOptionalValueOne(optionalValueOne, true);
    }
    public void setOptionalValueOne(String optionalValueOne, boolean storeDb) {
        this.mine.setOptionalValueOne(optionalValueOne, storeDb);
    }

    public void setOptionalValueTwo(String optionalValueTwo) {
        this.setOptionalValueTwo(optionalValueTwo, true);
    }
    public void setOptionalValueTwo(String optionalValueTwo, boolean storeDb) {
        this.mine.setOptionalValueTwo(optionalValueTwo, storeDb);
    }

    public PositionPointInterest getMinePosition(){return this.mine;}
    public static ArrayList<VMine> getLstVMine(){
        return lstVMine;
    }
    public static ArrayList<VMine> getLstVMine(PositionPointInterest.Type context){
        ArrayList<VMine> lstVMineByContext = new ArrayList<>();
        for(VMine vMine : lstVMine){
            if(vMine.getMine().getContext().equals(context))lstVMineByContext.add(vMine);
        }
        return lstVMineByContext;
    }

    public ItemStack getItem(){

        ItemStack item = new ItemStack(Material.CHEST);

        StoredInventory storedInventory = this.getStoredInventory();
        item = GeneralItem.renameItemStackWithName(item, "§6§lMine");

        GeneralItem.cleanItemMeta(item);
        return item;
    }

    @Nullable public static VMine getByPositionOfInterest(PositionPointInterest positionPointInterest){
        for(VMine vMine : VMine.getLstVMine()){
            if(vMine.getMinePosition().equals(positionPointInterest))return vMine;
        }

        return null;
    }

    public void remove(){
        VMine.lstVMine.remove(this);
    }
}
