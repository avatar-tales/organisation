package fr.avatar_returns.organisation.territory;


import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.util.Direction;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.FlagContext;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.ArrayList;

public abstract class Territory {

    private static ArrayList<Territory> lstTerritory = new ArrayList<>();
    protected boolean canOtherGoIn;
    protected String name;
    private ProtectedRegion worldGuardRegion;

    protected Territory(ProtectedRegion worldGuardRegion) {

        this.worldGuardRegion = worldGuardRegion;
        this.name = worldGuardRegion.getId();
        this.canOtherGoIn = true;

        final StateFlag flyFlag = (com.sk89q.worldguard.protection.flags.StateFlag) WorldGuard.getInstance().getFlagRegistry().get("fly");
        if(flyFlag !=null) this.getWorldGuardRegion().setFlag(flyFlag, StateFlag.State.DENY);

        Territory.getAllTerritory().add(this);
    }
    protected Territory(ProtectedRegion worldGuardRegion, boolean canOtherGoIn) {
        this.name = worldGuardRegion.getId();
        this.canOtherGoIn = canOtherGoIn;
        this.worldGuardRegion = worldGuardRegion;

        final StateFlag flyFlag = (com.sk89q.worldguard.protection.flags.StateFlag) WorldGuard.getInstance().getFlagRegistry().get("fly");
        if(flyFlag !=null) this.getWorldGuardRegion().setFlag(flyFlag, StateFlag.State.DENY);

        Territory.getAllTerritory().add(this);
    }

    public static ArrayList<String> getLstWGId() {
        ArrayList<String> lstTerritoryId =  new ArrayList<>();
        for(Territory territory : Territory.getAllTerritory())lstTerritoryId.add(territory.getWorldGuardRegion().getId());
        return lstTerritoryId;
    }
    public @Nullable ProtectedRegion getWorldGuardRegion() {
        return this.worldGuardRegion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){
        this.setName(name, true);
    }
    public void setName(String name, boolean storeDb) {

        this.name = name;

        if(this instanceof Land) {
            VLand vLand = VLand.getVlandById(this.getId());
            if (vLand != null) vLand.setName(this.name);
        }

        if(storeDb){
            if(this instanceof Land)DBUtils.setLandName((Land) this);
            else if (this instanceof LandSubTerritory)DBUtils.setSubTerritoryName((LandSubTerritory) this, this.name);
        }
    }

    public ArrayList<PositionPointInterest> getPositionPointOfInterest(){
        ArrayList<PositionPointInterest> lstTerritoryPositionPointOfInterest = new ArrayList<>();
        for(PositionPointInterest positionPointInterest : PositionPointInterest.getLstPositionPointInterest()){
            if(positionPointInterest.getTerritory() == this)lstTerritoryPositionPointOfInterest.add(positionPointInterest);
        }
        return  lstTerritoryPositionPointOfInterest;
    }
    public static final ArrayList<PositionPointInterest> getAllPositionPointOfInterest(){
        return PositionPointInterest.getLstPositionPointInterest();
    }

    public static ArrayList<Territory> getAllTerritory(){
        return Territory.lstTerritory;
    }
    public static ArrayList<Land> getAllLand(){
        ArrayList<Land> lstLand = new ArrayList<>();
        for(Territory territory: Territory.getAllTerritory()){
            if(territory instanceof Land)lstLand.add((Land)territory);
        }
        return lstLand;
    }

    public abstract boolean remove();
    public abstract boolean remove(boolean storeDb);

    public abstract int getId();

    public boolean canOtherGoIn() {
        return this.canOtherGoIn;
    }
    public boolean setCanOtherGoIn(boolean canOtherGoIn){return this.setCanOtherGoIn(canOtherGoIn, true);}
    public abstract boolean setCanOtherGoIn(boolean canOtherGoIn, boolean storeDb);

    public @Nullable static Territory getTerritoryByLocation(final Location location){

        if(location == null)return null;

        BlockVector3 position = BlockVector3.at(location.getBlockX(), location.getBlockY(), location.getBlockZ());
        for(Territory territory : Territory.getAllTerritory()){
            if(territory.getWorldGuardRegion().contains(position)) {
                return territory;
            }
        }
        return null;
    }
    public @Nullable static Territory getTerritoryByRegion(ProtectedRegion protectedRegion) {
        for(Territory territory : Territory.getAllTerritory()){
            if(territory.getWorldGuardRegion().equals(protectedRegion))return territory;
        }
        return null;
    }
    public @Nullable static Territory getTerritoryByRegionName(String protectedRegionId) {
        for(Territory territory : Territory.getAllTerritory()){
            if(territory.getWorldGuardRegion().getId().equals(protectedRegionId))return territory;
        }
        return null;
    }

    public ArrayList<Player> getLstPlayerOnTerritory(){
        ArrayList<Player> lstPlayer = new ArrayList<>();
        for(Player player :Bukkit.getOnlinePlayers()){

            Location location = player.getLocation();
            if(location == null)continue;
            BlockVector3 position = BlockVector3.at(location.getBlockX(), location.getBlockY(), location.getBlockZ());
            if(this.getWorldGuardRegion().contains(position))lstPlayer.add(player);
        }
        return lstPlayer;
    }

}

