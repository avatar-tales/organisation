package fr.avatar_returns.organisation.territory;


import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.VMinePacket;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.storedInventory.MineUtils;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Totem;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.*;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import javax.annotation.Nullable;
import java.util.ArrayList;

public class PositionPointInterest {

    public enum Type {

        TOTEM("TOTEM"),
        MINE("MINE"),
        MINE_FIXE("MINE_FIXE"),
        DOMINATION_FLAG("DOMINATION_FLAG"),
        LAND_FLAG("LAND_FLAG"),
        LAND_RESPAWN_POINT("LAND_RESPAWN_POINT"),
        FHOME("FHOME");

        private String context;

        Type(String context) {
            this.context = context;
        }

        public String getContext(){return this.context;}
        public boolean equals (String context){ return context.equals(this.context);}
        public boolean equalsIgnoreCase(String context) {return context.equalsIgnoreCase(this.context);}

        @Override
        public String toString() {
            return this.getContext();
        }
    }

    final private int id;

    final private transient Location location;
    private transient static ArrayList<PositionPointInterest> lstPositionPointInterest = new ArrayList<>();
    private Type context;
    final private transient Territory territory;

    private int minRankPlayer;
    private int maxRankPlayer;
    private int minRankGroup;
    private int maxRankGroup;

    private String optionalValueOne;
    private String optionalValueTwo;

    public PositionPointInterest(Location location, Territory territory, Type context){

        this.territory = territory;
        this.context = context;
        this.location = location;
        this.minRankGroup = 0;
        this.maxRankGroup = -1;
        this.minRankPlayer = 0;
        this.maxRankPlayer = -1;
        this.optionalValueOne = "";
        this.optionalValueTwo = "";

        this.id = DBUtils.addPositionInterest(this);

        lstPositionPointInterest.add(this);
        if(this.context.equals(Type.MINE)){
            VMine vMine = new VMine(GeneralMethods.getServerId(), VLand.getVlandById(((Mine) territory).getLand().getId()) ,  this);
            GeneralMethods.sendPacket(new VMinePacket(vMine, OrganisationPacket.Action.CREATE));
            MineUtils.autoLinkMineToInventory(vMine);
        }
        if(this.context.equals(Type.LAND_FLAG)){

            Land land = null;
            if(this.getTerritory() instanceof Land){
                land = (Land) this.getTerritory();
            }
            else if(this.getTerritory() instanceof LandSubTerritory){
                land = ((LandSubTerritory) this.getTerritory()).getLand();
            }

            if(land != null) {
                PositionPointInterest.setBanner(this, land.getOwner());
            }
        }
        if(this.context.equals(Type.FHOME)){
            VLand vLand = VLand.getVlandById(this.territory.getId());
            if(vLand != null){
                vLand.setFhome(""+this.location.getWorld().getName()+":"+this.location.getBlockX()+":"+(this.location.getBlockY() + 1)+":"+this.location.getBlockZ());
            }
        }
    }
    public PositionPointInterest(int id, Location location, Territory territory, Type context,
                                 int minRankGroup, int maxRankGroup, int minRankPlayer, int maxRankPlayer,
                                 String optionalValueOne, String optionalValueTwo){

        this.id = id;
        this.context = context;
        this.location = location;
        this.territory = territory;

        this.minRankGroup = minRankGroup;
        this.maxRankGroup = maxRankGroup;
        this.minRankPlayer = minRankPlayer;
        this.maxRankPlayer = maxRankPlayer;

        this.optionalValueOne = optionalValueOne;
        this.optionalValueTwo = optionalValueTwo;

        if(this.context.equals(Type.FHOME)){
            VLand vLand = VLand.getVlandById(this.territory.getId());
            if(vLand != null){
                vLand.setFhome(""+this.location.getWorld().getName()+":"+this.location.getBlockX()+":"+(this.location.getBlockY() + 1)+":"+this.location.getBlockZ());
            }
        }
        if(this.context.equals(Type.MINE)){
            VMine vMine = new VMine(GeneralMethods.getServerId(), VLand.getVlandById(((Mine) territory).getLand().getId()) ,  this);
            GeneralMethods.sendPacket(new VMinePacket(vMine, OrganisationPacket.Action.CREATE));
        }

        lstPositionPointInterest.add(this);
    }

    public int getId() {
        return id;
    }
    public Type getContext(){return this.context;}

    public Location getLocation() {
        return location;
    }

    public int getMinRankPlayer() {
        return minRankPlayer;
    }
    public int getMaxRankPlayer() {
        return maxRankPlayer;
    }
    public int getMinRankGroup() {
        return minRankGroup;
    }
    public int getMaxRankGroup() {
        return maxRankGroup;
    }

    public void teleport(OfflinePlayer offlinePlayer){
        if(!offlinePlayer.isOnline())return;

        Player player = (Player) offlinePlayer;
        player.teleport(this.getLocation());
    }

    public static ArrayList<PositionPointInterest> getByContext(Type context) {
        return PositionPointInterest.getByContext(context.getContext());
    }
    public static ArrayList<PositionPointInterest> getByContext(String context) {

        ArrayList<PositionPointInterest> lstPositionPointInterestType = new ArrayList<>();

        for(PositionPointInterest positionPointInterest : PositionPointInterest.getLstPositionPointInterest()){
            if(positionPointInterest.getContext().getContext().equalsIgnoreCase(context))lstPositionPointInterestType.add(positionPointInterest);
        }

        return lstPositionPointInterestType;
    }
    public static ArrayList<PositionPointInterest> getLstPositionPointInterest() {return lstPositionPointInterest;}

    public @Nullable static PositionPointInterest getById(int id){
        for(PositionPointInterest positionPointInterest: PositionPointInterest.getLstPositionPointInterest()){
            if(positionPointInterest.getId() == id)return positionPointInterest;
        }
        return null;
    }
    public @Nullable static PositionPointInterest getByLocation(Location location) {

        if(location == null)return null;

        for(PositionPointInterest positionPointInterest: PositionPointInterest.getLstPositionPointInterest()){
            if(positionPointInterest.getLocation().getBlockX () == location.getBlockX() &&
                    positionPointInterest.getLocation().getBlockY() == location.getBlockY() &&
            positionPointInterest.getLocation().getBlockZ() == location.getBlockZ())return positionPointInterest;
        }
        return null;
    }

    public Territory getTerritory() {return territory;}
    public String getOptionalValueOne() {return optionalValueOne;}
    public  String getOptionalValueTwo(){
        return this.optionalValueTwo;
    }

    public void setOptionalValueOne(String optionalValueOne) {
        this.setOptionalValueOne(optionalValueOne, true);
    }
    public void setOptionalValueOne(String optionalValueOne, boolean storeDb) {
        this.optionalValueOne = optionalValueOne;
        if(storeDb)DBUtils.setPositionInterestOptionalValueOne(this, optionalValueOne);
    }

    public void setOptionalValueTwo(String optionalValueTwo) {
        this.setOptionalValueTwo(optionalValueTwo, true);
    }
    public void setOptionalValueTwo(String optionalValueTwo, boolean storeDb) {
        this.optionalValueTwo = optionalValueTwo;
        if(storeDb)DBUtils.setPositionInterestOptionalValueTwo(this, optionalValueTwo);

    }

    public boolean remove(){return this.remove(true);}
    public boolean remove(boolean storeDb){

        lstPositionPointInterest.remove(this);

        if(this.context.equals(Type.FHOME)){
            VLand vLand = VLand.getVlandById(this.territory.getId());
            if(vLand != null){
                vLand.setFhome("");
            }
        }

        if(storeDb)DBUtils.removePositionInterest(this);
        return  true;
    }

    public static ArrayList<String> getPossibleContext(Territory territory){

        ArrayList<String> lstPossibleContext = new ArrayList<>();
        lstPossibleContext.add(Type.DOMINATION_FLAG.toString());
        lstPossibleContext.add(Type.LAND_FLAG.toString());

        if(territory instanceof Mine){
            lstPossibleContext.add(Type.MINE.toString());
            lstPossibleContext.add(Type.MINE_FIXE.toString());
        }
        else if(territory instanceof Totem){
            lstPossibleContext.add(Type.TOTEM.toString());
        }
        else if(territory instanceof Land){
            lstPossibleContext.add(Type.FHOME.toString());
            lstPossibleContext.add(Type.LAND_RESPAWN_POINT.toString());
        }
        return lstPossibleContext;
    }
    @Nullable public static Type getContextFromString(String type) {


        if(Type.DOMINATION_FLAG.equals(type)){
            return Type.DOMINATION_FLAG;
        }
        else if(Type.LAND_FLAG.equals(type)){
            return Type.LAND_FLAG;
        }
        else if(Type.MINE.equals(type)){
            return Type.MINE;
        }
        else if(Type.MINE_FIXE.equals(type)){
            return Type.MINE_FIXE;
        }
        else if(Type.TOTEM.equals(type)){
            return Type.TOTEM;
        }
        else if(Type.FHOME.equals(type)){
            return Type.FHOME;
        }
        else if(Type.LAND_RESPAWN_POINT.equals(type)){
            return Type.LAND_RESPAWN_POINT;
        }

        return null;
    }

    public static void setBanner(PositionPointInterest positionPointInterest, Group group){

        if(group == null)return;
        if(positionPointInterest == null)return;
        if(group.getFlag() == null)return;

        if((positionPointInterest.getContext().equals(Type.LAND_FLAG) ||  positionPointInterest.getContext().equals(Type.DOMINATION_FLAG)) && group.getFlag().getType().name().toUpperCase().contains("BANNER")){


            String blockType = positionPointInterest.getLocation().getBlock().getType().name();
            ItemStack flag = (group.getFlag() != null)? group.getFlag() : new ItemStack(Material.RED_BANNER);

            Block block = positionPointInterest.getLocation().getBlock();
            block.setType(flag.getType());

            if (block.getState() instanceof Banner) {
                Banner banner = (Banner) block.getState();
                BannerMeta bannerMeta = (BannerMeta) flag.getItemMeta();
                //banner.setBaseColor(bannerMeta.getBaseColor());
                banner.setPatterns(bannerMeta.getPatterns());
                banner.update();
            }


//            if(!blockType.contains("BANNER")) {
//                positionPointInterest.getLocation().getBlock().setType(group.getFlag().getType());
//                return;
//            }
//
//            byte dataValue = group.getFlag().getData().getData();
//            String bannerColor = DyeColor.getByDyeData(dataValue).toString().toUpperCase();
//
//            Material material = Material.valueOf(bannerColor + "_" + blockType.split("_",2)[1]);
//
//            System.out.println(positionPointInterest.getLocation().getBlock().getBlockData());
//
//            BlockFace facing = null;
//            Rotatable rotatable = null;
//
//            if(positionPointInterest.getLocation().getBlock().getState().getBlockData() instanceof Directional)facing =  ((Directional) positionPointInterest.getLocation().getBlock().getState().getBlockData().clone()).getFacing();
//            if(positionPointInterest.getLocation().getBlock().getState().getBlockData() instanceof Rotatable)rotatable =  ((Rotatable) positionPointInterest.getLocation().getBlock().getState().getBlockData().clone());
//
//            positionPointInterest.getLocation().getBlock().setType((material == null) ? group.getFlag().getType() : material);
//
//            // System.out.println(" FACING = " + facing);
//            // System.out.println(" ROTATION = " + rotatable);
//
//            Banner bannerBlock = (Banner) positionPointInterest.getLocation().getBlock().getState();
//            if(rotatable != null)((Rotatable) bannerBlock.getBlockData()).setRotation(rotatable.getRotation());
//            if(facing != null)((Directional)bannerBlock.getBlockData()).setFacing(facing);
//
//            BannerMeta bannerMeta = (BannerMeta) group.getFlag().getItemMeta();
//            bannerBlock.setPatterns(bannerMeta.getPatterns());
//            bannerBlock.update();
        }
    }
}
