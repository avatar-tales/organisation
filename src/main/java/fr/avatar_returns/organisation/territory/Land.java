package fr.avatar_returns.organisation.territory;

import com.sk89q.worldedit.math.BlockVector2;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.VLandPacket;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Totem;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.jetbrains.annotations.Nullable;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class Land extends Territory{

    private ArrayList<LandSubTerritory> lstSubTerritory;
    private LargeGroup owner;
    private boolean isActive;
    private boolean isMainLand;
    private long ownedTime;
    final private int id;

    public Land(ProtectedRegion worldGuardRegion, Province province) {

        super(worldGuardRegion);

        this.lstSubTerritory = new ArrayList<>();
        this.isMainLand = false;
        this.isActive = true;
        this.ownedTime = 0;

        if(province != null) province.addLand(this);

        this.id = DBUtils.addLand(this);
        this.getWorldGuardRegion().setFlag(Flags.BUILD, StateFlag.State.ALLOW);
    }

    public Land(int id, ProtectedRegion worldGuardRegion, Province province,LargeGroup owner,  boolean isMainLand, String name, boolean canOtherGoIn, boolean isActive, long ownedTime) {
        // Constructor Land from DB.
        // This Land will not be store.

        super(worldGuardRegion, canOtherGoIn);

        this.id = id;
        this.name = name;
        this.owner = owner;
        this.isMainLand = isMainLand;
        this.isActive = isActive;
        this.lstSubTerritory = new ArrayList<>();
        this.ownedTime = ownedTime;

        if(province != null) province.addLand(this, false);

        this.getWorldGuardRegion().setFlag(Flags.BUILD, StateFlag.State.ALLOW);
    }

    @Override public int getId() {return this.id;}
    public @Nullable Province getProvince() {
        for(Province province:Province.getLstProvince()){
            if(province.getLstLand().contains(this))return province;
        }
        return null;
    }

    @Override public boolean remove(){
        return this.remove(true);
    }
    public boolean remove(boolean storeDb){

        Province province = this.getProvince();
        boolean result = true;

        Organisation.worldRgManager.removeRegion(this.getWorldGuardRegion().getId());
        for(LandSubTerritory landSubTerritory: this.getLstSubTerritory()){
            result &= landSubTerritory.remove();
        }

        for(PositionPointInterest positionPointInterest : this.getPositionPointOfInterest())positionPointInterest.remove();
        if(province != null) result &= province.removeLand(this);
        result &= Territory.getAllTerritory().remove(this);

        if(storeDb) {
            DBUtils.removeLand(this);
        }

        VLand vLand = VLand.getVlandById(this.getId());
        if(vLand != null) {
            vLand.remove();
        }

        return result;
    }

    public ArrayList<LandSubTerritory> getLstSubTerritory(){
        return this.lstSubTerritory;
    }
    public void setProvince(Province province){
        this.setProvince(province, true);
    }
    public void setProvince(Province province, boolean storeDb){

        Province currentProvince  = this.getProvince();

        if(currentProvince != null)currentProvince.removeLand(this, storeDb);
        if(province != null) province.addLand(this, storeDb);
    }

    public ArrayList<Mine> getLstMine(){
        ArrayList<Mine> lstMine = new ArrayList<>();
        for(LandSubTerritory landSubTerritory : this.getLstSubTerritory()){
            if(landSubTerritory instanceof Mine)lstMine.add((Mine)landSubTerritory);
        }
        return lstMine;
    }
    public ArrayList<Totem> getLstTotem(){
        ArrayList<Totem> lstTotem = new ArrayList<>();
        for(LandSubTerritory landSubTerritory : this.getLstSubTerritory()){
            if(landSubTerritory instanceof Totem)lstTotem.add((Totem)landSubTerritory);
        }
        return lstTotem;
    }

    public void addSubTerriTorry(LandSubTerritory subTerritory){
        this.addSubTerriTorry(subTerritory, true);
    }
    public void addSubTerriTorry(LandSubTerritory subTerritory, boolean storeDb){
        if(this.getLstSubTerritory().contains(subTerritory))return;
        lstSubTerritory.add(subTerritory);
    }
    public ArrayList<LandSubTerritory> getSubTerritory() {
        return lstSubTerritory;
    }

    public boolean removeSubTerriTorry(LandSubTerritory subTerritory){
        return this.removeSubTerriTorry(subTerritory, true);
    }
    public boolean removeSubTerriTorry(LandSubTerritory subTerritory, boolean storeDb){

        boolean result = true;
        result &= subTerritory.remove(storeDb);
        result &= this.lstSubTerritory.remove(subTerritory);
        return result;
    }

    public boolean isOwnableLand(){return this.isLandReady() && this.getOwner() == null;}
    public boolean isLandReady(){
        return this.getLstMine().size() >0 && this.getLstTotem().size() >0 && this.getProvince() != null && this.isActive();
    }
    public boolean setOwner(LargeGroup owner){
        return this.setOwner(owner, true);
    }
    public boolean setOwner(LargeGroup owner, boolean storeDb){

        if(!this.isLandReady() && owner != null){
            return false;
        }

        this.owner = owner;

        VLand vLand = VLand.getVlandById(this.getId());
        if(vLand != null) vLand.setOwnerId((owner == null)? -1 : owner.getId());

        if(storeDb)DBUtils.setLandOwner(this);
        this.setOwnedTime(System.currentTimeMillis(), storeDb);

        return true;
    }

    public @Nullable LargeGroup getOwner(){ return this.owner;}
    public void removeOwner(){
        this.removeOwner(true);
    }
    public void removeOwner(boolean storeDb){
        this.owner = null;

        VLand vLand = VLand.getVlandById(this.getId());
        if(vLand != null) vLand.setOwnerId(-1);

        DBUtils.setLandOwner(this);
        this.setMainLand(false, storeDb);
    }

    public @Nullable static Land getLandByLocation(final Location location){

        if(location == null)return null;

        BlockVector3 position = BlockVector3.at(location.getBlockX(), location.getBlockY(), location.getBlockZ());
        for(Land land : Territory.getAllLand()){
            if(land.getWorldGuardRegion().contains(position)) {
                return land;
            }
        }
        return null;
    }
    public @Nullable static Land getLandByRegion(ProtectedRegion protectedRegion) {
        for (Land land : Land.getAllLand()) {
            if (land.getWorldGuardRegion().equals(protectedRegion)) return land;
        }
        return null;
    }
    public @Nullable static Land getLandByRegionName(String protectedRegionId) {

        for(Land land : Land.getAllLand()){
            if(land.getWorldGuardRegion().getId().equals(protectedRegionId))return land;
        }
        return null;
    }
    public static ArrayList<Land> getLstLandByGroup(Group group){

        ArrayList<Land> lstGroupLand = new ArrayList<>();
        for(Land land:Land.getAllLand()){
            if(land.getOwner() == null)continue;
            if(land.getOwner().equals(group))lstGroupLand.add(land);
        }

        return lstGroupLand;
    }

    public @Nullable static Land getLandById(int id){
        for(Land land : Land.getAllLand()){
            if(land.getId() == id)return land;
        }
        return null;
    }

    public boolean setMainLand(boolean status) {
        return this.setMainLand(status, true);
    }
    public boolean setMainLand(boolean status, boolean storeDb) {

        VLand vLand = VLand.getVlandById(this.getId());
        if(owner == null){
            this.isMainLand = false;

            if(vLand != null) vLand.setMainLand(false);
            if(storeDb)DBUtils.setIsMainLand(this);
            return false;
        }

        Land mainLand = owner.getMainLand();
        if(mainLand != this && mainLand != null) {
            this.isMainLand = false;

            if(vLand != null) vLand.setMainLand(false);
            if(storeDb)DBUtils.setIsMainLand(this);

            return false;
        }

        this.isMainLand = status;

        if(vLand != null) vLand.setMainLand(this.isMainLand);
        if(storeDb)DBUtils.setIsMainLand(this);

        return true;
    }
    public boolean isMainLand(){
        return this.isMainLand(true);
    }

    public boolean isMainLand(boolean storeDb){

        if(owner == null) {
            if(storeDb)this.setMainLand(false);
            return false;
        }

        return this.isMainLand;
    }

    public boolean setIsActive(boolean isActive){return this.setIsActive(isActive, true);}
    public boolean setIsActive(boolean isActive, boolean storeDb){
        this.isActive = isActive;

        VLand vLand = VLand.getVlandById(this.getId());
        if(vLand != null) vLand.setActive(isActive);

        if(storeDb) DBUtils.setLandIsActive(this);

        return true;
    }
    public boolean isActive() {
        return this.isActive;
    }

    @Override public boolean setCanOtherGoIn(boolean canOtherGoIn, boolean storeDb){
        this.canOtherGoIn = canOtherGoIn;

        VLand vLand = VLand.getVlandById(this.getId());
        if(vLand != null) vLand.setCanOtherGoIn(canOtherGoIn);

        if(storeDb)DBUtils.setLandCanOtherGoIn(this);

        return  true;
    }

    public long getOwnedTime() {
        return ownedTime;
    }
    public void setOwnedTime(long ownedTime){ this.setOwnedTime(ownedTime, true);}
    public void setOwnedTime(long ownedTime, boolean storeDb) {
        this.ownedTime = ownedTime;

        VLand vLand = VLand.getVlandById(this.getId());
        if(vLand != null) vLand.setOwnedTime(ownedTime);

        if(storeDb) DBUtils.setLandOwnedTime(this);
    }

    public boolean canHaveThisSubTerritory(ProtectedRegion subTerritoryWorldGuard) {
        return LandSubTerritory.canBeSubTerritory(this, subTerritoryWorldGuard);
    }

    public boolean containRegion(ProtectedRegion protectedRegion){

        for(BlockVector2 point: protectedRegion.getPoints()){
            if(this.getWorldGuardRegion().contains(point)){
                return true;
            }
        }

        return  false;
    }

    public ArrayList<Player> getPlayerOnLand(){
        return this.getLstPlayerOnTerritory();
    }
}
