package fr.avatar_returns.organisation.territory.renderland.commands;

import fr.alessevan.api.commands.AvatarCommand;
import fr.alessevan.api.commands.RegisterCommand;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.territory.renderland.render.RendererTask;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

@RegisterCommand(
        name = "renderland",
        description = "Render a land",
        permission = "organisaiton.renderland",
        aliases = { "render" },
        sender = RegisterCommand.Sender.PLAYER
)
public class CommandRender implements AvatarCommand {

    @Override
    public boolean execute(CommandSender commandSender, String s, List<String> list) {
        if (commandSender instanceof Player player)
            return execute(player, s, list);
        return false;
    }

    @Override
    public boolean execute(Player player, String commandLabel, List<String> args) {
        new RendererTask(player).runTaskTimer(Organisation.plugin, 0, 2);
        return false;
    }
}
