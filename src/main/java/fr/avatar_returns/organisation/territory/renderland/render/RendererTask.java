package fr.avatar_returns.organisation.territory.renderland.render;

import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.HeightMap;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

public class RendererTask extends BukkitRunnable {

    private Player player;
    private Material material;
    private final long instant;
    private RenderState state;
    private Location currentLocation;
    private List<Location> border;
    private ArrayDeque<Location> queue;
    private final int operationsByStep = 5_000;
    private int currentStep = 0;

    public RendererTask(Player player) {
        this.instant = System.currentTimeMillis();
        if (!player.isOnline())
            return;
        this.player = player;
        this.currentLocation = player.getLocation().add(0, -1, 0);
        this.material = this.currentLocation.getBlock().getType();
        this.state = RenderState.SEARCHING_BORDER;
        this.border = new ArrayList<>();
        this.queue = new ArrayDeque<>();
    }

    @Override
    public void run() {
        long time = (System.currentTimeMillis() - this.instant) / 1000;
        if (!this.player.isOnline()) {
            this.cancel();
            return;
        } else {
            // keep chunks loaded
            this.player.teleport(this.currentLocation.clone().add(0, 1, 0));
            if (this.state == RenderState.SEARCHING_BORDER)
                this.player.sendActionBar(LegacyComponentSerializer.legacyAmpersand().deserialize("&c&lSEARCHING BORDER &4&l(&c&l" + this.currentStep + " &4&lin &c&l" + time + "s&4&l)"));
            else if (this.state == RenderState.RESOLVE_BORDER)
                this.player.sendActionBar(LegacyComponentSerializer.legacyAmpersand().deserialize("&c&lRESOLVE BORDER &4&l(&c&l" + this.currentStep + " &4&lin &c&l" + time + "s&4&l)"));
            else if (this.state == RenderState.RENDERING)
                this.player.sendActionBar(LegacyComponentSerializer.legacyAmpersand().deserialize("&c&lRENDERING &4&l(&c&l" + this.currentStep + " &4&lin &c&l" + time + "s&4&l)"));
        }
        for (int i = 0; i < this.operationsByStep; i++) {
            // get a edge of the land
            if (this.state == RenderState.SEARCHING_BORDER) {
                this.currentLocation = this.currentLocation.add(0, 0, 1);
                if (this.getBlock(this.currentLocation.getWorld(), this.currentLocation.getBlockX(), this.currentLocation.getBlockZ()).getType() != this.material) {
                    this.currentLocation = this.currentLocation.add(0, 0, -1);
                    this.border.add(this.currentLocation);
                    this.queue.push(this.currentLocation);
                    this.state = RenderState.RESOLVE_BORDER;
                    this.player.sendMessage("Found border at x=" + this.currentLocation.getBlockX() + ",z=" + this.currentLocation.getBlockZ() + " in " + time + "s");
                    this.currentStep = 0;
                    return;
                }
            } else if (this.state == RenderState.RESOLVE_BORDER) {
                if (this.queue.isEmpty()) {
                    this.state = RenderState.RENDERING;
                    this.player.sendMessage("Border resolved in " + time + "s");
                    this.currentStep = 0;
                    continue;
                }
                Block block = this.getNextBlock(this.currentLocation);
                if (block == null) {
                    this.currentLocation = this.queue.pop();
                    this.border.add(this.currentLocation);
                    continue;
                }
                this.currentLocation = block.getLocation();
                if (this.border.size() > 1) {
                    Vector vector = this.border.get(this.border.size() - 1).toVector().subtract(this.border.get(this.border.size() - 2).toVector());
                    Vector newVector = this.currentLocation.toVector().subtract(this.border.get(this.border.size() - 1).toVector());
                    if (vector.dot(newVector) == 0)
                        this.queue.removeFirstOccurrence(this.border.remove(this.border.size() - 1));
                }
                this.border.add(this.currentLocation);
                this.queue.push(this.currentLocation);
            } else if (this.state == RenderState.RENDERING) {
                if (this.border.isEmpty()) {
                    this.player.sendMessage(LegacyComponentSerializer.legacyAmpersand().deserialize("FINISHED"));
                    this.player.performCommand("/expand vert");
                    this.state = RenderState.FINISHED;
                    this.cancel();
                    continue;
                }
                Location location = this.border.remove(0);
                if (this.currentStep == 0) {
                    this.player.performCommand("/sel");
                    this.player.performCommand("/sel poly");
                    this.player.performCommand("/pos1 " + location.getBlockX() + "," + location.getBlockY() + "," + location.getBlockZ());
                } else {
                    this.player.performCommand("/pos2 " + location.getBlockX() + "," + location.getBlockY() + "," + location.getBlockZ());
                }
            }
            this.currentStep++;
        }
    }

    public Block getBlock(World world, int x, int z) {
        return world.getHighestBlockAt(x, z, HeightMap.OCEAN_FLOOR);
    }

    public Block getNextBlock(Location location) {
        Block block;
        Block nextBlock = null;
        int neighbours;
        int nextNeighbours = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if ((i + j) % 2 == 0)
                    continue;
                block = this.getBlock(location.getWorld(), location.getBlockX() + i, location.getBlockZ() + j);
                if (block.getType() != this.material)
                    continue;
                if (this.border.contains(block.getLocation()))
                    continue;
                neighbours = calculateNeighbours(block.getLocation());
                if (neighbours > nextNeighbours) {
                    nextNeighbours = neighbours;
                    nextBlock = block;
                }
            }
        }
        return nextBlock;
    }

    private int calculateNeighbours(Location location) {
        int neighbours = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (i == 0 && j == 0)
                    continue;
                neighbours += this.getBlock(location.getWorld(), location.getBlockX() + i, location.getBlockZ() + j).getType() == this.material ? 0 : 1;
            }
        }
        return neighbours;
    }

    enum RenderState {
        SEARCHING_BORDER,
        RESOLVE_BORDER,
        RENDERING,
        FINISHED;
    }

}
