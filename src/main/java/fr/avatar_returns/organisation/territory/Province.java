package fr.avatar_returns.organisation.territory;

import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.largeGroup.Faction;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.cosmetics.SkullCreator;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.Nullable;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.HashMap;

public class Province{

    private String name;
    final private String id;
    private String description;
    private ArrayList<Land> lstLand;
    private static ArrayList<Province> lstProvince = new ArrayList();

    public Province(String id) {

        this.id = id;
        this.name = "";
        this.description = "";

        this.lstLand = new ArrayList<>();
        Province.lstProvince.add(this);
        DBUtils.addProvince(this);
    }

    public Province(String id, String name, String description, boolean storeDb) {
        this.id = id;
        this.name = name;
        this.description = description;

        this.lstLand = new ArrayList<>();
        Province.lstProvince.add(this);

        if(storeDb)DBUtils.addProvince(this);
    }

    public String getId() {
        return this.id;
    }

    public void setName(String name){
        setName(name,true);
    }
    public void setName(String name, boolean storeDb) {
        this.name = name;
        if(storeDb)DBUtils.setProvinceName(this, name);
    }
    public String getName(){
        return (this.name != "")?this.name: "Unamed";
    }

    public void setDescription(String description) {
        this.setDescription(description, true);
    }
    public void setDescription(String description, boolean storeDb){
        this.description = description;
        if(storeDb) DBUtils.setProvinceDescription(this, description);
    }

    public String getDescription(){
        return (this.description != "")?this.description: "No desc";
    }

    public ArrayList<Land> getLstLand(){
        return this.lstLand;
    }
    public ArrayList<String> getLstLandWGId(){
        ArrayList<String> lstWGId = new ArrayList<>();
        for(Land land : this.getLstLand()){
            lstWGId.add(land.getWorldGuardRegion().getId());
        }
        return lstWGId;
    }


    public static ArrayList<Province> getLstProvince() { return Province.lstProvince; }

    public boolean addLand(Land land) {
        return this.addLand(land, true);
    }
    public boolean addLand(Land land, boolean storeDB) {

        this.lstLand.add(land);

        VLand vLand = VLand.getVlandById(land.getId());
        if(vLand != null) vLand.setProvinceName(this.getName());

        if(storeDB) DBUtils.setLandProvince(land);
        return true;
    }

    public boolean removeLand(Land land){
        return this.removeLand(land, true);
    }
    public boolean removeLand(Land land, boolean storeDb){

        this.lstLand.remove(land);

        VLand vLand = VLand.getVlandById(land.getId());
        if(vLand != null) vLand.setProvinceName("");

        if(storeDb) DBUtils.setLandProvince(land);
        return true;
    }

    public boolean remove(){
        return this.remove(true, true);
    }

    public boolean remove(boolean removeLand, boolean storeDb){

        boolean result = true;
        for(Land land : this.getLstLand()) {
            if(removeLand)result &= land.remove();
            else land.setProvince(null);
        }
        result &= Province.lstProvince.remove(this);

        if(storeDb)DBUtils.removeProvince(this);

        return result;
    }

    public @Nullable static Province getProvinceByLocation(Location location){

        if(location == null)return null;

        for(Province province : Province.lstProvince){
            for(Land land : province.getLstLand()){
                if(land.getWorldGuardRegion().contains(location.getBlockX(), location.getBlockY(), location.getBlockZ())){
                    return province;
                }
            }
        }
        return null;
    }
    public @Nullable static Province getProvinceById(String id){

        for(Province province : Province.getLstProvince()){
           if(province.getId().equalsIgnoreCase(id))return province;
        }
        return null;
    }
    public @Nullable Faction getNation(boolean displayHide){

        HashMap<Faction, Integer> mapNation = new HashMap<Faction, Integer>();
        for(Land land : this.getLstLand()){

            LargeGroup owner = land.getOwner();

            if(owner == null) {
                continue;
            }
            if(!(owner instanceof Faction) || !owner.isVisible()) {
                continue;
            }
            if(owner.getMainLand() == null){
                continue;
            }
            if(!owner.getMainLand().getProvince().equals(this)){
                continue;
            }

            if(!mapNation.containsKey(owner)) mapNation.put((Faction) owner, 0);
            mapNation.put((Faction) owner, mapNation.get(owner)+1);

        }

        int max=0;
        Faction groupOwner = null;
        for(Faction faction : mapNation.keySet()){
            int groupNbLand = mapNation.get(faction);
            if(groupNbLand > max){
                max = groupNbLand;
                groupOwner = faction;
            }
        }
        return groupOwner;
    }

    public ItemStack getProvinceItem(){
        ItemStack provinceItem = SkullCreator.itemFromUrl("http://textures.minecraft.net/texture/b1dd4fe4a429abd665dfdb3e21321d6efa6a6b5e7b956db9c5d59c9efab25");

        ItemMeta tmpMeta = provinceItem.getItemMeta();
        tmpMeta.setDisplayName("§6§l"+this.getName().replace("_"," "));
        provinceItem.setItemMeta(tmpMeta);

        return provinceItem;
    }

}
