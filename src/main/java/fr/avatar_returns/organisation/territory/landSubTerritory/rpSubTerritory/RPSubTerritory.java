package fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory;

import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;

public abstract class RPSubTerritory extends LandSubTerritory {

    protected RPSubTerritory(ProtectedRegion worldGuardRegion, Land land) {

        super(worldGuardRegion, land);

        this.getWorldGuardRegion().setFlag(Flags.BUILD, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.BLOCK_BREAK, StateFlag.State.DENY);
    }

    public RPSubTerritory(int id, String name, ProtectedRegion worldGuardRegion, Land land, boolean canOtherGoIn) {
        super(id, name, worldGuardRegion, land, canOtherGoIn);

        this.getWorldGuardRegion().setFlag(Flags.BUILD, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.BLOCK_BREAK, StateFlag.State.DENY);
    }
}
