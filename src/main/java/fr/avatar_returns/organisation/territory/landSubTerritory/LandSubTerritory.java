package fr.avatar_returns.organisation.territory.landSubTerritory;

import com.sk89q.worldedit.math.BlockVector2;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.territory.Territory;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.ArrayList;

public abstract class LandSubTerritory extends Territory {

    private final Land land;
    private final int id;

    protected LandSubTerritory(ProtectedRegion worldGuardRegion, Land land) {

        super(worldGuardRegion);
        this.land = land;
        this.land.addSubTerriTorry(this );
        this.id = DBUtils.addLandSubTerritory(this);
    }
    protected LandSubTerritory(int id, String name, ProtectedRegion worldGuardRegion, Land land, boolean canOtherGoIn) {

        super(worldGuardRegion, canOtherGoIn);
        this.id = id;
        this.land = land;
        this.land.addSubTerriTorry(this);
        this.setName(name, false);
    }

    @Override public int getId() {return this.id;}
    @Override public String getName() {
        if (super.getName().equals(this.getWorldGuardRegion().getId())) return "";
        return super.getName();
    }

    @Override public boolean remove(){
        return this.remove(true);
    }
    @Override public boolean remove(boolean storeDb){

        Organisation.worldRgManager.removeRegion(this.getWorldGuardRegion().getId());
        Territory.getAllTerritory().remove(this);

        for(PositionPointInterest positionPointInterest : this.getPositionPointOfInterest())positionPointInterest.remove();

        if(storeDb) DBUtils.removeLandSubTerritory(this);
        return true;
    }

    public static ArrayList<ProtectedRegion> getFreeLandWordGuardRegion(Land land){

        ArrayList<ProtectedRegion> lstFreeLandRegion =  new ArrayList<>();

        for (ProtectedRegion protectedRegion : Organisation.worldRgManager.getRegions().values()) {
            if(LandSubTerritory.canBeSubTerritory(land, protectedRegion)) lstFreeLandRegion.add(protectedRegion);
        }

        return lstFreeLandRegion;
    }
    public static boolean canBeSubTerritory(Land land, ProtectedRegion protectedRegion){

        if (Territory.getLstWGId().contains(protectedRegion.getId())) return false;

        boolean subRegion = true;
        for(BlockVector2 point: protectedRegion.getPoints()){
            // If one of all point of protectedRegion is no include in the Land WGRegion.
            // That's means that this regions could not be used as a SubTerritory

            if(!land.getWorldGuardRegion().contains(point)){
                subRegion = false;
                break;
            }
        }

        return  subRegion;
    }

    @Override public boolean setCanOtherGoIn(boolean canOtherGoIn, boolean storeDb) {
        this.canOtherGoIn = canOtherGoIn;
        if(storeDb) DBUtils.setLandSubTerritoryCanOtherGoIn(this, canOtherGoIn);
        return true;
    }

    public Land getLand() {
        return this.land;
    }
    public @Nullable static LandSubTerritory getLandSubTerritoryById(int id){
        for(Territory territory : Territory.getAllTerritory()){
            if(territory.getId() == id && territory instanceof LandSubTerritory)return (LandSubTerritory) territory;
        }
        return null;
    }
    public @Nullable static LandSubTerritory getLandSubTerritoryByRegionName(String protectedRegionId) {
        for(LandSubTerritory landSubTerritory : LandSubTerritory.getAllLandSubTerritory()){
            if(landSubTerritory.getWorldGuardRegion().getId().equals(protectedRegionId))return landSubTerritory;
        }
        return null;
    }
    public @Nullable static LandSubTerritory getLandSubTerritoryByRegion(ProtectedRegion protectedRegion) {
        for(LandSubTerritory landSubTerritory : LandSubTerritory.getAllLandSubTerritory()){
            if(landSubTerritory.getWorldGuardRegion().equals(protectedRegion))return landSubTerritory;
        }
        return null;
    }
    public @Nullable static LandSubTerritory getLandSubTerritoryByLocation(final Location location){

        if(location == null)return null;

        BlockVector3 position = BlockVector3.at(location.getBlockX(), location.getBlockY(), location.getBlockZ());
        for(LandSubTerritory landSubTerritory : LandSubTerritory.getAllLandSubTerritory()){
            if(landSubTerritory.getWorldGuardRegion().contains(position)) {
                return landSubTerritory;
            }
        }
        return null;
    }

    public static ArrayList<LandSubTerritory> getAllLandSubTerritory(){

        ArrayList<LandSubTerritory> lstLandSubTerritory = new ArrayList<>();
        for(Territory territory: Territory.getAllTerritory()){
            if(territory instanceof LandSubTerritory)lstLandSubTerritory.add((LandSubTerritory) territory);
        }

        return lstLandSubTerritory;
    }
    public ArrayList<Player> getPlayerOnSubTerritory(){
        return this.getLstPlayerOnTerritory();
    }
}
