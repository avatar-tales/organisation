package fr.avatar_returns.organisation.territory.landSubTerritory.groupSubTerritory;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;

public abstract class GroupSubTerritory extends LandSubTerritory {

    protected GroupSubTerritory(ProtectedRegion worldGuardRegion, Land land) {
        super(worldGuardRegion, land);
    }

    protected GroupSubTerritory(int id, String name, ProtectedRegion worldGuardRegion, Land land, boolean canOtherGoIn) {
        super(id, name, worldGuardRegion, land, canOtherGoIn);
    }

}
