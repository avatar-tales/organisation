package fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory;

import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.avatar_returns.organisation.territory.Land;

public class Contest extends RPSubTerritory{
    public Contest(ProtectedRegion worldGuardRegion, Land land) {
        super(worldGuardRegion, land);
        final StateFlag bendingflag = (StateFlag) WorldGuard.getInstance().getFlagRegistry().get("bending");

        this.getWorldGuardRegion().setFlag(Flags.BUILD, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.PVP,  StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.MOB_DAMAGE,  StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.DAMAGE_ANIMALS,  StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.INTERACT, StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.INVINCIBILITY, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.USE_ANVIL, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.USE_DRIPLEAF, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.DAMAGE_ANIMALS, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.USE, StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.CHEST_ACCESS, StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.LEAF_DECAY, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.FIRE_SPREAD, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.LAVA_FIRE, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.CREEPER_EXPLOSION, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.OTHER_EXPLOSION, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.LEAF_DECAY, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.VINE_GROWTH, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.ICE_FORM, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.ICE_MELT, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.SNOW_FALL, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.SNOW_MELT, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.LAVA_FIRE, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.FIRE_SPREAD, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.LIGHTNING, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.MOB_SPAWNING, StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.SOIL_DRY, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.BLOCK_BREAK, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.BLOCK_PLACE, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.PLACE_VEHICLE, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.DESTROY_VEHICLE, StateFlag.State.ALLOW);
        if(bendingflag !=null) this.getWorldGuardRegion().setFlag(bendingflag, StateFlag.State.ALLOW);
    }

    public Contest(int id, String name, ProtectedRegion worldGuardRegion, Land land, boolean canOtherGoIn) {

        super(id, name, worldGuardRegion, land, canOtherGoIn);
        final StateFlag bendingflag = (StateFlag) WorldGuard.getInstance().getFlagRegistry().get("bending");

        this.getWorldGuardRegion().setFlag(Flags.BUILD, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.PVP,  StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.INTERACT, StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.USE_ANVIL, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.USE_DRIPLEAF, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.DAMAGE_ANIMALS, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.USE, StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.CHEST_ACCESS, StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.LEAF_DECAY, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.FIRE_SPREAD, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.LAVA_FIRE, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.CREEPER_EXPLOSION, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.OTHER_EXPLOSION, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.LEAF_DECAY, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.VINE_GROWTH, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.ICE_FORM, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.ICE_MELT, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.SNOW_FALL, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.SNOW_MELT, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.LAVA_FIRE, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.FIRE_SPREAD, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.LIGHTNING, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.MOB_SPAWNING, StateFlag.State.ALLOW);
        this.getWorldGuardRegion().setFlag(Flags.SOIL_DRY, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.BLOCK_BREAK, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.BLOCK_PLACE, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.PLACE_VEHICLE, StateFlag.State.DENY);
        this.getWorldGuardRegion().setFlag(Flags.DESTROY_VEHICLE, StateFlag.State.ALLOW);
        if(bendingflag !=null) this.getWorldGuardRegion().setFlag(bendingflag, StateFlag.State.ALLOW);
    }
}
