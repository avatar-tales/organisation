package fr.avatar_returns.organisation.modules.chat.messaging.packets;

import fr.alessevan.api.AvatarAPI;
import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.modules.chat.listeners.ChatHandler;
import fr.avatar_returns.organisation.rabbitMQPackets.Packet;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import net.kyori.adventure.text.Component;

import java.util.concurrent.TimeoutException;

public class ChatPacket implements Packet {


    private String formatNormal;
    private String formatSpy;
    private String message;
    private boolean isGlobal;
    private boolean coloured;

    public ChatPacket(String formatNormal, String formatSpy, String message, boolean isGlobal,  boolean coloured) {

        this.formatNormal = formatNormal;
        this.formatSpy = formatSpy;
        this.message = message;
        this.isGlobal = isGlobal;
        this.coloured = coloured;
    }


    public static Messaging.MessagingCodec<ChatPacket> getCodec(Organisation organisation) {

        Messaging.MessagingCodec<ChatPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(ChatPacket.class);

        defaultCodec.receive((packet, source) -> {

            try {
                if (source.equals(ConfigManager.getServerId())) return;

                if (packet.isGlobal) {
                    Component componentNormal = AvatarAPI.getAPI().formatMessage(packet.formatNormal);
                    Organisation.plugin.getServer().getOnlinePlayers().forEach(target -> ChatHandler.sendMessage(componentNormal, packet.message, target, packet.coloured));
                } else {
                    Component componentNormal = AvatarAPI.getAPI().formatMessage(packet.formatSpy);
                    Organisation.plugin.getServer().getOnlinePlayers().stream()
                            .filter(target -> target.hasPermission("avatarcore.chat.spy"))
                            .forEach(target -> ChatHandler.sendMessage(componentNormal, packet.message, target, packet.coloured));
                }
            }
            catch (Exception e) {
                Organisation.log.severe("[CHAT PACKET] Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });
        return defaultCodec;
    }

}
