package fr.avatar_returns.organisation.modules.chat.listeners;

import fr.alessevan.api.AvatarAPI;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.OrganisationChatCommand;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.hook.LuckPermsHook;
import fr.avatar_returns.organisation.modules.chat.messaging.packets.ChatPacket;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import net.citizensnpcs.api.CitizensAPI;
import net.kyori.adventure.key.Key;
import net.kyori.adventure.sound.Sound;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.TextReplacementConfig;
import net.kyori.adventure.text.format.Style;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.regex.Pattern;



public class ChatHandler implements Listener {


    @EventHandler(priority = EventPriority.MONITOR) public void onPlayerChat(AsyncPlayerChatEvent event) {
        try {

            if (event.isCancelled())return;
            if (event.getMessage().isBlank()) return;

            Player player = event.getPlayer();
            OrganisationPlayer oPlayer;

            try {
                oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
            } catch (OrganisationPlayerException e) {return;}
            event.setCancelled(true);

            if(oPlayer.isMuted()){
                GeneralMethods.sendPlayerErrorMessage(player, "Organisation.Muted", true);
                return;
            }

            boolean coloured = player.hasPermission("avatarchat.colour");
            boolean isLocalChatEnable = ConfigManager.getConfig().getBoolean("Organisation.chat.local-chat.enable", false);
            boolean isMultiServer = ConfigManager.getConfig().getBoolean("Organisation.chat.multi-server.enable");
            boolean isGlobal = event.getMessage().startsWith(ConfigManager.getConfig().getString("Organisation.chat.local-chat.default-token", "!")) || !isLocalChatEnable;
            String message = (isGlobal)? event.getMessage().substring(1) : event.getMessage();


            if(oPlayer.getChatLocked().equals("GLOBAL"))isGlobal = true;

            //We get faction informations
            String organisationTitle = "";
            String guildeTitle = "";
            for (Group group : oPlayer.getLstGroup()) {

                if (group instanceof LargeGroup) {
                    organisationTitle = "§2[" + group.getName() + "]§f ";
                    if(oPlayer.getChatLocked().equals("FACTION")){
                        OrganisationChatCommand.sendGroupMessage(oPlayer, group, message);
                        return;
                    }
                } else {
                    guildeTitle += "§3(" + group.getName() + ")§f ";
                    if(oPlayer.getChatLocked().equals("GUILDE")){
                        OrganisationChatCommand.sendGroupMessage(oPlayer, group, message);
                        return;
                    }
                }
            }

            String groupName = organisationTitle + guildeTitle;
            String nameColor = (Organisation.getHook(LuckPermsHook.class).isPresent())? LuckPermsHook.getPrefixColors(player.getUniqueId()):  "";

            //We format the message
            String formatNormal = ((isGlobal)? "" : "§a[L]") + ConfigManager.getConfig().getString("Organisation.chat.settings.format");
            formatNormal = formatNormal.replace("{group_name}", groupName);
            formatNormal = formatNormal.replace("{name_color}", nameColor);
            formatNormal = formatNormal.replace("{player}", oPlayer.getName());

            Organisation.log.info(formatNormal);

            String formatSpy = ConfigManager.getConfig().getString("Organisation.chat.local-chat.format.spy");
            formatSpy = formatSpy.replace("{group_name}", groupName);
            formatSpy = formatSpy.replace("{name_color}", nameColor);
            formatSpy = formatSpy.replace("{player}", oPlayer.getName());

            Component componentNormal = AvatarAPI.getAPI().formatMessage(formatNormal);

            List<Player> audience;
            if(!isGlobal) {

                audience = Bukkit.getScheduler().callSyncMethod(Organisation.plugin, () -> {
                    ArrayList<Player> audienceList = new ArrayList<>();

                    int range = hasMegaphone(player) ? ConfigManager.getConfig().getInt("Organisation.chat.local-chat.megaphone.radius", 100) : ConfigManager.getConfig().getInt("Organisation.chat.local-chat.radius", 50);
                    audienceList.add(player);
                    audienceList.addAll(player.getWorld()
                            .getNearbyEntities(player.getLocation(), range, range, range).stream()
                            .filter(entity -> entity instanceof Player)
                            .filter(entity -> CitizensAPI.getNPCRegistry().getNPC(entity) == null)
                            .map(entity -> (Player) entity)
                            .filter(entity -> entity != player)
                            .toList()
                    );

                    // Retourne la liste d'audience après avoir été construite
                    return audienceList;
                }).get();

                Component componentSpy = AvatarAPI.getAPI().formatMessage(formatSpy);
                Organisation.plugin.getServer().getOnlinePlayers().stream()
                        .filter(target -> target.hasPermission("avatarcore.chat.spy"))
                        .filter(target -> !audience.contains(target))
                        .forEach(target -> sendMessage(componentSpy, message, target, coloured));
                Organisation.log.info("[L] ["+groupName+"] " +player.getName() + " : " + message);
            }
            else {
                audience = (List<Player>) Organisation.plugin.getServer().getOnlinePlayers();
                Organisation.log.info("[G] ["+groupName+"] " +player.getName() + " : " + message);
            }

            audience.forEach(target -> sendMessage(componentNormal, message, target, coloured));
            if (isMultiServer)GeneralMethods.sendPacket(new ChatPacket(formatNormal, formatSpy, message, isGlobal, coloured));

        }
        catch (Exception e){Organisation.log.severe(e.getMessage());}
    }

    public static void sendMessage(Component format, String message, Player player, boolean coloured) {
        OrganisationPlayer oTarget = null;
        try{
            oTarget = OrganisationPlayer.getOrganisationPlayer(player);
            if(oTarget.isMuted())return;
        }catch (OrganisationPlayerException e){return;}

        Component messageComponent = formatMessage(coloured ? (TextComponent) AvatarAPI.getAPI().formatMessage(message) : Component.text(message));
        if (message.toLowerCase().contains(player.getName().toLowerCase())) {
            messageComponent = messageComponent.replaceText(TextReplacementConfig.builder()
                    .matchLiteral(player.getName())
                    .replacement(Component.text("@" + player.getName())
                            .style(Style.style(TextColor.color(251, 168, 0), TextDecoration.BOLD))
                    )
                    .build()
            );
            player.playSound(Sound.sound(Key.key("entity.experience_orb.pickup"), Sound.Source.RECORD, 100f, 1f), player);
        }

        format = format.replaceText(TextReplacementConfig.builder().match(Pattern.compile("\\s+")).replacement(" ").build());
        player.sendMessage(format.replaceText(TextReplacementConfig.builder().matchLiteral("{message}").replacement(messageComponent).build()));
    }

    public static Component formatMessage(TextComponent sentence) {

        for (char c : ConfigManager.getConfig().getString("Organisation.chat.settings.forbiddenChars", "").toCharArray()) sentence = (TextComponent) sentence.replaceText(TextReplacementConfig.builder().matchLiteral("" + c).replacement(" ").build());

        Pattern pattern = Pattern.compile("\\s+");
        sentence = (TextComponent) sentence.replaceText(TextReplacementConfig.builder().match(pattern).replacement(" ").build());

        HashMap<String, String> mapSmiley = ConfigManager.getConfig().getObject("Organisation.chat.settings.remap", HashMap.class);
        for(String key : mapSmiley.keySet()){sentence = (TextComponent) sentence.replaceText(TextReplacementConfig.builder().matchLiteral(key).replacement(ConfigManager.getConfig().getString("Organisation.chat.settings.remap." + key, "")).build());}

        return sentence;
    }

    public static boolean hasMegaphone(Player player) {
        if (!ConfigManager.getConfig().getBoolean("Organisation.chat.local-chat.megaphone.enable", false))
            return false;
        ItemStack item1 = player.getInventory().getItemInMainHand();
        ItemStack item2 = player.getInventory().getItemInOffHand();
        return item1.hasItemMeta() && item1.getItemMeta().getPersistentDataContainer().has(AvatarAPI.getAPI().generateOrGetKey(Organisation.plugin, "megaphone")) || item2.hasItemMeta() && item2.getItemMeta().getPersistentDataContainer().has(AvatarAPI.getAPI().generateOrGetKey(Organisation.plugin, "megaphone"));
    }
}
