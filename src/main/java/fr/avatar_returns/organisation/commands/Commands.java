package fr.avatar_returns.organisation.commands;


import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.view.ui.organisationUI.MainOrganisationUI;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;

import java.util.*;

public class Commands {

    private final Organisation plugin;
    public static boolean debugEnabled = false;

    public Commands(final Organisation plugin) {
        this.plugin = plugin;
        debugEnabled = Organisation.plugin.getConfig().getBoolean("debug");
        this.init();
    }

    public static String[] commandaliases = { "o", "f", "organisation", "faction"};

    private void init() {
        final PluginCommand organisation = this.plugin.getCommand("organisation");

        /*
         * Set of all of the Classes which extend Command
         */
        
        new HelpCommand();
        new ListCommand();
        new LandCommand();
        new SubTerritoryCommand();
        new InfoCommand();
        new InventoryCommand();
        new NickNameCommand();
        new JoinCommand();
        new KickCommand();
        new RoleCommand();
        new RankCommand();
        new EditCommand();
        new LeaveCommand();
        new ClaimCommand();
        new PositionPointOfInterestCommand();
        new CreateCommand();
        new InviteCommand();
        new DisbandCommand();
        new RaidCommand();
        new UnClaimCommand();
        new ProvinceCommand();
        new PrespawnCommand();
        new PlayerCommand();

        final CommandExecutor cmdExecutor = (s, c, label, args) -> {

            if (Arrays.asList(commandaliases).contains(label.toLowerCase())) {
                if (args.length > 0) {
                    final List<String> sendingArgs = Arrays.asList(args).subList(1, args.length);
                    for (final OrganisationCommand command : OrganisationCommand.instances.values()) {
                        if (Arrays.asList(command.getAliases()).contains(args[0].toLowerCase())) {
                            command.execute(s, sendingArgs);
                            return true;
                        }
                    }
                }
                else if(s instanceof Player){

                    try {
                        OrganisationPlayer oPlayer = OrganisationPlayer.getOrganisationPlayer((Player) s);
                        if(!oPlayer.isConnected())oPlayer.setIsConnected(true); //Un joueur déconnecté ne peut pas executer une commande...
                        ((Player) s).openInventory(new MainOrganisationUI(oPlayer).getInv());
                    } catch (OrganisationPlayerException e) {}
                }

                OrganisationCommand.instances.get("help").execute(s, new ArrayList<String>());
                return true;
            }

            return false;
        };
        organisation.setExecutor(cmdExecutor);
        organisation.setTabCompleter(new OrganisationTabCompleter());
    }
}
