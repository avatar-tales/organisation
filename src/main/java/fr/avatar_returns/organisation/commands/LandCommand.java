package fr.avatar_returns.organisation.commands;

import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.Province;
import fr.avatar_returns.organisation.territory.Territory;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class LandCommand extends OrganisationCommand{

    public LandCommand() {
        super("land", "/o land <list|register|unregister|setProvince|setIsAvailable|setCanOtherGoIn|setOwner|addSubTerritory|removeSubTerritory|defineMainLand|setName> <landWGName> <provinceWGName|groupOwner|subTerritoryWGName|name|<true/false>>", "Admin command to manage Land",new String[] { "land" });

    }

    @Override public void execute(final CommandSender sender, final List<String> args){

        if(!(sender instanceof Player))return;
        Player player = (Player)sender;

        if(args.size()<1){
            GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
            return;
        }

        if(!(player.hasPermission("organisation.command.land") || player.isOp())){
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.NoPermission");
            return;
        }

        if(args.size()<2 && (args.size()==1 && !args.get(0).equalsIgnoreCase("list"))){
            GeneralMethods.sendPlayerErrorMessage(player, this.getProperUse());
            return;
        }

        String subCommand = args.get(0);

        if(subCommand.equalsIgnoreCase("list")){
            GeneralMethods.sendPlayerMessage(player, "Liste des lands  : ");

            for(Land land: Land.getAllLand()){

                String available = (land.isLandReady())? "§2disponible" : "§cTravaux";
                String owner = (land.getOwner() != null)? "§7 occupé par " + land.getOwner().getName(): available;
                String province = (land.getProvince() == null)? "" : "["+land.getProvince().getId()+"] ";
                String mainLand = (land.isMainLand())? " §6۩" : "";
                player.sendMessage("§6 - " + province +"§e"+land.getName() + " ("+land.getWorldGuardRegion().getId()+") "+owner + mainLand);
            }

            if(Land.getAllLand().size() != 0)player.sendMessage("");
            player.sendMessage("§6Total : "+Land.getAllLand().size());

            if(VLand.getLstVland().size() > Land.getAllLand().size())player.sendMessage("§6Total land (multi-server) : "+VLand.getLstVland().size());
            return;
        }

        String landWGName =  args.get(1);
        if(subCommand.equalsIgnoreCase("register")){

            Territory territory = Territory.getTerritoryByRegionName(landWGName);
            if(territory != null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.TerritoryAlreadyUsed",true);
                return;
            }

            ProtectedRegion landWordGuard = Organisation.worldRgManager.getRegion(landWGName);
            if(landWordGuard == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.WorldGuard.NotFound",true);
                return;
            }

            if (Territory.getLstWGId().contains(landWordGuard.getId())){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.CantRegisterLandOnOtherTerritory",true);
                return;
            }

            landWordGuard.setFlag(Flags.BUILD, StateFlag.State.ALLOW);
            landWordGuard.setFlag(Flags.PVP,  StateFlag.State.ALLOW);
            landWordGuard.setFlag(Flags.INTERACT, StateFlag.State.ALLOW);
            landWordGuard.setFlag(Flags.USE_ANVIL, StateFlag.State.ALLOW);
            landWordGuard.setFlag(Flags.USE_DRIPLEAF, StateFlag.State.ALLOW);
            landWordGuard.setFlag(Flags.DAMAGE_ANIMALS, StateFlag.State.ALLOW);
            landWordGuard.setFlag(Flags.FALL_DAMAGE, StateFlag.State.ALLOW);
            landWordGuard.setFlag(Flags.USE, StateFlag.State.ALLOW);
            landWordGuard.setFlag(Flags.CHEST_ACCESS, StateFlag.State.ALLOW);
            landWordGuard.setFlag(Flags.LEAF_DECAY, StateFlag.State.DENY);
            landWordGuard.setFlag(Flags.FIRE_SPREAD, StateFlag.State.DENY);
            landWordGuard.setFlag(Flags.LAVA_FIRE, StateFlag.State.DENY);
            landWordGuard.setFlag(Flags.CREEPER_EXPLOSION, StateFlag.State.DENY);
            landWordGuard.setFlag(Flags.OTHER_EXPLOSION, StateFlag.State.DENY);

            new Land(landWordGuard, null);
            GeneralMethods.sendPlayerMessage(player,"Commands.Land.registerSuccess",true);
            return;
        }
        else if(subCommand.equalsIgnoreCase("unregister")){

            Land land = Land.getLandByRegionName(landWGName);
            if(land == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.LandNotFound",true);
                return;
            }

            land.remove();
            GeneralMethods.sendPlayerMessage(player,"Land supprimé avec succès (la region world guard a été suprimée aussi)");
            return;
        }

        if(args.size() < 3){
            GeneralMethods.sendPlayerErrorMessage(player, this.getProperUse());
            return;
        }

        Land land = Land.getLandByRegionName(landWGName);
        if(land == null){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.LandNotFound",true);
            return;
        }

        if(subCommand.equalsIgnoreCase("setProvince")){

            Province province = Province.getProvinceById(args.get(2));
            if(!args.get(2).equalsIgnoreCase("null")) {
                if (province == null) {
                    GeneralMethods.sendPlayerErrorMessage(player, "Commands.Province.ProvinceNotFound", true);
                    return;
                }
            }
            land.setProvince(province);
            GeneralMethods.sendPlayerMessage(player,"Commands.Land.setProvinceSuccess",true);
            return;
        }
        else if(subCommand.equalsIgnoreCase("setOwner")){

            Group group = null;
            if(!args.get(2).equalsIgnoreCase("null")) {
                group = Group.getGroupByName(args.get(2));
                if (group == null) {
                    GeneralMethods.sendPlayerErrorMessage(player, "Commands.Info.GroupDoesntExist", true);
                    return;
                }
                if (!(group instanceof LargeGroup)) {
                    GeneralMethods.sendPlayerErrorMessage(player, "Commands.Land.OnlyLargeGroupCanOwnLand", true);
                    return;
                }
            }
            boolean canSetOwner = land.setOwner((LargeGroup) group);
            if(!canSetOwner){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.BuildingLand",true);
                return;
            }
            GeneralMethods.sendPlayerMessage(player,"Commands.Land.setOwnerSuccessFull",true);
            return;
        }
        else if(subCommand.equalsIgnoreCase("addSubTerritory")){

            GeneralMethods.sendPlayerMessage(player,"Commande non implémentée");
            return;
        }
        else if(subCommand.equalsIgnoreCase("removeSubTerritory")){

            GeneralMethods.sendPlayerMessage(player,"Commande non implémentée");
            return;
        }
        else if(subCommand.equalsIgnoreCase("setName")){
            land.setName(getStringFromArgs(args,2));
            GeneralMethods.sendPlayerMessage(player,"Commands.Land.EditNameSuccess", true);
        }
        else if(subCommand.equalsIgnoreCase("setMainLand")){

            if(land.getOwner() == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.NoOwnerFoundError", true);
                return;
            }

            Boolean isMainsLand = false;
            if(args.get(2).equalsIgnoreCase("true")){
                isMainsLand = true;
            }
            else if(args.get(2).equalsIgnoreCase("false")){
                isMainsLand = false;
            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.SetMainLand.InvalidArg", true);
                return;
            }

            Land mainLand = land.getOwner().getMainLand();
            if( mainLand != null && isMainsLand == true){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.SetMainLand.AlreadyMainLand", true);
                player.sendMessage("§cIl s'agit du land : " + mainLand.getName() + "("+mainLand.getWorldGuardRegion().getId() + ")");
                return;
            }

            land.setMainLand(isMainsLand);
            if(isMainsLand) GeneralMethods.sendPlayerMessage(player,"Commands.Land.SetMainLand.Success.True", true);
            else GeneralMethods.sendPlayerMessage(player,"Commands.Land.SetMainSuccess.False", true);

            return;

        }
        else if(subCommand.equalsIgnoreCase("setIsAvailable")){

            Boolean isActive = false;
            if(args.get(2).equalsIgnoreCase("true")){
                isActive = true;
            }
            else if(args.get(2).equalsIgnoreCase("false")){
                isActive = false;
            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.SetIsActive.InvalidArg", true);
                return;
            }

            land.setIsActive(isActive);
            if(isActive) GeneralMethods.sendPlayerMessage(player,"Commands.Land.SetIsActive.Success.True", true);
            else GeneralMethods.sendPlayerMessage(player,"Commands.Land.SetIsActive.Success.False", true);

            return;

        }
        else if(subCommand.equalsIgnoreCase("setCanOtherGoIn")){

            Boolean setCanOtherGoIn = false;
            if(args.get(2).equalsIgnoreCase("true")){
                setCanOtherGoIn = true;
            }
            else if(args.get(2).equalsIgnoreCase("false")){
                setCanOtherGoIn = false;
            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.setCanOtherGoIn.InvalidArg", true);
                return;
            }

            land.setCanOtherGoIn(setCanOtherGoIn);
            if(setCanOtherGoIn) GeneralMethods.sendPlayerMessage(player,"Commands.Land.SetCanOtherGoIn.Success.True", true);
            else GeneralMethods.sendPlayerMessage(player,"Commands.Land.SetCanOtherGoIn.Success.False", true);

            return;

        }
    }

    @Override protected List<String> getTabCompletion(final CommandSender sender, final List<String> args) {

        final List<String> l = new ArrayList<String>();

        if (args.size() > 2 || !sender.hasPermission("organisation.command.land")) return l;
        if(!(sender instanceof  Player)) return l;

        Player player = (Player) sender;

        if (args.size() == 0) {

            l.add("list");
            l.add("register");
            l.add("unregister");
            l.add("setProvince");
            l.add("setOwner");
            l.add("setName");
            l.add("setMainLand");
            l.add("setIsAvailable");
            l.add("setCanOtherGoIn");
        }
        else if (args.size() == 1){

            if(args.get(0).equalsIgnoreCase("register")) {
                for (ProtectedRegion protectedRegion : Organisation.worldRgManager.getRegions().values()) {
                    if (Territory.getLstWGId().contains(protectedRegion.getId())) continue;
                    l.add(protectedRegion.getId());
                }
            }
            else if(!args.get(0).equalsIgnoreCase("list")) {
                for (Land land : Land.getAllLand()) {
                    l.add(land.getWorldGuardRegion().getId());
                }
            }
        }
        else if (args.size() == 2){

            if(args.get(0).equalsIgnoreCase("setProvince")){
                for (Province province : Province.getLstProvince()){
                    l.add(province.getId());
                }
                l.add("null");
            }
            else if(args.get(0).equalsIgnoreCase("setOwner")){
                for(LargeGroup largeGroup: LargeGroup.getLstLargeGroup()){
                    l.add(largeGroup.getName());
                }
                l.add("null");
            }
            else if(args.get(0).equalsIgnoreCase("setMainLand") || args.get(0).equalsIgnoreCase("setIsAvailable") || args.get(0).equalsIgnoreCase("setCanOtherGoIn")){
                l.add("true");
                l.add("false");
            }
            else{
                return l;
            }
        }
        return l;
    }
}
