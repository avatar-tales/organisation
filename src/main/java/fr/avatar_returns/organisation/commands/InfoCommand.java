package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import java.util.List;

public class InfoCommand extends OrganisationCommand {

    public InfoCommand() {
        super("info", "/o info <Group Name>", GeneralMethods.getStringConf("Commands.Info.Desc"), new String[]{"info", "information", "i"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

        if (!(sender instanceof Player)) return;
        Player player = (Player) sender;


        if (args.size() == 0) {

            try {
                OrganisationPlayer organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
                for(Group group : organisationPlayer.getLstGroup()){
                    displayGroupToPlayer(player,group);
                }

            } catch (OrganisationPlayerException e) {
                e.printStackTrace();
                GeneralMethods.sendPlayerErrorMessage(player, "Error.OrganisationPlayerDoesntExist", true);
            }

        }
        else if (args.size() == 1) {

            for(Group group :  Group.getLstGroup()){
                if(group.getName().equalsIgnoreCase(args.get(0))){
                    displayGroupToPlayer(player,group);
                    return;
                }
            }
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Info.GroupDoesntExist",true);

        }
        else {
            GeneralMethods.sendPlayerErrorMessage(player, getProperUse());
        }

    }

    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        ArrayList<String> lstTab = new ArrayList<>();

        if (args.size() == 0) {
            for (Group group : Group.getLstGroup()) {
                lstTab.add(group.getName());
            }
        }

        return lstTab;
    }

    private void displayGroupToPlayer(Player player, Group group) {


        player.sendMessage("§6--------------------| §9" + group.getName() + "§6 |--------------------");
        player.sendMessage("§6Nom : " + group.getName());
        player.sendMessage("§6Description : " + group.getDescription());
        player.sendMessage("§6Rang : " + group.getRank().getName());
        if(group instanceof LargeGroup){
            LargeGroup largeGroup = (LargeGroup) group;
            if(largeGroup.getLord() !=null){
                player.sendMessage("§Suzerain : " + largeGroup.getLord().getName());
            }
            player.sendMessage("§6Nombre de lands : " + largeGroup.getLstVland().size() + "/" +largeGroup.getMaxLand());
            if(largeGroup.isCanVassalize()){
                player.sendMessage("§6Nombre de vassaux : " + largeGroup.getLstVassal().size() + "/" +largeGroup.getMaxVassal());
            }
        }
        player.sendMessage("§6Nombre de joueur minimum : "+group.getMinPlayer());
        player.sendMessage("§6Nombre de joueur maximum : "+group.getMaxPlayer());
        player.sendMessage("§6Membre en ligne ("+group.getLstGroupConnectedMember().size()+"):");

        String onlineMembers ="";
        int cpt = 0;
        for(OrganisationMember onlineMember : group.getLstGroupConnectedMember()){

                String separator = (cpt+1 != group.getLstGroupConnectedMember().size())? " §e|" : "";
                onlineMembers += "§a["+onlineMember.getRole().getName()+"§a] "+onlineMember.getName()+separator+"§a";

            cpt += 1;
        }

        player.sendMessage(onlineMembers);
        player.sendMessage("§6Membre en hors ligne ("+(group.getLstGroupNotConnectedMember().size())+"):");

        String offlineMembers ="";
        cpt = 0;
        for(OrganisationMember offLineMember : group.getLstGroupNotConnectedMember() ){

            String separator = (cpt+1 != group.getLstGroupConnectedMember().size())? " §e|" : "";
            offlineMembers += "§a["+offLineMember.getRole().getName()+"§a] "+offLineMember.getName()+separator+"§a";

            cpt +=0;
        }
        player.sendMessage(offlineMembers);

    }
}
