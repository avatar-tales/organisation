package fr.avatar_returns.organisation.commands;


import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationGroupPlayerException;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.organisations.largeGroup.Faction;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.organisations.Group;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import java.util.List;

public class CreateCommand extends OrganisationCommand {

    public CreateCommand() {
        super("create", "/o create <Faction|Group> <name>", GeneralMethods.getStringConf("Commands.Create.Desc"),new String[] { "create", "c" });
    }

    @Override
    public void execute(final CommandSender sender, final List<String> args){

        if( !(sender instanceof Player))return;

        Player player =  (Player) sender;
        if(args.size()!=2){
            GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
            return;
        }

        OrganisationPlayer organisationPlayerSender = getCommandOrganisationPlayer(player);
        if(organisationPlayerSender == null) return;

        String type = args.get(0).toUpperCase();
        String newGroupName = args.get(1);

        if(newGroupName.equalsIgnoreCase("null")){
            GeneralMethods.sendPlayerMessage(player,"Impossible de nomer une organisation 'null'");
            return;
        }

        if(Group.getGroupByName(newGroupName) != null ){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Create.AlreadyExist",true);
            return;
        }

        if(!organisationPlayerSender.canJoinOrganisation(Group.getGroupByName(newGroupName)) || !player.hasPermission("organisation.command.create")){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.NoPermission",true);
            return;
        }

        switch (type){

            case "FACTION" :

                if(isPlayerAlreadyInLargeOrganisation(player))return;

                Faction newFaction = new Faction(newGroupName);
                newFaction.initDefaultRole();

                try {
                    newFaction.addMember(player);
                } catch (OrganisationGroupPlayerException e) {
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Error.OrganisationPlayerDoesntExist",true);
                    e.printStackTrace();
                }

                break;

            case "GUILDE" :

                int nbGuildeOfPlayer = 0;
                for(Guilde guilde : Guilde.getLstGuilde()){
                    if(guilde.getLocalPlayers().contains(player))nbGuildeOfPlayer++;
                }
                if(nbGuildeOfPlayer>=Guilde.getMaxMultiGuilde()){
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Create.Max_Guilde_Join",true);
                    return;
                }

                Guilde newGuilde = new Guilde(newGroupName);
                newGuilde.initDefaultRole();

                try {
                    newGuilde.addMember(player);
                }catch (OrganisationGroupPlayerException e ) {
                    GeneralMethods.sendPlayerErrorMessage(e.getOfflinePlayer(),"Impossible d'ajouter le membre au groupe "+e.getGroup().getName());
                }
                break;

            default:
                GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
                return;
        }
        GeneralMethods.sendPlayerMessage(player,"Commands.Create.Successful",true);
    }

    @Override
    protected List<String> getTabCompletion(final CommandSender sender, final List<String> args) {
        if (args.size() >= 2) {
            return new ArrayList<String>();
        }

        final List<String> l = new ArrayList<String>();
        if (args.size() == 0) {

            l.add("Faction");
            l.add("Guilde");

        } else {
            for (final Player p : Bukkit.getOnlinePlayers()) {
                l.add(p.getName());
            }
        }
        return l;
    }

    private boolean isPlayerAlreadyInLargeOrganisation(Player player){

        for(Group group : Group.getLstGroup()){

            if(group instanceof Guilde) continue;
            if(group.getLocalPlayers().contains(player)){

                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Create.Max_LargeOrganisation_Join",true);
                return true;
            }
        }

        return false;
    }

}
