package fr.avatar_returns.organisation.commands;


import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;

import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.banner.Pattern;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class EditCommand extends OrganisationCommand {

    public EditCommand() {
        super("edit", "/o edit <name|desc|connexionMessage|maxPlayer|lord||minPlayer|isVisible|rank> <value> <Organisation>", GeneralMethods.getStringConf("Commands.edit.Desc"),new String[] { "edit", "e" });
    }

    @Override public void execute(final CommandSender sender, final List<String> args) {

        if (!(sender instanceof Player)) return;

        Player player = (Player) sender;
        String subcommand = args.get(0).toLowerCase(Locale.ROOT);
        String value = "";

        OrganisationPlayer organisationPlayer = getCommandOrganisationPlayer(player);
        if (organisationPlayer == null) return;

        Group playerGroup = null;
        if((args.size() == 1 || args.size() == 2) && (subcommand.equalsIgnoreCase("flag") || subcommand.equalsIgnoreCase("desc") || subcommand.equalsIgnoreCase("description") || subcommand.equalsIgnoreCase("connexionMessage"))){
            playerGroup = getCommandPlayerGroup(organisationPlayer, args, 1);
            GeneralMethods.sendPlayerMessage(player,"Commands.edit.emptyValue",true);
        }
        else if (args.size() < 2 || args.size() > 3) {
            GeneralMethods.sendPlayerErrorMessage(player, this.getProperUse());
            return;
        }
        else {
            playerGroup = getCommandPlayerGroup(organisationPlayer, args, 2);
            value = args.get(1);
        }

        if (playerGroup == null) return;

        if (!player.hasPermission("organisation.command.edit." + subcommand.toLowerCase()) || !playerGroup.hasPermission(player, Permission.management.setDescription)) {
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.NoPermission", true);
            return;
        }

        if (subcommand.equalsIgnoreCase("name")){

            if(value.equalsIgnoreCase("null")){
                GeneralMethods.sendPlayerMessage(player,"Impossible de nomer une organisation 'null'");
                return;
            }

            playerGroup.setName(value);
            GeneralMethods.sendPlayerMessage(player, "Commands.edit.rename.Successful", true);
        }
        else if (subcommand.equalsIgnoreCase("flag")){

            ItemStack flag = player.getItemInHand();
            if(!flag.getType().name().toUpperCase().contains("BANNER")){
                GeneralMethods.sendPlayerErrorMessage(player,"Cet item n'est pas une bannière : " + flag.getType().name());
                return;
            }

            playerGroup.setFlag(flag);
            if(playerGroup instanceof LargeGroup){
                ((LargeGroup) playerGroup).updateBanner();
            }
            GeneralMethods.sendPlayerMessage(player,"Bannière mise a jour avec succès.");

        }
        else if(subcommand.equalsIgnoreCase("minplayer") ||
                subcommand.equalsIgnoreCase("maxplayer") ||
                subcommand.equalsIgnoreCase("maxLand") ||
                subcommand.equalsIgnoreCase("maxRaid") ||
                subcommand.equalsIgnoreCase("dailyRaid")
        ){

            int intValue;
            try{
                intValue = Integer.parseInt(value);
                if(intValue < 1){
                    GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.ValueError.Int", true);
                    return;
                }
            }
            catch (NumberFormatException ex){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.ValueError.Int", true);
                return;
            }

            if(subcommand.equalsIgnoreCase("minplayer")){

                if(intValue >playerGroup.getLstMember().size()){
                    GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.minPlayer.Error", true);
                    return;
                }

                playerGroup.setMinPlayer(intValue);
                GeneralMethods.sendPlayerMessage(player, "Commands.edit.minPlayer.Successful", true);
            }
            else if(subcommand.equalsIgnoreCase("maxplayer")){

                if(intValue < playerGroup.getMinPlayer()){
                    GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.maxPlayer.Error", true);
                    return;
                }

                playerGroup.setMaxPlayer(intValue);
                GeneralMethods.sendPlayerMessage(player, "Commands.edit.maxPlayer.Successful", true);
            }
            if( playerGroup instanceof LargeGroup) {
                LargeGroup largePlayerGroup = (LargeGroup) playerGroup;
                if (subcommand.equalsIgnoreCase("maxLand")) {

                    if (intValue < 0) {
                        GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.maxLand.Error", true);
                        return;
                    }

                    largePlayerGroup.setMaxLand(intValue);
                    GeneralMethods.sendPlayerMessage(player, "Commands.edit.maxLand.Successful", true);
                }
                else if (subcommand.equalsIgnoreCase("maxRaid")) {

                    if (intValue < 0) {
                        GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.maxRaid.Error", true);
                        return;
                    }

                    largePlayerGroup.setMaxRaid(intValue);
                    GeneralMethods.sendPlayerMessage(player, "Commands.edit.maxRaid.Successful", true);
                }
                else if (subcommand.equalsIgnoreCase("dailyRaid")) {

                    if (intValue < 0) {
                        GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.dailyRaid.Error", true);
                        return;
                    }

                    largePlayerGroup.setDailyRaid(intValue);
                    GeneralMethods.sendPlayerMessage(player, "Commands.edit.dailyRaid.Successful", true);
                }
            }
        }
        else if(subcommand.equalsIgnoreCase("desc") || subcommand.equalsIgnoreCase("description")){
            playerGroup.setDescription(value);
            GeneralMethods.sendPlayerMessage(player, "Commands.edit.desc.Successful", true);
        }
        else if(subcommand.equalsIgnoreCase("connexionMessage")){
            playerGroup.setConnexionMessage(value);
            GeneralMethods.sendPlayerMessage(player, "Commands.edit.connexionMessage.Successful", true);
        }
        else if(subcommand.equalsIgnoreCase("rank")){

            int rankId;
            try{
                rankId = Integer.parseInt(value);
                if(rankId < 0){
                    GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.ValueError.Int", true);
                    return;
                }
            }
            catch (NumberFormatException e){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.ValueError.Int", true);
                return;
            }

            GroupRank groupRank = GroupRank.getById(rankId);
            if(groupRank == null){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.rank.BadId", true);
                return;
            }
            if(groupRank.getGroupType().equalsIgnoreCase("FACTION") && !(playerGroup instanceof LargeGroup)){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.rank.BadRank", true);
                return;
            }

            playerGroup.setRank(groupRank);
            GeneralMethods.sendPlayerMessage(player, "Commands.edit.rank.Successful",true);
        }
        else if(subcommand.equalsIgnoreCase("lord")){


            String largeGroupName = value;
            LargeGroup lordGroup = LargeGroup.getLargeGroupByName(value);
            if(largeGroupName.equalsIgnoreCase("null")){
                ((LargeGroup)playerGroup).setLord(null);
                GeneralMethods.sendPlayerMessage(player, "Commands.edit.lord.Successful",true);
            }
            if(!(playerGroup instanceof  LargeGroup)){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.lord.NotLargeGroup",true);
                return;
            }
            if(lordGroup == null){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.lord.LordNotFound",true);
                return;
            }
            if(lordGroup == playerGroup){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.lord.EqualGroup", true);
                return;
            }
            if(lordGroup.getRank().getOrder() < playerGroup.getRank().getOrder()){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.lord.CantHaveLordWithLowerRank",true);
                return;
            }
            if(lordGroup.getLstVassal().size() >= lordGroup.getMaxVassal()){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.lord.ToMutchVassal",true);
                return;
            }

            ((LargeGroup)playerGroup).setLord(lordGroup);
            GeneralMethods.sendPlayerMessage(player, "Commands.edit.lord.Successful",true);
        }
        else if(subcommand.equalsIgnoreCase("isVisible")){

            boolean isVisible;
            if(value.equalsIgnoreCase("true"))isVisible = true;
            else if(value.equalsIgnoreCase("false"))isVisible = false;
            else {
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.ValueError.Bool", true);
                return;
            }

            playerGroup.setIsVisible(isVisible);
            GeneralMethods.sendPlayerMessage(player, "Commands.edit.isVisible.Successful",true);
        }
        else if(subcommand.equalsIgnoreCase("canUnClaim")){

            boolean canUnClaim;
            if(value.equalsIgnoreCase("true"))canUnClaim = true;
            else if(value.equalsIgnoreCase("false"))canUnClaim = false;
            else {
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.edit.ValueError.Bool", true);
                return;
            }

            playerGroup.setCanUnClaim(canUnClaim);
            GeneralMethods.sendPlayerMessage(player, "Commands.edit.canUnClaim.Successful",true);
        }
        else{
            GeneralMethods.sendPlayerErrorMessage(player, this.getProperUse());
        }
    }
    @Override protected List<String> getTabCompletion(final CommandSender sender, final List<String> args) {

        if (args.size() > 2 || !(sender instanceof Player)) {
            return new ArrayList<String>();
        }

        Player player = (Player) sender;
        final List<String> l = new ArrayList<String>();

        if (args.isEmpty()) {

            if(player.isOp() || player.hasPermission("organisation.command.edit.tabComplete")){

                l.add("name");
                l.add("minPlayer");
                l.add("maxPlayer");
                l.add("rank");
                l.add("isVisible");
                l.add("canUnClaim");
                l.add("maxLand");
                l.add("lord");
                l.add("maxRaid");
                l.add("flag");
                l.add("dailyRaid");
            }

            l.add("desc");
            l.add("connexionMessage");

        }
        else if (args.size() == 1 && (player.isOp() && player.hasPermission("organisation.command.edit.tabComplete"))) {

            OrganisationPlayer organisationPlayer = getCommandOrganisationPlayer((Player) sender);
            if(args.get(0).equalsIgnoreCase("minPlayer") ||
                    args.get(0).equalsIgnoreCase("maxPlayer") ||
                    args.get(0).equalsIgnoreCase("maxLand") ||
                    args.get(0).equalsIgnoreCase("maxRaid") ||
                    args.get(0).equalsIgnoreCase("dailyRaid")
            ){
                for(int i = 5; i< 50; i+=5){l.add(String.valueOf(i));}
            }
            else if (args.get(0).equalsIgnoreCase("rank") ){
                for(GroupRank groupRank : GroupRank.getLstGroupRank())l.add(String.valueOf(groupRank.getId()));
            }
            else if(args.get(0).equalsIgnoreCase("lord")){
                l.add("null");
                for(LargeGroup largeGroup : LargeGroup.getLstLargeGroup()){
                    l.add(largeGroup.getName());
                }
            }
            else if(args.get(0).equalsIgnoreCase("desc") ||args.get(0).equalsIgnoreCase("description") || args.get(0).equalsIgnoreCase("connexionMessage") || args.get(0).equalsIgnoreCase("name")) {
                l.add("text");
            }
            else if(args.get(0).equalsIgnoreCase("isVisible") || args.get(0).equalsIgnoreCase("canUnClaim")){
                l.add("true");
                l.add("false");
            }
            else if(args.get(0).equalsIgnoreCase("flag")){
                for(Group group : organisationPlayer.getLstGroup())l.add(group.getName());
            }
        }
        else if (args.size() == 2 && !args.get(0).equalsIgnoreCase("flag") && (player.isOp() && player.hasPermission("organisation.command.edit.tabComplete"))){
            OrganisationPlayer organisationPlayer = getCommandOrganisationPlayer((Player) sender);
            if(organisationPlayer == null)return new ArrayList<String>();

            for(Group group : organisationPlayer.getLstGroup())l.add(group.getName());
        }
        return l;
    }

}
