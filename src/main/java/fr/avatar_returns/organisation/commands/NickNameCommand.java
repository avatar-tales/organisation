package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class NickNameCommand extends OrganisationCommand{

    public NickNameCommand() {
        super("nickname", "/o nickname <newName>", GeneralMethods.getStringConf("Commands.NickName.Desc"), new String []{"nick", "nickname"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

        if (sender instanceof Player) {

            Player playerSender =  (Player) sender;

            if( args.size() > 1) {
                GeneralMethods.sendPlayerErrorMessage(playerSender,this.getProperUse());
                return;
            }

            OrganisationPlayer oPlayer = getCommandOrganisationPlayer(playerSender);
            if(oPlayer == null)return;

            if(args.size() == 1) {
                GeneralMethods.sendPlayerMessage(playerSender, "Vous êtes renommé en " + args.get(0));
                oPlayer.setNickName(args.get(0));
            }
            else {
                GeneralMethods.sendPlayerMessage(playerSender, "Vous êtes de nouveau " + playerSender.getName());
                oPlayer.setNickName("");
            }
        }
    }
}
