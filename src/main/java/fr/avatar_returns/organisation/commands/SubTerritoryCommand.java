package fr.avatar_returns.organisation.commands;

import com.sk89q.worldguard.protection.flags.Flags;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.territory.Territory;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Contest;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.RP;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Totem;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class SubTerritoryCommand extends OrganisationCommand {

    public SubTerritoryCommand() {
        super("subterritory", "/o sub <list|register|unregister|setCanOtherGoIn|setName> <subTerritoryWGName>  <type|<true|false>|newName> <landWGName>", "Command to manage SubTerritory",new String[] { "subterritory", "sub", "s" });
    }

    @Override
    public void execute(final CommandSender sender, final List<String> args) {

        if(!(sender instanceof Player))return;

        Player player = (Player) sender;
        if(!(player.isOp() || player.hasPermission("organisation.command.subterritory"))){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.NoPermission", true);
            return;
        }


        if(args.isEmpty()){
            GeneralMethods.sendPlayerErrorMessage(player, this.getProperUse());
            return;
        }

        String subCommand = args.get(0);
        String subTerritoryWGName = "";

        if(subCommand.equalsIgnoreCase("list")){

            String msg  = "";

            ArrayList<LandSubTerritory> lstSubTerritory;

            if(args.size() == 1){
                lstSubTerritory = LandSubTerritory.getAllLandSubTerritory();
                msg = "Liste des Sous Territoire  : \n";
            }
            else if(args.size() == 2){
                subTerritoryWGName = args.get(1);
                Land land = Land.getLandByRegionName(subTerritoryWGName);
                if(land == null){
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.LandNotFound", true);
                    return;
                }
                lstSubTerritory = land.getSubTerritory();
                msg = "Liste des sous territoires du land '"+  subTerritoryWGName +"' : \n";
            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player, this.getProperUse());
                return;
            }

            for(LandSubTerritory landSubTerritory: lstSubTerritory ){
                String province = (landSubTerritory.getLand().getProvince() == null) ? "" : "§6["+landSubTerritory.getLand().getProvince().getId()+"] ";
                String type = (landSubTerritory instanceof Mine)? "§b(Mine) " : (landSubTerritory instanceof Contest)? "§b(Zone a risque) " : (landSubTerritory instanceof RP)? "§b(RP) " : (landSubTerritory instanceof Totem)? "§b(Totem) " : "§b(RP-Joueur) ";
                String land = "§e" + landSubTerritory.getLand().getName() + " ";
                String subTerritoryName = "§a" +landSubTerritory.getName() + " ";
                String canOtherGoIn = (landSubTerritory.canOtherGoIn())? "§2(open) " : "(§cclose) ";
                String numberPointOfInterest = "§7("+ String.valueOf(landSubTerritory.getPositionPointOfInterest().size())+")";
                String id =  "§a(" + landSubTerritory.getWorldGuardRegion().getId() + ") ";

                msg += " - " + province + land + subTerritoryName + id + type + canOtherGoIn +numberPointOfInterest +"\n";
            }

            GeneralMethods.sendPlayerMessage(player, msg);
            if(lstSubTerritory.size() != 0)player.sendMessage("");
            player.sendMessage("§6Total : "+lstSubTerritory.size());
            return;
        }

        subTerritoryWGName = (args.size() >= 2)?args.get(1) :"";

        if(subCommand.equalsIgnoreCase("register") && args.size() == 4){

            Land land = Land.getLandByRegionName(args.get(3));
            if(land == null){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Land.LandNotFound", true);
                return;
            }

            // Check if SubTerritoryWorldGuard region is fully include in LAND.
            ProtectedRegion subTerritoryWorldGuard = Organisation.worldRgManager.getRegion(subTerritoryWGName);
            if(subTerritoryWorldGuard == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.WorldGuard.NotFound",true);
                return;
            }

            //Check if this worldguard region is already used as Territory

            Territory territory = Territory.getTerritoryByRegion(subTerritoryWorldGuard);
            if(territory != null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.LandSubTerritory.register.TerritoryAlreadyUsed", true);
                return;
            }

            //Check if this worldguard region is fully include is Land worldguard region.
            if(!land.canHaveThisSubTerritory(subTerritoryWorldGuard)){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.LandSubTerritory.register.LandSubTerritoryNotIncludeInLand", true);
                return;
            }

            String type = args.get(2);
            if(type.equalsIgnoreCase("Mine")){
                setDefaultRPWorldGuardFlag(subTerritoryWorldGuard);
                new Mine(subTerritoryWorldGuard, land);
                GeneralMethods.sendPlayerMessage(player,"Commands.LandSubTerritory.register.Success.Mine",true);
            }
            else if(type.equalsIgnoreCase("Totem")) {
                setDefaultRPWorldGuardFlag(subTerritoryWorldGuard);
                new Totem(subTerritoryWorldGuard, land);
                GeneralMethods.sendPlayerMessage(player,"Commands.LandSubTerritory.register.Success.Totem",true);
            }
            else if(type.equalsIgnoreCase("RP")){
                setDefaultRPWorldGuardFlag(subTerritoryWorldGuard);
                new RP(subTerritoryWorldGuard, land);
                GeneralMethods.sendPlayerMessage(player,"Commands.LandSubTerritory.register.Success.RP",true);
            }
            else if(type.equalsIgnoreCase("Contest")){
                setDefaultRPWorldGuardFlag(subTerritoryWorldGuard);
                new Contest(subTerritoryWorldGuard, land);
                GeneralMethods.sendPlayerMessage(player,"Commands.LandSubTerritory.register.Success.Contest",true);
            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.LandSubTerritory.register.InvalidType", true);
                return;
            }

            return;
        }
        else if(args.size() >= 2)  {

            LandSubTerritory landSubTerritory = LandSubTerritory.getLandSubTerritoryByRegionName(subTerritoryWGName);
            if(landSubTerritory == null){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.LandSubTerritory.SubTerritoryNotFound");
                return;
            }

            if(subCommand.equalsIgnoreCase("unregister") && args.size() == 2){
                landSubTerritory.getLand().removeSubTerriTorry(landSubTerritory);
                GeneralMethods.sendPlayerMessage(player, "Commands.LandSubTerritory.SubTerritoryUnregisterSuccess", true);
            }
            else if(subCommand.equalsIgnoreCase("setName") && args.size() >= 3){
                String newName = getStringFromArgs(args,2);
                landSubTerritory.setName(newName);
                GeneralMethods.sendPlayerMessage(player, "Commands.LandSubTerritory.SubTerritorySetNameSuccess", true);
            }
            else if(subCommand.equalsIgnoreCase("setCanOtherGoIn") && args.size() == 3) {
                String canOtherGoIn = args.get(2);
                if (canOtherGoIn.equalsIgnoreCase("true")) {
                    landSubTerritory.setCanOtherGoIn(true);
                    GeneralMethods.sendPlayerMessage(player, "Commands.LandSubTerritory.setCanOtherGoInSuccess.True", true);
                }
                else if(canOtherGoIn.equalsIgnoreCase("false")){
                    landSubTerritory.setCanOtherGoIn(false);
                    GeneralMethods.sendPlayerMessage(player, "Commands.LandSubTerritory.setCanOtherGoInSuccess.False", true);
                }
                else{
                    GeneralMethods.sendPlayerErrorMessage(player, "Commands.LandSubTerritory.setCanOtherGoInError", true);
                }

            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
                return;
            }
        }
        else{
            GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
            return;
        }

    }

    protected void setDefaultRPWorldGuardFlag(ProtectedRegion protectedRegion){

        protectedRegion.setFlag(Flags.BUILD, StateFlag.State.ALLOW);
        protectedRegion.setFlag(Flags.PVP,  StateFlag.State.ALLOW);
        protectedRegion.setFlag(Flags.INTERACT, StateFlag.State.ALLOW);
        protectedRegion.setFlag(Flags.USE_ANVIL, StateFlag.State.ALLOW);
        protectedRegion.setFlag(Flags.USE_DRIPLEAF, StateFlag.State.ALLOW);
        protectedRegion.setFlag(Flags.DAMAGE_ANIMALS, StateFlag.State.ALLOW);
        protectedRegion.setFlag(Flags.FALL_DAMAGE, StateFlag.State.ALLOW);
        protectedRegion.setFlag(Flags.USE, StateFlag.State.ALLOW);
        protectedRegion.setFlag(Flags.CHEST_ACCESS, StateFlag.State.ALLOW);
        protectedRegion.setFlag(Flags.LEAF_DECAY, StateFlag.State.DENY);
        protectedRegion.setFlag(Flags.FIRE_SPREAD, StateFlag.State.DENY);
        protectedRegion.setFlag(Flags.LAVA_FIRE, StateFlag.State.DENY);
        protectedRegion.setFlag(Flags.CREEPER_EXPLOSION, StateFlag.State.DENY);
        protectedRegion.setFlag(Flags.OTHER_EXPLOSION, StateFlag.State.DENY);
        
    }


    @Override
    protected List<String> getTabCompletion(final CommandSender sender, final List<String> args) {

        final List<String> l = new ArrayList<String>();

        if(!(sender instanceof  Player))return l;
        Player player = (Player) sender;

        if(args.size() == 0){
            l.add("list");
            l.add("register");
            l.add("unregister");
            l.add("setCanOtherGoIn");
            l.add("setName");
        }
        else if(args.size() == 1){
            if(args.get(0).equalsIgnoreCase("register")){

                for (ProtectedRegion protectedRegion : Organisation.worldRgManager.getRegions().values()) {
                    if (Territory.getLstWGId().contains(protectedRegion.getId())) continue;
                    l.add(protectedRegion.getId());
                }
            }
            else if(args.get(0).equalsIgnoreCase("unregister") ||
                    args.get(0).equalsIgnoreCase("setCanOtherGoIn") ||
                    args.get(0).equalsIgnoreCase("setName")){

                    for(LandSubTerritory landSubTerritory : LandSubTerritory.getAllLandSubTerritory())l.add(landSubTerritory.getWorldGuardRegion().getId());
            }
            else if(args.get(0).equalsIgnoreCase("list")){
                for(Land land : Land.getAllLand())l.add(land.getWorldGuardRegion().getId());
            }
        }
        else if (args.size() == 2){

            if(args.get(0).equalsIgnoreCase("setCanOtherGoIn")){
                l.add("True");
                l.add("False");
            }
            else if(args.get(0).equalsIgnoreCase("setName")) {
                l.add("<new_name>");
            }
            else if(args.get(0).equalsIgnoreCase("register")){
                l.add("Mine");
                l.add("Totem");
                l.add("RP");
                l.add("Contest");
            }
        }
        else if (args.size() == 3 && args.get(0).equalsIgnoreCase("register")){
            for(Land land : Land.getAllLand())l.add(land.getWorldGuardRegion().getId());
        }

        return l;
    }
}
