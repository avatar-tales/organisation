package fr.avatar_returns.organisation.commands;


import fr.alessevan.api.AvatarAPI;
import fr.alessevan.api.hooks.defaults.IMoneyHook;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.manager.SecondeTaskManager;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class OrganisationHomeCommand {


    private class OrganisationHomeCommandTab implements TabCompleter {

        @Override public List<String> onTabComplete(final CommandSender sender, final Command command, final String alias, final String[] args) {

            List<String> argsSuggestion = new ArrayList<>();
            if(sender instanceof Player && args.length == 0){
                if(sender.isOp() || sender.hasPermission("organisaton.command.home.admin")){
                    for(LargeGroup largeGroup : LargeGroup.getLstLargeGroup()){
                        argsSuggestion.add(largeGroup.getName());
                    }
                }
            }

            return argsSuggestion;
        }
    }


    private final Organisation plugin;
    public static boolean debugEnabled = false;

    public OrganisationHomeCommand(final Organisation plugin) {
        this.plugin = plugin;
        debugEnabled = Organisation.plugin.getConfig().getBoolean("debug");
        this.init();
    }

    public static String[] commandaliases = { "home","ohome","fhome"};

    private void init() {
        final PluginCommand organisation = this.plugin.getCommand("ohome");


        final CommandExecutor cmdExecutor = (sender, c, executedCommand, args) -> {

            if (Arrays.asList(commandaliases).contains(executedCommand.toLowerCase())) {
                this.execute(sender, args);

                OrganisationCommand.instances.get("help").execute(sender, new ArrayList<String>());
                return true;
            }

            return false;
        };
        organisation.setExecutor(cmdExecutor);
        organisation.setTabCompleter(new OrganisationHomeCommandTab());
    }

    private void execute(CommandSender commandSender, String[] args){

        if(!(commandSender instanceof Player))return;
        Player player = (Player)commandSender;
        OrganisationPlayer oPlayer = null;

        try{
            oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        }
        catch (OrganisationPlayerException e){
            return;
        }

        if(oPlayer.isLocalyInSafeZone() && !(player.isOp() || player.hasPermission("organisation.command.home.admin"))){
            GeneralMethods.sendPlayerErrorMessage(player, "Organisation.TP.CantTPFromSafeZone", true);
            return;
        }

        if(oPlayer.isInFightMod() && !(player.isOp() || player.hasPermission("organisation.command.home.admin"))){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.FightMod.CantExecuteCommand", true);
            return;
        }
        if(oPlayer.isInPrespawnMod() && !(player.isOp() || player.hasPermission("organisation.command.home.admin"))){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.Prespawn.CantExecuteCommand", true);
            return;
        }

        int cost = GeneralMethods.getIntConf("Organisation.Home.Teleportation.Cost");

        AtomicBoolean canPay = new AtomicBoolean(true);
        if(!(player.isOp() || player.hasPermission("organisation.command.home.admin") || oPlayer.isYoung())) {
            AvatarAPI.getAPI().getServiceHook(IMoneyHook.class)
                    .ifPresent(hook -> {
                        if (hook.getMoney(player) < cost) {
                            GeneralMethods.sendPlayerErrorMessage(player, "Commands.Home.Teleportation.NotEnouthMoney", true);
                            canPay.set(false);
                        }
                    });
        }

        if(!canPay.get())return;
        if(oPlayer.isYoung() && cost > 0)GeneralMethods.sendPlayerMessage(player,"Commands.Teleportation.YoungDontPay",true);
        if(SecondeTaskManager.mapPlayerToTPFHome.containsKey(oPlayer)){
            GeneralMethods.sendPlayerMessage(player,"Commands.Home.Teleportation.Cancel", true);
            SecondeTaskManager.mapPlayerToTPFHome.remove(oPlayer);
            SecondeTaskManager.mapPlayerToTPFHomeVland.remove(oPlayer);
        }

        LargeGroup largeGroup = null;
        if(args.length == 0 || !(player.isOp() || player.hasPermission("organisaton.command.home.admin"))){

            for(LargeGroup lg :  LargeGroup.getLstLargeGroup()){
                if(lg.getLocalPlayers().contains(player)){
                    largeGroup = lg;
                    break;
                }
            }

            if(largeGroup == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Home.NoOrganisationFound.Admin", true);
                return;
            }


        }
        else if(args.length == 1) {

            String groupName = args[0];
            largeGroup = LargeGroup.getLargeGroupByName(groupName);

            if(largeGroup == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Home.NoOrganisationFound.Player", true);
                return;
            }
        }
        else{
            return;
        }

        for(VLand vLand : VLand.getLstVland()){
            if(vLand.isMainLand()){
                if(vLand.getOwnerId() == largeGroup.getId()){
                    if(vLand.getFhome().equalsIgnoreCase("")){
                        GeneralMethods.sendPlayerErrorMessage(player,"Votre organisation n'a pas de home", false);
                    }
                    else{
                        GeneralMethods.sendPlayerMessage(player,"Commands.Home.Teleportation.Successful", true);

                        int timeToFhome = (player.isOp() || player.hasPermission("organisaton.command.home.admin"))? 0 : GeneralMethods.getIntConf("Organisation.Home.Teleportation.Duration");

                        SecondeTaskManager.mapPlayerToTPFHome.put(oPlayer, System.currentTimeMillis() + (timeToFhome * 1000L));
                        SecondeTaskManager.mapPlayerToTPFHomeVland.put(oPlayer, vLand);
                    }
                    return;
                }
            }
        }
        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Home.Teleportation.NoMainLandFound", true);
    }
}
