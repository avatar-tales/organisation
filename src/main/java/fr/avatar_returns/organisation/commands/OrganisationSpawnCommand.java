package fr.avatar_returns.organisation.commands;


import fr.alessevan.api.AvatarAPI;
import fr.alessevan.api.hooks.defaults.IMoneyHook;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.manager.SecondeTaskManager;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class OrganisationSpawnCommand {

    private class OrganisationSpawnCommandTab implements TabCompleter {

        @Override public List<String> onTabComplete(final CommandSender sender, final Command command, final String alias, final String[] args) {

            List<String> argsSuggestion = new ArrayList<>();
            return argsSuggestion;
        }
    }


    private final Organisation plugin;
    public static boolean debugEnabled = false;

    public OrganisationSpawnCommand(final Organisation plugin) {
        this.plugin = plugin;
        debugEnabled = Organisation.plugin.getConfig().getBoolean("debug");
        this.init();
    }

    public static String[] commandaliases = { "spawn", "ukiyo"};

    private void init() {
        final PluginCommand organisation = this.plugin.getCommand("spawn");

        final CommandExecutor cmdExecutor = (sender, c, executedCommand, args) -> {

            if (Arrays.asList(commandaliases).contains(executedCommand.toLowerCase())) {
                if (args.length == 0) {
                    this.execute(sender, args);
                }
                return true;
            }

            return false;
        };
        organisation.setExecutor(cmdExecutor);
        organisation.setTabCompleter(new OrganisationSpawnCommandTab());
    }

    private void execute(CommandSender commandSender, String[] args){


        if(!(commandSender instanceof Player))return;
        Player player = (Player) commandSender;


        OrganisationPlayer oPlayer = OrganisationCommand.getCommandOrganisationPlayer(player);
        if(oPlayer == null)return;

        if(oPlayer.isLocalyInSafeZone() && !(player.isOp() || player.hasPermission("organisation.command.home.admin"))){
            GeneralMethods.sendPlayerErrorMessage(player, "Organisation.TP.CantTPFromSafeZone", true);
            return;
        }


        if(SecondeTaskManager.mapPlayerToTPSpawn.containsKey(oPlayer)){
            GeneralMethods.sendPlayerMessage(player,"Commands.Spawn.Teleportation.Cancel", true);
            SecondeTaskManager.mapPlayerToTPSpawn.remove(oPlayer);
            return;
        }

        if(oPlayer.isInFightMod() && !(player.isOp() || player.hasPermission("organisation.command.spawn.admin"))){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.FightMod.CantExecuteCommand", true);
            return;
        }
        if(oPlayer.isInPrespawnMod() && !(player.isOp() || player.hasPermission("organisation.command.spawn.admin"))){
            GeneralMethods.sendPlayerErrorMessage(player,"Organisation.Prespawn.CantExecuteCommand", true);
            return;
        }

        int cost = GeneralMethods.getIntConf("Organisation.Spawn.Teleportation.Cost");
        AtomicBoolean canPay = new AtomicBoolean(true);
        if(!(player.isOp() || player.hasPermission("organisation.command.spawn.admin")) || oPlayer.isYoung()) {
            AvatarAPI.getAPI().getServiceHook(IMoneyHook.class)
                    .ifPresent(hook -> {
                        if (hook.getMoney(player) < cost) {
                            GeneralMethods.sendPlayerErrorMessage(player, "Commands.Spawn.Teleportation.NotEnouthMoney", true);
                            canPay.set(false);
                        }
                    });
        }
        if(!canPay.get())return;
        if(oPlayer.isYoung() && cost > 0)GeneralMethods.sendPlayerMessage(player,"Commands.Teleportation.YoungDontPay",true);

        int waitDelay  = (player.isOp() || player.hasPermission("organisation.command.spawn.admin")) ? 0 : GeneralMethods.getIntConf("Organisation.Spawn.Teleportation.Duration");
        long timeToTeleport = System.currentTimeMillis() + (waitDelay * 1000);

        GeneralMethods.sendPlayerMessage(player,"Commands.Spawn.Teleportation.Successful", true);
        SecondeTaskManager.mapPlayerToTPSpawn.put(oPlayer, timeToTeleport);
    }
}
