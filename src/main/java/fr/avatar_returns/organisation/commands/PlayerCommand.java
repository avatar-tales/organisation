package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.OrganisationCommand;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.view.ui.organisationUI.MainOrganisationUI;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class PlayerCommand extends OrganisationCommand {


    public PlayerCommand() {
        super("player", "/o player <playerName ><info|prespawn|jail> <True|False>", GeneralMethods.getStringConf("Commands.Player.Desc"), new String []{"player", "p"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

        super.execute(sender, args);
        if(args.size() < 2){
            sender.sendMessage("§c"+this.getProperUse());
            return;
        }

        if(sender instanceof Player){
            if(!(sender.hasPermission("organisation.player") || sender.isOp())){
                GeneralMethods.sendPlayerErrorMessage((Player) sender,"Commands.NoPermission",true);
                return;
            }
        }

        OrganisationPlayer oPlayer;
        try {
            oPlayer = OrganisationPlayer.getOrganisationPlayerByName(args.get(0));
        }
        catch (OrganisationPlayerException e){
            sender.sendMessage("§cLe joueur " + args.get(1) + " n'existe pas...");
            return;
        }

        if (args.get(1).equalsIgnoreCase("info") && args.size() ==2 ){
            if(sender instanceof Player){
                ((Player) sender).openInventory(new MainOrganisationUI(oPlayer).getInv());
            }
            return;
        }
        else if(args.size() != 3){
            sender.sendMessage("§c"+this.getProperUse());
            return;
        }

        String boolValue = args.get(2);
        if (args.get(1).equalsIgnoreCase("prespawn")){
            if(boolValue.equalsIgnoreCase("True")){
                oPlayer.setIsInPrespawnMod(true);
            }
            else{
                oPlayer.setIsInPrespawnMod(false);
            }
        }
        else if (args.get(1).equalsIgnoreCase("jail")){
            //TODO
            sender.sendMessage("§cLa commande /o player <name> jail n'est pas encore finis...");
        }
        else{
            sender.sendMessage("§c"+this.getProperUse());
            return;
        }
    }

    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        ArrayList<String> lstTab = new ArrayList<>();

        if (args.size() < 1) {

            for(OrganisationPlayer oPlayer : OrganisationPlayer.getLstOrganisationPlayer())lstTab.add(oPlayer.getName());
        }
        else if (args.size() == 1) {
            lstTab.add("info");
            lstTab.add("prespawn");
            lstTab.add("jail");
        }
        else if (args.size() == 2 && !args.get(0).equalsIgnoreCase("info")){
           lstTab.add("True");
           lstTab.add("False");
        }

        return lstTab;
    }

}
