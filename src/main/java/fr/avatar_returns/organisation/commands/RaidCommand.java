package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.relation.Raid;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.Location;
import java.util.ArrayList;
import java.util.List;

public class RaidCommand extends OrganisationCommand {

    public RaidCommand() {
        super("raid", "/o raid <list|add|remove> <playerName>", GeneralMethods.getStringConf("Commands.Raid.Desc"), new String[]{"raid"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

        if (!(sender instanceof Player)) return;

        Player player = (Player) sender;
        Location playerLocation = player.getLocation();
        OrganisationPlayer oPlayer = getCommandOrganisationPlayer(player);

        if (oPlayer == null) return;

        LargeGroup playerSenderLargeGroup = null;
        for(LargeGroup largeGroup : LargeGroup.getLstLargeGroup()){
            if(largeGroup.getLstGroupConnectedOrganisationPlayer().contains(oPlayer)){
                playerSenderLargeGroup = largeGroup;
                break;
            }
        }
        if (playerSenderLargeGroup == null) return;

        OrganisationMember organisationMember = playerSenderLargeGroup.getOrganisationMember(player);
        if (!(organisationMember.getRole().hasPermission(Permission.diplomatie.launchRaid) || player.isOp() || player.hasPermission("organisation.command.raid"))) {
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.NoPermission", true);
            return;
        }

        if(args.isEmpty()) {

            Land land = Land.getLandByLocation(playerLocation);
            if (land == null) {
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Raid.NullLand", true);
                return;
            }
            if (land.getOwner() == null) {
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Raid.EmptyLand", true);
                return;
            }
            if (land.isMainLand()){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Raid.MainLand", true);
                return;
            }
            if (land.getOwner() == playerSenderLargeGroup) {
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Raid.YourLand", true);
                return;
            }
            if(playerSenderLargeGroup.getLstVland().size() >= playerSenderLargeGroup.getMaxLand()){
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Raid.MaxLand", true);
                return;
            }

            if (playerSenderLargeGroup.getDailyRaid() >= playerSenderLargeGroup.getMaxRaid() && !(player.isOp() || player.hasPermission("organisaton.admin"))) {
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Raid.MaxDailyRaidReach", true);
                return;
            }
            if (Raid.getRaidByAttacker(playerSenderLargeGroup) != null) {
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Raid.AttackerAlreadyInRaid", true);
                return;
            }
            if (Raid.getRaidByDefender(land.getOwner()) != null) {
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Raid.DefenderAlreadyInRaid", true);
                return;
            }
            if (Raid.getOnLineExperimentedPlayer(land.getOwner()) <= 0) {
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Raid.NotEnafExperimentedPlayer", true);
                return;
            }

            Raid raid = new Raid(playerSenderLargeGroup, land);
            raid.getLstAttacker().add(oPlayer);

            raid.sendAttackerTitle("§cRaid","§cVous attaquez le land "+land.getName());
            raid.sendDefenderTitle("§cVous subissez un Raid","§c"+playerSenderLargeGroup.getName()+" attaque votre land "+land.getName());

            for(OrganisationPlayer tmpOPlayer : playerSenderLargeGroup.getLstGroupConnectedOrganisationPlayer())GeneralMethods.sendPlayerMessage(tmpOPlayer,"Votre organisation raid le land "+land.getName());
            for(OrganisationPlayer tmpOPlayer : land.getOwner().getLstGroupConnectedOrganisationPlayer())GeneralMethods.sendPlayerMessage(tmpOPlayer,playerSenderLargeGroup.getName() + " raid votre land "+land.getName()+"\nDéfendez le !");

            GeneralMethods.sendPlayerMessage(player, "Le raid est lancé.\n" +
                    "Vous pouvez composer une équipe de "+raid.getMaxAttacker()+" joueur(s)\n" +
                    "§7/o raid add <pseudo>");

        }
        else {

            Raid raid = Raid.getRaidByAttacker(playerSenderLargeGroup);
            if(raid == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Raid.YouAreNotInRaid", true);
                return;
            }

            if(args.size() == 1 && args.get(0).equalsIgnoreCase("list")){

                String lst = "Liste des joueurs dans votre équipe : \n";
                for(OrganisationPlayer oPlayerTeam : raid.getLstAttacker())lst += " - "+oPlayerTeam.getName()+"\n";
                lst += "\nNombre de joueurs de l'équipe : "+raid.getLstAttacker().size() + "/" + raid.getMaxAttacker();
                GeneralMethods.sendPlayerMessage(player, lst);

                return;
            }
            if(args.size() == 1 && args.get(0).equalsIgnoreCase("interrupt") && (player.isOp() || player.hasPermission("organisation.raid.interrupt"))) {
                raid.interrupt();
                GeneralMethods.sendPlayerMessage(player,"Commands.Raid.Interrupt",true);
                return;
            }
            if(raid.getPhase() != "MATCHMAKING"){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Raid.NotMatchMaking", true);
                return;
            }
            else if(args.size() == 2){

                String subCommand = args.get(0);
                String playerName = args.get(1);

                OrganisationPlayer oPlayerTarget = null;
                try {
                    oPlayerTarget = OrganisationPlayer.getOrganisationPlayerByName(playerName);
                }catch (OrganisationPlayerException e){
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Raid.Remove.Success", true);
                    return;
                }

                if(subCommand.equalsIgnoreCase("add")){

                    if(raid.getLstAttacker().contains(oPlayerTarget)){
                        GeneralMethods.sendPlayerMessage(player,"Commands.Raid.Add.PlayerAlreadyInRaid", true);
                        return;
                    }
                    else if(raid.getLstAttacker().size() >= raid.getMaxAttacker()){
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Raid.Add.ToMutchAttacker", true);
                        return;
                    }
                    else{
                        raid.getLstAttacker().add(oPlayerTarget);
                        //if(Organisation.getHook(ProjectKorraHook.class).isPresent())Organisation.getHook(ProjectKorraHook.class).get().disableActionBar(oPlayerTarget);
                        GeneralMethods.sendPlayerMessage(oPlayerTarget,"Vous avez été ajouté au raid");
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Raid.Add.Success", true);
                        return;
                    }

                }
                else if(subCommand.equalsIgnoreCase("remove")){
                    if(raid.getLstAttacker().contains(oPlayerTarget)){
                        raid.getLstAttacker().remove(oPlayerTarget);
                        //if(Organisation.getHook(ProjectKorraHook.class).isPresent())Organisation.getHook(ProjectKorraHook.class).get().enableActionBar(oPlayerTarget);
                        GeneralMethods.sendPlayerMessage(oPlayerTarget,"Vous avez été retiré du raid");
                        GeneralMethods.sendPlayerMessage(player,"Commands.Raid.Remove.Success", true);
                        return;
                    }
                    else{
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Raid.Remove.PlayerNotInRaid", true);
                        return;
                    }
                }
                else{
                    GeneralMethods.sendPlayerErrorMessage(player,"/o raid <list|add|remove> <playerName>");
                    return;
                }
            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player,"/o raid <list|add|remove> <playerName>");
                return;
            }
        }
    }

    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        final List<String> l = new ArrayList<>();

        if(!(sender instanceof Player))return l;

        Player player = (Player) sender;

        OrganisationPlayer organisationPlayerSender = getCommandOrganisationPlayer(player);
        if (organisationPlayerSender == null) return l;

        LargeGroup playerSenderLargeGroup = null;
        for(LargeGroup largeGroup : LargeGroup.getLstLargeGroup()){
            if(largeGroup.getLstGroupConnectedOrganisationPlayer().contains(organisationPlayerSender)){
                playerSenderLargeGroup = largeGroup;
                break;
            }
        }
        if (playerSenderLargeGroup == null) return l;

        OrganisationMember organisationMember = playerSenderLargeGroup.getOrganisationMember(player);
        if (!(organisationMember.getRole().hasPermission(Permission.diplomatie.launchRaid) || player.isOp() || player.hasPermission("organisation.command.raid")))return l;

        Raid raid = Raid.getRaidByAttacker(playerSenderLargeGroup);
        if(raid == null)return l;

        if(args.size() == 0){

            l.add("list");
            if(raid.getPhase().equals("MATCHMAKING")){
                l.add("add");
                l.add("remove");
            }

            if(player.isOp() || player.hasPermission("organisation.raid.interrupt"))l.add("interrupt");
        }
        else if(args.size() == 1 && args.get(0).equalsIgnoreCase("add")){
            for(OrganisationPlayer oPlayer: playerSenderLargeGroup.getLstGroupConnectedOrganisationPlayer()){
                if(!raid.getLstAttacker().contains(oPlayer))l.add(oPlayer.getName());
            }
        }
        else if(args.size() == 1 && args.get(0).equalsIgnoreCase("remove")){
            for(OrganisationPlayer oPlayer: raid.getLstAttacker())l.add(oPlayer.getName());
        }

        return l;
    }


}
