package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.needClass.Disband;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import java.util.List;

public class DisbandCommand extends OrganisationCommand {


    public DisbandCommand() {
        super("disband", "/o disband <Organisation>", GeneralMethods.getStringConf("Commands.Disband.Desc"), new String []{"disband", "d"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

        if (sender instanceof Player) {

            Player playerSender =  (Player) sender;

            if( args.size() < 1 && args.size() > 2) {
                GeneralMethods.sendPlayerErrorMessage(playerSender,this.getProperUse());
                return;
            }

            OrganisationPlayer organisationPlayerSender = getCommandOrganisationPlayer(playerSender);
            if(organisationPlayerSender == null)return;

            //Get group of players.
            Group playerSenderGroup = getCommandPlayerGroup(organisationPlayerSender, args,0);
            if(playerSenderGroup == null) return;

            if (!playerSenderGroup.hasPermission(playerSender, Permission.management.disband) && !playerSender.hasPermission("organisation.command.disband")){
                GeneralMethods.sendPlayerErrorMessage(playerSender,"Commands.NoPermission",true);
                return;
            }

            if(args.size() == 2){
                if( Disband.isDisbandValid(organisationPlayerSender, playerSenderGroup, args.get(1)) ){
                    Group.remove(playerSenderGroup);
                    GeneralMethods.sendPlayerMessage(playerSender,"Commands.Disband.Success",true);
                }
                else{
                    GeneralMethods.sendPlayerErrorMessage(playerSender,"Commands.Disband.TokenExpiration",true);
                }
            }
            else{
                Disband disband = new Disband(organisationPlayerSender,playerSenderGroup);
                String token = disband.getToken();
                GeneralMethods.sendPlayerMessage(playerSender,"Pour disband, veuillez confirmer en tapant : /o disband "+playerSenderGroup.getName()+" "+token);
            }
        }
    }


    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        ArrayList<String> lstTab = new ArrayList<>();

        if (!(sender instanceof Player)) return lstTab;

        Player playerSender = (Player) sender;
        try {
            OrganisationPlayer organisationPlayerSender = OrganisationPlayer.getOrganisationPlayer(playerSender);
            for (Group group : organisationPlayerSender.getLstGroup()) lstTab.add(group.getName());
        } catch (OrganisationPlayerException e) {
            return lstTab;
        }

        return lstTab;
    }
}
