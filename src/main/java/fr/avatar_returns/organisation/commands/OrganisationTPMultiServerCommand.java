package fr.avatar_returns.organisation.commands;


import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.utils.teleportation.MultiServerTPMethodimplements;
import org.bukkit.command.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OrganisationTPMultiServerCommand {


    private class OrganisationTPMultiServerCommandTab implements TabCompleter {

        @Override public List<String> onTabComplete(final CommandSender sender, final Command command, final String alias, final String[] args) {

            List<String> argsSuggestion = new ArrayList<>();
            if (args.length == 1)
                argsSuggestion.add("<Serveur>");
            if (args.length == 2) {
                argsSuggestion.add("all");
                for (OrganisationPlayer oPlayer : OrganisationPlayer.getLstConnectedOrganisationPlayer()) argsSuggestion.add(oPlayer.getName());
            }
            if (args.length == 3)
                argsSuggestion.add("<X>");
            if (args.length == 4)
                argsSuggestion.add("<Y>");
            if (args.length == 5)
                argsSuggestion.add("<Z>");
            if (args.length == 6)
                argsSuggestion.add("<World>");
            return argsSuggestion;
        }
    }


    private final Organisation plugin;
    public static boolean debugEnabled = false;

    public OrganisationTPMultiServerCommand(final Organisation plugin) {
        this.plugin = plugin;
        debugEnabled = Organisation.plugin.getConfig().getBoolean("debug");
        this.init();
    }

    public static String[] commandaliases = { "servertps"};

    private void init() {
        final PluginCommand organisation = this.plugin.getCommand("servertps");


        final CommandExecutor cmdExecutor = (sender, c, executedCommand, args) -> {

            if (Arrays.asList(commandaliases).contains(executedCommand.toLowerCase())) {
                if (args.length != 0) {
                    this.execute(sender, args);
                }

                OrganisationCommand.instances.get("help").execute(sender, new ArrayList<String>());
                return true;
            }

            return false;
        };
        organisation.setExecutor(cmdExecutor);
        organisation.setTabCompleter(new OrganisationTPMultiServerCommandTab());
    }

    private void execute(CommandSender commandSender, String[] args){

        if (commandSender.hasPermission("tpvelocity.*")) {
            if (args.length == 6) {
                if (args[1].equalsIgnoreCase("all")) {
                    for (OrganisationPlayer oPlayer : OrganisationPlayer.getLstConnectedOrganisationPlayer()) {
                        MultiServerTPMethodimplements.sendTPCommand(args[0], oPlayer.getName(), Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]), args[5]);
                    }
                } else {
                    try {
                        OrganisationPlayer oPlayer = OrganisationPlayer.getOrganisationPlayerByName(args[1]);
                        if(oPlayer.isConnected()) MultiServerTPMethodimplements.sendTPCommand(args[0], args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]), args[5]);
                        else commandSender.sendMessage("§cLe joueur "+args[1]+" n'est pas connecté...'");
                    } catch (OrganisationPlayerException e) {
                        commandSender.sendMessage("§cLe joueur "+args[1]+" n'as pas trouvé");
                    }
                }
            }
        }
    }
}
