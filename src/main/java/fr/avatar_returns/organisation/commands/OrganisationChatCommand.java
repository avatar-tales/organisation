package fr.avatar_returns.organisation.commands;


import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.rabbitMQPackets.player.SimpleChatPacket;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class OrganisationChatCommand {

    private class OrganisationChatCommandTab implements TabCompleter {

        @Override public List<String> onTabComplete(final CommandSender sender, final Command command, final String alias, final String[] args) {

            List<String> argsSuggestion = new ArrayList<>();
            return argsSuggestion;
        }
    }

    private final Organisation plugin;
    public static boolean debugEnabled = false;

    public OrganisationChatCommand(final Organisation plugin) {
        this.plugin = plugin;
        debugEnabled = Organisation.plugin.getConfig().getBoolean("debug");
        this.init();
    }

    public static String[] commandaliases = { "oo", "ff", "fchat", "ochat"};

    private void init() {
        final PluginCommand organisation = this.plugin.getCommand("oo");


        final CommandExecutor cmdExecutor = (sender, c, executedCommand, args) -> {

            if (Arrays.asList(commandaliases).contains(executedCommand.toLowerCase())) {
                if (args.length > 0) {
                    this.execute(sender, args);
                }

                OrganisationCommand.instances.get("help").execute(sender, new ArrayList<String>());
                return true;
            }

            return false;
        };
        organisation.setExecutor(cmdExecutor);
        organisation.setTabCompleter(new OrganisationChatCommandTab());
    }

    private void execute(CommandSender commandSender, String[] args){

        if(!(commandSender instanceof Player))return;
        Player player = (Player) commandSender;

        OrganisationPlayer organisationPlayerSender = OrganisationCommand.getCommandOrganisationPlayer(player);
        if(organisationPlayerSender == null)return;

        if(organisationPlayerSender.getLstGroup().isEmpty())return;
        else if(organisationPlayerSender.getLstGroup().size() == 1){

            Group group = organisationPlayerSender.getLstGroup().get(0);
            sendGroupMessage(organisationPlayerSender, group, OrganisationCommand.getStringFromArgs(Arrays.asList(args) , 0));
        }
        else{
            Group group = OrganisationCommand.getCommandPlayerGroup(organisationPlayerSender, Arrays.asList(args) , 0);
            if(group == null)return;

            sendGroupMessage(organisationPlayerSender, group, OrganisationCommand.getStringFromArgs(Arrays.asList(args) , 1));
        }
    }

    public static void sendGroupMessage(OrganisationPlayer oPlayer, Group group, String text){

        if(!oPlayer.isConnected())oPlayer.setIsConnected(true); //Correctif, un joueur qui n'est pas connecté ne peut pas envoyer de message.

        if(oPlayer.isMuted()){
            if(oPlayer.isMuted()){
                GeneralMethods.sendPlayerErrorMessage(oPlayer.getOfflinePlayer(), "Organisation.Muted", true);
                return;
            }
        }
        OrganisationMember member = group.getOrganisationMember(oPlayer.getOfflinePlayer());

        String color = (group instanceof LargeGroup)? "§2" : "§a";
        String message = color+"["+member.getRole().getName() + color +"] ";
        message += oPlayer.getName() + color + " >> ";
        message += text;

        HashMap<String, String> mapSmiley = ConfigManager.getConfig().getObject("Organisation.chat.settings.remap", HashMap.class);
        for(String key : mapSmiley.keySet()) message = message.replace(key, "§f"+ConfigManager.getConfig().getString("Organisation.chat.settings.remap." + key)+color);

        for(OrganisationPlayer oConnectedPlayer : group.getLstGroupConnectedOrganisationPlayer()){
            if(oConnectedPlayer.isMuted())continue;
            if(oConnectedPlayer.getOfflinePlayer().isOnline())oConnectedPlayer.getOfflinePlayer().getPlayer().sendMessage(message);
            else {
                GeneralMethods.sendPacket(new SimpleChatPacket(oConnectedPlayer, message));
            }
        }
    }
}
