package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationGroupPlayerException;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class LeaveCommand extends OrganisationCommand{

    public LeaveCommand() {
        super("leave", "/o leave <Organisation>", GeneralMethods.getStringConf("Commands.Leave.Desc"), new String []{"leave", "l"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

        if (sender instanceof Player) {

            Player playerSender =  (Player) sender;

            if( args.size() > 1) {
                GeneralMethods.sendPlayerErrorMessage(playerSender,this.getProperUse());
                return;
            }

            OrganisationPlayer organisationPlayerSender = getCommandOrganisationPlayer(playerSender);
            if(organisationPlayerSender == null)return;

            Group playerSenderGroup = getCommandPlayerGroup(organisationPlayerSender, args, 0);
            if(playerSenderGroup == null)return;

            playerSenderGroup.removeMember(organisationPlayerSender);
            GroupRank.checkProgression(playerSenderGroup);
            GeneralMethods.sendPlayerMessage(playerSender, "Vous avez quitté " + playerSenderGroup.getName());

            if (playerSenderGroup.getLstMember().isEmpty() && !playerSender.isOp() && !playerSender.hasPermission("organisation.command.leave.dontDisbandEmptyOrganisation")){
                GeneralMethods.sendPlayerMessage(playerSender,"Commands.Leave.LastPlayerDestroy",true);
                Group.remove(playerSenderGroup);
            }
        }
    }


    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        ArrayList<String> lstTab = new ArrayList<>();

        if (!(sender instanceof Player)) return lstTab;

        Player playerSender = (Player) sender;
        try {
            OrganisationPlayer organisationPlayerSender = OrganisationPlayer.getOrganisationPlayer(playerSender);
            for (Group group : organisationPlayerSender.getLstGroup()) lstTab.add(group.getName());
        } catch (OrganisationPlayerException e) {
            return lstTab;
        }

        return lstTab;
    }
}
