package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.storedInventory.MineUtils;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.storedInventory.StoredInventory;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Panda;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import javax.swing.text.Position;
import java.util.ArrayList;
import java.util.List;

public class InventoryCommand extends OrganisationCommand {


    public InventoryCommand() {
        super("inventory", "/o inventory <list|create|edit|remove|rotateMine|link> <type|id> <name|inventory, name, ratio> <ratio|name, idInventory> <idPosition>", GeneralMethods.getStringConf("Commands.Inventory.Desc"), new String []{"inventory", "inv"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args){

        if(!(sender instanceof Player))return;

        Player player = (Player) sender;

        if(!(player.hasPermission("organisation.command.inventory") || player.isOp())){
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.NoPermission");
            return;
        }

        if(args.size()< 1){
            GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
            return;
        }
        String subCommand = args.get(0);

        if(subCommand.equalsIgnoreCase("list")){

            String msg = "";
            ArrayList<StoredInventory> lstStoredInventory = new ArrayList<>();

            if(args.size() == 2){
                lstStoredInventory = StoredInventory.getLstStoredInventoryType(args.get(1));
                msg += "§6Liste des inventaires de types §e" + args.get(1).toUpperCase() + "§6 : \n";
            }
            else{
                msg += "§6Liste des inventaires : \n";
                lstStoredInventory = StoredInventory.getLstStoredInventory();
            }

            for(StoredInventory storedInventory : lstStoredInventory){
                String id = "§d  - " + ((storedInventory.getId() != 1)?storedInventory.getId() : "Mine Default (1)");
                String type = "§e [" +storedInventory.getType()+"]";
                String name = "§a " + storedInventory.getName();
                String ratio = "§7 " + ((storedInventory.getId() != 0)?((storedInventory.getRatio() > 0.0)? (int)((storedInventory.getRatio())*100) +"/100" : ""+storedInventory.getMaxNumber()) : "");

                msg += id + type + name + ratio + "\n";
            }

            msg += "§6Total : "+lstStoredInventory.size()+"\n";
            GeneralMethods.sendPlayerMessage(player,msg);
            return;
        }
        if(subCommand.equalsIgnoreCase("rotateMine")){
            MineUtils.rotateMine();
            GeneralMethods.sendPlayerMessage(player,"Commands.Inventory.RotateMine", true);
        }
        else if(subCommand.equalsIgnoreCase("create") && args.size() == 4){

            String type = args.get(1).toUpperCase();
            String name = args.get(2);
            String ratioStr = args.get(3);

            int maxNumber = -1;
            double ratio = -1.0;

            try{
                ratio = Double.parseDouble(ratioStr);
                if(ratio%1 == 0){
                    maxNumber = (int)ratio;
                    ratio = -1.0;
                }

            } catch (NumberFormatException e){
                try{
                    maxNumber = Integer.parseInt(ratioStr);
                }catch (NumberFormatException exception){
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Create.InvalidArgs", true);
                    return;
                }
            }

            if(!StoredInventory.Type.isValidType(type)){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Create.InvalidType", true);
                return;
            }

            StoredInventory tmpStoredInventory =  new StoredInventory(name, player, ratio, maxNumber, type);
            player.openInventory(tmpStoredInventory.getInventory());
            GeneralMethods.sendPlayerMessage(player,"Commands.Inventory.Create.Success", true);
        }
        else if(subCommand.equalsIgnoreCase("edit")){

            int id = 0;
            try {
                id = Integer.parseInt(args.get(1));
            } catch(NumberFormatException e){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Edit.InvalidInt", true);
                return;
            }
            StoredInventory stroredInventory =  StoredInventory.getStoredInventoryById(id);
            if(stroredInventory == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Edit.InventoryNotFound", true);
                return;
            }

            if( args.size() < 3){
                GeneralMethods.sendPlayerErrorMessage(player,"/o inventory edit "+id+" <name|inventory|ratio> <ratio|name>");
                return;
            }

            String actionEdit = args.get(2);
            if(actionEdit.equalsIgnoreCase("inventory") && args.size() == 3){
                player.openInventory(stroredInventory.getInventory());
                GeneralMethods.sendPlayerMessage(player,"Commands.Inventory.Edit.Inventory.Open", true);
            }
            else if (actionEdit.equalsIgnoreCase("name") && args.size() == 4){
                stroredInventory.setName(args.get(3));
                GeneralMethods.sendPlayerMessage(player,"Commands.Inventory.Edit.Name.Success", true);
            }
            else if (actionEdit.equalsIgnoreCase("ratio") && args.size() == 4){

                int maxNumber = -1;
                double ratio = -1.0;

                try{
                    maxNumber = Integer.parseInt(args.get(3));
                } catch (NumberFormatException e){
                    try{
                        ratio = Double.parseDouble(args.get(3));
                    }catch (NumberFormatException exception){
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Edit.Ratio.InvalidArgs", true);
                        return;
                    }
                }
                stroredInventory.setRatio(ratio);
                stroredInventory.setMaxNumber(maxNumber);

                GeneralMethods.sendPlayerMessage(player,"Commands.Inventory.Edit.Ratio.Success", true);
            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player,"/o inventory edit "+id+" <name|inventory|ratio> <ratio|name>");
                return;
            }
        }
        else if(subCommand.equalsIgnoreCase("remove") && args.size() == 2){

            int id = 0;
            try {
                id = Integer.parseInt(args.get(1));
            } catch(NumberFormatException e){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Remove.InvalidInt", true);
                return;
            }

            if(id == 1){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Remove.CantRemoveDefaultMine", true);
                return;
            }

            StoredInventory stroredInventory =  StoredInventory.getStoredInventoryById(id);
            if(stroredInventory == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Remove.InventoryNotFound", true);
                return;
            }

            stroredInventory.remove();
            GeneralMethods.sendPlayerMessage(player,"Commands.Inventory.Remove.Success", true);
        }
        else if(subCommand.equalsIgnoreCase("link") && args.size() == 4){

            String type = args.get(1);
            int idStoredInventory = 0;
            int idPosition = 0;

            try{
                idStoredInventory = Integer.valueOf(args.get(2));
                idPosition = Integer.valueOf(args.get(3));
            }
            catch (NumberFormatException e){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Edit.InvalidInt", true);
                return;
            }

            if(type.equalsIgnoreCase(""+StoredInventory.Type.MINE_FIXE.toString())) {

                StoredInventory storedInventory = StoredInventory.getStoredInventoryById(idStoredInventory);
                if(storedInventory == null){
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Link.StoredInventory.NotFound",true);
                    return;
                }
                else if (!storedInventory.getType().equals(StoredInventory.Type.MINE_FIXE.toString())) {
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Link.StoredInventory.NotMineFixe",true);
                    return;
                }

                PositionPointInterest positionPointInterest = PositionPointInterest.getById(idPosition);
                if(positionPointInterest == null){
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Link.Position.NotFound",true);
                    return;
                }
                else if (!positionPointInterest.getContext().equals(PositionPointInterest.Type.MINE_FIXE)) {
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Inventory.Link.Position.NotMineFixe",true);
                    return;
                }

                positionPointInterest.setOptionalValueOne(""+storedInventory.getId());
                GeneralMethods.sendPlayerMessage(player,"Commands.Inventory.Link.Mine.Success", true);
            }
            else{
                GeneralMethods.sendPlayerMessage(player,"/o inventory link <TYPE> <idInventory> <idPosition>");
            }

        }
        else{
            GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
            return;
        }
    }

    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args){

        ArrayList<String> l = new ArrayList<>();
        if (!(sender instanceof Player)) return l;

        Player player = (Player) sender;
        if(args.size() == 0){
            l.add("list");
            l.add("create");
            l.add("edit");
            l.add("link");
            l.add("remove");
            l.add("rotateMine");
        }
        else if (args.size() >= 1){

            String subCommand = args.get(0).toLowerCase();
            if(args.size() == 1) {
                if (subCommand.equals("remove") || subCommand.equals("edit")) {
                    for (StoredInventory storedInventory : StoredInventory.getLstStoredInventory()) {
                        l.add("" + storedInventory.getId());
                    }
                }
                else if (subCommand.equals("list") || subCommand.equals("create")) {
                    l.add("" + StoredInventory.Type.MINE);
                    l.add("" + StoredInventory.Type.MINE_FIXE);
                    l.add("" +StoredInventory.Type.RANK_OBJECTIF);
                }
                else if (subCommand.equals("link")){
                    l.add("" + StoredInventory.Type.MINE_FIXE);
                }
            }
            else if (args.size() == 2) {

                if (subCommand.equals("create")) {
                    l.add("<name>");
                }
                else if (subCommand.equals("edit")) {
                    l.add("inventory");
                    l.add("name");
                    l.add("ratio");
                }
                else if (subCommand.equals("link")) {
                    if (args.get(1).equalsIgnoreCase("" + StoredInventory.Type.MINE_FIXE.toString())) {
                        for (StoredInventory mineFixe : StoredInventory.getLstStoredInventoryType(StoredInventory.Type.MINE_FIXE)) {
                            l.add("" + mineFixe.getId());
                        }
                    }
                }

            }
            else if (args.size() == 3) {
                if (subCommand.equals("create")) {
                    l.add("<number|percentage>");
                }
                else if (subCommand.equals("edit")) {
                    if (args.get(2).equalsIgnoreCase("name")) l.add("<new name>");
                    if (args.get(2).equalsIgnoreCase("ratio")) l.add("<number|percentage>");
                }
                else if (subCommand.equals("link")) {
                    if (args.get(1).equalsIgnoreCase("" + StoredInventory.Type.MINE_FIXE.toString())) {
                        for (PositionPointInterest positionMineFixe : PositionPointInterest.getByContext(PositionPointInterest.Type.MINE_FIXE)) {
                            l.add("" + positionMineFixe.getId());
                        }
                    }
                }
            }
        }

        return l;
    }
}
