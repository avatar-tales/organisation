package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.largeGroup.Faction;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.rank.GroupObjective;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ListCommand extends OrganisationCommand {

    public ListCommand() {
        super("list", "/o list <Nation|Faction|Guilde> <--hide>", GeneralMethods.getStringConf("Commands.List.Desc"), new String[]{"list","l"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

        if(!(sender instanceof Player)) return;
        Player player = (Player) sender;

        if(args.size() == 0){

            player.sendMessage("§6--------------- List -----------------");
            player.sendMessage("");

            if(displayFaction(player, false))player.sendMessage("");
            if(displayGuilde(player, false))player.sendMessage("");
        }
        else if(args.size() == 1 || args.size() == 2){

            args.set(0,args.get(0).toUpperCase());
            boolean displayHide = false;
            if(args.size() == 2 && player.hasPermission("organisation.command.list.hide")){
                displayHide = true;
            }

            switch (args.get(0)){

                case "FACTION" :
                    displayFaction(player, displayHide);
                    break;

                case "GUILDE" :
                    displayGuilde(player, displayHide);
                    break;
                case "--HIDE":
                    boolean hasPermission = player.hasPermission("organisation.command.list.hide");
                    if(displayFaction(player, hasPermission))player.sendMessage("");
                    if(displayGuilde(player, hasPermission))player.sendMessage("");
                    break;
                default:
                    GeneralMethods.sendPlayerErrorMessage(player,getProperUse());
            }


        }
        else{
            GeneralMethods.sendPlayerErrorMessage(player,getProperUse());
        }

    }

    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        ArrayList<String> lstTab = new ArrayList<>();

        if(args.size()==0){

            lstTab.add("Faction");
            lstTab.add("Guilde");
            if(sender.hasPermission("organisation.command.list.hide")) lstTab.add("--hide");
        }
        else  if(args.size()==1){
            if(sender.hasPermission("organisation.command.list.hide"))lstTab.add("--hide");
        }

        return lstTab;
    }

    private boolean displayFaction(Player player, boolean displayHide){

        ArrayList<Faction> lstDisplayFaction = Faction.getLstFaction(displayHide);
        Collections.sort(lstDisplayFaction, new Comparator<Faction>() {
                    @Override
                    public int compare(Faction faction1, Faction faction2) {

                        int orderComparison = Integer.compare(faction1.getRank().getOrder(), faction2.getRank().getOrder());
                        if (orderComparison != 0)return orderComparison;
                        else{
                            // On compare par nombre de vassaux
                            int vassalComparison = Integer.compare(faction1.getLstVassal().size(), faction2.getLstVassal().size());
                            if (vassalComparison != 0)return vassalComparison;
                            else{
                                // On compare par nombre de land
                                int landComparison = Integer.compare(faction1.getLstVland().size(), faction2.getLstVland().size());
                                if (landComparison != 0)return landComparison;
                                else{
                                    // On compare par nombre d'objectif accomplis
                                    int nbObjectAcomplishFaction1 = 0;
                                    for(GroupObjective groupObjective : faction1.getLstGroupObjective()) if(groupObjective.isAcomplished()) nbObjectAcomplishFaction1 ++;
                                    int nbObjectAcomplishFaction2 = 0;
                                    for(GroupObjective groupObjective : faction2.getLstGroupObjective()) if(groupObjective.isAcomplished()) nbObjectAcomplishFaction2 ++;

                                    int objectiveComparison = Integer.compare(nbObjectAcomplishFaction1, nbObjectAcomplishFaction2);
                                    if (objectiveComparison != 0)return objectiveComparison;
                                    else{
                                        //Sinon on compare le nombre de joueur des factions.
                                        return Integer.compare(faction1.getLstMember().size(), faction2.getLstMember().size());
                                    }
                                }
                            }
                        }
                    }
                });
        Collections.reverse(lstDisplayFaction);

        player.sendMessage("§6Faction (" + lstDisplayFaction.size() + ")");
        for( Faction faction : lstDisplayFaction){
            player.sendMessage("§7 - ["+ faction.getRank().getName()+ "] " + faction.getName() + " " + displayGroupInfo(faction));
        }

        return lstDisplayFaction.size()> 0;
    }
    private boolean displayGuilde(Player player, boolean displayHide){

        ArrayList<Guilde> lstDisplayGuilde = Guilde.getLstGuilde(displayHide);
        Collections.sort(lstDisplayGuilde, new Comparator<Guilde>() {
            @Override public int compare(Guilde guilde1, Guilde guilde2) {

                int orderComparison = Integer.compare(guilde1.getRank().getOrder(), guilde2.getRank().getOrder());
                if (orderComparison != 0)return orderComparison;

                else{
                    // On compare par nombre d'objectif accomplis
                    int nbObjectAcomplishGuilde1 = 0;
                    for(GroupObjective groupObjective : guilde1.getLstGroupObjective()) if(groupObjective.isAcomplished()) nbObjectAcomplishGuilde1 ++;
                    int nbObjectAcomplishGuilde2 = 0;
                    for(GroupObjective groupObjective : guilde2.getLstGroupObjective()) if(groupObjective.isAcomplished()) nbObjectAcomplishGuilde2 ++;

                    int objectiveComparison = Integer.compare(nbObjectAcomplishGuilde1, nbObjectAcomplishGuilde2);
                    if (objectiveComparison != 0)return objectiveComparison;
                    else{
                        // On compare par la taille des joueurs.
                        return Integer.compare(guilde1.getLstMember().size(), guilde2.getLstMember().size());
                    }
                }
            }
        });
        Collections.reverse(lstDisplayGuilde);

        player.sendMessage("§6Guilde ("+lstDisplayGuilde.size()+")");
        for(Guilde guilde : lstDisplayGuilde){
            player.sendMessage("§7 - ["+guilde.getRank().getName()+"] "+guilde.getName() + " " + displayGroupInfo(guilde));
        }
        return Guilde.getLstGuilde(displayHide).size()> 0;
    }

    public String displayGroupInfo(Group group){
        return "( "+group.getLstGroupConnectedMember().size()+"/"+group.getLstMember().size()+")";
    }
}
