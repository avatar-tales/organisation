package fr.avatar_returns.organisation.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class HelpCommand extends OrganisationCommand{

    public HelpCommand() {
        super("help", "/o help", "How use Organisation plugin",new String[] { "help", "h" });
    }

    @Override
    public void execute(final CommandSender sender, final List<String> args) {

    }

    @Override
    protected List<String> getTabCompletion(final CommandSender sender, final List<String> args) {
        if (args.size() >= 2 || !sender.hasPermission("organisation.admin.default")) {
            return new ArrayList<String>();
        }

        final List<String> l = new ArrayList<String>();
        if (args.size() == 0) {

            l.add("Faction");
            l.add("Group");

        } else {
            for (final Player p : Bukkit.getOnlinePlayers()) {
                l.add(p.getName());
            }
        }
        return l;
    }
}
