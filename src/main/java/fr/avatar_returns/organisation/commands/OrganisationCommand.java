package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationGroupPlayerException;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.*;

/**
 * Abstract representation of a command executor. Implements {@link SubCommand}.
 *
 * @author kingbirdy
 * Adapt by FengWan for Organisation
 */

public class OrganisationCommand implements SubCommand {

    protected String noPermissionMessage, mustBePlayerMessage;

    /**
     * The full name of the command.
     */
    private final String name;
    /**
     * The proper use of the command, in the form '/f {@link OrganisationCommand#name
     * name} arg1 arg2 ... '
     */
    private final String properUse;
    /**
     * A description of what the command does.
     */
    private final String description;
    /**
     * String[] of all possible aliases of the command.
     */
    private final String[] aliases;
    /**
     * List of all command executors which extends PKCommand
     */
    public static Map<String, OrganisationCommand> instances = new HashMap<String, OrganisationCommand>();

    public OrganisationCommand(final String name, final String properUse, final String description, final String[] aliases) {
        this.name = name;
        this.properUse = properUse;
        this.description = description;
        this.aliases = aliases;

        this.noPermissionMessage = ChatColor.RED + ConfigManager.defaultConfig.get().getString("Commands.NoPermission");
        this.mustBePlayerMessage = ChatColor.RED + ConfigManager.defaultConfig.get().getString("Commands.MustBePlayer");

        instances.put(name, this);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getProperUse() {
        return this.properUse;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public String[] getAliases() {
        return this.aliases;
    }

    @Override
    public void help(final CommandSender sender, final boolean description) {
        sender.sendMessage(ChatColor.GOLD + "Proper Usage: " + ChatColor.DARK_AQUA + this.properUse);
        if (description) {
            sender.sendMessage(ChatColor.YELLOW + this.description);
        }
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

    }

    /**
     * Checks if the {@link CommandSender} has permission to execute the
     * command. The permission is in the format 'organisation.command.
     *
     *
     * @param sender The CommandSender to check
     * @return True if they have permission, false otherwise
     */

    protected boolean hasPermission(final CommandSender sender) {
        if (sender.hasPermission("organisation.command." + this.name)) {
            return true;
        } else {
            sender.sendMessage(this.noPermissionMessage);
            return false;
        }
    }

    /**
     * Checks if the {@link CommandSender} has permission to execute the
     * command. The permission is in the format 'organisation.command.
     *
     *
     * @param sender The CommandSender to check
     * @param extra The additional node to check
     * @return True if they have permission, false otherwise
     */
    protected boolean hasPermission(final CommandSender sender, final String extra) {
        if (sender.hasPermission("organisation.command." + this.name + "." + extra)) {
            return true;
        } else {
            sender.sendMessage("§c"+this.noPermissionMessage);
            return false;
        }
    }

    /**
     * Checks if the argument length is within certain parameters, and if not,
     * informs the CommandSender of how to correctly use the command.
     *
     * @param sender The CommandSender who issued the command
     * @param size The length of the arguments list
     * @param min The minimum acceptable number of arguments
     * @param max The maximum acceptable number of arguments
     * @return True if min < size < max, false otherwise
     */
    protected boolean correctLength(final CommandSender sender, final int size, final int min, final int max) {
        if (size < min || size > max) {
            this.help(sender, false);
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if the CommandSender is an instance of a Player. If not, it tells
     * them they must be a Player to use the command.
     *
     * @param sender The CommandSender to check
     * @return True if sender instanceof Player, false otherwise
     */
    protected boolean isPlayer(final CommandSender sender) {
        if (sender instanceof Player) {
            return true;
        } else {
            sender.sendMessage("§c"+this.mustBePlayerMessage);
            return false;
        }
    }

    /**
     * Returns a boolean if the string provided is numerical.
     *
     * @param id
     * @return boolean
     */
    protected boolean isNumeric(final String id) {
        final NumberFormat formatter = NumberFormat.getInstance();
        final ParsePosition pos = new ParsePosition(0);
        formatter.parse(id, pos);
        return id.length() == pos.getIndex();
    }

    /**
     * Returns a list for of commands for a page.
     *
     * @param entries
     * @param title
     * @param page
     * @return
     */
    protected List<String> getPage(final List<String> entries, final String title, int page, final boolean sort) {
        final List<String> strings = new ArrayList<String>();
        if (sort) {
            Collections.sort(entries);
        }

        if (page < 1) {
            page = 1;
        }
        if ((page * 8) - 8 >= entries.size()) {
            page = Math.round(entries.size() / 8) + 1;
            if (page < 1) {
                page = 1;
            }
        }
        strings.add(ChatColor.DARK_GRAY + "- [" + ChatColor.GRAY + page + "/" + (int) Math.ceil((entries.size() + .0) / (8 + .0)) + ChatColor.DARK_GRAY + "]");
        strings.add(title);
        if (entries.size() > ((page * 8) - 8)) {
            for (int i = ((page * 8) - 8); i < entries.size(); i++) {
                if (entries.get(i) != null) {
                    strings.add(entries.get(i).toString());
                }
                if (i >= (page * 8) - 1) {
                    break;
                }
            }
        }
        return strings;
    }

    /** Gets a list of valid arguments that can be used in tabbing. */
    protected List<String> getTabCompletion(final CommandSender sender, final List<String> args) {
        return new ArrayList<String>();
    }

    protected static LargeGroup getCommandPlayerLargeGroup(OrganisationPlayer organisationPlayer, List<String> args, int groupPosition){

        LargeGroup playerGroup = null;
        if(args.size() > groupPosition){
            playerGroup = LargeGroup.getLargeGroupByName(args.get(groupPosition));

            //Check if player have permission to use other group than his group
            if(!organisationPlayer.getOfflinePlayer().isOp()){
                if(!organisationPlayer.getLstLargeGroup().contains(playerGroup)){
                    GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.BadGroup",true);
                    return null;
                }
            }
        }
        else if(organisationPlayer.getLstLargeGroup().size() > 1){
            GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.ToManyOrganisation",true);
            return null;
        }
        else if(organisationPlayer.getLstLargeGroup().size() <= 0){
            GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.NoOrganisationFound",true);
            return null;
        }
        else{
            playerGroup = organisationPlayer.getLstLargeGroup().get(0);
        }
        return playerGroup;
    }

    public static Group getCommandPlayerGroup(OrganisationPlayer organisationPlayer, List<String> args, int groupPosition){

        Group playerGroup = null;
        if(args.size() > groupPosition){
            playerGroup = Group.getGroupByName(args.get(groupPosition));

            //Check if player have permission to use other group than his group
            if(!organisationPlayer.getOfflinePlayer().isOp()){
                if(!organisationPlayer.getLstGroup().contains(playerGroup)){
                    GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.BadGroup",true);
                    return null;
                }
            }
        }
        else if(organisationPlayer.getLstGroup().size() > 1){
            GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.ToManyOrganisation",true);
            return null;
        }
        else if(organisationPlayer.getLstGroup().size() <= 0){
            GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.NoOrganisationFound",true);
            return null;
        }
        else{
            playerGroup = organisationPlayer.getLstGroup().get(0);
        }
        return playerGroup;
    }

    public static OrganisationPlayer getCommandOrganisationPlayer(OfflinePlayer player){

        try {
            return OrganisationPlayer.getOrganisationPlayer(player);
        }
        catch (OrganisationPlayerException e){
            GeneralMethods.sendPlayerErrorMessage(player,"Error.OrganisationPlayerDoesntExist",true);
            return null;
        }
    }

    public static String getStringFromArgs(List<String> args, int argsMin){

        String value = "";
        for(int i = argsMin; i< args.size(); i++){
            value += args.get(i)+" ";
        }

        return value;
    }
}
