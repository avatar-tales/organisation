package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.exception.InvitationGroupException;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class InviteCommand extends OrganisationCommand {

    public static ArrayList<Invitation> lstInvitation = new ArrayList<>();

    public InviteCommand() {
        super("invite", "/invite <name> <organisation>", GeneralMethods.getStringConf("Commands.Invite.Desc"), new String[]{"invite","add"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

        if(!(sender instanceof Player))return;

        Player player =  (Player) sender;
        String pseudoPlayerInvited = args.get(0);

        if( args.size() != 1 && args.size() != 2) {
            GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
            return;
        }

        OrganisationPlayer organisationPlayerSender = getCommandOrganisationPlayer(player);
        if(organisationPlayerSender == null)return;


        Group groupInvitSender = getCommandPlayerGroup(organisationPlayerSender,args, 1);
        if(groupInvitSender == null)return;

        OrganisationPlayer oPlayerInvite = null;
        try {
            oPlayerInvite = OrganisationPlayer.getOrganisationPlayerByName(pseudoPlayerInvited);
        }catch (OrganisationPlayerException e){
            GeneralMethods.sendPlayerErrorMessage(player,"Le joueur "+ pseudoPlayerInvited +" n'a jamais joué sur le serveur avant...");
            return;
        }


        if(groupInvitSender.getOrganisationMember(oPlayerInvite) != null){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Invite.SameGroupError",true);
            return;
        }

        Invitation invitation;
        OfflinePlayer playerSenderInvit = organisationPlayerSender.getOfflinePlayer();
        try {
            invitation = new Invitation(organisationPlayerSender,groupInvitSender,oPlayerInvite);
        } catch (InvitationGroupException e) {
            GeneralMethods.sendPlayerErrorMessage(playerSenderInvit,"Commands.BadGroup",true);
            return;
        }

        if(!groupInvitSender.hasPermission(playerSenderInvit, Permission.management.invite) && !playerSenderInvit.getPlayer().getPlayer().hasPermission("organisation.command.invite") && !playerSenderInvit.isOp()) {
            GeneralMethods.sendPlayerErrorMessage(playerSenderInvit.getPlayer().getPlayer(),"Commands.NoPermission",true);
            return;
        }

        for(Invitation inv : InviteCommand.lstInvitation){
            if(inv.getGroupHost().equals(groupInvitSender) && inv.getOPlayerInvite().equals(oPlayerInvite)){

                InviteCommand.lstInvitation.remove(inv);
                DBUtils.deleteInvitation(inv);

                GeneralMethods.sendPlayerMessage(playerSenderInvit,"Commands.Invite.InvitationRemove",true);
                GeneralMethods.sendPlayerMessage(oPlayerInvite,"L'invitation vers "+groupInvitSender.getName()+" a été suprimé par "+playerSenderInvit.getName());
                return;
            }
        }

        InviteCommand.lstInvitation.add(invitation);
        DBUtils.addInvitation(invitation);
        GeneralMethods.sendPlayerMessage(playerSenderInvit,"Commands.Invite.InvitationSend",true);
        GeneralMethods.sendPlayerMessage(oPlayerInvite,"Vous avez été inviter a rejoindre "+groupInvitSender.getName()+" par "+playerSenderInvit.getName());
        GeneralMethods.sendPlayerMessage(oPlayerInvite,"Pour le rejoindre, utilisez /o join "+groupInvitSender.getName());

    }

    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        ArrayList<String> lstTab = new ArrayList<>();
        if(!(sender instanceof Player) || args.size() != 1 ){

            if(args.size() == 0){

                for(OrganisationPlayer oPlayer: OrganisationPlayer.getLstOrganisationPlayer()){
                    lstTab.add(oPlayer.getName());
                }
                return lstTab;
            }

            return super.getTabCompletion(sender, args);
        }

        Player player = (Player) sender;
        OrganisationPlayer organisationPlayer;

        try {
            organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException e) {
            return super.getTabCompletion(sender, args);
        }

        for(Group group : organisationPlayer.getLstGroup()){
            lstTab.add(group.getName());
        }

        return lstTab;
    }

    public static ArrayList<Invitation> getAllInvitation(OfflinePlayer offlinePlayer){

        ArrayList<Invitation> tmpLstInvitation = new ArrayList<>();

        for(Invitation invitation : lstInvitation){
            if(invitation.getOPlayerInvite().getUuid().equals(offlinePlayer.getUniqueId()))tmpLstInvitation.add(invitation);
        }
        return tmpLstInvitation;
    }

}
