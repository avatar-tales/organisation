package fr.avatar_returns.organisation.commands.needClass;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.InvitationGroupException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.view.GeneralItem;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;

public class Invitation {

    private OrganisationPlayer oPlayerHost;
    private Group groupHost;
    private OrganisationPlayer oPlayerInvite;

    public Invitation(OrganisationPlayer oPlayerHost,Group groupHost, OrganisationPlayer oPlayerInvite) throws InvitationGroupException {

        if(!groupHost.getLstGroup().contains(groupHost) && !oPlayerHost.getOfflinePlayer().getPlayer().hasPermission("Organisation.invitation.notInFaction")){
            throw  new InvitationGroupException(this);
        }

        this.groupHost = groupHost;
        this.oPlayerHost = oPlayerHost;
        this.oPlayerInvite = oPlayerInvite;

    }

    public Group getGroupHost() {
        return groupHost;
    }
    public OrganisationPlayer getOPlayerInvite() {
        return oPlayerInvite;
    }
    public OrganisationPlayer getOPlayerHost() {
        return oPlayerHost;
    }

    public ItemStack getInvitationItem() {

        ItemStack flag = (this.getGroupHost().getFlag() != null)?this.getGroupHost().getFlag() :  new ItemStack(Material.RED_BANNER);
        ItemStack item = GeneralItem.renameItemStackWithName(flag,"§6§l" + this.groupHost.getName());
        GeneralItem.cleanItemMeta(item);
        GeneralItem.setLoreFromList(item, Arrays.asList("§8Envoyé par " + this.oPlayerHost.getLastSeenName()));

        return  item;
    }
}
