package fr.avatar_returns.organisation.commands.needClass;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.organisations.Group;
import org.checkerframework.checker.units.qual.A;

import java.util.ArrayList;

import static org.spigotmc.SpigotConfig.config;

public class Disband {

    private static ArrayList<Disband> lstDisband =  new ArrayList<>();
    private long tokenTimeValidity;
    private final OrganisationPlayer organisationPlayer;
    private final String token;
    private final long time_start;
    private final Group group;

    public Disband(OrganisationPlayer organisationPlayer, Group group){
        this.organisationPlayer = organisationPlayer;
        this.group = group;
        this.token = Integer.toString(1000+(int)(Math.random() * (10000)));
        this.time_start = System.currentTimeMillis();
        tokenTimeValidity = 5*60*1000;
        lstDisband.add(this);
    }

    public String getToken(){return this.token;}
    public static boolean isDisbandValid(OrganisationPlayer organisationPlayer, Group group, String token) {

        long currentTimeStamp = System.currentTimeMillis();
        ArrayList<Disband> lstDisbandOutDated = new ArrayList<>();

        for(Disband disband : lstDisband){
            if(disband.time_start + disband.tokenTimeValidity  < currentTimeStamp){
                lstDisbandOutDated.add(disband);
            }
        }

        for(Disband disband: lstDisbandOutDated){
            lstDisband.remove(disband);
        }


        for(Disband disband: lstDisband){
            if(disband.getToken().equals(token) && disband.organisationPlayer == organisationPlayer && disband.group == group){
                return true;
            }
        }
        return false;
    }
}
