package fr.avatar_returns.organisation.commands;

import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.Province;
import fr.avatar_returns.organisation.territory.Territory;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProvinceCommand extends OrganisationCommand{

    public ProvinceCommand() {
        super("province", "/o province <register|list|unregister|setName|setDescription|addLand|removeLand> <provinceNameId> <newName|description|WGProtectedRegionId>", "Admin command to manage Province",new String[] { "province" });
    }

    @Override
    public void execute(final CommandSender sender, final List<String> args) {

        if(!(sender instanceof Player))return;
        Player player = (Player)sender;

        if(!(player.hasPermission("organisation.command.province") || player.isOp())){
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.NoPermission");
            return;
        }

        if(args.size()<2 && (args.size()==1 && !args.get(0).equalsIgnoreCase("list"))){
            GeneralMethods.sendPlayerErrorMessage(player, this.getProperUse());
            return;
        }

        String subCommand = args.get(0);

        if(subCommand.equalsIgnoreCase("list")){
            GeneralMethods.sendPlayerMessage(player, "Liste des provinces  : ");
            for(Province province : Province.getLstProvince()){
                String name = (province.getName() == "")? "unamed" : province.getName();
                player.sendMessage("§6 - "+province.getId() + " (" + name + ")");
            }

            if(Province.getLstProvince().size() != 0)player.sendMessage("");
            player.sendMessage("§6Total : "+Province.getLstProvince().size());
            return;
        }

        String provinceId =  args.get(1);
        if(subCommand.equalsIgnoreCase("register")){

            if(Province.getProvinceById(provinceId) != null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Province.ProvinceAlreadyExist",true);
                return;
            }
            new Province(provinceId);
            GeneralMethods.sendPlayerMessage(player,"Commands.Province.registerSuccess",true);
            return;
        }
        else if(subCommand.equalsIgnoreCase("unregister")){

            Province province = Province.getProvinceById(provinceId);
            if(province == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Province.ProvinceNotFound",true);
                return;
            }

            province.remove();
            GeneralMethods.sendPlayerMessage(player,"Commands.Province.unregisterSuccess",true);
            return;
        }

        if(args.size() < 3){
            GeneralMethods.sendPlayerErrorMessage(player, this.getProperUse());
            return;
        }

        if(subCommand.equalsIgnoreCase("setName")){

            Province province = Province.getProvinceById(provinceId);
            if(province == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Province.ProvinceNotFound",true);
                return;
            }

            province.setName(getStringFromArgs(args,2));
            GeneralMethods.sendPlayerMessage(player,"Commands.Province.EditNameSuccess",true);
            return;
        }
        else if(subCommand.equalsIgnoreCase("setDescription")){

            Province province = Province.getProvinceById(provinceId);
            if(province == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Province.ProvinceNotFound",true);
                return;
            }

            province.setDescription(getStringFromArgs(args,2));
            GeneralMethods.sendPlayerMessage(player,"Commands.Province.EditDescriptionSuccess",true);
            return;
        }
        else if(subCommand.equalsIgnoreCase("addLand")){

            String WGProtectedRegionId = args.get(2);
            Province province = Province.getProvinceById(provinceId);
            if(province == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Province.ProvinceNotFound",true);
                return;
            }

            ProtectedRegion protectedRegion = Organisation.worldRgManager.getRegion(WGProtectedRegionId);
            if(protectedRegion == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.WorldGuard.NotFound",true);
                return;
            }
            Land land = Land.getLandByRegion(protectedRegion);
            if(land == null){
                new Land(protectedRegion, province);
            }
            else{
                land.setProvince(province);
            }
            GeneralMethods.sendPlayerMessage(player,"Commands.Province.AddLandSuccess",true);
            return;
        }
        else if(subCommand.equalsIgnoreCase("removeLand")){

            String WGProtectedRegionId = args.get(2);
            Province province = Province.getProvinceById(provinceId);
            if(province == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Province.ProvinceNotFound",true);
                return;
            }

            ProtectedRegion protectedRegion = Organisation.worldRgManager.getRegion(WGProtectedRegionId);
            if(protectedRegion == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.WorldGuard.NotFound",true);
                return;
            }
            Land land = Land.getLandByRegion(protectedRegion);
            if(land == null){
                GeneralMethods.sendPlayerMessage(player,"Commands.Land.NotFound", true);
                return;
            }
            else if(!land.getProvince().equals(province)){
                GeneralMethods.sendPlayerMessage(player,"Commands.Province.LandNotInThisProvince",true);
                return;
            }
            land.setProvince(null);
            GeneralMethods.sendPlayerMessage(player,"Commands.Province.RemoveLandSuccess",true);
            return;
        }

    }

    @Override
    protected List<String> getTabCompletion(final CommandSender sender, final List<String> args) {

        if (args.size() > 2 || !sender.hasPermission("organisation.command.province")) {
            return new ArrayList<String>();
        }

        if ( !(sender instanceof Player) ) return new ArrayList<String>();


        final List<String> l = new ArrayList<String>();
        if (args.size() == 0) {

            l.add("register");
            l.add("list");
            l.add("unregister");
            l.add("setName");
            l.add("setDescription");
            l.add("addLand");
            l.add("removeLand");

        } else if(args.size() == 1) {


            if (args.get(0).equalsIgnoreCase("unregister") ||
                    args.get(0).equalsIgnoreCase("setName") ||
                    args.get(0).equalsIgnoreCase("setDescription") ||
                    args.get(0).equalsIgnoreCase("removeLand") ||
                    args.get(0).equalsIgnoreCase("addLand")) {
                for (Province province : Province.getLstProvince()) {
                    l.add(province.getId());
                }
            }

            else if (args.get(0).equalsIgnoreCase("register")) {
                l.add("<ProvinceId>");
            }
        }
        else if(args.size() == 2){

            Province province = Province.getProvinceById(args.get(1));

            if(province == null){

                Map<String, ProtectedRegion> mapProtectedRegion = Organisation.worldRgManager.getRegions();
                mapProtectedRegion.forEach((key, value) ->{
                    l.add(key);
                });
                return l;
            }

            if(args.get(0).equalsIgnoreCase("addLand")){

                for(ProtectedRegion protectedRegion: Organisation.worldRgManager.getRegions().values()){
                    if( Territory.getLstWGId().contains(protectedRegion.getId()))continue;
                    l.add(protectedRegion.getId());
                }
            }
            else if(args.get(0).equalsIgnoreCase("removeLand")){

                for(Land lan : province.getLstLand()){
                    l.add(lan.getWorldGuardRegion().getId());
                }
            }
        }
        return l;
    }
}
