package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class RoleCommand extends OrganisationCommand {


    public RoleCommand() {
        super("role", "/o role <set|update|list|info|remove|create|rename> <role name> (<playerName|power|permission name>|new name>) (<true|false>) <Organisation name>", GeneralMethods.getStringConf("Commands.Role.Desc"), new String []{"role", "r"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {
        super.execute(sender, args);

        if( !(sender instanceof Player) )return;
        Player player = (Player)sender;

        if (args.size() < 1 || args.size() > 4){
            GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
            return;
        }

        OrganisationPlayer organisationPlayer;
        try {
            organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException e) {
            GeneralMethods.sendPlayerErrorMessage(player,"Error.OrganisationPlayerDoesntExist",true);
            e.printStackTrace();
            return;
        }

        if (args.get(0).equalsIgnoreCase("list")){

            if(args.size() < 1 ||  args.size() > 2){
                GeneralMethods.sendPlayerErrorMessage(player,"Usage : /o role list <organisation>");
                return;
            }

            Group playerGroup = getCommandPlayerGroup(organisationPlayer,args,1);
            if(playerGroup == null) return;

            if(playerGroup.getLstRole().size()>1){
                player.sendMessage("§6Role disponible dans "+playerGroup.getName()+" : \n");
                for(Role role : playerGroup.getLstRole()){
                    if(role.isRecrue())continue;
                    String power =(role.isLeader())? "" : " ("+role.getRolePower()+")";
                    player.sendMessage(" - "+role.getName() + power);
                }
                if(playerGroup.getDefaultRole() != null){
                    player.sendMessage(" - "+playerGroup.getDefaultRole().getName());
                }
                player.sendMessage("\n§6Total : "+playerGroup.getLstRole().size()+" roles.");
            }
            else{
                player.sendMessage("Aucun role disponible dans "+playerGroup.getName()+".");
            }

        }
        else if (args.get(0).equalsIgnoreCase("info")){

            if(args.size() < 2 ||  args.size() > 3){
                GeneralMethods.sendPlayerErrorMessage(player,"Usage : /o role info <name> <organisation>");
                return;
            }

            Group playerGroup = getCommandPlayerGroup(organisationPlayer,args,2);
            if(playerGroup == null) return;

            Role targetRole = null;
            for(Role role : playerGroup.getLstRole()){
                if(role.getName().replace("&","§").equalsIgnoreCase(args.get(1))) {
                    targetRole = role;
                }
            }

            if(targetRole == null) {
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.RoleNotFound",true);
                return;
            }

            player.sendMessage("§6Permission pour le role : "+ targetRole.getName()+"\n");
            for(String permission : Permission.getAllPermissions()){

                String status, color;
                if (targetRole.getLstPermission().contains(permission)){
                    status = "True";
                    color = "§a";
                }
                else{
                    status = "False";
                    color = "§c";
                }

                player.sendMessage(" - §6"+permission+" : " + color + status);
            }
        }
        else if (args.get(0).equalsIgnoreCase("create")){

            if(args.size() < 3 ||  args.size() > 4){
                GeneralMethods.sendPlayerErrorMessage(player,"Usage : /o role create <name> <power> <organisation>");
                return;
            }

            Group playerGroup = getCommandPlayerGroup(organisationPlayer,args,3);
            if(playerGroup == null) return;

            Role targetRole = null;
            String newRoleName = args.get(1);
            for(Role role : playerGroup.getLstRole()){
                if(role.getName().equalsIgnoreCase(newRoleName.replace("&","§"))) {
                    targetRole = role;
                }
            }

            if(targetRole != null) {
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Create.RoleExist",true);
                return;
            }
            if(!playerGroup.hasPermission(player, Permission.management.createRole)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Create.PermissionError",true);
                return;
            }

            int power = 900;
            String strPower = args.get(2);

            try{
                power = Integer.parseInt(strPower);
            }
            catch (NumberFormatException ex){
                ex.printStackTrace();
            }

            if (power <= 0){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.ReservedPower",true);
                return;
            }

            OrganisationMember senderOrganisationMember = playerGroup.getOrganisationMember(player);
            if( power <= senderOrganisationMember.getRole().getRolePower()){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Create.NotEnafRolePowerError",true);
                return;
            }

            Role newRole = new Role(newRoleName, power, playerGroup);
            if(!hasEnafPower(senderOrganisationMember, newRole)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Create.NotEnafRolePowerError",true);
                return;
            }

            newRole.getLstPermission().addAll(Permission.getBuildPermissions());
            newRole.getLstPermission().addAll(Permission.getInteractPermissions());

            playerGroup.addRole(newRole, false);
            GeneralMethods.sendPlayerMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Create.RoleCreate",true);

        }
        else if (args.get(0).equalsIgnoreCase("setPower")){

            if(args.size() < 3 ||  args.size() > 4){
                GeneralMethods.sendPlayerErrorMessage(player,"Usage : /o role setPower <name> <power> <organisation>");
                return;
            }

            Group playerGroup = getCommandPlayerGroup(organisationPlayer,args,3);
            if(playerGroup == null) return;

            Role targetRole = null;
            String newRoleName = args.get(1);
            for(Role role : playerGroup.getLstRole()){
                if(role.getName().equalsIgnoreCase(newRoleName.replace("&","§"))) {
                    targetRole = role;
                }
            }

            if(targetRole == null) {
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.SetPower.RoleExist",true);
                return;
            }


            if(!playerGroup.hasPermission(player, Permission.management.setPower)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.SetPower.PermissionError",true);
                return;
            }

            if(targetRole.isLeader()){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.SetPower.CantChangeLeader",true);
                return;
            }
            else if (targetRole.isRecrue()){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.SetPower.CantChangeRecrue",true);
                return;
            }

            int power = 900;
            String strPower = args.get(2);

            try{
                power = Integer.parseInt(strPower);
            }
            catch (NumberFormatException ex){
                ex.printStackTrace();
            }

            if (power <= 0){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.ReservedPower",true);
                return;
            }

            OrganisationMember senderOrganisationMember = playerGroup.getOrganisationMember(player);
            if(!hasEnafPower(senderOrganisationMember, targetRole)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.SetPower.NotEnafRolePowerError",true);
                return;
            }

            if(senderOrganisationMember.getRole().getRolePower()>= power){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.SetPower.CantSetMorePower",true);
                return;
            }

            targetRole.setRolePower(power);
            GeneralMethods.sendPlayerMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.SetPower.SetPower",true);

        }
        else if (args.get(0).equalsIgnoreCase("rename")){

            if(args.size() < 3 ||  args.size() > 4){
                GeneralMethods.sendPlayerErrorMessage(player,"Usage : /o role rename <name> <new name> <organisation>");
                return;
            }

            Group playerGroup = getCommandPlayerGroup(organisationPlayer,args,3);
            if(playerGroup == null) return;

            Role targetRole = null;
            boolean findNewName = false;
            String oldRoleName = args.get(1);
            String newRoleName = args.get(2);
            for(Role role : playerGroup.getLstRole()){
                if(role.getName().equalsIgnoreCase(oldRoleName.replace("&","§"))) {
                    targetRole = role;
                }
                else if(role.getName().equalsIgnoreCase(newRoleName.replace("&","§"))){
                    findNewName = true;
                }
            }

            if(targetRole == null) {
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.RoleNotFound",true);
                return;
            }

            if(findNewName){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Rename.NewNameExistingError",true);
                return;
            }
            if(!playerGroup.hasPermission(player, Permission.management.renameRole)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Rename.PermissionError",true);
                return;
            }

            OrganisationMember senderOrganisationMember = playerGroup.getOrganisationMember(player);
            if(!hasEnafPower(senderOrganisationMember, targetRole)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Rename.NotEnafRolePowerError",true);
                return;
            }

            targetRole.setName(newRoleName);
            GeneralMethods.sendPlayerMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Rename.RoleRename",true);
        }
        else if (args.get(0).equalsIgnoreCase("remove")){

            if(args.size() < 2 ||  args.size() > 3){
                GeneralMethods.sendPlayerErrorMessage(player,"Usage : /o role remove <name> <organisation>");
                return;
            }

            Group playerGroup = getCommandPlayerGroup(organisationPlayer,args,2);
            if(playerGroup == null) return;

            Role targetRole = null;
            String newRoleName = args.get(1);
            for(Role role : playerGroup.getLstRole()){
                if(role.getName().equalsIgnoreCase(newRoleName.replace("&","§"))) {
                    targetRole = role;
                }
            }

            if(targetRole == null) {
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Remove.RoleDoesntExist",true);
                return;
            }
            if(!playerGroup.hasPermission(player, Permission.management.removeRole)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Remove.PermissionError",true);
                return;
            }

            OrganisationMember senderOrganisationMember = playerGroup.getOrganisationMember(player);
            if(!hasEnafPower(senderOrganisationMember, targetRole)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Remove.NotEnafRolePowerError",true);
                return;
            }
            if(organisationPlayer.hasRolePermission(targetRole, Permission.management.removeRole)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Remove.NotEnafRolePowerError",true);
                return;
            }

            if(targetRole.getRolePower() == 0){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Remove.CantRemoveLeaderRole",true);
                return;
            }
            if(targetRole.getRolePower() == -1){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Remove.CantRemoveRecrueRole",true);
                return;
            }

            playerGroup.removeRole(targetRole);
            GeneralMethods.sendPlayerMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Remove.RoleRemove",true);

        }
        else if (args.get(0).equalsIgnoreCase("update")){

            if(args.size() <4 ||  args.size() > 5){
                GeneralMethods.sendPlayerErrorMessage(player,"Usage : /o role update <name> <permission> <True|False> <organistion>");
                return;
            }

            Group playerGroup = getCommandPlayerGroup(organisationPlayer,args,4);
            if(playerGroup == null) return;

            Role targetRole = null;
            for(Role role : playerGroup.getLstRole()){
                if(role.getName().equalsIgnoreCase(args.get(1).replace("&","§"))) {
                    targetRole = role;
                }
            }

            if(targetRole == null) {
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.RoleNotFound",true);
                return;
            }

            if(!playerGroup.hasPermission(player, Permission.management.setPermission)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Rename.PermissionError",true);
                return;
            }

            OrganisationMember senderOrganisationMember = playerGroup.getOrganisationMember(player);
            if(!hasEnafPower(senderOrganisationMember, targetRole)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Update.NotEnafRolePowerError",true);
                return;
            }

            String permission = args.get(2);
            String newStatus = args.get(3);

            if(targetRole.getRolePower() == 0 && permission.equalsIgnoreCase(String.valueOf(Permission.management.setPermission))){
                //No one can remove permission to leader role to edit permission.
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Update.RemoveLeaderSetError",true);
                return;
            }

            if(!Permission.getAllPermissions().contains(permission)){
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Update.PermissionDoesntExist",true);
                return;
            }
            if(!playerGroup.hasPermission(player, permission)){
                Organisation.log.info("----> Tentative de donation permission non possédée par"+player.getName());
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Role.Update.PermissionNotGiven",true);
                return;
            }

            if (newStatus.equalsIgnoreCase("True")) {

                if(!targetRole.getLstPermission().contains(permission)){
                    targetRole.getLstPermission().add(permission);
                    DBUtils.addRolePermission(targetRole,permission);
                }

                GeneralMethods.sendPlayerMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Update.PermissionAdd",true);
            }
            else if (newStatus.equalsIgnoreCase("False")){

                if(targetRole.getLstPermission().contains(permission)){
                    targetRole.getLstPermission().remove(permission);
                    DBUtils.deleteRolePermission(targetRole,permission);
                }

                GeneralMethods.sendPlayerMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Update.PermissionRemove",true);
            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player,"Usage : /o role set "+targetRole.getName().replace("§","&")+" "+permission+" <True|False>"+playerGroup.getName());
                return;
            }

        }
        else if (args.get(0).equalsIgnoreCase("set")) {

            if (args.size() < 3 || args.size() > 4) {
                GeneralMethods.sendPlayerErrorMessage(player, "Usage : /o role set <name> <playerName> <organisation>");
                return;
            }

            Group playerGroup = getCommandPlayerGroup(organisationPlayer, args, 3);
            if (playerGroup == null) return;

            Role targetRole = null;

            for (Role role : playerGroup.getLstRole()) {
                if(role.getName().equalsIgnoreCase(args.get(1).replace("&","§"))) {
                    targetRole = role;
                }
            }

            if (targetRole == null) {
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.RoleNotFound", true);
                return;
            }

            //Get player who is kick
            String playerName = args.get(2);
            OfflinePlayer targetPlayer = GeneralMethods.getOfflinePlayerFromName(playerName);

            OrganisationPlayer oPlayerTarget = null;
            try{
                oPlayerTarget = OrganisationPlayer.getOrganisationPlayerByName(playerName);
            }
            catch (OrganisationPlayerException e){
                GeneralMethods.sendPlayerErrorMessage(player,"Le joueur "+playerName +" n'a jamais joué sur le serveur avant..");
                return;
            }

            OrganisationMember targetOrganisationMember = playerGroup.getOrganisationMember(oPlayerTarget);
            OrganisationMember senderOrganisationMember = playerGroup.getOrganisationMember(player);

            if (targetOrganisationMember == null) {
                GeneralMethods.sendPlayerMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Set.TargetUserIsNotInTargetGroup", true);
            }

            if (!playerGroup.hasPermission(player, Permission.management.promote)) {
                GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Set.PermissionError", true);
                return;
            }

            // If sender is leader, then he can give its own rank to any one.
            if(!(senderOrganisationMember.isLeader() || player.isOp() || player.hasPermission("organisation.command.role.admin"))){
                if (targetOrganisationMember.getRole().equals(senderOrganisationMember.getRole())) {
                    GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Set.SameRoleError", true);
                    return;
                }
                if (!hasEnafPower(senderOrganisationMember, targetOrganisationMember.getRole()) ) {
                    GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Set.SenderPowerToLowError", true);
                    return;
                }
                if (!hasEnafPower(senderOrganisationMember, targetRole)) {
                    GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Set.NotEnafRolePowerError", true);
                    return;
                }
            }

            if(targetRole == targetOrganisationMember.getRole()){
                GeneralMethods.sendPlayerMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Set.SameRole", true);
                return;
            }

            // We check that we will have enaf leader after update.
            if(targetOrganisationMember.isLeader()) {
                int nbLeader = 0;
                for (OrganisationMember organisationMember : playerGroup.getLstMember()) {
                    if (organisationMember.getRole().isLeader()) {
                        nbLeader++;
                    }
                }

                if( (nbLeader - 1) < 1 ){
                    GeneralMethods.sendPlayerErrorMessage(organisationPlayer.getOfflinePlayer(), "Commands.Role.Set.OneLeaderNeededError", true);
                    return;
                }
            }

            targetOrganisationMember.setRole(targetRole);
            GeneralMethods.sendPlayerMessage(organisationPlayer.getOfflinePlayer(),"Commands.Role.Set.SuccessRole",true);
        }
        else{
            GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
        }

    }

    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        ArrayList<String> lstTab = new ArrayList<>();

        if(!(sender instanceof Player)) return lstTab;

        Player player = (Player) sender;
        OrganisationPlayer organisationPlayer;

        try {
            organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException e) {
            return lstTab;
        }

        if (args.size() < 1) {

            lstTab.add("set");
            lstTab.add("list");
            lstTab.add("info");
            lstTab.add("update");
            lstTab.add("create");
            lstTab.add("setPower");
            lstTab.add("remove");
            lstTab.add("rename");

        }
        else if (args.size() == 1) {

            if (args.get(0).equalsIgnoreCase("list")){
                for (Group group : organisationPlayer.getLstGroup()) {
                    lstTab.add(group.getName());
                }
            }
            else {
                for (Group group : organisationPlayer.getLstGroup()) {
                    for (Role role : group.getLstRole()) {
                        lstTab.add(role.getName().replace("§","&"));
                    }
                }
            }
        }
        else if (args.size() == 2) {

            if (args.get(0).equalsIgnoreCase("set")){
                for (Group group : organisationPlayer.getLstGroup()) {
                    for (OrganisationMember member : group.getLstMember()){
                        lstTab.add(member.getName());
                    }
                }
            }
            else if (args.get(0).equalsIgnoreCase("update")){
                lstTab = Permission.getAllPermissions();
            }else if (args.get(0).equalsIgnoreCase("rename")){
                lstTab.add("<new_name>");
            }
            else if (args.get(0).equalsIgnoreCase("create") ||
                    args.get(0).equalsIgnoreCase("setPower")){

                lstTab.add("1100");
                lstTab.add("800");
                lstTab.add("500");
                lstTab.add("200");
                lstTab.add("100");
            }
            else if (!args.get(0).equalsIgnoreCase("list")){
                for (Group group : organisationPlayer.getLstGroup()) {
                    lstTab.add(group.getName());
                }
            }
        }
        else if (args.size() == 3) {

            if (args.get(0).equalsIgnoreCase("update")){
                lstTab.add("true");
                lstTab.add("false");
            }
            else if (args.get(0).equalsIgnoreCase("set") ||
                    args.get(0).equalsIgnoreCase("rename") ||
                    args.get(0).equalsIgnoreCase("create") ||
                    args.get(0).equalsIgnoreCase("setPower")){

                for (Group group : organisationPlayer.getLstGroup()) {
                    lstTab.add(group.getName());
                }
            }
        }
        else if (args.size() == 4 && args.get(0).equalsIgnoreCase("update")) {

            for (Group group : organisationPlayer.getLstGroup()) {
                lstTab.add(group.getName());
            }
        }

        return lstTab;
    }

    protected static boolean hasEnafPower(OrganisationMember senderOrganisationMember, Role targetRole){

        if(senderOrganisationMember.getOfflinePlayer().isOp())return true;
        if(senderOrganisationMember.getOfflinePlayer().getPlayer() != null){
            if(senderOrganisationMember.getOfflinePlayer().getPlayer().hasPermission("organisation.command.role.admin")) return true;
            else if(senderOrganisationMember.getOfflinePlayer().getPlayer().hasPermission("organisation.admin")) return true;
        }
        if(targetRole.isRecrue() && !senderOrganisationMember.isRecrue())return true;
        return senderOrganisationMember.getRole().getRolePower() < targetRole.getRolePower();
    }
}
