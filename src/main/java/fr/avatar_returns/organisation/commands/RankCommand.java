package fr.avatar_returns.organisation.commands;

import com.sk89q.worldedit.command.GeneralCommands;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.rank.*;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class RankCommand extends OrganisationCommand {


    public RankCommand() {
        super("rank", "/o rank <create|edit|list|remove> <type> <id,name> <newname|order>", GeneralMethods.getStringConf("Commands.Rank.Desc"), new String []{"rank", "ra"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {
        super.execute(sender, args);

        if( !(sender instanceof Player) )return;
        Player player = (Player)sender;

        if(!(player.hasPermission("organisation.command.rank") || player.isOp())){
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.NoPermission");
            return;
        }

        String subCommand = args.get(0).toLowerCase();

        if(subCommand.equals("list")){

            String msg = "Liste des rangs ";
            if(args.size() == 2 && args.get(1).equalsIgnoreCase("groupRank")){
                msg += "'GroupRank' : \n\n";

                ArrayList<GroupRank> lstGuildeGroupRank = new ArrayList<>();
                msg += "§6Rang de factions : \n";
                for (GroupRank groupRank : GroupRank.getLstGroupRank()){

                    if (groupRank.getGroupType().equalsIgnoreCase("FACTION")) {

                        msg += "§6 - " + groupRank.getId()
                                + " §e[" + groupRank.getOrder()
                                + "] §a" + groupRank.getName()
                                + " §b[mxVassal: " + groupRank.getMaxVassal()
                                + "; mxLand: " + groupRank.getMaxLand()
                                + "; mxPlayer: " + groupRank.getMaxPlayer()
                                + "; Vassalize: " + ((groupRank.isCanVassalize()) ? "§aTrue" : "§cFalse") + "§b]\n";
                    }
                    else{
                        lstGuildeGroupRank.add(groupRank);
                    }
                }

                if(lstGuildeGroupRank.size() > 0) {
                    msg += "\n \n§6Rang de guilde : \n";
                    for (GroupRank groupRank : lstGuildeGroupRank) {

                        msg += "§6 - " + groupRank.getId()
                                + " §e[" + groupRank.getOrder()
                                + "] §a" + groupRank.getName()
                                + " §b[mxPlayer: " + groupRank.getMaxPlayer() + "§b]\n";
                    }
                }

                msg += "§6\nTotal : " + GroupRank.getLstGroupRank().size();
            }
            else if(args.size() == 2 && args.get(1).equalsIgnoreCase("playerRank")){
                msg += "'PlayerRank' : \n\n";

                for (PlayerRank playerRank : PlayerRank.getLstPlayerRank()){
                    msg += "§6 - " + playerRank.getId()
                            + " §e[" + playerRank.getOrder()
                            + "] §a" + playerRank.getName()
                            + " §b[mxMinHeart: " + playerRank.getMinHeart()
                            + "; mxSlotInv: " + playerRank.getMaxSlotInv()
                            + "; unLockHand: " + ((playerRank.isUnlockLeftHand())? "§cayes" : "§cno")  + "§b]\n";
                }

                msg += "§6\nTotal : " + PlayerRank.getLstPlayerRank().size();
            }
            else{

                msg += "'GroupRank' : \n\n";

                ArrayList<GroupRank> lstGuildeGroupRank = new ArrayList<>();
                msg += "§6Rang de factions : \n";
                for (GroupRank groupRank : GroupRank.getLstGroupRank()){

                    if (groupRank.getGroupType().equalsIgnoreCase("FACTION")) {

                        msg += "§6 - " + groupRank.getId()
                                + " §e[" + groupRank.getOrder()
                                + "] §a" + groupRank.getName()
                                + " §b[mxVassal: " + groupRank.getMaxVassal()
                                + "; mxLand: " + groupRank.getMaxLand()
                                + "; mxPlayer: " + groupRank.getMaxPlayer()
                                + "; Vassalize: " + ((groupRank.isCanVassalize()) ? "§aTrue" : "§cFalse") + "§b]\n";
                    }
                    else{
                        lstGuildeGroupRank.add(groupRank);
                    }
                }

                if(lstGuildeGroupRank.size() > 0) {
                    msg += "\n \n§6Rang de guilde : \n";
                    for (GroupRank groupRank : lstGuildeGroupRank) {

                        msg += "§6 - " + groupRank.getId()
                                + " §e[" + groupRank.getOrder()
                                + "] §a" + groupRank.getName()
                                + " §b[mxPlayer: " + groupRank.getMaxPlayer() + "§b]\n";
                    }
                }

                msg += "§6\nListe des rangs 'PlayerRank' : \n\n";

                for (PlayerRank playerRank : PlayerRank.getLstPlayerRank()){
                    msg += "§6 - " + playerRank.getId()
                            + " §e[" + playerRank.getOrder()
                            + "] §a" + playerRank.getName()
                            + " §b[mxMinHeart: " + playerRank.getMinHeart()
                            + "; mxSlotInv: " + playerRank.getMaxSlotInv()
                            + "; unLockHand: " + ((playerRank.isUnlockLeftHand())? "§cayes" : "§cno")  + "§b]\n";
                }

                msg += "§6\nTotal : " + (GroupRank.getLstGroupRank().size() + PlayerRank.getLstPlayerRank().size());

            }

            msg += "\n\n";

            GeneralMethods.sendPlayerMessage(player, msg);
        }
        else if (args.size() >= 2){

            String rankType = args.get(1).toLowerCase();

            if(rankType.equals("grouprank")){

                if(subCommand.equals("create") && args.size() == 9) {

                    String name = args.get(2);
                    String groupType = args.get(4).toUpperCase();

                    if(!groupType.equals("FACTION") && !groupType.equals("GUILDE")){
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.Create.NotValidGroupType", true);
                        return;
                    }

                    try {

                        int order = Integer.parseInt(args.get(3));
                        int maxLand = Integer.parseInt(args.get(5));
                        int maxVassal = Integer.parseInt(args.get(6));
                        int maxPlayer = Integer.parseInt(args.get(7));
                        boolean canVassalize = (args.get(8).equalsIgnoreCase("true"))? true : false;

                        order = Math.max(order, 0);
                        maxLand = Math.max(maxLand, 1);
                        maxVassal = Math.max(maxVassal, 0);
                        maxPlayer = Math.max(maxPlayer, 1);

                        if(GroupRank.getLstGroupRank().size() == 0) order =0;

                        GroupRank groupRank = new GroupRank(name, order, groupType, maxLand, maxVassal, maxPlayer, canVassalize);
                        groupRank.addObjectif("Validation_staff",
                                "Objectif a valider auprès du staff",
                                0,
                                null,
                                Objective.Type.BOOLEAN.toString()
                        );

                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.Create.Success", true);

                    }
                    catch (NumberFormatException e){
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.InvalidInt", true);
                        return;
                    }
                }
                if(subCommand.equals("edit") && args.size() >= 5){
                    try {
                        int id = Integer.parseInt(args.get(2));
                        GroupRank groupRank = GroupRank.getById(id);
                        String editionCmd = args.get(3).toLowerCase();

                        if (groupRank == null) {
                            GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.NotExist", true);
                            return;
                        }

                        if (editionCmd.equalsIgnoreCase("objectif")) {
                            performRankObjectifAction(args.subList(4, args.size()), groupRank, player);
                        }
                        else {

                            if (editionCmd.equalsIgnoreCase("canVassalize")) {
                                groupRank.setCanVassalize((args.get(4).equalsIgnoreCase("true"))?true : false);
                            }
                            else{
                                int value = Integer.parseInt(args.get(4));
                                value = Math.max(value, 0);

                                if (editionCmd.equalsIgnoreCase("maxLand")) {
                                    groupRank.setMaxLand(value);
                                } else if (editionCmd.equalsIgnoreCase("maxVassal")) {
                                    groupRank.setMaxVassal(value);
                                } else if (editionCmd.equalsIgnoreCase("maxPlayer")) {
                                    value = Math.max(value, 1);
                                    groupRank.setMaxPlayer(value);
                                } else if (editionCmd.equalsIgnoreCase("order")) {
                                    groupRank.setOrder(value);
                                } else {
                                    GeneralMethods.sendPlayerMessage(player, "/o rank edit GroupRank " + id + "<maxLand|maxPlayer|maxVassal|canVassalize|objectif> <int> <objective optional values>");
                                    return;
                                }
                            }
                            GeneralMethods.sendPlayerMessage(player, "Commands.Rank.Edit.Success", true);
                        }
                    }
                    catch (NumberFormatException e){
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.InvalidInt", true);
                    }
                }
                if(subCommand.equals("remove") && args.size() == 3){

                    try {

                        int id = Integer.parseInt(args.get(2));
                        GroupRank groupRank = GroupRank.getById(id);
                        if (groupRank == null){
                            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.NotExist", true);
                        }
                        else {

                            if(groupRank.getOrder() == 0){
                                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.Remove.CantDefault", true);
                            }
                            else {
                                groupRank.remove();
                                GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Remove.Success", true);
                            }
                        }
                    }
                    catch (NumberFormatException e){
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.InvalidInt", true);
                    }
                }

            }
            else if(rankType.equals("playerrank")){

                if(subCommand.equals("create") && args.size() == 5) {

                    String name = args.get(2);
                    boolean unlockLeftHand = true;
                    if (args.get(5).equalsIgnoreCase("false")) unlockLeftHand = false;
                    else if (!args.get(5).equalsIgnoreCase("true")){
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.UnLockLeftHand.Error", true);
                        return;
                    }

                    try {

                        int order = Integer.parseInt(args.get(2));
                        int minHeart = Integer.parseInt(args.get(3));
                        int maxSlotInv = Integer.parseInt(args.get(4));

                        order = Math.max(order, 0);
                        minHeart = Math.max(minHeart, 0);
                        maxSlotInv = Math.max(maxSlotInv, 9);

                        if(PlayerRank.getLstPlayerRank().size() <= 0)order = 0;

                        PlayerRank playerRank  = new PlayerRank(name, order, minHeart, maxSlotInv, unlockLeftHand);
                        playerRank.addObjectif("Validation_staff",
                                "Objectif a valider auprès du staff",
                                0,
                                null,
                                Objective.Type.BOOLEAN.toString()
                        );

                        GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Create.Success", true);

                    }
                    catch (NumberFormatException e){
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.InvalidInt", true);
                        return;
                    }

                }
                if(subCommand.equals("edit")){

                    try {
                        int id = Integer.parseInt(args.get(2));
                        PlayerRank playerRank = PlayerRank.getById(id);
                        String editionCmd = args.get(3).toLowerCase();
                        try {

                            if (editionCmd.equalsIgnoreCase("objectif")) {
                                performRankObjectifAction(args.subList(4, args.size()), playerRank, player);
                            }
                            else {

                                int value = Integer.parseInt(args.get(4));
                                value = Math.max(value, 0);

                                if (playerRank == null) {
                                    GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.NotExist", true);
                                } else {
                                    if (editionCmd.equalsIgnoreCase("minHeart")) {
                                        playerRank.setMinHeart(value);
                                    } else if (editionCmd.equalsIgnoreCase("maxSlotInv")) {
                                        playerRank.setMaxSlotInv(value);
                                    } else if (editionCmd.equalsIgnoreCase("order")) {
                                        playerRank.setOrder(value);
                                    } else {
                                        GeneralMethods.sendPlayerMessage(player, "/o rank edit GroupRank " + id + "<maxLand|maxPlayer|maxVassal|objectif> <int> <objective optional values>");
                                        return;
                                    }
                                    GeneralMethods.sendPlayerMessage(player, "Commands.Rank.Edit.Success", true);
                                }
                            }
                        }
                        catch (NumberFormatException e){

                            if (editionCmd.equalsIgnoreCase("UnLockLeftHand")) {
                                if (args.get(4).equalsIgnoreCase("True") || args.get(4).equalsIgnoreCase("False")) {
                                    boolean unlock = (args.get(4).equalsIgnoreCase("True")) ? true : false;
                                    playerRank.setUnLockLeftHand(unlock);
                                }
                            }
                            else{
                                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.InvalidInt", true);
                            }
                        }
                    }
                    catch (NumberFormatException e){
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.InvalidInt", true);
                    }
                }
                if(subCommand.equals("remove") && args.size() == 3){
                    try {

                        int id = Integer.parseInt(args.get(2));
                        PlayerRank playerRank = PlayerRank.getById(id);
                        if (playerRank == null){
                            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.NotExist", true);
                        }
                        else {
                            if(playerRank.getOrder() == 0){
                                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.Remove.CantDefault", true);
                            }
                            else {
                                playerRank.remove();
                                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.Remove.Success", true);
                            }
                        }
                    }
                    catch (NumberFormatException e){
                        GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.InvalidInt", true);
                    }
                }
            }
            else {
                GeneralMethods.sendPlayerErrorMessage(player,"Only 'GroupRank' and 'PlayerRank are allowed for type'\n" + this.getProperUse());
            }
        }
        else{
            GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
        }

    }

    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        ArrayList<String> lstTab = new ArrayList<>();

        if(!(sender instanceof Player)) return lstTab;
        Player player = (Player) sender;

        if(!(player.isOp() || player.hasPermission("")))return lstTab;

        if (args.size() < 1) {

            lstTab.add("create");
            lstTab.add("edit");
            lstTab.add("list");
            lstTab.add("remove");

        }
        else {
            String subCommand = args.get(0).toLowerCase();

            if(args.size() == 1){
                lstTab.add("PlayerRank");
                lstTab.add("GroupRank");
            }

            if (subCommand.equals("create") && args.size() > 1){

                String rankType = args.get(1).toLowerCase();

                if( !rankType.equals("playerrank") && ! rankType.equals("grouprank"))return lstTab;
                else if (args.size() == 2) lstTab.add("name");
                else if (args.size() == 3) lstTab.add("order");
                else {

                    if (rankType.equals("playerrank")) {
                        if (args.size() == 4) lstTab.add("minHeart");
                        else if (args.size() == 5) lstTab.add("maxSlotInv");
                        else if (args.size() == 6) {
                            lstTab.add("true");
                            lstTab.add("false");
                            lstTab.add("unlockLeftHand");
                            return lstTab;
                        }

                    } else if (rankType.equals("grouprank")) {
                        if (args.size() == 4) {
                            lstTab.add("FACTION");
                            lstTab.add("GUILDE");
                            return lstTab;
                        }
                        else if (args.size() == 5) lstTab.add("maxLand");
                        else if (args.size() == 6) lstTab.add("maxVassal");
                        else if (args.size() == 7) lstTab.add("maxPlayer");
                        else if (args.size() == 8) {
                            lstTab.add("canVassalize");
                            lstTab.add("true");
                            lstTab.add("false");
                            return lstTab;
                        }
                    }
                }

               if (args.size() > 3 && lstTab.size() > 0){
                   lstTab.add("1");
                   lstTab.add("3");
                   lstTab.add("7");
                   lstTab.add("8");
               }
            }
            else if ( args.size() > 1 ){

                if(args.get(1).equalsIgnoreCase("PlayerRank")) {

                    if (args.size() == 2) {
                        for (PlayerRank playerRank : PlayerRank.getLstPlayerRank())lstTab.add(String.valueOf(playerRank.getId()));
                    }
                    else if(subCommand.equalsIgnoreCase("edit")){

                        try {
                            PlayerRank rank = PlayerRank.getById(Integer.parseInt(args.get(2)));
                            if(rank == null)return lstTab;

                            if (args.size() == 3) {
                                lstTab.add("order");
                                lstTab.add("objectif");
                                lstTab.add("minHeart");
                                lstTab.add("maxSlotInv");
                                lstTab.add("unlockLeftHand");
                            }
                            if (args.size() >= 4 && args.get(3).equalsIgnoreCase("objectif")) {
                                lstTab = getObjectiveCompletion(args.subList(4, args.size()), rank);
                            } else if (args.size() == 4 && !args.get(3).equalsIgnoreCase("unlockLeftHand")) {
                                for (int i = 0; i < 100; i = (i + 1) * 2) lstTab.add(String.valueOf(i));
                            }
                        }
                        catch (Exception e){}
                    }
                }
                else if(args.get(1).equalsIgnoreCase("GroupRank")) {
                    if (args.size() == 2) {
                        for (GroupRank groupRank : GroupRank.getLstGroupRank()) lstTab.add(String.valueOf(groupRank.getId()));
                    }
                    else if(subCommand.equalsIgnoreCase("edit")){

                        try {
                            GroupRank rank = GroupRank.getById(Integer.parseInt(args.get(2)));
                            if(rank == null)return lstTab;

                            if (args.size() == 3) {

                                lstTab.add("order");
                                lstTab.add("objectif");
                                lstTab.add("maxPlayer");

                                if(rank.getGroupType().equalsIgnoreCase("FACTION")) {
                                    lstTab.add("maxLand");
                                    lstTab.add("maxVassal");
                                    lstTab.add("canVassalize");
                                }
                            }
                            if (args.size() >= 4 && args.get(3).equalsIgnoreCase("objectif")){
                                lstTab = getObjectiveCompletion(args.subList(4, args.size()), rank);
                            }
                            else if (args.size() == 4 && args.get(3).equalsIgnoreCase("canVassalize")){
                                lstTab.add("true");
                                lstTab.add("false");
                            }
                            else if (args.size() == 4) {
                                for(int i =0; i < 100; i = (i+1)*2)lstTab.add(String.valueOf(i));
                            }
                        }
                        catch (Exception e){}
                    }
                }
            }
        }

        return lstTab;
    }
    private void performRankObjectifAction(List<String> args, Rank rank, Player player){

        if(!args.isEmpty()){

            String subObjectiveCommand = args.get(0);
            if (subObjectiveCommand.equalsIgnoreCase("list")){
                String objectiveList = "Liste des objectifs du rank '"+rank.getName()+"' : \n";
                for (Objective objective : rank.getLstObjectif()){
                    objectiveList += "§6 - [" + objective.getName() + "] ";
                    objectiveList += "§e" +objective.getType().toUpperCase() + "§b ";
                    objectiveList += (objective.getType().equalsIgnoreCase(Objective.Type.RESSOURCE.toString()))? objective.getItem().getType().name().toUpperCase()+" : " : "";
                    objectiveList += (!objective.getType().equalsIgnoreCase(Objective.Type.BOOLEAN.toString()))? objective.getObjectifValue() : "";
                    objectiveList += "\n";
                }

                objectiveList += "\n§6Total : " + rank.getLstObjectif().size() + " objectifs";
                GeneralMethods.sendPlayerMessage(player, objectiveList);
            }
            else if (subObjectiveCommand.equalsIgnoreCase("create") && args.size() >=3 ){
                String name = args.get(1);
                String type = args.get(2);
                String description = "";

                if(rank.getObjectiveByName(name) != null){
                    GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Create.Fail", true);
                    return;
                }

                if (type.equalsIgnoreCase(Objective.Type.BOOLEAN.toString())){
                    description = OrganisationCommand.getStringFromArgs(args, 3);
                    rank.addObjectif(name,description,0,null,type);
                    GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Create.Success", true);
                }
                else if (type.equalsIgnoreCase(Objective.Type.MINVASALL.toString()) && args.size() >=4) {
                    try {
                        int minVassal = Integer.parseInt(args.get(3));
                        description = (args.size() >=5)? OrganisationCommand.getStringFromArgs(args, 4) : "";
                        rank.addObjectif(name, description, minVassal, null, type);
                        GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Create.Success", true);
                    } catch (NumberFormatException e) {
                        GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.InvalidInt", true);
                        return;
                    }
                }
                else if (type.equalsIgnoreCase(Objective.Type.MINPLAYER.toString()) && args.size() >=4) {
                    try {
                        int minPlayer = Integer.parseInt(args.get(3));
                        description = (args.size() >=5)? OrganisationCommand.getStringFromArgs(args, 4) : "";
                        rank.addObjectif(name, description, minPlayer, null, type);
                        GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Create.Success", true);
                    } catch (NumberFormatException e) {
                        GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.InvalidInt", true);
                        return;
                    }
                }
                else if (type.equalsIgnoreCase(Objective.Type.MINLAND.toString()) && args.size() >=4) {
                    try {
                        int minLand = Integer.parseInt(args.get(3));
                        description = (args.size() >=5)? OrganisationCommand.getStringFromArgs(args, 4) : "";
                        rank.addObjectif(name, description, minLand, null, type);
                        GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Create.Success", true);
                    } catch (NumberFormatException e) {
                        GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.InvalidInt", true);
                        return;
                    }
                }
                else if(type.equalsIgnoreCase(Objective.Type.RESSOURCE.toString()) && args.size() >=4){

                    try {
                        int amount = Integer.parseInt(args.get(3));
                        description = (args.size() >=5)? OrganisationCommand.getStringFromArgs(args, 4) : "";

                        ItemStack item = player.getItemInHand();
                        if(item == null){
                            GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.NullItem", true);
                            return;
                        }
                        if(item.getType().equals(Material.AIR)){
                            GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.NullItem", true);
                            return;
                        }

                        item.setAmount(1);
                        rank.addObjectif(name, description, amount, item.clone(), type);
                        GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Create.Success", true);
                    } catch (NumberFormatException e) {
                        GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.InvalidInt", true);
                        return;
                    }
                }
                else{
                    GeneralMethods.sendPlayerErrorMessage(player,"/o rank create GroupRank " + rank.getId() + " objectif create <new_name> <type> <value | description > .... description");
                    return;
                }

                // On met a jour la liste des objectifs
                for(Group group : Group.getLstGroup()){
                    if(group.getRank().getId() == rank.getId()){
                        group.setLstGroupObjective(GroupObjective.resetObjective(group));
                    }
                }
            }
            else if (subObjectiveCommand.equalsIgnoreCase("edit") && args.size() >= 3){
                String objectiveName  = args.get(1);
                Objective objective = rank.getObjectiveByName(objectiveName);


                if (objective == null) {
                    GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.Objective.Edit.Fail", true);
                    return;
                }

                if (args.get(2).equalsIgnoreCase("description")){
                    String description = OrganisationCommand.getStringFromArgs(args, 3);
                    objective.setDescription(description);
                    GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Edit.Success.Desc", true);
                }
                else if(args.get(2).equalsIgnoreCase("value") && objective.getType().equalsIgnoreCase(Objective.Type.MINVASALL.toString())){
                    try{
                        int newValue = Integer.parseInt(args.get(3));
                        objective.setObjectifValue(newValue);
                        GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Edit.Success.MinVassal", true);
                    }
                    catch (NumberFormatException e){
                        GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.InvalidInt", true);
                    }
                }
                else if(args.get(2).equalsIgnoreCase("value") && objective.getType().equalsIgnoreCase(Objective.Type.MINPLAYER.toString())){
                    try{
                        int newValue = Integer.parseInt(args.get(3));
                        objective.setObjectifValue(newValue);
                        GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Edit.Success.MinPlayer", true);
                    }
                    catch (NumberFormatException e){
                        GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.InvalidInt", true);
                    }
                }
                else if(args.get(2).equalsIgnoreCase("value") && objective.getType().equalsIgnoreCase(Objective.Type.MINLAND.toString())){
                    try{
                        int newValue = Integer.parseInt(args.get(3));
                        objective.setObjectifValue(newValue);
                        GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Edit.Success.MinLand", true);
                    }
                    catch (NumberFormatException e){
                        GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.InvalidInt", true);
                    }
                }
                else if(args.get(2).equalsIgnoreCase("value") && objective.getType().equalsIgnoreCase(Objective.Type.RESSOURCE.toString())){
                    try{
                        int newValue = Integer.parseInt(args.get(3));
                        objective.setObjectifValue(newValue);
                        GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Edit.Success.NbRessources", true);
                    }
                    catch (NumberFormatException e){
                        GeneralMethods.sendPlayerErrorMessage(player, "Commands.Rank.InvalidInt", true);
                    }
                }
                else{
                    GeneralMethods.sendPlayerErrorMessage(player,"/o rank create GroupRank " + rank.getId() + " objectif edit" + objectiveName + "<description|minVassal|nbRessources>");
                }
            }
            else if (subObjectiveCommand.equalsIgnoreCase("remove") && args.size() == 2){

                String objectiveName  = args.get(1);
                if (rank.deleteObjectif(objectiveName)){
                    GeneralMethods.sendPlayerMessage(player,"Commands.Rank.Objective.Remove.Success", true);
                    // On met a jour la liste des objectifs
                    for(Group group : Group.getLstGroup()){
                        if(group.getRank().getId() == rank.getId()){
                            group.setLstGroupObjective(GroupObjective.resetObjective(group));
                        }
                    }
                }
                else{
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Rank.Objective.Remove.Fail", true);
                }
            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player,"/o rank create GroupRank " + rank.getId() + " objectif <create|edit|list|remove>");
            }
        }
    }
    private ArrayList<String> getObjectiveCompletion(List<String> args, Rank rank){

        ArrayList<String> lstTab = new ArrayList<>();
        if (args.size() < 1) {

            lstTab.add("create");
            lstTab.add("list");
            lstTab.add("edit");
            lstTab.add("remove");
        }
        else{
            String subCommand = args.get(0);
            if(subCommand.equalsIgnoreCase("create")){
                if(args.size() == 1){
                    lstTab.add("new_name");
                }
                else if(args.size() == 2 && rank instanceof GroupRank){
                    lstTab.addAll(Objective.Type.getLstType());
                }
                else if(args.size() == 2 && rank instanceof PlayerRank){
                   lstTab.add(Objective.Type.BOOLEAN.toString());
                   lstTab.add(Objective.Type.RESSOURCE.toString());
                }
                else if(args.size() >= 3 &&
                        (args.get(2).equalsIgnoreCase(Objective.Type.MINVASALL.toString())
                        || args.get(2).equalsIgnoreCase(Objective.Type.MINPLAYER.toString())
                        || args.get(2).equalsIgnoreCase(Objective.Type.MINLAND.toString())
                        || args.get(2).equalsIgnoreCase(Objective.Type.RESSOURCE.toString()))){

                    if (args.size() == 3){

                        lstTab.add("amount");
                        lstTab.add("1");
                        lstTab.add("2");
                        lstTab.add("3");
                        lstTab.add("40");
                        lstTab.add("50");
                        lstTab.add("600");
                        lstTab.add("800");
                        lstTab.add("9000");
                    }
                    else{
                        lstTab.add("description");
                    }
                }
                else if(args.size() >= 3 && args.get(3).equalsIgnoreCase(Objective.Type.BOOLEAN.toString())){
                    lstTab.add("description");
                }
            }
            else if(subCommand.equalsIgnoreCase("edit")){

                if(args.size() == 1){
                    for(Objective objective: rank.getLstObjectif())lstTab.add(objective.getName());
                }
                else if(args.size() == 2){
                    lstTab.add("value");
                    lstTab.add("description");
                }
                else {
                    Objective objective = rank.getObjectiveByName(args.get(1));
                    if (objective == null)return lstTab;

                    if (args.size() == 2) {
                        lstTab.add("description");
                        if(!objective.getType().equalsIgnoreCase(Objective.Type.BOOLEAN.toString())) lstTab.add("value");

                    } else if (args.size() == 3 && (args.get(2).equalsIgnoreCase("value"))
                            && !objective.getType().equalsIgnoreCase(Objective.Type.BOOLEAN.toString())) {

                        lstTab.add("5");
                        lstTab.add("50");
                        lstTab.add("100");
                        lstTab.add("1");
                        lstTab.add("10");
                        lstTab.add("500");
                        lstTab.add("1000");
                        lstTab.add("1500");
                        lstTab.add("3000");
                    }
                }
            }
            else if(subCommand.equalsIgnoreCase("remove") && args.size() == 1){
                for(Objective objective: rank.getLstObjectif())lstTab.add(objective.getName());
            };
        }

        return lstTab;
    }
}
