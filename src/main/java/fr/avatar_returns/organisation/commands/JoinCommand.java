package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class JoinCommand extends OrganisationCommand {

    public JoinCommand() {
        super("join", "/join <organisation name>", GeneralMethods.getStringConf("Commands.Join.Desc"), new String[]{"join", "j"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args){

        if (!(sender instanceof Player)){
            if (args.size() != 2) {
                Organisation.log.severe("Bad console usage of '/o join <factionName> <playerName>'");
                return;
            }

            Group groupJoin = Group.getGroupByName(args.get(0));
            if(groupJoin == null)return;
            Player playerTarget = Organisation.plugin.getServer().getPlayer(args.get(1));

            OrganisationPlayer organisationPlayerTargetJoin = getCommandOrganisationPlayer(playerTarget);
            if (organisationPlayerTargetJoin == null) return;

            groupJoin.addMember(organisationPlayerTargetJoin);
            GeneralMethods.sendPlayerMessage(playerTarget,"Commands.Join.SuccessJoin", true);

            GroupRank.checkProgression(groupJoin);
            awarePeopleJoin(groupJoin, playerTarget);
            return;
        }

        Player player = (Player) sender;

        if (args.size() != 1) {
            GeneralMethods.sendPlayerErrorMessage(player, this.getProperUse());
            return;
        }

        OrganisationPlayer organisationPlayerJoin = getCommandOrganisationPlayer(player);
        if (organisationPlayerJoin == null) return;

        Group groupJoin = Group.getGroupByName(args.get(0));
        if(groupJoin == null) {
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.NoOrganisationFound",true);
            return;
        }

        if(groupJoin.getLocalPlayers().contains(player)){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Join.CanNotJoinYourGroup", true);
            return;
        }

        Invitation invitation = null;
        for(Invitation invit: InviteCommand.getAllInvitation(player)){
            if(invit.getGroupHost().equals(groupJoin)){
                invitation = invit;
                break;
            }
        }

        if(player.isOp() || player.hasPermission("organisation.command.join.bypassInvitation")){
            groupJoin.addMember(organisationPlayerJoin);
            GeneralMethods.sendPlayerMessage(player,"Commands.Join.SuccessJoin", true);
            if(invitation != null){
                InviteCommand.lstInvitation.remove(invitation);
                DBUtils.deleteInvitation(invitation);
            }
            GroupRank.checkProgression(groupJoin);
            awarePeopleJoin(groupJoin, player);
            return;
        }

        if(invitation == null){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Join.PlayerOrGroupNotFound",true);
            return;
        }

        if(groupJoin.getLstMember().size() >= groupJoin.getMaxPlayer()){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.Join.ToMutchPlayer",true);
            return;
        }

        if(!player.hasPermission("organisation.command.join")) {
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.NoPermission",true);
            return;
        }
        else{
            if(!groupJoin.addMember(organisationPlayerJoin)){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.NoPermission",true);
                return;
            }
        }

        InviteCommand.lstInvitation.remove(invitation);
        DBUtils.deleteInvitation(invitation);

        //On supprime toutes les invitation du joueur dans ce groupe.
        ArrayList<Invitation> lstInvitationToRemove =  new ArrayList<>();
        for(Invitation otherInvitation : InviteCommand.lstInvitation){
            if(otherInvitation.getOPlayerInvite().getUuid().equals(invitation.getOPlayerInvite().getUuid())
                    && otherInvitation.getGroupHost().getName().equals(invitation.getGroupHost().getName())){
                lstInvitationToRemove.add(otherInvitation);
            }
        }

        for(Invitation otherInvitation : lstInvitationToRemove){
            InviteCommand.lstInvitation.remove(otherInvitation);
            DBUtils.deleteInvitation(otherInvitation);
        }

        GeneralMethods.sendPlayerMessage(player, "Commands.Join.SuccessJoin", true);
        awarePeopleJoin(groupJoin, player);
    }

    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        ArrayList<String> lstTab = new ArrayList<>();
        if (!(sender instanceof Player) || args.size() != 0) return super.getTabCompletion(sender, args);

        Player player = (Player) sender;
        OrganisationPlayer oPlayer;

        try {
            oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException e) {
            return super.getTabCompletion(sender, args);
        }

        for (Invitation inv : InviteCommand.lstInvitation) {
            if (inv.getOPlayerInvite().equals(oPlayer)) {
                lstTab.add(inv.getGroupHost().getName());
            }
        }

        return lstTab;
    }

    public static void awarePeopleJoin(Group group, OfflinePlayer playerJoiner){
        for(OrganisationPlayer oPlayer: group.getLstGroupConnectedOrganisationPlayer()){
            GeneralMethods.sendPlayerMessage(oPlayer, playerJoiner.getName()+" a rejoint votre organisation !!!");
        }
    }

}
