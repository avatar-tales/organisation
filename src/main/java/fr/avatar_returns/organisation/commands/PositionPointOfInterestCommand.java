package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.territory.Territory;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


import java.util.ArrayList;
import java.util.List;

public class PositionPointOfInterestCommand extends OrganisationCommand{

    public PositionPointOfInterestCommand() {
        super("position", "/o position <list|add|remove|teleport> <landWGName|<PositionFromEyes|PositionFromBody>|position_id> <context>", GeneralMethods.getStringConf("Commands.Position.Desc"), new String []{"position", "pos"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {
        super.execute(sender, args);

        if( !(sender instanceof Player) )return;
        Player player = (Player)sender;

        if(!(player.isOp() || player.hasPermission("organisation.command.position"))){
            GeneralMethods.sendPlayerErrorMessage(player,"Commands.NoPermission", true);
            return;
        }

        if(args.size() < 1 ){
            GeneralMethods.sendPlayerMessage(player,this.getProperUse());
            return;
        }

        String subCommand = args.get(0);

        if(subCommand.equalsIgnoreCase("list") && ((args.size() == 1 || args.size() == 2))){

            ArrayList<PositionPointInterest> lstPositionPointOfInterest;
            String msg = "";

            if(args.size() == 2){

                Land land = Land.getLandByRegionName(args.get(1));
                if(land == null){
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Land.LandNotFound", true);
                    return;
                }
                msg = "Liste des sous territoires du land '"+ args.get(0)  +"' : \n";;
                lstPositionPointOfInterest = land.getPositionPointOfInterest();
                for(LandSubTerritory landSubTerritory : land.getLstSubTerritory()){
                    lstPositionPointOfInterest.addAll(landSubTerritory.getPositionPointOfInterest());
                }
            }
            else{
                msg = "Liste des sous territoires : \n";
                lstPositionPointOfInterest = PositionPointInterest.getLstPositionPointInterest();
            }

            for(PositionPointInterest position : lstPositionPointOfInterest){

                Territory territory = position.getTerritory();

                String province = "§6";
                String landName = "§e";
                String landSubTerritoryName = "§a";

                if(territory instanceof LandSubTerritory){
                    LandSubTerritory landSubTerritory = (LandSubTerritory) territory;
                    province += "[" + landSubTerritory.getLand().getProvince().getId() +"] ";
                    landName += landSubTerritory.getLand().getWorldGuardRegion().getId() + " ";
                    landSubTerritoryName += landSubTerritory.getWorldGuardRegion().getId() + " ";
                }
                else if(territory instanceof Land){
                    Land land = (Land) territory;
                    province += "[" + land.getProvince().getId() +"] ";
                    landName += land.getWorldGuardRegion().getId() + " ";
                }

                String id = "§d - " + (position.getId()) + " ";
                String locationStr = "§7("+position.getLocation().getBlockX()+", "+position.getLocation().getBlockY()+", "+position.getLocation().getBlockZ()+") ";
                String type = " §d"+position.getContext().toString() + " ";

                msg += id + province + landName + landSubTerritoryName + locationStr + type + "\n";
            }

            GeneralMethods.sendPlayerMessage(player, msg);
            return;
        }
        else if(subCommand.equalsIgnoreCase("add") && args.size() == 3){

            String context = args.get(2);
            String positionFrom = args.get(1);
            PositionPointInterest.Type positionContext = PositionPointInterest.getContextFromString(context);

            if(positionContext == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Position.Add.Position.Context.Unknow", true);
                return;
            }

            Location location;
            if(positionFrom.equalsIgnoreCase("PositionFromEyes")){
                boolean lastAirBlock = (!(positionContext == PositionPointInterest.Type.FHOME
                        || positionContext == PositionPointInterest.Type.TOTEM
                        || positionContext == PositionPointInterest.Type.DOMINATION_FLAG
                        || positionContext == PositionPointInterest.Type.LAND_FLAG ));

                location = GeneralMethods.getPlayerTargetBlock(player, 10, lastAirBlock).getLocation();

                if(!location.getBlock().getType().name().contains("BANNER") &&  (positionContext == PositionPointInterest.Type.DOMINATION_FLAG || positionContext == PositionPointInterest.Type.LAND_FLAG )){
                    location = GeneralMethods.getPlayerTargetBlock(player, 10, true).getLocation();
                }

            }
            else if(positionFrom.equalsIgnoreCase("PositionFromBody")){
                location = player.getLocation();
            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
                return;
            }

            if(PositionPointInterest.getByLocation(location) != null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Position.Add.AlreadyExist", true);
                return;
            }

            Territory territory = LandSubTerritory.getLandSubTerritoryByLocation(location);
            //We try to get the lower territory (first SubTerritory which are include in Land, then Land which are the hightest level)
            if(territory == null){
                territory = Land.getLandByLocation(location);
                if(territory == null){
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Position.Add.Position.BadTerritory", true);
                    return;
                }
            }

            //Check if we can create this position point of interest with this context on this territory.
            if(!PositionPointInterest.getPossibleContext(territory).contains(context)){

                if(positionContext == PositionPointInterest.Type.MINE || positionContext == PositionPointInterest.Type.MINE_FIXE){
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Position.Add.Position.Context.MineOnly", true);
                    return;
                }
                else if(positionContext == PositionPointInterest.Type.TOTEM){
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Position.Add.Position.Context.TotemOnly", true);
                    return;
                }
                else if(positionContext == PositionPointInterest.Type.FHOME
                        || positionContext == PositionPointInterest.Type.LAND_RESPAWN_POINT ){
                    GeneralMethods.sendPlayerErrorMessage(player,"Commands.Position.Add.Position.Context.LandOnly", true);
                    return;
                }
            }

            //Check if we can add FHOME (to avoid multiple Fhome by land)
            if(positionContext == PositionPointInterest.Type.FHOME) {

                if(territory instanceof Land){
                    if (!(((Land) territory).isMainLand())){
                        GeneralMethods.sendPlayerErrorMessage(player,"Le home ne se pose que sur un mainLand.", false);
                        return;
                    }
                }
                else{
                    GeneralMethods.sendPlayerErrorMessage(player,"Le home ne se pose que sur un mainLand.", false);
                    return;
                }

                for (PositionPointInterest pos : territory.getPositionPointOfInterest()){
                    if(pos.getContext() == positionContext){
                        GeneralMethods.sendPlayerErrorMessage(player,"Il y a déja un home dans ce mainland.", false);
                        return;
                    }
                }
            }

            if(positionContext == PositionPointInterest.Type.MINE || positionContext == PositionPointInterest.Type.MINE_FIXE){
                if(!location.getBlock().getType().equals(Material.CHEST))location.getBlock().setType(Material.CHEST);
            }
            else if(positionContext == PositionPointInterest.Type.TOTEM){
                if(positionFrom.equalsIgnoreCase("PositionFromBody"))location.set(location.getBlockX(), location.getBlockY() -1, location.getBlockZ());
                if(!location.getBlock().getType().equals(Material.EMERALD_BLOCK))location.getBlock().setType(Material.EMERALD_BLOCK);
            }
            else if(positionContext == PositionPointInterest.Type.FHOME){
                if(positionFrom.equalsIgnoreCase("PositionFromBody"))location.set(location.getBlockX(), location.getBlockY() -1, location.getBlockZ());
                if(!location.getBlock().getType().equals(Material.GOLD_BLOCK))location.getBlock().setType(Material.GOLD_BLOCK);
            }
            else if(positionContext == PositionPointInterest.Type.LAND_FLAG){
                if(!location.getBlock().getType().name().contains("BANNER")) location.getBlock().setType(Material.RED_BANNER);
            }
            else if(positionContext == PositionPointInterest.Type.DOMINATION_FLAG){
                if(!location.getBlock().getType().name().contains("BANNER"))location.getBlock().setType(Material.ORANGE_BANNER);
            }

            new PositionPointInterest(location, territory, positionContext);
            GeneralMethods.sendPlayerMessage(player, "Commands.Position.Add.Success", true);
        }
        else if(args.size() == 2){

            int id;
            try {
                id = Integer.parseInt(args.get(1));
            }
            catch (NumberFormatException e){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Position.InvalidArgs.Int",true);
                return;
            }

           PositionPointInterest positionPointInterest = PositionPointInterest.getById(id);
            if(positionPointInterest == null){
                GeneralMethods.sendPlayerErrorMessage(player,"Commands.Position.NotFound", true);
                return;
            }

            if(subCommand.equalsIgnoreCase("remove")){
                positionPointInterest.remove();
                GeneralMethods.sendPlayerMessage(player, "Commands.Position.Add.Remove", true);
            }
            else if(subCommand.equalsIgnoreCase("teleport")){
                positionPointInterest.teleport(player);
                GeneralMethods.sendPlayerMessage(player, "Commands.Position.Add.Teleport", true);
            }
            else{
                GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
                return;
            }
        }
        else{
            GeneralMethods.sendPlayerErrorMessage(player,this.getProperUse());
        }
    }

    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        ArrayList<String> l = new ArrayList<>();
        if(!(sender instanceof Player)) return l;

        Player player = (Player) sender;

        if (args.size() < 1) {
            l.add("list");
            l.add("add");
            l.add("remove");
            l.add("teleport");
        }
        else if(args.size() == 1){

            if(args.get(0).equalsIgnoreCase("list")){
                for(Land land : Land.getAllLand()){
                    l.add(land.getWorldGuardRegion().getId());
                }
            }
            else if(args.get(0).equalsIgnoreCase("add")){
                l.add("PositionFromEyes");
                l.add("PositionFromBody");
            }
            else if(args.get(0).equalsIgnoreCase("remove") || args.get(0).equalsIgnoreCase("teleport")){
                for(PositionPointInterest positionPointInterest : PositionPointInterest.getLstPositionPointInterest())l.add(Integer.toString(positionPointInterest.getId()));
            }
        }
        else if(args.size() == 2){
            if(args.get(0).equalsIgnoreCase("add")){
                Location location;
                if(args.get(1).equalsIgnoreCase("PositionFromEyes")){
                    location = player.getEyeLocation();
                }
                else if (args.get(1).equalsIgnoreCase("PositionFromBody")){
                    location = player.getLocation();
                }
                else {
                    l.add("INVALID_ARGS_DETECTED");
                    return l;
                }

                Territory territory = LandSubTerritory.getLandSubTerritoryByLocation(location);
                //We try to get the lower territory (first SubTerritory which are include in Land, then Land which are the hightest level)
                if(territory == null){
                    territory = Land.getLandByLocation(location);
                }

                if(territory != null){
                    for(String possibleContext : PositionPointInterest.getPossibleContext(territory))l.add(possibleContext);
                }
            }
        }

        return l;
    }
}
