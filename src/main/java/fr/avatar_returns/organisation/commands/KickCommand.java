package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationGroupPlayerException;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;


public class KickCommand extends OrganisationCommand {

    private ArrayList<Group> lstValidationKick = new ArrayList<Group>();

    public KickCommand() {
        super("kick", "/kick <name> <Organisation>", GeneralMethods.getStringConf("Commands.Kick.Desc"), new String []{"kick", "k"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

        if (!(sender instanceof Player))return;
        Player playerSender =  (Player) sender;

        if( args.size() != 1 && args.size() != 2) {
            GeneralMethods.sendPlayerErrorMessage(playerSender,this.getProperUse());
            return;
        }

        OrganisationPlayer organisationPlayerSender = getCommandOrganisationPlayer(playerSender);
        if(organisationPlayerSender == null)return;

        String playerName = args.get(0);

        //Get player who is kick

        OrganisationPlayer oPlayerKick = null;
        try{
            oPlayerKick = OrganisationPlayer.getOrganisationPlayerByName(playerName);
        }
        catch (OrganisationPlayerException e){
            GeneralMethods.sendPlayerErrorMessage(playerSender,"Le joueur "+playerName +" n'a jamais joué sur le serveur avant..");
            return;
        }

        Group playerSenderGroup = getCommandPlayerGroup(organisationPlayerSender,args, 2);

        OrganisationMember playerKickMember = playerSenderGroup.getOrganisationMember(oPlayerKick);
        if(playerKickMember == null){
            GeneralMethods.sendPlayerErrorMessage(playerSender,"Commands.Kick.PlayerDontExistInGroup",true);
            return;
        }

        //On vérifie que le joueur à bien la permission de kick un autre joueur
        if(!playerSenderGroup.hasPermission(playerSender, Permission.management.kick)){
            GeneralMethods.sendPlayerErrorMessage(playerSender,"Commands.Kick.PlayerNoPermission",true);
            return;
        }

        //Check
        if(!Role.canExecuteOnOtherRole(organisationPlayerSender, playerKickMember.getRole())){
            GeneralMethods.sendPlayerErrorMessage(playerSender,"Commands.Kick.PlayerNoPermission",true);
            return;
        }

        kick(oPlayerKick, playerSenderGroup);
    }

    private void kick(OrganisationPlayer oKickPlayer, Group group) {

        group.removeMember(oKickPlayer);
        GeneralMethods.sendPlayerMessage(oKickPlayer, "Vous avez été kick de " + group.getName());
        for(OrganisationPlayer oPlayer: group.getLstGroupConnectedOrganisationPlayer()){
            GeneralMethods.sendPlayerMessage(oPlayer,oKickPlayer.getName()+" a été kick de "+ group.getName());
        }
        GroupRank.checkProgression(group);
    }

    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        ArrayList<String> lstTab = new ArrayList<>();
        if(!(sender instanceof Player) )return lstTab;

        Player player = (Player) sender;
        OrganisationPlayer organisationPlayer;

        try {
            organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException e) {
            return lstTab;
        }

        if(args.isEmpty()){

            for(Group group : organisationPlayer.getLstGroup()){
                if(group.getOrganisationMember(organisationPlayer).getRole().hasPermission(Permission.management.kick)){
                    for(OrganisationMember oMember : group.getLstMember()){
                        if(oMember.getOrganisationPlayer().equals(organisationPlayer))continue;
                        if(oMember.getRole().getRolePower() > group.getOrganisationMember(organisationPlayer).getRole().getRolePower() || group.getOrganisationMember(organisationPlayer).isLeader()){
                            lstTab.add(oMember.getName());
                        }
                    }
                }
            }

            return lstTab;
        }
        else if(args.size() == 1) {
            for(Group group : organisationPlayer.getLstGroup()){
                lstTab.add(group.getName());
            }
        }

        return lstTab;
    }

}
