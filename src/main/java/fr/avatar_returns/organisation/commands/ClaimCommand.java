package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationGroupPlayerException;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;


public class ClaimCommand extends OrganisationCommand {

    private ArrayList<Group> lstValidationKick = new ArrayList<Group>();

    public ClaimCommand() {
        super("claim", "/o claim <LargeGroupName>", GeneralMethods.getStringConf("Commands.Claim.Desc"), new String []{"claim"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

        if (!(sender instanceof Player)) return;

        Player player = (Player) sender;
        Location playerLocation = player.getLocation();
        OrganisationPlayer organisationPlayerSender = getCommandOrganisationPlayer(player);

        if (organisationPlayerSender == null) return;

        LargeGroup playerSenderLargeGroup = getCommandPlayerLargeGroup(organisationPlayerSender, args, 0);
        if (playerSenderLargeGroup == null) return;

        OrganisationMember organisationMember = playerSenderLargeGroup.getOrganisationMember(player);
        if (!(organisationMember.getRole().hasPermission(Permission.land.claim) || player.isOp() || player.hasPermission("organisation.admin"))) {
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.NoPermission", true);
            return;
        }

        Land land = Land.getLandByLocation(playerLocation);
        if (land == null) {
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.Claim.nullLand", true);
            return;
        }
        if(playerSenderLargeGroup.getLstVland().size() >= playerSenderLargeGroup.getMaxLand()){
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.Claim.MaxLand", true);
            return;
        }
        if (!land.isOwnableLand()) {
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.Claim.nonOwnableLand", true);
            return;
        }
        if(playerSenderLargeGroup.joinLand(land)) {
            GeneralMethods.sendPlayerMessage(player, "Commands.Claim.success", true);
            for(PositionPointInterest positionPointInterest : land.getPositionPointOfInterest()){
                PositionPointInterest.setBanner(positionPointInterest, playerSenderLargeGroup);
            }
            GroupRank.checkProgression(playerSenderLargeGroup);
        }
        else GeneralMethods.sendPlayerErrorMessage(player, "Commands.Claim.error", true);
    }


    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        final List<String> l = new ArrayList<String>();
        if(args.size() == 0){
            Player player = (Player) sender;
            OrganisationPlayer organisationPlayer;

            try {
                organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
            } catch (OrganisationPlayerException e) {
                return super.getTabCompletion(sender, args);
            }

            for(LargeGroup largeGroup : organisationPlayer.getLstLargeGroup()){
                l.add(largeGroup.getName());
            }
        }
        return l;
    }
}
