package fr.avatar_returns.organisation.commands;

import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.manager.SecondeTaskManager;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;


public class PrespawnCommand extends OrganisationCommand {


    public PrespawnCommand() {
        super("prespawn", "/o prespawn <playerName>", GeneralMethods.getStringConf("Commands.Prespawn.Desc"), new String[]{"prespawn"});
    }

    @Override
    public void execute(CommandSender sender, List<String> args) {

        if (!(sender instanceof Player)) return;
        Player player = (Player) sender;
        int nbSupportTeleporting = 0;

        if(!(sender.hasPermission("organisation.prespawn") || sender.isOp())){
            GeneralMethods.sendPlayerErrorMessage((Player) sender,"Commands.NoPermission",true);
            return;
        }

        // Verif proper use
        if(args.size() != 1){
            GeneralMethods.sendPlayerErrorMessage(player, this.getProperUse());
            return;
        }

        // Init Oplayers
        OrganisationPlayer oPlayer = null;
        try {
            oPlayer = OrganisationPlayer.getOrganisationPlayer(player);
            if(oPlayer.isInFightMod()){
                GeneralMethods.sendPlayerErrorMessage(player,"Organisation.FightMod.CantExecuteCommand", true);
                return;
            }
        }catch (OrganisationPlayerException e){return;}

        OrganisationPlayer oTargetPlayer = null;
        try{oTargetPlayer = OrganisationPlayer.getOrganisationPlayerByName(args.get(0));}
        catch (OrganisationPlayerException e){
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.Prespawn.Target.NotFound", true);
            return;
        }

        // Count nbSupportTeleporting
        for (OrganisationPlayer support : SecondeTaskManager.mapPlayerToTPPrespawnTarget.keySet()) {
            if(SecondeTaskManager.mapPlayerToTPPrespawnTarget.get(support).getUuid() == oTargetPlayer.getUuid()) nbSupportTeleporting++;
        }

        // Self Cancel
        if(SecondeTaskManager.mapPlayerToTPPrespawnTarget.containsKey(oPlayer)){

            GeneralMethods.sendPlayerMessage(player, "La téléportation a " + SecondeTaskManager.mapPlayerToTPPrespawnTarget.get(oPlayer).getName() + " a bien été annulée.");
            if(nbSupportTeleporting == 1)GeneralMethods.sendPlayerMessage(SecondeTaskManager.mapPlayerToTPPrespawnTarget.get(oPlayer) , "Commands.Prespawn.Teleportation.Inform.Cancel", true);
            SecondeTaskManager.mapPlayerToTPPrespawnTarget.remove(oPlayer);
            SecondeTaskManager.mapPlayerToTPPrespawn.remove(oPlayer);

            return;
        }

        // Verif multi support tp
        if(!GeneralMethods.getBooleanConf("Organisation.Prespawn.AllowMultipleTeleportation")) {
            if (nbSupportTeleporting >= GeneralMethods.getIntConf("Organisation.Prespawn.MaxMultipleTeleportation") && nbSupportTeleporting != 0) {
                GeneralMethods.sendPlayerErrorMessage(player, "Commands.Prespawn.ToMutchSupportTeleportation", true);
                return;
            }
        }

        // Check valid target
        if(!oTargetPlayer.isConnected()){
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.Prespawn.Target.NotConnected", true);
            return;
        }
        if(!oTargetPlayer.isInPrespawnMod()){
            GeneralMethods.sendPlayerErrorMessage(player, "Commands.Prespawn.Target.NotInPrespawn", true);
            return;
        }

        int waitDelay  = (player.isOp() || player.hasPermission("organisation.command.prespawn.admin")) ? 0 : GeneralMethods.getIntConf("Organisation.Spawn.Teleportation.Duration");
        long timeToTeleport = System.currentTimeMillis() + (waitDelay * 1000L);

        SecondeTaskManager.mapPlayerToTPPrespawnTarget.put(oPlayer, oTargetPlayer);
        SecondeTaskManager.mapPlayerToTPPrespawn.put(oPlayer, timeToTeleport);
        GeneralMethods.sendPlayerMessage(player, "Commands.Prespawn.Teleportation.Successful", true);
        if(nbSupportTeleporting == 0)GeneralMethods.sendPlayerMessage(oTargetPlayer, "Commands.Prespawn.Teleportation.Inform.Successful", true);
    }


    @Override
    protected List<String> getTabCompletion(CommandSender sender, List<String> args) {

        final List<String> l = new ArrayList<String>();

        if(args.isEmpty()){
            for(OrganisationPlayer oPlayer : OrganisationPlayer.getLstConnectedOrganisationPlayer()){
                if(oPlayer.isInPrespawnMod())l.add(oPlayer.getName());
            }
        }
        return l;
    }

}
