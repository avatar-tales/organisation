package fr.avatar_returns.organisation.storage;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.rank.Objective;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

import java.sql.Connection;
import java.sql.SQLException;

public class DBConnection {

    public static Database sql;


    private static HikariConfig dataConfig;
    private static HikariDataSource dataSource;

    private static String host;
    private static int port;
    private static String db;
    private static String user;
    private static String pass;

    public static boolean init()
    {
        host = ConfigManager.getConfig().getString("Storage.MySQL.host");
        port = ConfigManager.getConfig().getInt("Storage.MySQL.port");
        pass = ConfigManager.getConfig().getString("Storage.MySQL.pass");
        db = ConfigManager.getConfig().getString("Storage.MySQL.db");
        user = ConfigManager.getConfig().getString("Storage.MySQL.user");

        if ("mysql".equalsIgnoreCase(Organisation.plugin.getConfig().getString("Storage.engine")) ||
                "mariadb".equalsIgnoreCase(Organisation.plugin.getConfig().getString("Storage.engine"))) {
            dataConfig = new HikariConfig();
            dataConfig.setDriverClassName("org.mariadb.jdbc.Driver");
            dataConfig.setJdbcUrl("jdbc:mariadb://" + host + ":" + port + "/" + db);
            dataConfig.setUsername(user);
            dataConfig.setPassword(pass);
            dataConfig.setPoolName("OrganisationMariaDBPool");
            dataConfig.setMaxLifetime(60000);
            dataConfig.setMaximumPoolSize(10);
            dataConfig.setAutoCommit(true);
            dataSource = new HikariDataSource(dataConfig);

            sql = new Database("mysql", Organisation.log, "Establishing MySQL Connection.", "[MySQL] ", dataSource);
            Organisation.log.info("Database connection established.");

            try (Connection connection = sql.getConnection()) {
            /* Code example to edit db if needed
            else {

                try {
                    final DatabaseMetaData md = sql.connection.getMetaData();
                    if (!md.getColumns(null, null, "oPlayer", "column").next()) {
                        Organisation.log.info("Updating Database with new column...");
                        sql.getConnection().setAutoCommit(false);
                        sql.modifyQuery("ALTER TABLE `oPlayer` ADD column TEXT;", false);
                        sql.getConnection().commit();
                        sql.modifyQuery("UPDATE oPlayer SET column = '-';", false);
                        sql.getConnection().setAutoCommit(true);
                        Organisation.log.info("Database Updated.");
                    }
                } catch (final SQLException e) {
                    e.printStackTrace();
                }
            }
            */

                if (!sql.tableExists(connection, "oServer")) {
                    Organisation.log.info("Creating organisation oServer table");
                    final String query = "CREATE TABLE `oServer` (" +
                            " `id` VARCHAR(36) NOT NULL," +
                            " `lastDayRunningTask` BIGINT NOT NULL," +
                            " `isSpawnServer` BOOLEAN NOT NULL," +
                            " PRIMARY KEY (id));";
                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oPlayer")) {
                    Organisation.log.info("Creating organisation oPlayer table");
                    final String query = "CREATE TABLE `oPlayer` (" +
                            " `uuid` VARCHAR(36) NOT NULL," +
                            " `firstTimeConnexion` BIGINT NOT NULL," +
                            " `effectivePlayedTime` BIGINT NOT NULL," +
                            " `rank` INT NOT NULL," +
                            " `lastConnexionTime` BIGINT NOT NULL," +
                            " `lastIdServerConnexion` VARCHAR(36) NOT NULL," +
                            " `lastSeenName` VARCHAR(1000) NOT NULL," +
                            " `nickName` VARCHAR(1000) NOT NULL," +
                            " `isConnected` BOOLEAN NOT NULL," +
                            " `isInPrespawnMod` BOOLEAN NOT NULL," +
                            " `lastTimeFightMod` BIGINT NOT NULL," +
                            " `lastPlayerHeadSeen` VARCHAR(10000) NOT NULL," +
                            " `nbMobsKill` INT NOT NULL," +
                            " `nbDeath` INT NOT NULL," +
                            " `nbPlayerKill` INT NOT NULL," +
                            " `chatLocked` VARCHAR(1000) NOT NULL," +
                            " `lastStrElementSeen` VARCHAR(1000) NOT NULL," +
                            " PRIMARY KEY (uuid)," +
                            " FOREIGN KEY (lastIdServerConnexion) REFERENCES oServer(id));";
                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oGroup")) {
                    Organisation.log.info("Creating oGroup table");
                    final String query = "CREATE TABLE `oGroup` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `type` VARCHAR(1000) NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `description` VARCHAR(1000)," +
                            " `connexionMessage` VARCHAR(1000)," +
                            " `maxPlayer` INT NOT NULL," +
                            " `minPlayer` INT NOT NULL," +
                            " `maxRaid` INT NOT NULL," +
                            " `maxLand` INT NOT NULL," +
                            " `flagItem` VARCHAR(10000) NOT NULL," +
                            " `rank` INT NOT NULL," +
                            " `isVisible` BOOLEAN NOT NULL," +
                            " `canUnClaim` BOOLEAN NOT NULL," +
                            " `dailyRaid` INT NOT NULL," +
                            " `nextimeAllowedWar` BIGINT NOT NULL," +
                            " `lord` BIGINT NOT NULL," +
                            " PRIMARY KEY (id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oRole")) {
                    Organisation.log.info("Creating oRole table");
                    final String query = "CREATE TABLE `oRole` ( `idGroup` INT NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `rolePower` INT," +
                            " PRIMARY KEY (idGroup, name)," +
                            " CONSTRAINT UC_Role UNIQUE (idGroup, name)," +
                            " FOREIGN KEY (idGroup) REFERENCES oGroup(id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oRolePermission")) {
                    Organisation.log.info("Creating oRolePermission table");
                    final String query = "CREATE TABLE `oRolePermission` ( `idGroup` INT NOT NULL," +
                            "`roleName` VARCHAR(1000) NOT NULL," +
                            "`permission` VARCHAR(1000) NOT NULL," +
                            " PRIMARY KEY (idGroup, roleName, permission)," +
                            " FOREIGN KEY (idGroup, roleName) REFERENCES oRole(idGroup, name) ON UPDATE CASCADE);";

                    //CREATE TABLE `oRolePermission` ( `idGroup` BIGINT NOT NULL, `roleName` VARCHAR(1000) NOT NULL, `permission` VARCHAR(1000) NOT NULL, PRIMARY KEY (idGroup, roleName, permission), FOREIGN KEY (idGroup, roleName) REFERENCES oRole(idGroup, name));

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oRank")) {
                    Organisation.log.info("Creating oRank table");
                    final String query = "CREATE TABLE `oRank` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `type` VARCHAR(1000) NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `groupType` VARCHAR(1000) NOT NULL," +
                            " `rankOrder` INT NOT NULL," +
                            " `nbMaxLand` INT NOT NULL," +
                            " `nbMaxVassal` INT NOT NULL," +
                            " `nbMaxPlayer` INT NOT NULL," +
                            " `canVassalize` BOOLEAN NOT NULL," +
                            " `nbMinHeart` INT NOT NULL," +
                            " `nbMaxSlotInv` INT NOT NULL," +
                            " `unlockLeftHand` BOOLEAN NOT NULL," +
                            " PRIMARY KEY (id, type));";

                    sql.modifyQuery(connection, query, false);

                    // Create default rank

                    new GroupRank("Tribu",0, "FACTION", 1,0,5, false);
                    new GroupRank("Clan",1,"FACTION",1,0,7, false);
                    new GroupRank("Village",2,"FACTION",1,0,9, false);
                    new GroupRank("Cité",3,"FACTION",2,1,15, true);
                    new GroupRank("Capitale",4,"FACTION",3,2,20, true);
                    new GroupRank("Royaume",5,"FACTION",4,5,30, true);
                    new GroupRank("Empire",6,"FACTION",4,8,45, true);

                    new GroupRank("Guilde",0,"GUILDE",0,0,5, false);
                }
                if (!sql.tableExists(connection, "oObjective")) {
                    Organisation.log.info("Creating oObjective table");
                    final String query = "CREATE TABLE `oObjective` ( `rankId` INT NOT NULL," +
                            " `rankType` VARCHAR(1000) NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `description` VARCHAR(10000) NOT NULL," +
                            " `item` VARCHAR(10000) NOT NULL," +
                            " `objectifValue` INT NOT NULL," +
                            " `objectifType` VARCHAR(1000) NOT NULL," +
                            " PRIMARY KEY (rankId, rankType, name)," +
                            " FOREIGN KEY (rankId, rankType) REFERENCES oRank(id, type));";

                    sql.modifyQuery(connection, query, false);

                    // For each rank add default staff validation objective
                    for(GroupRank groupRank: GroupRank.getLstGroupRank()){
                        groupRank.addObjectif("Validation_staff", "Autorisation de valider le rang", 0, null,Objective.Type.BOOLEAN.toString(), true);
                    }
                }
                if (!sql.tableExists(connection, "oObjectiveProgression")) {
                    Organisation.log.info("Creating oObjectiveProgression table");
                    final String query = "CREATE TABLE `oObjectiveProgression` ( `rankId` INT NOT NULL," +
                            " `groupId` INT NOT NULL," +
                            " `rankType` VARCHAR(1000) NOT NULL," +
                            " `objectiveName` VARCHAR(1000) NOT NULL," +
                            " `currentObjectiveValue` INT NOT NULL," +
                            " PRIMARY KEY (rankId, rankType, objectiveName, groupId)," +
                            " FOREIGN KEY (groupId) REFERENCES oGroup(id), " +
                            " FOREIGN KEY (rankId, rankType, objectiveName) REFERENCES oObjective(rankId, rankType, name));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oGroupMember")) {
                    Organisation.log.info("Creating oGroupMember table");
                    final String query = "CREATE TABLE `oGroupMember` ( `uuid` VARCHAR(36) NOT NULL," +
                            " `idGroup` INT NOT NULL," +
                            "`roleName` VARCHAR(1000) NOT NULL," +
                            " `firstTimeConnexion` BIGINT NOT NULL," +
                            " `effectivePlayedTime` BIGINT NOT NULL," +
                            " PRIMARY KEY (uuid, idGroup)," +
                            " FOREIGN KEY (uuid) REFERENCES oPlayer(uuid)," +
                            " FOREIGN KEY (idGroup, roleName) REFERENCES oRole(idGroup, name) ON UPDATE CASCADE);";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oInvitations")) {
                    Organisation.log.info("Creating oInvitations table");
                    final String query = "CREATE TABLE `oInvitations` ( `uuidHost` VARCHAR(36) NOT NULL," +
                            " `idGroup` INT NOT NULL," +
                            " `uuidInvite` VARCHAR(36) NOT NULL," +
                            " PRIMARY KEY (uuidHost, idGroup, uuidInvite)," +
                            " FOREIGN KEY (uuidHost) REFERENCES oPlayer(uuid)," +
                            " FOREIGN KEY (uuidInvite) REFERENCES oPlayer(uuid)," +
                            " FOREIGN KEY (idGroup) REFERENCES oGroup(id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oProvince")) {

                    Organisation.log.info("Creating oProvince table");
                    final String query = "CREATE TABLE `oProvince` ( `id` VARCHAR(1000) NOT NULL," +
                            "`name` VARCHAR(1000) NOT NULL," +
                            "`description` VARCHAR(10000) NOT NULL," +
                            " PRIMARY KEY (id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oLand")) {
                    Organisation.log.info("Creating oLand table");
                    final String query = "CREATE TABLE `oLand` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `idServer` VARCHAR(1000) NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `idRegionWorldGuard` VARCHAR(1000) NOT NULL," +
                            " `isMainLand` BOOLEAN NOT NULL," +
                            " `idProvince` VARCHAR(1000)," +
                            " `idGroup` INT," +
                            " `canOtherGoIn` BOOLEAN NOT NULL," +
                            " `isActive` BOOLEAN NOT NULL," +
                            " `ownedTime` BIGINT NOT NULL," +
                            " PRIMARY KEY (id)," +
                            " FOREIGN KEY (idProvince) REFERENCES oProvince(id)," +
                            " FOREIGN KEY (idServer) REFERENCES oServer(id)," +
                            " FOREIGN KEY (idGroup) REFERENCES oGroup(id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oLandSubTerritory")) {
                    Organisation.log.info("Creating oLandSubTerritory table");
                    final String query = "CREATE TABLE `oLandSubTerritory` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `idServer` VARCHAR(1000) NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `idRegionWorldGuard` VARCHAR(1000) NOT NULL," +
                            " `idLand` INT NOT NULL," +
                            " `type` VARCHAR(1000) NOT NULL," +
                            " `canOtherGoIn` BOOLEAN NOT NULL," +
                            " PRIMARY KEY (id)," +
                            " FOREIGN KEY (idServer) REFERENCES oServer(id)," +
                            " FOREIGN KEY (idLand) REFERENCES oLand(id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oPositionPointOfInterest")) {
                    Organisation.log.info("Creating oPositionPointOfInterest table");
                    final String query = "CREATE TABLE `oPositionPointOfInterest` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `idServer` VARCHAR(1000) NOT NULL," +
                            " `type` VARCHAR(1000) NOT NULL," +
                            " `idTerritory` INT NOT NULL," +
                            " `worldName` VARCHAR(1000) NOT NULL," +
                            " `x` INT NOT NULL," +
                            " `y` INT NOT NULL," +
                            " `z` INT NOT NULL," +
                            " `pitch` FLOAT NOT NULL," +
                            " `yaw` FLOAT NOT NULL," +
                            " `OptionalValueOne` VARCHAR(1000)," +
                            " `OptionalValueTwo` VARCHAR(1000)," +
                            " `minPlayerRank` INT NOT NULL," +
                            " `maxPlayerRank` INT NOT NULL," +
                            " `minOrganisationRank` INT NOT NULL," +
                            " `maxOrganisationRank` INT NOT NULL," +
                            " FOREIGN KEY (idServer) REFERENCES oServer(id)," +
                            " PRIMARY KEY (id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oInventory")) {
                    Organisation.log.info("Creating oInventory table");
                    final String query = "CREATE TABLE `oInventory` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `stringInv` TEXT(1000000) NOT NULL," +
                            " `ratio` DOUBLE NOT NULL," +
                            " `maxNumber` INT NOT NULL," +
                            " `type` VARCHAR(1000) NOT NULL," +
                            " PRIMARY KEY (id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oRelation")) {
                    Organisation.log.info("Creating oRelation table");
                    final String query = "CREATE TABLE `oRelation` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `startRelationTime` BIGINT NOT NULL," +
                            " `endRelationTime` BIGINT NOT NULL," +
                            " `rolePlay` VARCHAR(10000) NOT NULL," +
                            " `phase` VARCHAR(1000) NOT NULL," +
                            " `initiator` INT NOT NULL," +
                            " `target` INT NOT NULL," +
                            " `relationEnder` VARCHAR(36)," +
                            " FOREIGN KEY (relationEnder) REFERENCES oPlayer(uuid)," +
                            " FOREIGN KEY (initiator) REFERENCES oGroup(id)," +
                            " FOREIGN KEY (target) REFERENCES oGroup(id)," +
                            " PRIMARY KEY (id));";

                    sql.modifyQuery(connection, query, false);
                }
/*
CREATE TABLE `oRelation` ( `id` INT NOT NULL AUTO_INCREMENT, `startRelationTime` BIGINT NOT NULL, `endRelationTime` BIGINT NOT NULL, `rolePlay` VARCHAR(10000) NOT NULL, `phase` VARCHAR(1000) NOT NULL, `initiator` INT NOT NULL, `target` INT NOT NULL, `relationEnder` VARCHAR(36), PRIMARY KEY (id), FOREIGN KEY (relationEnder) REFERENCES oPlayer(uuid), FOREIGN KEY (initiator,target) REFERENCES oGroup(id,id));
 */
/*
drop table oRelation;
drop table oInventory;
drop table oPositionPointOfInterest;
drop table oLandSubTerritory;
drop table oLand;
drop table oProvince;
drop table oInvitations;
drop table oGroupMember;
drop table oRolePermission;
drop table oRole;
drop table oObjectiveProgression;
drop table oObjective;
drop table oGroup;
drop table oRank;
drop table oPlayer;
drop table oServer;
drop table oInventory;
 */
                // drop table oInventory; drop table oPositionPointOfInterest;drop table oLandSubTerritory; drop table oLand;drop table oProvince;drop table oInvitations;drop table oGroupMember;drop table oRolePermission;drop table oRole;drop table oGroup;drop table oPlayer;
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }

        }
        else {

            dataConfig = new HikariConfig();
            dataConfig.setPoolName("ProjectKorraSQLitePool");
            dataConfig.setDriverClassName("org.sqlite.JDBC");
            dataConfig.setJdbcUrl("jdbc:sqlite:" + Organisation.plugin.getDataFolder().getAbsolutePath() + "/organisation.db");
            dataConfig.setMaxLifetime(30000);
            dataConfig.setMaximumPoolSize(50);
            dataConfig.setAutoCommit(true);
            dataSource = new HikariDataSource(dataConfig);
            sql = new Database("sqlite", Organisation.log, "Establishing SQLite Connection.", "[SQLite] ", dataSource);

            try (Connection connection = sql.getConnection()) {

                if (!sql.tableExists(connection, "oServer")) {
                    Organisation.log.info("Creating organisation oServer table");
                    final String query = "CREATE TABLE `oServer` (" +
                            " `id` VARCHAR(36) NOT NULL," +
                            " `lastDayRunningTask` BIGINT NOT NULL," +
                            " `isSpawnServer` BOOLEAN NOT NULL," +
                            " PRIMARY KEY (id));";
                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oPlayer")) {
                    Organisation.log.info("Creating organisation oPlayer table");
                    final String query = "CREATE TABLE `oPlayer` (" +
                            " `uuid` VARCHAR(36) NOT NULL," +
                            " `firstTimeConnexion` BIGINT NOT NULL," +
                            " `effectivePlayedTime` BIGINT NOT NULL," +
                            " `rank` INT NOT NULL," +
                            " `lastConnexionTime` BIGINT NOT NULL," +
                            " `lastIdServerConnexion` VARCHAR(36) NOT NULL," +
                            " `lastSeenName` VARCHAR(1000) NOT NULL," +
                            " `nickName` VARCHAR(1000) NOT NULL," +
                            " `isConnected` BOOLEAN NOT NULL," +
                            " `isInPrespawnMod` BOOLEAN NOT NULL," +
                            " `lastTimeFightMod` BIGINT NOT NULL," +
                            " `lastPlayerHeadSeen` VARCHAR(10000) NOT NULL," +
                            " `nbMobsKill` INT NOT NULL," +
                            " `nbDeath` INT NOT NULL," +
                            " `nbPlayerKill` INT NOT NULL," +
                            " `chatLocked` VARCHAR(1000) NOT NULL," +
                            " `lastStrElementSeen` VARCHAR(1000) NOT NULL," +
                            " PRIMARY KEY (uuid)," +
                            " PRIMARY KEY (uuid)," +
                            " FOREIGN KEY (lastIdServerConnexion) REFERENCES oServer(id));";
                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oGroup")) {
                    Organisation.log.info("Creating oGroup table");
                    final String query = "CREATE TABLE `oGroup` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `type` VARCHAR(1000) NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `description` VARCHAR(1000)," +
                            " `connexionMessage` VARCHAR(1000)," +
                            " `maxPlayer` INT NOT NULL," +
                            " `minPlayer` INT NOT NULL," +
                            " `maxRaid` INT NOT NULL," +
                            " `maxLand` INT NOT NULL," +
                            " `flagItem` VARCHAR(10000) NOT NULL," +
                            " `rank` INT NOT NULL," +
                            " `isVisible` BOOLEAN NOT NULL," +
                            " `canUnClaim` BOOLEAN NOT NULL," +
                            " `dailyRaid` INT NOT NULL," +
                            " `nextimeAllowedWar` BIGINT NOT NULL," +
                            " `lord` BIGINT NOT NULL," +
                            " PRIMARY KEY (id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oRole")) {
                    Organisation.log.info("Creating oRole table");
                    final String query = "CREATE TABLE `oRole` ( `idGroup` INT NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `rolePower` INT," +
                            " PRIMARY KEY (idGroup, name)," +
                            " CONSTRAINT UC_Role UNIQUE (idGroup, name)," +
                            " FOREIGN KEY (idGroup) REFERENCES oGroup(id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oRolePermission")) {
                    Organisation.log.info("Creating oRolePermission table");
                    final String query = "CREATE TABLE `oRolePermission` ( `idGroup` INT NOT NULL," +
                            "`roleName` VARCHAR(1000) NOT NULL," +
                            "`permission` VARCHAR(1000) NOT NULL," +
                            " PRIMARY KEY (idGroup, roleName, permission)," +
                            " FOREIGN KEY (idGroup, roleName) REFERENCES oRole(idGroup, name));";

                    //CREATE TABLE `oRolePermission` ( `idGroup` BIGINT NOT NULL, `roleName` VARCHAR(1000) NOT NULL, `permission` VARCHAR(1000) NOT NULL, PRIMARY KEY (idGroup, roleName, permission), FOREIGN KEY (idGroup, roleName) REFERENCES oRole(idGroup, name));

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oRank")) {
                    Organisation.log.info("Creating oRank table");
                    final String query = "CREATE TABLE `oRank` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `type` VARCHAR(1000) NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `groupType` VARCHAR(1000) NOT NULL," +
                            " `rankOrder` INT NOT NULL," +
                            " `nbMaxLand` INT NOT NULL," +
                            " `nbMaxVassal` INT NOT NULL," +
                            " `nbMaxPlayer` INT NOT NULL," +
                            " `canVassalize` BOOLEAN NOT NULL," +
                            " `nbMinHeart` INT NOT NULL," +
                            " `nbMaxSlotInv` INT NOT NULL," +
                            " `unlockLeftHand` BOOLEAN NOT NULL," +
                            " PRIMARY KEY (id, type));";

                    sql.modifyQuery(connection, query, false);

                    // Create default rank

                    new GroupRank("Tribu",0, "FACTION", 1,0,5, false);
                    new GroupRank("Clan",1,"FACTION",1,0,7, false);
                    new GroupRank("Village",2,"FACTION",1,0,9, false);
                    new GroupRank("Cité",3,"FACTION",2,1,15, true);
                    new GroupRank("Capitale",4,"FACTION",3,2,20, true);
                    new GroupRank("Royaume",5,"FACTION",4,5,30, true);
                    new GroupRank("Empire",6,"FACTION",4,8,45, true);

                    new GroupRank("Guilde",0,"GUILDE",0,0,5, false);

                }
                if (!sql.tableExists(connection, "oObjective")) {
                    Organisation.log.info("Creating oObjective table");
                    final String query = "CREATE TABLE `oObjective` ( `rankId` INT NOT NULL," +
                            " `rankType` VARCHAR(1000) NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `description` VARCHAR(10000) NOT NULL," +
                            " `item` VARCHAR(10000) NOT NULL," +
                            " `objectifValue` INT NOT NULL," +
                            " `objectifType` VARCHAR(1000) NOT NULL," +
                            " PRIMARY KEY (rankId, rankType, name)," +
                            " FOREIGN KEY (rankId, rankType) REFERENCES oRank(id, type));";

                    sql.modifyQuery(connection, query, false);

                    // For each rank add default staff validation objective
                    for(GroupRank groupRank: GroupRank.getLstGroupRank()){
                        groupRank.addObjectif("Validation_staff", "Autorisation de valider le rang", 0, null,Objective.Type.BOOLEAN.toString(), true);
                    }
                }
                if (!sql.tableExists(connection, "oObjectiveProgression")) {
                    Organisation.log.info("Creating oObjectiveProgression table");
                    final String query = "CREATE TABLE `oObjectiveProgression` ( `rankId` INT NOT NULL," +
                            " `groupId` INT NOT NULL," +
                            " `rankType` VARCHAR(1000) NOT NULL," +
                            " `objectiveName` VARCHAR(1000) NOT NULL," +
                            " `currentObjectiveValue` INT NOT NULL," +
                            " PRIMARY KEY (rankId, rankType, objectiveName, groupId)," +
                            " FOREIGN KEY (groupId) REFERENCES oGroup(id), " +
                            " FOREIGN KEY (rankId, rankType, objectiveName) REFERENCES oObjective(rankId, rankType, name));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oGroupMember")) {
                    Organisation.log.info("Creating oGroupMember table");
                    final String query = "CREATE TABLE `oGroupMember` ( `uuid` VARCHAR(36) NOT NULL," +
                            " `idGroup` INT NOT NULL," +
                            "`roleName` VARCHAR(1000) NOT NULL," +
                            " `firstTimeConnexion` BIGINT NOT NULL," +
                            " `effectivePlayedTime` BIGINT NOT NULL," +
                            " PRIMARY KEY (uuid, idGroup)," +
                            " FOREIGN KEY (uuid) REFERENCES oPlayer(uuid)," +
                            " FOREIGN KEY (idGroup, roleName) REFERENCES oRole(idGroup, name));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oInvitations")) {
                    Organisation.log.info("Creating oInvitations table");
                    final String query = "CREATE TABLE `oInvitations` ( `uuidHost` VARCHAR(36) NOT NULL," +
                            " `idGroup` INT NOT NULL," +
                            " `uuidInvite` VARCHAR(36) NOT NULL," +
                            " PRIMARY KEY (uuidHost, idGroup, uuidInvite)," +
                            " FOREIGN KEY (uuidHost) REFERENCES oPlayer(uuid)," +
                            " FOREIGN KEY (uuidInvite) REFERENCES oPlayer(uuid)," +
                            " FOREIGN KEY (idGroup) REFERENCES oGroup(id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oProvince")) {

                    Organisation.log.info("Creating oProvince table");
                    final String query = "CREATE TABLE `oProvince` ( `id` VARCHAR(1000) NOT NULL," +
                            "`name` VARCHAR(1000) NOT NULL," +
                            "`description` VARCHAR(10000) NOT NULL," +
                            " PRIMARY KEY (id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oLand")) {
                    Organisation.log.info("Creating oLand table");
                    final String query = "CREATE TABLE `oLand` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `idServer` VARCHAR(1000) NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `idRegionWorldGuard` VARCHAR(1000) NOT NULL," +
                            " `isMainLand` BOOLEAN NOT NULL," +
                            " `idProvince` VARCHAR(1000)," +
                            " `idGroup` INT," +
                            " `canOtherGoIn` BOOLEAN NOT NULL," +
                            " `isActive` BOOLEAN NOT NULL," +
                            " `ownedTime` BIGINT NOT NULL," +
                            " PRIMARY KEY (id)," +
                            " FOREIGN KEY (idProvince) REFERENCES oProvince(id)," +
                            " FOREIGN KEY (idServer) REFERENCES oServer(id)," +
                            " FOREIGN KEY (idGroup) REFERENCES oGroup(id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oLandSubTerritory")) {
                    Organisation.log.info("Creating oLandSubTerritory table");
                    final String query = "CREATE TABLE `oLandSubTerritory` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `idServer` VARCHAR(1000) NOT NULL," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `idRegionWorldGuard` VARCHAR(1000) NOT NULL," +
                            " `idLand` INT NOT NULL," +
                            " `type` VARCHAR(1000) NOT NULL," +
                            " `canOtherGoIn` BOOLEAN NOT NULL," +
                            " PRIMARY KEY (id)," +
                            "FOREIGN KEY (idServer) REFERENCES oServer(id)," +
                            " FOREIGN KEY (idLand) REFERENCES oLand(id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oPositionPointOfInterest")) {
                    Organisation.log.info("Creating oPositionPointOfInterest table");
                    final String query = "CREATE TABLE `oPositionPointOfInterest` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `idServer` VARCHAR(1000) NOT NULL," +
                            " `type` VARCHAR(1000) NOT NULL," +
                            " `idTerritory` INT NOT NULL," +
                            " `worldName` VARCHAR(1000) NOT NULL," +
                            " `x` INT NOT NULL," +
                            " `y` INT NOT NULL," +
                            " `z` INT NOT NULL," +
                            " `pitch` FLOAT NOT NULL," +
                            " `yaw` FLOAT NOT NULL," +
                            " `OptionalValueOne` VARCHAR(1000)," +
                            " `OptionalValueTwo` VARCHAR(1000)," +
                            " `minPlayerRank` INT NOT NULL," +
                            " `maxPlayerRank` INT NOT NULL," +
                            " `minOrganisationRank` INT NOT NULL," +
                            " `maxOrganisationRank` INT NOT NULL," +
                            " FOREIGN KEY (idServer) REFERENCES oServer(id)," +
                            " PRIMARY KEY (id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oInventory")) {
                    Organisation.log.info("Creating oInventory table");
                    final String query = "CREATE TABLE `oInventory` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `name` VARCHAR(1000) NOT NULL," +
                            " `stringInv` VARCHAR(1000000) NOT NULL," +
                            " `ratio` DOUBLE NOT NULL," +
                            " `maxNumber` INT NOT NULL," +
                            " `type` TEXT(1000) NOT NULL," +
                            " PRIMARY KEY (id));";

                    sql.modifyQuery(connection, query, false);
                }
                if (!sql.tableExists(connection, "oRelation")) {
                    Organisation.log.info("Creating oRelation table");
                    final String query = "CREATE TABLE `oRelation` ( `id` INT NOT NULL AUTO_INCREMENT," +
                            " `startRelationTime` BIGINT NOT NULL," +
                            " `endRelationTime` BIGINT NOT NULL," +
                            " `rolePlay` VARCHAR(10000) NOT NULL," +
                            " `phase` VARCHAR(1000) NOT NULL," +
                            " `initiator` INT NOT NULL," +
                            " `target` INT NOT NULL," +
                            " `relationEnder` VARCHAR(36)," +
                            " FOREIGN KEY (relationEnder) REFERENCES oPlayer(uuid)," +
                            " FOREIGN KEY (initiator) REFERENCES oGroup(id)," +
                            " FOREIGN KEY (target) REFERENCES oGroup(id)," +
                            " PRIMARY KEY (id));";

                    sql.modifyQuery(connection, query, false);
                }

            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
}
