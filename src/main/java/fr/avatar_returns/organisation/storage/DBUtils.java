package fr.avatar_returns.organisation.storage;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.events.Event;
import fr.avatar_returns.organisation.events.Phases;
import fr.avatar_returns.organisation.events.Rewards;
import fr.avatar_returns.organisation.exception.InvitationGroupException;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.hook.AvatarRoadsHook;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.largeGroup.Faction;
import fr.avatar_returns.organisation.rabbitMQPackets.*;
import fr.avatar_returns.organisation.rabbitMQPackets.group.*;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.GroupObjectivePacket;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.GroupRankPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.ObjectivePacket;
import fr.avatar_returns.organisation.rabbitMQPackets.rank.PlayerRankPacket;
import fr.avatar_returns.organisation.rank.*;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.relation.Relation;
import fr.avatar_returns.organisation.territory.*;
import fr.avatar_returns.organisation.territory.landSubTerritory.LandSubTerritory;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Contest;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.RP;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Totem;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.ItemUtils;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import fr.avatar_returns.organisation.storedInventory.StoredInventory;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.UUID;


public abstract class DBUtils {

    private static boolean registerServer(){

        final String query = "INSERT IGNORE INTO oServer (id, lastDayRunningTask, isSpawnServer) " +
                "VALUES (?,?,?);";

        int milisToDay = 1000*60*60*24;
        long lastDayRunningTask = (System.currentTimeMillis()/milisToDay) * milisToDay;

        Organisation.lastDayRunningTask = lastDayRunningTask;
        Organisation.isSpawnServer = false;

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {


                queryStatement.setString(1, ConfigManager.getServerId());
                queryStatement.setLong(2, lastDayRunningTask);
                queryStatement.setBoolean(3, false);
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new server in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setIsSpawnServer(){

        final String queryRemovePreviousSpawnServer = "UPDATE oServer " +
                "SET isSpawnServer = ? " +
                "WHERE id!=?;";
        final String querySetThisServerAsSpawn = "UPDATE oServer " +
                "SET isSpawnServer = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {

            try (PreparedStatement queryStatement = connection.prepareStatement(queryRemovePreviousSpawnServer)) {

                queryStatement.setString(1, ConfigManager.getServerId());
                queryStatement.setBoolean(2, false);

                queryStatement.execute();
            }
            try (PreparedStatement queryStatement = connection.prepareStatement(querySetThisServerAsSpawn)) {

                queryStatement.setString(1, ConfigManager.getServerId());
                queryStatement.setBoolean(2, false);

                queryStatement.execute();
            }

        } catch (SQLException e) {
            Organisation.log.severe("Unable to change server '" +  ConfigManager.getServerId() + "' isSpawnServer to 'true' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean updateDailyRunningTask(){
        final String query = "UPDATE oServer " +
                "SET lastDayRunningTask = ? " +
                "WHERE id=?;";

        int milisToDay = 1000*60*60*24;

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, (System.currentTimeMillis()/milisToDay) * milisToDay);
                queryStatement.setString(2, ConfigManager.getServerId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change server '" +  ConfigManager.getServerId() + "' lastRunningDay to '" + ((System.currentTimeMillis()/milisToDay) * milisToDay) + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreServer(){

        final String query = "SELECT * FROM oServer;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                int nbResult = 0;
                while (rs.next()) {

                    final String id = rs.getString("id");
                    if(id != ConfigManager.getServerId())continue;

                    nbResult ++;
                    final long lastDayRunningTask = rs.getLong("firstTimeConnexion");
                    final boolean isSpawnServer = rs.getBoolean("effectivePlayedTime");

                    Organisation.lastDayRunningTask = lastDayRunningTask;
                    Organisation.isSpawnServer = isSpawnServer;
                    break;
                }

                if(nbResult == 0);DBUtils.registerServer();
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean addOrganisationPlayer(OrganisationPlayer organisationPlayer) {

        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.CREATE));

        final String query = "INSERT INTO oPlayer (uuid, " +
                "firstTimeConnexion, " +
                "effectivePlayedTime," +
                " rank, " +
                "lastConnexionTime," +
                " lastIdServerConnexion, " +
                "lastSeenName, " +
                "isConnected, " +
                "isInPrespawnMod, " +
                "lastTimeFightMod," +
                "lastPlayerHeadSeen," +
                "nbMobsKill," +
                "nbDeath," +
                "nbPlayerKill, " +
                "lastStrElementSeen, " +
                "chatLocked," +
                "nickName) " +

                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {


                queryStatement.setString(1, organisationPlayer.getUuid().toString());
                queryStatement.setLong(2, organisationPlayer.getFirstTimeConnexion());
                queryStatement.setLong(3, organisationPlayer.getLastEffectivePlayedTime());
                queryStatement.setInt(4, organisationPlayer.getRank());
                queryStatement.setLong(5, System.currentTimeMillis());
                queryStatement.setString(6, ConfigManager.getServerId());
                queryStatement.setString(7, organisationPlayer.getLastSeenName());
                queryStatement.setBoolean(8, organisationPlayer.isConnected());
                queryStatement.setBoolean(9, organisationPlayer.isInPrespawnMod());
                queryStatement.setLong(10, organisationPlayer.getLastTimeFightMod());
                queryStatement.setString(11, organisationPlayer.getLastHeadSeen());
                queryStatement.setInt(12, organisationPlayer.getMobsKill());
                queryStatement.setInt(13, organisationPlayer.getNbDeath());
                queryStatement.setInt(14, organisationPlayer.getPlayerKill());
                queryStatement.setString(15, organisationPlayer.getLastServerSeenName());
                queryStatement.setString(16, organisationPlayer.getChatLocked());
                queryStatement.setString(17, organisationPlayer.getNickName());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new organisationPlayer in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean newOrganisationPlayerConnexion(OrganisationPlayer organisationPlayer) {

        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET lastConnexionTime = ?, lastIdServerConnexion = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, System.currentTimeMillis());
                queryStatement.setString(2, ConfigManager.getServerId());
                queryStatement.setString(3, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' last connexion time in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setOrganisationPlayerEffectivePlayedTime(OrganisationPlayer organisationPlayer) {

        final String query = "UPDATE oPlayer " +
                "SET effectivePlayedTime = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, organisationPlayer.getEffectivePlayedTime());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' effective played time in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setOrganisationPlayerLastSeenName(OrganisationPlayer organisationPlayer) {

        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET lastSeenName = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, organisationPlayer.getLastSeenName());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' lastSeenName in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setOrganisationPlayerNickName(OrganisationPlayer organisationPlayer) {

        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET nickName = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, organisationPlayer.getNickName());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' lastSeenName in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setOrganisationPlayerIsConnected(OrganisationPlayer organisationPlayer) {

        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET isConnected = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setBoolean(1, organisationPlayer.isConnected());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' isConnected in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setOrganisationPlayerRank(OrganisationPlayer organisationPlayer) {

        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET rank = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, organisationPlayer.getRank());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' effective rank to '"+organisationPlayer.getRank()+"' in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setOrganisationIsInPrespawnMod(OrganisationPlayer organisationPlayer) {

        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET isInPrespawnMod = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setBoolean(1, organisationPlayer.isInPrespawnMod());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' effective isInPrespawnMod to '"+organisationPlayer.isInPrespawnMod()+"' in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setOrganisationLastTimeFightMod(OrganisationPlayer organisationPlayer) {
        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET lastTimeFightMod = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, organisationPlayer.getLastTimeFightMod());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' effective lastTimeFightMod to '"+organisationPlayer.getLastTimeFightMod()+"' in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setOrganisationLastPlayerHeadSeen(OrganisationPlayer organisationPlayer) {

        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET lastPlayerHeadSeen = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, organisationPlayer.getLastHeadSeen());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' lastHeadSeen in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setOrganisationPlayerLastStrElementSeen(OrganisationPlayer organisationPlayer) {

        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET lastStrElementSeen = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                //queryStatement.setString(1, organisationPlayer.getLastStrElementSeen());
                queryStatement.setString(1, "");
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' lastStrElementSeen in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setOrganisationPlayerChatLocked(OrganisationPlayer organisationPlayer) {

        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET chatLocked = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, organisationPlayer.getChatLocked());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' chat locked in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setOrganisationPlayerMobsKill(OrganisationPlayer organisationPlayer) {
        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET nbMobsKill = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, organisationPlayer.getMobsKill());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' lastHeadSeen in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setOrganisationPlayerNbDeath(OrganisationPlayer organisationPlayer) {
        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET nbDeath = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, organisationPlayer.getNbDeath());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' lastHeadSeen in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setOrganisationPlayerKill(OrganisationPlayer organisationPlayer) {
        GeneralMethods.sendPacket(new OrganisationPlayerPacket(organisationPlayer, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oPlayer " +
                "SET nbPlayerKill = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, organisationPlayer.getPlayerKill());
                queryStatement.setString(2, organisationPlayer.getUuid().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update organisationPlayer '"+organisationPlayer.getOfflinePlayer().getName()+"' lastHeadSeen in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreOrganisationPlayer() {

        final String query = "SELECT * FROM oPlayer;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                while (rs.next()) {

                    final String uuid = rs.getString("uuid");
                    final long firstTimeConnexion = rs.getLong("firstTimeConnexion");
                    final long effectivePlayedTime = rs.getLong("effectivePlayedTime");
                    final int rank = rs.getInt("rank");
                    final String lastSeenName = rs.getString("lastSeenName");
                    final boolean isConnected = rs.getBoolean("isConnected");
                    final boolean isInPrespawnMod = rs.getBoolean("isInPrespawnMod");
                    final long lastTimeFightMod = rs.getLong("lastTimeFightMod");
                    final String lastServerSeenName = rs.getString("lastIdServerConnexion");
                    final String lastPlayerHeadSeen = rs.getString("lastPlayerHeadSeen");
                    final int nbMobsKill = rs.getInt("nbMobsKill");
                    final int nbDeath = rs.getInt("nbDeath");
                    final int nbPlayerKill = rs.getInt("nbPlayerKill");
                    final String lastStrElementSeen = rs.getString("lastStrElementSeen");
                    final String chatLocked = rs.getString("chatLocked");
                    final String nickName = rs.getString("nickName");


                    new OrganisationPlayer(
                            uuid,
                            firstTimeConnexion,
                            effectivePlayedTime,
                            rank,
                            lastSeenName,
                            isConnected,
                            isInPrespawnMod,
                            lastTimeFightMod,
                            lastServerSeenName,
                            lastPlayerHeadSeen,
                            nbMobsKill,
                            nbPlayerKill,
                            nbDeath,
                            lastStrElementSeen,
                            chatLocked,
                            (nickName == null)?nickName : "");
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static int addGroup(Group group) {

        final String query = "INSERT INTO oGroup (type, " +
                "name, description, " +
                "connexionMessage," +
                " maxPlayer, minPlayer, maxLand, maxRaid, rank, flagItem, isVisible, nextimeAllowedWar, dailyRaid, lord, canUnClaim) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        int id = 0;
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

                queryStatement.setString(1, group.getClass().toString());
                queryStatement.setString(2, group.getName());
                queryStatement.setString(3, group.getDescription());
                queryStatement.setString(4, group.getConnexionMessage());
                queryStatement.setInt(5, group.getMaxPlayer());
                queryStatement.setInt(6, group.getMinPlayer());
                queryStatement.setInt(7, (group instanceof LargeGroup)?((LargeGroup) group).getMaxLand() : 0);
                queryStatement.setInt(8, (group instanceof LargeGroup)?((LargeGroup) group).getMaxRaid() : 0);
                queryStatement.setInt(9, group.getRank().getId());
                queryStatement.setString(10, "");
                queryStatement.setBoolean(11,group.isVisible());
                queryStatement.setInt(12,0);
                queryStatement.setInt(13,0);
                queryStatement.setInt(14,-1);
                queryStatement.setBoolean(15, group.canUnClaim());

                queryStatement.execute();
                ResultSet generatedKeys = queryStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt("insert_id");
                }
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new group '" + group.getName() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return -1;
        }
        return id;
    }
    public static boolean setGroupName(Group group, String name) {
        GeneralMethods.sendPacket((group instanceof Faction)?new FactionPacket((Faction) group, OrganisationPacket.Action.UPDATE) : new GuildePacket((Guilde) group, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET name = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, name);
                queryStatement.setInt(2, group.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + group.getName() + "' name to '" + name + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setGroupDesc(Group group) {
        GeneralMethods.sendPacket((group instanceof Faction)?new FactionPacket((Faction) group, OrganisationPacket.Action.UPDATE) : new GuildePacket((Guilde) group, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET description = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, group.getDescription());
                queryStatement.setInt(2, group.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + group.getName() + "' description to '" + group.getDescription() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setConnexionMessage(Group group) {
        GeneralMethods.sendPacket((group instanceof Faction)?new FactionPacket((Faction) group, OrganisationPacket.Action.UPDATE) : new GuildePacket((Guilde) group, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET connexionMessage = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, group.getConnexionMessage());
                queryStatement.setInt(2, group.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + group.getName() + "' connexion message to '" + group.getConnexionMessage() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setGroupMinPlayer(Group group) {
        GeneralMethods.sendPacket((group instanceof Faction)?new FactionPacket((Faction) group, OrganisationPacket.Action.UPDATE) : new GuildePacket((Guilde) group, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET minPlayer = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, group.getMinPlayer());
                queryStatement.setInt(2, group.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + group.getName() + "' min player to '" + group.getMinPlayer() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setGroupMaxPlayer(Group group) {
        GeneralMethods.sendPacket((group instanceof Faction)?new FactionPacket((Faction) group, OrganisationPacket.Action.UPDATE) : new GuildePacket((Guilde) group, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET maxPlayer = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, group.getMaxPlayer());
                queryStatement.setInt(2, group.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + group.getName() + "' max player to '" + group.getMaxPlayer() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setGroupRank(Group group) {
        GeneralMethods.sendPacket((group instanceof Faction)?new FactionPacket((Faction) group, OrganisationPacket.Action.UPDATE) : new GuildePacket((Guilde) group, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET rank = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, group.getRank().getId());
                queryStatement.setInt(2, group.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + group.getName() + "' rank to '" + group.getRank().getId() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setIsVisible(Group group) {
        GeneralMethods.sendPacket((group instanceof Faction)?new FactionPacket((Faction) group, OrganisationPacket.Action.UPDATE) : new GuildePacket((Guilde) group, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET isVisible = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setBoolean(1, group.isVisible());
                queryStatement.setInt(2, group.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + group.getName() + "' isVisible to '" + group.isVisible() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setGroupCanUnClaim(Group group) {
        GeneralMethods.sendPacket((group instanceof Faction)?new FactionPacket((Faction) group, OrganisationPacket.Action.UPDATE) : new GuildePacket((Guilde) group, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET canUnClaim = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setBoolean(1, group.canUnClaim());
                queryStatement.setInt(2, group.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + group.getName() + "' canUnClaim to '" + group.canUnClaim() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setFlagItem(Group group) {
        GeneralMethods.sendPacket((group instanceof Faction)?new FactionPacket((Faction) group, OrganisationPacket.Action.UPDATE) : new GuildePacket((Guilde) group, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET flagItem = ? " +
                "WHERE id=?;";

        ItemStack[] lstFlag = {group.getFlag()};
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, ItemUtils.itemStackArrayToBase64(lstFlag));
                queryStatement.setInt(2, group.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + group.getName() + " flag in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setLargeGroupMaxRaid(LargeGroup largeGroup) {
        GeneralMethods.sendPacket(new FactionPacket((Faction) largeGroup, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET maxRaid = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, largeGroup.getMaxRaid());
                queryStatement.setInt(2, largeGroup.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + largeGroup.getName() + " maxRaid in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setLargeGroupMaxLand(LargeGroup largeGroup) {
        GeneralMethods.sendPacket(new FactionPacket((Faction) largeGroup, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET maxLand = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, largeGroup.getMaxLand());
                queryStatement.setInt(2, largeGroup.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + largeGroup.getName() + " maxLand in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setNextimeAllowedWar(LargeGroup largeGroup) {
        GeneralMethods.sendPacket(new FactionPacket((Faction) largeGroup, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET nextimeAllowedWar = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, largeGroup.getNextimeAllowedWar());
                queryStatement.setInt(2, largeGroup.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + largeGroup.getName() + " NextimeAllowedWar in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setDailyRaid(LargeGroup largeGroup) {
        GeneralMethods.sendPacket(new FactionPacket((Faction) largeGroup, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET dailyRaid = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, largeGroup.getDailyRaid());
                queryStatement.setInt(2, largeGroup.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + largeGroup.getName() + " dailyRaid in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setGroupLord(LargeGroup largeGroup) {
        GeneralMethods.sendPacket(new FactionPacket((Faction) largeGroup, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroup " +
                "SET lord = ? " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, (largeGroup.getLord()!=null)?largeGroup.getLord().getId():-1);
                queryStatement.setInt(2, largeGroup.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change groupe '" + largeGroup.getName() + " lord in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean deleteGroup(Group group) {

        // Supression of inviration
        ArrayList<Invitation> lstInvitationToRemove = new ArrayList<>();
        for(Invitation inv : InviteCommand.lstInvitation){
            if(inv.getGroupHost().getId() == group.getId())lstInvitationToRemove.add(inv);
        }

        for(Invitation inv : lstInvitationToRemove){
            InviteCommand.lstInvitation.remove(inv);
            DBUtils.deleteInvitation(inv);
        }

        // Supression of Land
        for(Land land : Land.getLstLandByGroup(group)){
            land.setOwner(null);
        }

        GeneralMethods.sendPacket((group instanceof Faction)?new FactionPacket((Faction) group, OrganisationPacket.Action.REMOVE) : new GuildePacket((Guilde) group, OrganisationPacket.Action.REMOVE));
        final String queryRemoveGroupLand = "UPDATE oLand " +
                "SET idGroup = ? " +
                "WHERE idGroup=?;";
        final String queryDeleteAllGroupMember = "DELETE FROM oGroupMember " +
                "WHERE idGroup=?;";
        final String queryDeleteAllGroupRolePermission = "DELETE FROM oRolePermission " +
                "WHERE idGroup=?;";
        final String queryAllGroupRole = "DELETE FROM oRole " +
                "WHERE idGroup=?;";
        final String queryAllObjectiveProgression = "DELETE FROM oObjectiveProgression " +
                "WHERE groupId=?;";

        final String queryDeleteRole = "DELETE FROM oGroup " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            /*
            try (PreparedStatement queryStatement = connection.prepareStatement(queryRemoveGroupLand)) {
                queryStatement.setNull(1, Types.INTEGER);
                queryStatement.setInt(2, group.getId());
                queryStatement.execute();
            }
            //*/
            try (PreparedStatement queryStatement = connection.prepareStatement(queryAllObjectiveProgression)) {
                queryStatement.setInt(1, group.getId());
                queryStatement.execute();
            }

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteAllGroupMember)) {
                queryStatement.setInt(1, group.getId());
                queryStatement.execute();
            }

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteAllGroupRolePermission)) {
                queryStatement.setInt(1, group.getId());
                queryStatement.execute();
            }

            try (PreparedStatement queryStatement = connection.prepareStatement(queryAllGroupRole)) {
                queryStatement.setInt(1, group.getId());
                queryStatement.execute();
            }

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteRole)) {
                queryStatement.setInt(1, group.getId());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete group " + group.getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreGroup() {

        final String query = "SELECT * FROM oGroup;";
        final String queryLord = "SELECT id, lord FROM oGroup WHERE lord != -1";
        try (Connection connection = DBConnection.sql.getConnection()) {

            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                while (rs.next()) {

                    final int id = rs.getInt("id");
                    final String type = rs.getString("type");
                    final String name = rs.getString("name");
                    final String description = rs.getString("description");
                    final String connexionMessage = rs.getString("connexionMessage");
                    final int maxPlayer = rs.getInt("maxPlayer");
                    final int minPlayer = rs.getInt("minPlayer");
                    final int maxLand = rs.getInt("maxLand");
                    final int maxRaid = rs.getInt("maxRaid");
                    final int rankId = rs.getInt("rank");
                    final String flagItemb64 = rs.getString("flagItem");
                    final boolean isVisible = rs.getBoolean("isVisible");
                    final boolean canUnClaim = rs.getBoolean("canUnClaim");
                    final int dailyRaid = rs.getInt("dailyRaid");
                    final long nextimeAllowedWar = rs.getLong("nextimeAllowedWar");

                    ItemStack flagItem = null;
                    try {
                        ItemStack[] lstItem = ItemUtils.itemStackArrayFromBase64(flagItemb64);
                        flagItem = (lstItem.length < 1) ? null : lstItem[0];

                    }catch (IOException e){}

                    GroupRank groupRank = GroupRank.getById(rankId);
                    Group group = null;

                    if (type.equals("class fr.avatar_returns.organisation.organisations.simpleGroup.Guilde")) {
                        if(groupRank == null) groupRank = Guilde.getDefaultGroupRank();
                        group = new Guilde(id, name, maxPlayer, minPlayer, description, connexionMessage, groupRank, flagItem, isVisible, canUnClaim);
                    } else {
                        if (groupRank == null) groupRank = LargeGroup.getDefaultGroupRank();
                        if (type.equals("class fr.avatar_returns.organisation.organisations.largeGroup.Faction")) {
                            group = new Faction(id, name, maxPlayer, minPlayer, maxLand, maxRaid, description, connexionMessage, groupRank, flagItem, isVisible, dailyRaid, nextimeAllowedWar, canUnClaim);
                        }
                        else{
                            Organisation.log.severe("Unable to restore " + name +" with unsuported type : '" + type+"'");
                        }
                    }
                    if (group != null) {
                        group.setDescription(description, false);
                        group.setLstGroupObjective(DBUtils.getGroupRankObjective(group));
                    }
                }
            }
            try (ResultSet rs = DBConnection.sql.readQuery(connection, queryLord)) {
                while (rs.next()) {

                    final int id = rs.getInt("id");
                    final int lordId = rs.getInt("lord");

                    Group largeGroup = LargeGroup.getGroupById(id);
                    Group lordGroup = LargeGroup.getGroupById(lordId);

                    if(largeGroup == null || lordGroup == null)continue;
                    if(!(largeGroup instanceof LargeGroup && lordGroup instanceof LargeGroup))continue;

                    ((LargeGroup) largeGroup).setLord((LargeGroup) lordGroup, false);
                }
            }

        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean addGroupMember(Group group, OrganisationMember organisationMember) {
        GeneralMethods.sendPacket(new GroupMemberPacket(organisationMember, group, OrganisationPacket.Action.CREATE));
        if (organisationMember.getRole() == null)
            Organisation.log.severe("ROLE NULL...");
        final String query = "INSERT INTO oGroupMember (uuid, idGroup, roleName, firstTimeConnexion, effectivePlayedTime)" +
                "VALUES (?,?,?,?, ?);";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, organisationMember.getOfflinePlayer().getUniqueId().toString());
                queryStatement.setInt(2, group.getId());
                queryStatement.setString(3, organisationMember.getRole().getName().replace("§","&"));
                queryStatement.setLong(4, organisationMember.getFirstTimeConnexion());
                queryStatement.setLong(5, organisationMember.getLastEffectivePlayedTime());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe(" Unable to add member to groupe '" + group.getName() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean changeRoleGroupMember(Role role, OrganisationMember organisationMember) {
        GeneralMethods.sendPacket(new GroupMemberPacket(organisationMember, organisationMember.getGroup(), OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oGroupMember " +
                "SET roleName = ? " +
                "WHERE uuid=? AND idGroup=?; ";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, role.getName().replace("§","&"));
                queryStatement.setString(2, organisationMember.getOfflinePlayer().getUniqueId().toString());
                queryStatement.setInt(3, organisationMember.getGroup().getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change role to'" + role.getName() + "' for '" + organisationMember.getOfflinePlayer().getName() + "' in group " + role.getGroup().getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setGroupMemberEffectivePlayedTime(OrganisationMember organisationMember) {

        final String query = "UPDATE oGroupMember " +
                "SET effectivePlayedTime = ? " +
                "WHERE uuid=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, organisationMember.getEffectivePlayedTime());
                queryStatement.setString(2, organisationMember.getOrganisationPlayer().getUuid().toString());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update groupMmeber '"+organisationMember.getOfflinePlayer().getName()+"' in '"+organisationMember.getGroup().getName()+"' effective played time in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean deleteGroupMember(Group group, OrganisationMember organisationMember) {

        GeneralMethods.sendPacket(new GroupMemberPacket(organisationMember, group, OrganisationPacket.Action.REMOVE));
        final String queryDeleteGroupMemberInvitation = "DELETE FROM oInvitations " +
                "WHERE uuidHost=? AND idGroup=?;";
        final String queryDeleteGroupMember = "DELETE FROM oGroupMember " +
                "WHERE uuid=? AND idGroup=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteGroupMemberInvitation)) {
                queryStatement.setString(1, organisationMember.getOfflinePlayer().getUniqueId().toString());
                queryStatement.setInt(2, group.getId());
                queryStatement.execute();
            }

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteGroupMember)) {
                queryStatement.setString(1, organisationMember.getOfflinePlayer().getUniqueId().toString());
                queryStatement.setInt(2, group.getId());
                queryStatement.execute();
            }

        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete organisationMember from group " + group.getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreMember() {

        final String query = "SELECT * FROM oGroupMember;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                while (rs.next()) {
                    final UUID uuid = UUID.fromString(rs.getString("uuid"));
                    final int idGroup = rs.getInt("idGroup");
                    final String roleName = rs.getString("roleName").replace("&","§");
                    final long firstTimeConnexion = rs.getLong("firstTimeConnexion");
                    final long effectivePlayedTime = rs.getLong("effectivePlayedTime");

                    Group group = Group.getGroupById(idGroup);
                    if (group == null) return false;

                    OfflinePlayer offlinePlayer = Organisation.plugin.getServer().getOfflinePlayer(uuid);

                    Role role = null;
                    for (Role tmpRole : group.getLstRole()) {
                        if (tmpRole.getName().equals(roleName)) {
                            role = tmpRole;
                            break;
                        }
                    }
                    if (role == null) return false;

                    OrganisationPlayer organisationPlayer = OrganisationPlayer.getOrganisationPlayer(offlinePlayer);
                    OrganisationMember organisationMember = new OrganisationMember(group, organisationPlayer, role, firstTimeConnexion, effectivePlayedTime);

                    group.getLstMember().add(organisationMember);
                    //group.getTeam().addPlayer(offlinePlayer); //TODO Check for player who nether connected

                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        } catch (OrganisationPlayerException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean addRole(Role role) {

        GeneralMethods.sendPacket(new RolePacket(role, OrganisationPacket.Action.CREATE, role.getName()));

        String name = role.getName().replace("§","&");
        final String query = "INSERT INTO oRole (idGroup, name, rolePower) " +
                "VALUES (?,?,?);";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, role.getGroup().getId());
                queryStatement.setString(2, name);
                queryStatement.setInt(3, role.getRolePower());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new role '" + name + "' for " + role.getGroup().getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean renameRole(Role role, String newName) {

        GeneralMethods.sendPacket(new RolePacket(role, OrganisationPacket.Action.UPDATE, newName));
        newName = newName.replace("§","&");
        final String renameRoleQuery = "UPDATE oRole " +
                "SET name = ? " +
                "WHERE idGroup=? AND name=?; ";

        try (Connection connection = DBConnection.sql.getConnection()) {

            try (PreparedStatement queryStatement = connection.prepareStatement(renameRoleQuery)) {
                queryStatement.setString(1, newName);
                queryStatement.setInt(2, role.getGroup().getId());
                queryStatement.setString(3, role.getName().replace("§","&"));
                queryStatement.execute();

            }

        } catch (SQLException e) {
            Organisation.log.severe("Unable to rename role from'" + role.getName() + "' to '" + newName + "' in group " + role.getGroup().getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }

        return true;
    }
    public static boolean setPowerRole(Role role, int newPower) {

        GeneralMethods.sendPacket(new RolePacket(role, OrganisationPacket.Action.UPDATE, role.getName()));
        final String query = "UPDATE oRole " +
                "SET rolePower=? " +
                "WHERE idGroup=? AND name=?; ";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, newPower);
                queryStatement.setInt(2, role.getGroup().getId());
                queryStatement.setString(3, role.getName());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to change power of role '" + role.getName() + "' to '" + newPower + "' in group " + role.getGroup().getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean deleteRole(Role role) {

        GeneralMethods.sendPacket(new RolePacket(role, OrganisationPacket.Action.REMOVE, role.getName()));
        final String queryDeleteAllRolePermission = "DELETE FROM oRolePermission " +
                "WHERE idGroup=? AND roleName=?";
        final String queryDeleteRole = "DELETE FROM oRole " +
                "WHERE idGroup=? AND name=?";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteAllRolePermission)) {
                queryStatement.setInt(1, role.getGroup().getId());
                queryStatement.setString(2, role.getName());
                queryStatement.execute();
            }

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteRole)) {
                queryStatement.setInt(1, role.getGroup().getId());
                queryStatement.setString(2, role.getName());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete role from'" + role.getName() + "' in group " + role.getGroup().getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreRole() {

        final String query = "SELECT * FROM oRole;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {

                while (rs.next()) {
                    final int idGroup = rs.getInt("idGroup");
                    final String name = rs.getString("name").replace("&", "§");
                    final int rolePower = rs.getInt("rolePower");

                    Group group = Group.getGroupById(idGroup);
                    if(group == null)continue;
                    group.getLstRole().add(new Role(name, rolePower, group, true));
                }

                for(Group group : Group.getLstGroup()){
                    if(group.getLstRole().isEmpty())group.initDefaultRole();
                }

            }


        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public static boolean addRolePermission(Role role, String permission) {

        GeneralMethods.sendPacket(new PermissionPacket(role, permission, OrganisationPacket.Action.CREATE));
        final String query = "INSERT INTO oRolePermission (idGroup, roleName, permission)\n" +
                "VALUES (?,?,?)";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, role.getGroup().getId());
                queryStatement.setString(2, role.getName().replace("§","&"));
                queryStatement.setString(3, permission);

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to add new permission '" + permission + "' for role '" + role.getName() + "' in group " + role.getGroup().getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean deleteRolePermission(Role role, String permission) {
        GeneralMethods.sendPacket(new PermissionPacket(role, permission, OrganisationPacket.Action.REMOVE));
        final String query = "DELETE FROM oRolePermission " +
                "WHERE idGroup=? AND roleName=? AND permission=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, role.getGroup().getId());
                queryStatement.setString(2, role.getName());
                queryStatement.setString(3, permission);

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to remove new permission '" + permission + "' for role '" + role.getName() + "' in group " + role.getGroup().getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restorePermissions() {

        final String query = "SELECT * FROM oRolePermission;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                while (rs.next()) {
                    final int idGroup = rs.getInt("idGroup");
                    final String roleName = rs.getString("roleName").replace("&","§");
                    final String permission = rs.getString("permission");

                    Group group = Group.getGroupById(idGroup);
                    if (group != null) {
                        Role role = null;
                        for (Role existingRole : group.getLstRole()) {
                            if (existingRole.getName().equals(roleName)) {
                                role = existingRole;
                                break;
                            }
                        }
                        if (role != null) {
                            role.getLstPermission().add(permission);
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean addInvitation(Invitation inv) {

        GeneralMethods.sendPacket(new InvitationPacket(inv, OrganisationPacket.Action.CREATE));
        final String query = "INSERT INTO oInvitations (uuidHost, idGroup, uuidInvite)\n" +
                "VALUES (?,?,?)";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, inv.getOPlayerHost().getUuid().toString());
                queryStatement.setInt(2, inv.getGroupHost().getId());
                queryStatement.setString(3, inv.getOPlayerInvite().getUuid().toString());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to add new invitation in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean deleteInvitation(Invitation inv) {

        GeneralMethods.sendPacket(new InvitationPacket(inv, OrganisationPacket.Action.REMOVE));
        final String query = "DELETE FROM oInvitations " +
                "WHERE uuidHost=? AND idGroup=? AND uuidInvite=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, inv.getOPlayerHost().getUuid().toString());
                queryStatement.setInt(2, inv.getGroupHost().getId());
                queryStatement.setString(3, inv.getOPlayerInvite().getUuid().toString());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to remove invitation  in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreInvitation() {

        final String query = "SELECT * FROM oInvitations;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                while (rs.next()) {

                    final String uuidHost = rs.getString("uuidHost");
                    final int idGroup = rs.getInt("idGroup");
                    final String uuidInvite = rs.getString("uuidInvite");

                    Group group = Group.getGroupById(idGroup);
                    if (group == null) {
                        Organisation.log.severe("Unable to restore oInvitations from '" + uuidHost + "' to '" + uuidInvite + "' because '" + idGroup + "' is not coresponding to any group in this server");
                        continue;
                    }

                    OfflinePlayer playerHost = GeneralMethods.getOfflinePlayerFromUUID(UUID.fromString(uuidHost));
                    if (playerHost == null) {
                        Organisation.log.severe("Unable to restore oInvitations from '" + uuidHost + "' to '" + uuidInvite + "' in '" + group.getName() + "' because '" + uuidHost + "' is not coresponding to any player in this server");
                        continue;
                    }

                    OrganisationPlayer organisationPlayerHost;
                    try {
                        organisationPlayerHost = OrganisationPlayer.getOrganisationPlayer(playerHost);
                    } catch (OrganisationPlayerException e) {
                        Organisation.log.severe("Unable to restore oInvitations from '" + playerHost.getName() + "' to '" + uuidInvite + "' in '" + group.getName() + "' because organisationPlayer is not found from '" + playerHost.getName() + "' is not coresponding to any organisationPlayer in this server");
                        continue;
                    }

                    OrganisationPlayer oPlayerInvite = null;
                    try {
                        oPlayerInvite = OrganisationPlayer.getOrganisationPlayerByUUID(UUID.fromString(uuidInvite));
                    }catch (OrganisationPlayerException e){
                        Organisation.log.severe("Unable to restore oInvitations from '" + playerHost.getName() + "' to '" + uuidInvite + "' in '" + group.getName() + "' because '" + uuidInvite + "' is not coresponding to any player in this server");
                        continue;
                    }

                    try {
                        InviteCommand.lstInvitation.add(new Invitation(organisationPlayerHost, group, oPlayerInvite));
                    } catch (InvitationGroupException e) {
                        continue;
                    }
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean addProvince(Province province) {

        final String query = "INSERT INTO oProvince (id, name, description) " +
                "VALUES (?,?,?);";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {
                queryStatement.setString(1, province.getId());
                queryStatement.setString(2, province.getName());
                queryStatement.setString(3, province.getDescription());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new Province in databse");
            Organisation.log.severe(e.getMessage());
            return false;
        }

        return true;
    }
    public static boolean setProvinceName(Province province, String newname) {

        final String query = "UPDATE oProvince " +
                "SET name = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, newname);
                queryStatement.setString(2, province.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the province name of '" + province.getId() + "' to '" + newname + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setProvinceDescription(Province province, String newDesc) {

        final String query = "UPDATE oProvince " +
                "SET description = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, newDesc);
                queryStatement.setString(2, province.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the province description of '" + province.getId() + "' to '" + newDesc + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean removeProvince(Province province) {

        final String queryDeleteAllProvinceSubLand = "";
        final String queryDeleteAllProvinceTotem = "";
        final String queryDeleteAllProvinceLand = "DELETE FROM oLand " +
                "WHERE idProvince=?;";

        final String queryDeleteProvince = "DELETE FROM oProvince " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteAllProvinceLand)) {
                queryStatement.setString(1, province.getId());
                queryStatement.execute();
            }

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteProvince)) {
                queryStatement.setString(1, province.getId());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete Province " + province.getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreProvince() {

        final String query = "SELECT * FROM oProvince;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                while (rs.next()) {

                    final String id = rs.getString("id");
                    final String name = rs.getString("name");
                    final String description = rs.getString("description");

                    new Province(id, name, description, false);
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    public static int addLand(Land land) {

        final String query = "INSERT INTO oLand (idServer, name, idRegionWorldGuard, isMainLand, idProvince, idGroup, canOtherGoIn, isActive, ownedTime) " +
                "VALUES (?,?,?,?,?,?,?,?,?);";

        int id = 0;
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                queryStatement.setString(1, ConfigManager.getServerId());
                queryStatement.setString(2, land.getName());
                queryStatement.setString(3, land.getWorldGuardRegion().getId());
                queryStatement.setBoolean(4, land.isMainLand());
                queryStatement.setString(5, (land.getProvince() == null) ? null : land.getProvince().getId());

                if (land.getOwner() == null) queryStatement.setNull(6, Types.INTEGER);
                else queryStatement.setInt(6, land.getOwner().getId());

                queryStatement.setBoolean(7, land.canOtherGoIn());
                queryStatement.setBoolean(8, land.isActive());
                queryStatement.setLong(9, land.getOwnedTime());

                queryStatement.execute();
                ResultSet generatedKeys = queryStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt("insert_id");
                }
                VLand vLand = VLand.getVlandById(land.getId());
                if(vLand != null) GeneralMethods.sendPacket(new VLandPacket(vLand, OrganisationPacket.Action.CREATE));
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new Land in database");
            Organisation.log.severe(e.getMessage());
            return -1;
        }

        VLand vland = new VLand(id,
                ConfigManager.getServerId(),
                land.getWorldGuardRegion().getId(),
                (land.getOwner() == null )? -1 : land.getOwner().getId(),
                land.isActive(), land.isMainLand(),
                land.getOwnedTime(),
                land.canOtherGoIn(),
                land.getName(),
                (land.getProvince() == null)? "" : land.getName());

        GeneralMethods.sendPacket(new VLandPacket(vland, OrganisationPacket.Action.CREATE));
        return id;
    }
    public static boolean setLandProvince(Land land) {

        VLand vLand = VLand.getVlandById(land.getId());
        if(vLand != null) GeneralMethods.sendPacket(new VLandPacket(vLand, OrganisationPacket.Action.UPDATE));

        final String query = "UPDATE oLand " +
                "SET idProvince = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, (land.getProvince() != null) ? land.getProvince().getId() : null);
                queryStatement.setInt(2, land.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the province of '" + land.getName() + "' to '" + ((land.getProvince() != null) ? land.getProvince().getName() : "<NULL>") + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setLandOwner(Land land) {

        VLand vLand = VLand.getVlandById(land.getId());
        if(vLand != null) GeneralMethods.sendPacket(new VLandPacket(vLand, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oLand " +
                "SET idGroup = ? " +
                "WHERE id=?;";

        LargeGroup largeGroup = land.getOwner();
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {
                if (largeGroup != null) queryStatement.setInt(1, largeGroup.getId());
                else queryStatement.setNull(1, Types.INTEGER);

                queryStatement.setInt(2, land.getId());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the land owner of '" + land.getName() + "' to '" + largeGroup.getName() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setIsMainLand(Land land) {

        VLand vLand = VLand.getVlandById(land.getId());
        Organisation.log.severe("SET IS MAIN LAND" + land.getName());
        if(vLand != null) GeneralMethods.sendPacket(new VLandPacket(vLand, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oLand " +
                "SET isMainLand = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setBoolean(1, land.isMainLand(false));
                queryStatement.setInt(2, land.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the land '" + land.getName() + "' to mainLandValue : '" + land.isMainLand() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }

        return true;
    }
    public static boolean setLandCanOtherGoIn(Land land) {

        VLand vLand = VLand.getVlandById(land.getId());
        if(vLand != null) GeneralMethods.sendPacket(new VLandPacket(vLand, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oLand " +
                "SET canOtherGoIn = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setBoolean(1, land.canOtherGoIn());
                queryStatement.setInt(2, land.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the land '" + land.getName() + "' canOtherGoIn : '" + land.canOtherGoIn() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setLandIsActive(Land land) {

        VLand vLand = VLand.getVlandById(land.getId());
        if(vLand != null) GeneralMethods.sendPacket(new VLandPacket(vLand, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oLand " +
                "SET isActive = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setBoolean(1, land.isActive());
                queryStatement.setInt(2, land.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the land '" + land.getName() + "'  active : '" + land.isActive() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setLandName(Land land) {

        VLand vLand = VLand.getVlandById(land.getId());
        if(vLand != null) GeneralMethods.sendPacket(new VLandPacket(vLand, OrganisationPacket.Action.UPDATE));

        final String query = "UPDATE oLand " +
                "SET name = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, land.getName());
                queryStatement.setInt(2, land.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the land name of '" + land.getName() + "' to '" + land.getName() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setLandOwnedTime(Land land) {

        VLand vLand = VLand.getVlandById(land.getId());
        if(vLand != null) GeneralMethods.sendPacket(new VLandPacket(vLand, OrganisationPacket.Action.UPDATE));

        final String query = "UPDATE oLand " +
                "SET ownedTime = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, land.getOwnedTime());
                queryStatement.setInt(2, land.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the ownedTime name of '" + land.getName() + "' to '" + land.getOwnedTime() + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean removeLand(Land land) {

        VLand vLand = VLand.getVlandById(land.getId());
        if(vLand != null) GeneralMethods.sendPacket(new VLandPacket(vLand, OrganisationPacket.Action.REMOVE));

        final String queryDeleteLand = "DELETE FROM oLand " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteLand)) {
                queryStatement.setInt(1, land.getId());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete Land " + land.getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreLand() {

        final String query = "SELECT * FROM oLand;";

        try (Connection connection = DBConnection.sql.getConnection()) {

            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {

                while (rs.next()) {

                    final int id = rs.getInt("id");
                    final String idServer = rs.getString("idServer");
                    final String name = rs.getString("name");
                    final String idRegionWorldGuard = rs.getString("idRegionWorldGuard");
                    final Boolean isMainLand = rs.getBoolean("isMainLand");
                    final String idProvince = rs.getString("idProvince");
                    final int idGroup = rs.getInt("idGroup");
                    final boolean isActive = rs.getBoolean("isActive");
                    final boolean canOtherGoIn = rs.getBoolean("canOtherGoIn");
                    final long ownedTime = rs.getLong("ownedTime");

                    Province province = Province.getProvinceById(idProvince);
                    if (province == null) {
                        Organisation.log.info("Unable to set land '" + name + "' province to '" + idProvince + " because province was not found.");
                    }

                    new VLand(id, idServer, idRegionWorldGuard, (idGroup>0)?idGroup : -1, isActive, isMainLand, ownedTime, canOtherGoIn, name,(province == null)? "" : province.getName());
                    if (!idServer.equals(ConfigManager.getServerId())) continue;

                    Group group = Group.getGroupById(idGroup);
                    LargeGroup largeGroup = null;
                    if ((!(group instanceof LargeGroup)) && idGroup > 0) {
                        Organisation.log.severe("Unable to restore land '" + name + "' with idGroup '" + idGroup + "' was not find as group.");
                        continue;
                    }
                    else if(idGroup > 0){
                        largeGroup = (LargeGroup) group;
                    }

                    ProtectedRegion worldGuardRegion = Organisation.worldRgManager.getRegion(idRegionWorldGuard);

                    if (worldGuardRegion == null) {
                        Organisation.log.severe("Unable to restore land '" + name + "' because the WG region '" + idRegionWorldGuard + "' was not find.");
                        continue;
                    }
                    if (Land.getLandById(id) != null) {
                        Organisation.log.info("Unable to set land '" + name + "' because id already exist");
                        continue;
                    }

                    Land land = new Land(id, worldGuardRegion, province, largeGroup, isMainLand, name,canOtherGoIn, isActive,  ownedTime);
                    if(Organisation.getHook(AvatarRoadsHook.class).isPresent()) {
                        Organisation.getHook(AvatarRoadsHook.class).get().enableMarkerInRegion(land.getWorldGuardRegion());
                    }
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
        Organisation.log.info("ASK FOR ALL VLAND");
        GeneralMethods.sendPacket(new VLandPacket(null, OrganisationPacket.Action.GET_ALL));

        return true;
    }

    public static int addLandSubTerritory(LandSubTerritory landSubTerritory) {

        final String query = "INSERT INTO oLandSubTerritory (idServer, name, idRegionWorldGuard, idLand, type, canOtherGoIn) " +
                "VALUES (?,?,?,?,?,?);";


        int id = 0;
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                queryStatement.setString(1, ConfigManager.getServerId());
                queryStatement.setString(2, landSubTerritory.getName());
                queryStatement.setString(3, landSubTerritory.getWorldGuardRegion().getId());
                queryStatement.setInt(4, landSubTerritory.getLand().getId());
                queryStatement.setString(5, landSubTerritory.getClass().getName());
                queryStatement.setBoolean(6, landSubTerritory.canOtherGoIn());

                queryStatement.execute();
                ResultSet generatedKeys = queryStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt("insert_id");
                }
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new LandSubTerritory in database");
            Organisation.log.severe(e.getMessage());
            return -1;
        }

        return id;
    }
    public static boolean setSubTerritoryName(LandSubTerritory landSubTerritory, String newName) {
        final String query = "UPDATE oLandSubTerritory " +
                "SET name = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, newName);
                queryStatement.setInt(2, landSubTerritory.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the landSubTerritory name of '" + landSubTerritory.getName() + "' to '" + newName + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setLandSubTerritoryCanOtherGoIn(LandSubTerritory landSubTerritory, boolean canOtherGoIn) {

        final String query = "UPDATE oLandSubTerritory " +
                "SET canOtherGoIn = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setBoolean(1, canOtherGoIn);
                queryStatement.setInt(2, landSubTerritory.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the landSubTerritory '" + landSubTerritory.getName() + "' canOtherGoIn : '" + canOtherGoIn + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean removeLandSubTerritory(LandSubTerritory landSubTerritory) {

        final String queryDeleteAllLinkedPositionPointOfInterest = "";
        final String queryDeleteLandSubTerritory = "DELETE FROM oLandSubTerritory " +
                "WHERE id=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteLandSubTerritory)) {
                queryStatement.setInt(1, landSubTerritory.getId());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete landSubTerritory " + landSubTerritory.getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreLandSubTerritory() {

        final String query = "SELECT * FROM oLandSubTerritory;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                while (rs.next()) {

                    final int id = rs.getInt("id");
                    final String idServer = rs.getString("idServer");
                    final String name = rs.getString("name");
                    final String idRegionWorldGuard = rs.getString("idRegionWorldGuard");
                    final int idLand = rs.getInt("idLand");
                    final String type = rs.getString("type");
                    final boolean canOtherGoIn = rs.getBoolean("canOtherGoIn");

                    if (!idServer.equals(ConfigManager.getServerId())) continue;

                    ProtectedRegion worldGuardRegion = Organisation.worldRgManager.getRegion(idRegionWorldGuard);
                    if (worldGuardRegion == null) {
                        Organisation.log.severe("Unable to restore LandSubTerritory '" + name + "' because the WG region '" + idRegionWorldGuard + "' was not find.");
                        continue;
                    }

                    Land land = Land.getLandById(idLand);
                    if (land == null) {
                        Organisation.log.info("Unable to set landSubTerritory '" + name + "' land to '" + idLand + " because land was not found.");
                    }

                    if (LandSubTerritory.getLandSubTerritoryById(id) != null) {
                        Organisation.log.info("Unable to restore landSubTerritory '" + name + "' because id already exist");
                        continue;
                    }
                    try {
                        if (type.equals("fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine"))
                            new Mine(id, name, worldGuardRegion, land, canOtherGoIn);
                        else if (type.equals("fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Totem"))
                            new Totem(id, name, worldGuardRegion, land, canOtherGoIn);
                        else if (type.equals("fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.RP"))
                            new RP(id, name, worldGuardRegion, land, canOtherGoIn);
                        else if (type.equals("fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Contest"))
                            new Contest(id, name, worldGuardRegion, land, canOtherGoIn);
                    }
                    catch (Exception e){}
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static int addPositionInterest(PositionPointInterest positionPointInterest) {
        final String query = "INSERT INTO oPositionPointOfInterest " +
                "(idServer, type, idTerritory, worldName, x, y,z, pitch, yaw, OptionalValueOne, OptionalValueTwo, " +
                "minPlayerRank, maxPlayerRank, minOrganisationRank, maxOrganisationRank) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        int id = 0;
        VMine vMine = VMine.getByPositionOfInterest(positionPointInterest);
        if(vMine != null) GeneralMethods.sendPacket(new VMinePacket(vMine, OrganisationPacket.Action.CREATE));
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

                queryStatement.setString(1, ConfigManager.getServerId());
                queryStatement.setString(2, positionPointInterest.getContext().toString());
                queryStatement.setInt(3, positionPointInterest.getTerritory().getId());
                queryStatement.setString(4, positionPointInterest.getLocation().getWorld().getName());
                queryStatement.setInt(5, positionPointInterest.getLocation().getBlockX());
                queryStatement.setInt(6, positionPointInterest.getLocation().getBlockY());
                queryStatement.setInt(7, positionPointInterest.getLocation().getBlockZ());
                queryStatement.setFloat(8, positionPointInterest.getLocation().getPitch());
                queryStatement.setFloat(9, positionPointInterest.getLocation().getYaw());
                queryStatement.setString(10, positionPointInterest.getOptionalValueOne());
                queryStatement.setString(11, positionPointInterest.getOptionalValueTwo());
                queryStatement.setInt(12, positionPointInterest.getMinRankPlayer());
                queryStatement.setInt(13, positionPointInterest.getMaxRankPlayer());
                queryStatement.setInt(14, positionPointInterest.getMinRankGroup());
                queryStatement.setInt(15, positionPointInterest.getMaxRankGroup());

                queryStatement.execute();
                ResultSet generatedKeys = queryStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt("insert_id");
                }
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new PositionPointInterest in database");
            Organisation.log.severe(e.getMessage());
            return -1;
        }

        return id;
    }

    public static boolean setPositionInterestOptionalValueOne(PositionPointInterest positionPointInterest, String optionalValueOne) {
        final String query = "UPDATE oPositionPointOfInterest " +
                "SET OptionalValueOne = ? " +
                "WHERE id=?;";
        VMine vMine = VMine.getByPositionOfInterest(positionPointInterest);
        if(vMine != null) GeneralMethods.sendPacket(new VMinePacket(vMine, OrganisationPacket.Action.UPDATE));
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, optionalValueOne);
                queryStatement.setInt(2, positionPointInterest.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the PositionPointInterest OptionalValueOne of '" + positionPointInterest.getOptionalValueOne() + "' to '" + optionalValueOne + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setPositionInterestOptionalValueTwo(PositionPointInterest positionPointInterest, String optionalValueTwo) {
        final String query = "UPDATE oPositionPointOfInterest " +
                "SET OptionalValueTwo = ? " +
                "WHERE id=?;";

        VMine vMine = VMine.getByPositionOfInterest(positionPointInterest);
        if(vMine != null) GeneralMethods.sendPacket(new VMinePacket(vMine, OrganisationPacket.Action.UPDATE));
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, optionalValueTwo);
                queryStatement.setInt(2, positionPointInterest.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the PositionPointInterest OptionalValueTwo of '" + positionPointInterest.getOptionalValueTwo() + "' to '" + optionalValueTwo + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean removePositionInterest(PositionPointInterest positionPointInterest) {
        final String queryDeletePositionPointInterest = "DELETE FROM oPositionPointOfInterest " +
                "WHERE id=?;";

        VMine vMine = VMine.getByPositionOfInterest(positionPointInterest);
        if(vMine != null) GeneralMethods.sendPacket(new VMinePacket(vMine, OrganisationPacket.Action.REMOVE));

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeletePositionPointInterest)) {
                queryStatement.setInt(1, positionPointInterest.getId());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete positionPointInterest " + positionPointInterest.getId() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restorePositionInterest() {
        final String query = "SELECT * FROM oPositionPointOfInterest;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                while (rs.next()) {

                    final int id = rs.getInt("id");
                    final String idServer = rs.getString("idServer");

                    if(!idServer.equals(ConfigManager.getServerId())){
                        continue;
                    }

                    final String type = rs.getString("type");
                    final int idTerritory = rs.getInt("idTerritory");
                    final int x = rs.getInt("x");
                    final int y = rs.getInt("y");
                    final int z = rs.getInt("z");
                    final float pitch = rs.getFloat("pitch");
                    final float yaw = rs.getFloat("yaw");
                    final String worldName = rs.getString("worldName");
                    final String optionalValueOne = rs.getString("OptionalValueOne");
                    final String optionalValueTwo = rs.getString("OptionalValueTwo");
                    final int minPlayerRank = rs.getInt("minPlayerRank");
                    final int maxPlayerRank = rs.getInt("maxPlayerRank");
                    final int minOrganisationRank = rs.getInt("minOrganisationRank");
                    final int maxOrganisationRank = rs.getInt("maxOrganisationRank");

                    Land land = Land.getLandById(idTerritory);
                    LandSubTerritory landSubTerritory = LandSubTerritory.getLandSubTerritoryById(idTerritory);

                    Territory territory = (landSubTerritory != null) ? landSubTerritory : land;
                    if (territory == null) {
                        Organisation.log.info("Unable to set PositionPointOfInterest terrytory '" + idServer + "' to '" + idTerritory + " because territory was not found.");
                    }

                    World world = Organisation.plugin.getServer().getWorld(worldName);

                    if(world == null) {
                        Organisation.log.info("Unable to restore PositionPointOfInterest '" + id + "' because the world '"+worldName+"' is not found on this server");
                        continue;
                    }

                    Location location = new Location(world, x, y, z, pitch, yaw);

                    if (location == null) {
                        Organisation.log.info("Unable to restore PositionPointOfInterest location '" + worldName + "' because the location is not found.");
                        continue;
                    }

                    PositionPointInterest.Type context = PositionPointInterest.getContextFromString(type);
                    if(context == null){
                        Organisation.log.info("Unable to restore PositionPoint context '" + type + "' because this context is not supported on this server.");
                        continue;
                    }

                    new PositionPointInterest(id,
                            location,
                            territory,
                            context,
                            minOrganisationRank,
                            maxOrganisationRank,
                            minPlayerRank,
                            maxPlayerRank,
                            optionalValueOne,
                            optionalValueTwo);

                }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }

        GeneralMethods.sendPacket(new VMinePacket(null, OrganisationPacket.Action.GET_ALL));
        return true;
    }
        catch (SQLException e) {
            Organisation.log.severe("Unable to restore PositionPointOfInterest from database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
    }

    public static int addStoredInventory(StoredInventory storedInventory) {

        final String query = "INSERT INTO oInventory (name, stringInv, ratio, maxNumber, type) " +
                "VALUES (?,?,?,?,?);";

        int id = 0;
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

                queryStatement.setString(1, storedInventory.getName());
                queryStatement.setString(2, storedInventory.getStringFromStoredInventory());
                queryStatement.setDouble(3, storedInventory.getRatio());
                queryStatement.setInt(4, storedInventory.getMaxNumber());
                queryStatement.setString(5, storedInventory.getType());

                queryStatement.execute();
                ResultSet generatedKeys = queryStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt("insert_id");
                }
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new StoredInventory in database");
            Organisation.log.severe(e.getMessage());
            return -1;
        }

        return id;
    }
    public static boolean setNameStoredInventory(StoredInventory storedInventory, String newName) {

        GeneralMethods.sendPacket(new StoredInventoryPacket(storedInventory, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oInventory " +
                "SET name = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, newName);
                queryStatement.setInt(2, storedInventory.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the storedInventory name of '" + storedInventory.getName() + "' to '" + newName + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setInventoryStoredInventory(StoredInventory storedInventory) {

        GeneralMethods.sendPacket(new StoredInventoryPacket(storedInventory, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oInventory " +
                "SET stringInv = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, storedInventory.getStringFromStoredInventory());
                queryStatement.setInt(2, storedInventory.getId());

                queryStatement.execute();
            }
        }
        catch (SQLException e) {
            Organisation.log.severe("Unable to update the storedInventory inventoryString '" + storedInventory.getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setMaxNumberStoredInventory(StoredInventory storedInventory, int maxNumber) {

        GeneralMethods.sendPacket(new StoredInventoryPacket(storedInventory, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oInventory " +
                "SET maxNumber = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, maxNumber);
                queryStatement.setInt(2, storedInventory.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the storedInventory maxNumber of '" + storedInventory.getName() + "' to '" + maxNumber + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setRatioStoredInventory(StoredInventory storedInventory, double ratio) {

        GeneralMethods.sendPacket(new StoredInventoryPacket(storedInventory, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oInventory " +
                "SET ratio = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setDouble(1, ratio);
                queryStatement.setInt(2, storedInventory.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the storedInventory ratio of '" + storedInventory.getName() + "' to '" + ratio + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean removeStoredInventory(StoredInventory storedInventory) {

        GeneralMethods.sendPacket(new StoredInventoryPacket(storedInventory, OrganisationPacket.Action.REMOVE));
        final String queryDeletePositionPointInterest = "DELETE FROM oInventory " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeletePositionPointInterest)) {
                queryStatement.setInt(1, storedInventory.getId());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete StoredInventory " + storedInventory.getId() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreStoredInventory(){
        final String query = "SELECT * FROM oInventory;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {

                while (rs.next()) {

                    final int id = rs.getInt("id");
                    final String name = rs.getString("name");
                    final String stringInv = rs.getString("stringInv");
                    final double ratio = rs.getDouble("ratio");
                    final int maxNumber = rs.getInt("maxNumber");
                    final String type = rs.getString("type");

                    Inventory tmpInventory = StoredInventory.getInventoryFromString(stringInv);
                    if(tmpInventory == null){
                        Organisation.log.severe("Unable to restore StoredInventory "+id+ " because it's stringInv cannot be restored as Spigot Inventory");
                        continue;
                    }

                    Inventory inventory = Bukkit.createInventory(null, 54, "§3§l" + name);

                    // Clone inventory
                    for(int i = 0; i< tmpInventory.getSize(); i++){
                        if(tmpInventory.getItem(i) != null)inventory.setItem(i, tmpInventory.getItem(i));
                    }

                    new StoredInventory(id, name, inventory, ratio, maxNumber, type);
                }
            } catch (final SQLException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        catch (SQLException e) {
            Organisation.log.severe("Unable to restore oInventory from database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
    }

    public static int addRelation(Relation relation) {

        final String query = "INSERT INTO oRelation (type, startRelationTime, endRelationTime, rolePlay, phase, initiator, target, relationEnder) " +
                             "VALUES (?,?,?,?,?,?,?,?);";

        int id  = 0;
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

                queryStatement.setString(1, relation.getClass().getName());
                queryStatement.setLong(2, relation.getStartRelationTime());
                queryStatement.setLong(3, relation.getEndRelationTime());
                queryStatement.setString(4, relation.getRolePlay());
                queryStatement.setString(5, relation.getPhase());
                queryStatement.setInt(6, relation.getInitiator().getId());
                queryStatement.setInt(7, relation.getTarget().getId());
                if(relation.getRelationEnder() != null) queryStatement.setString(8, relation.getRelationEnder().getUniqueId().toString());
                else queryStatement.setString(8, null);

                queryStatement.execute();
                ResultSet generatedKeys = queryStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt("insert_id");
                }
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new StoredInventory in database");
            Organisation.log.severe(e.getMessage());
            return -1;
        }

        return id;
    }
    public static boolean setRelationPhase(Relation relation, String phase) {
        final String query = "UPDATE oRelation " +
                "SET phase = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, phase);
                queryStatement.setInt(2, relation.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the relation phase of'" + relation.getId() + "' to '" + phase + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setRelationRolePlay(Relation relation, String rolePlay) {
        final String query = "UPDATE oRelation " +
                "SET rolePlay = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, rolePlay);
                queryStatement.setInt(2, relation.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the relation roleplay of'" + relation.getId() + "' to '" + rolePlay + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setEndRelationTime(Relation relation, Long endRelationTime) {
        final String query = "UPDATE oRelation " +
                "SET endRelationTime = ? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, endRelationTime);
                queryStatement.setInt(2, relation.getId());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the relation endRelationTime of'" + relation.getId() + "' to '" + endRelationTime + "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreRelation(){
        final String query = "SELECT * FROM oRelation;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                while (rs.next()) {

                    final int id = rs.getInt("id");
                    final long startRelationTime = rs.getLong("startRelationTime");
                    final long endRelationTime = rs.getLong("endRelationTime");
                    final String rolePlay = rs.getString("rolePlay");
                    final String phase = rs.getString("phase");
                    final int initiator = rs.getInt("initiator");
                    final int target = rs.getInt("target");
                    final String relationEnder = rs.getString("relationEnder");
                    final String type = rs.getString("type");

                }
            } catch (final SQLException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        catch (SQLException e) {
            Organisation.log.severe("Unable to restore oRelation from database");
            Organisation.log.severe(e.getMessage());
            return false;
        }
    }

    public static int addRank(Rank rank) {

        final String query = "INSERT INTO oRank (type, name, groupType, rankOrder, nbMaxLand, nbMaxPlayer, nbMaxVassal, canVassalize, nbMinHeart, nbMaxSlotInv, unlockLeftHand) " +
                             "VALUES (?,?,?,?,?,?,?,?,?,?,?);";

        int id = 0;
        try (Connection connection = DBConnection.sql.getConnection()) {

            try (PreparedStatement queryStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

                queryStatement.setString(1, rank.getClass().toString());
                queryStatement.setString(2, rank.getName());
                queryStatement.setString(3, (rank instanceof GroupRank)? ((GroupRank) rank).getGroupType() : "");
                queryStatement.setInt(4, rank.getOrder());
                queryStatement.setInt(5, (rank instanceof GroupRank)? ((GroupRank) rank).getMaxLand() : 0);
                queryStatement.setInt(6, (rank instanceof GroupRank)? ((GroupRank) rank).getMaxPlayer() : 0);
                queryStatement.setInt(7, (rank instanceof GroupRank)? ((GroupRank) rank).getMaxVassal(): 0);
                queryStatement.setBoolean(8, (rank instanceof GroupRank)? ((GroupRank) rank).isCanVassalize(): false);
                queryStatement.setInt(9, (rank instanceof PlayerRank)? ((PlayerRank) rank).getMinHeart(): 0);
                queryStatement.setInt(10, (rank instanceof PlayerRank)? ((PlayerRank) rank).getMaxSlotInv(): 0);
                queryStatement.setBoolean(11, (rank instanceof PlayerRank)? ((PlayerRank) rank).isUnlockLeftHand(): false);

                queryStatement.execute();
                ResultSet generatedKeys = queryStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt("insert_id");
                }
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new Rank in database");
            Organisation.log.severe(e.getMessage());
            return -1;
        }

        return id;
    }
    public static boolean setRankName(Rank rank, String newName) {

        GeneralMethods.sendPacket((rank instanceof PlayerRank)?new PlayerRankPacket((PlayerRank) rank, OrganisationPacket.Action.UPDATE) : new GroupRankPacket((GroupRank) rank, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oRank " +
                "SET name = ? " +
                "WHERE id=? AND type=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, newName);
                queryStatement.setInt(2, rank.getId());
                queryStatement.setString(3, rank.getClass().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the rank name to '" + newName +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setRankOrder(Rank rank) {

        GeneralMethods.sendPacket((rank instanceof PlayerRank)?new PlayerRankPacket((PlayerRank) rank, OrganisationPacket.Action.UPDATE) : new GroupRankPacket((GroupRank) rank, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oRank " +
                "SET rankOrder=? " +
                "WHERE id=? AND type=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, rank.getOrder());
                queryStatement.setInt(2, rank.getId());
                queryStatement.setString(3, rank.getClass().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the rank order to '" + rank.getOrder() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setRankNbMinHeart(PlayerRank playerRank) {

        GeneralMethods.sendPacket(new PlayerRankPacket(playerRank, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oRank " +
                "SET nbMinHeart = ? " +
                "WHERE id=? AND type=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, playerRank.getMinHeart());
                queryStatement.setInt(2, playerRank.getId());
                queryStatement.setString(3, playerRank.getClass().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the rank nb min heart to '" + playerRank.getMinHeart() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setRankMaxSlotInv(PlayerRank playerRank) {

        GeneralMethods.sendPacket(new PlayerRankPacket(playerRank, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oRank " +
                "SET nbMaxSlotInv = ? " +
                "WHERE id=? AND type=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, playerRank.getMaxSlotInv());
                queryStatement.setInt(2, playerRank.getId());
                queryStatement.setString(3, playerRank.getClass().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the rank max slot inv to '" + playerRank.getMaxSlotInv() +  "' in database.");
            Organisation.log.severe(e.getMessage());

            return false;
        }
        return true;

    }
    public static boolean setRankUnlockLeftHand(PlayerRank playerRank) {

        GeneralMethods.sendPacket(new PlayerRankPacket(playerRank, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oRank " +
                "SET unlockLeftHand = ? " +
                "WHERE id=? AND type=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setBoolean(1, playerRank.isUnlockLeftHand());
                queryStatement.setInt(2, playerRank.getId());
                queryStatement.setString(3, playerRank.getClass().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the rank unlockLeftHand to '" + playerRank.isUnlockLeftHand() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setRankNbMaxLand(GroupRank groupRank){

        GeneralMethods.sendPacket(new GroupRankPacket(groupRank, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oRank " +
                "SET nbMaxLand = ? " +
                "WHERE id=? AND type=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, groupRank.getMaxLand());
                queryStatement.setInt(2, groupRank.getId());
                queryStatement.setString(3, groupRank.getClass().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the rank maxLand to '" + groupRank.getMaxLand() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setRankMaxVassal(GroupRank groupRank){

        GeneralMethods.sendPacket(new GroupRankPacket(groupRank, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oRank " +
                "SET nbMaxVassal = ? " +
                "WHERE id=? AND type=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, groupRank.getMaxVassal());
                queryStatement.setInt(2, groupRank.getId());
                queryStatement.setString(3, groupRank.getClass().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the rank maxVassal to '" + groupRank.getMaxVassal()+  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setRankGroupType(GroupRank groupRank){

        GeneralMethods.sendPacket(new GroupRankPacket(groupRank, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oRank " +
                "SET groupType = ? " +
                "WHERE id=? AND type=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, groupRank.getGroupType());
                queryStatement.setInt(2, groupRank.getId());
                queryStatement.setString(3, groupRank.getClass().toString());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the rank groupType to '" + groupRank.getGroupType()+  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean setRankMaxPlayer(GroupRank groupRank){

        GeneralMethods.sendPacket(new GroupRankPacket(groupRank, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oRank " +
                "SET nbMaxPlayer = ? " +
                "WHERE id=? AND type=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, groupRank.getMaxPlayer());
                queryStatement.setInt(2, groupRank.getId());
                queryStatement.setString(3, groupRank.getClass().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the rank maxPlayer to '" + groupRank.getMaxPlayer()+  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setRankCanVassalize(GroupRank groupRank, boolean canVassalize){

        GeneralMethods.sendPacket(new GroupRankPacket(groupRank, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oRank " +
                "SET canVassalize = ? " +
                "WHERE id=? AND type=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setBoolean(1, canVassalize);
                queryStatement.setInt(2, groupRank.getId());
                queryStatement.setString(3, groupRank.getClass().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the rank canVassalize to '" + canVassalize+  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean removeRank(Rank rank){

        GeneralMethods.sendPacket((rank instanceof PlayerRank)?new PlayerRankPacket((PlayerRank) rank, OrganisationPacket.Action.REMOVE) : new GroupRankPacket((GroupRank) rank, OrganisationPacket.Action.REMOVE));
        final String queryDeleteRank = "DELETE FROM oRank " +
                "WHERE id=? AND type=?;";
        final String queryDeleteAllRankObjectifs = "DELETE FROM oObjective " +
                "WHERE rankId=? AND rankType=?;";
        final String queryDeleteObjectiveProgression= "DELETE FROM oObjectiveProgression " +
                "WHERE rankId=? AND rankType=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteObjectiveProgression)) {
                queryStatement.setInt(1, rank.getId());
                queryStatement.setString(2, rank.getClass().toString());
                queryStatement.execute();
            }

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteAllRankObjectifs)) {
                queryStatement.setInt(1, rank.getId());
                queryStatement.setString(2, rank.getClass().toString());
                queryStatement.execute();
            }

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteRank)) {
                queryStatement.setInt(1, rank.getId());
                queryStatement.setString(2, rank.getClass().toString());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete rank " + rank.getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean restoreRank() {
        final String query = "SELECT * FROM oRank;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                while (rs.next()) {

                    final int id = rs.getInt("id");
                    final String type = rs.getString("type");
                    final String name = rs.getString("name");
                    final String groupType = rs.getString("groupType");
                    final int order = rs.getInt("rankOrder");
                    final int maxLand = rs.getInt("nbMaxLand");
                    final int maxPlayer = rs.getInt("nbMaxPlayer");
                    final int maxVassal = rs.getInt("nbMaxVassal");
                    final boolean canVassalize = rs.getBoolean("canVassalize");
                    final int minHeart = rs.getInt("nbMinHeart");
                    final int maxSlotInv = rs.getInt("nbMaxSlotInv");
                    final boolean unlockLeftHand = rs.getBoolean("unlockLeftHand");

                    if(type.equals("class fr.avatar_returns.organisation.rank.GroupRank")){
                        new GroupRank(id, name, order, groupType, maxLand, maxVassal, maxPlayer, canVassalize, new ArrayList<>());
                    }
                    else if(type.equals("class fr.avatar_returns.organisation.rank.PlayerRank")){
                        new PlayerRank(id, name, order, new ArrayList<>(), minHeart, maxSlotInv, unlockLeftHand);
                    }
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    public static boolean addGRankObjectif(Objective objective) {

        GeneralMethods.sendPacket(new ObjectivePacket(objective, OrganisationPacket.Action.CREATE));
        final String query = "INSERT INTO oObjective (rankId, rankType, name, description, item, objectifValue, objectifType) " +
                             "VALUES (?,?,?,?,?,?,?);";

        try (Connection connection = DBConnection.sql.getConnection()) {

            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, objective.getRank().getId());
                queryStatement.setString(2, objective.getRank().getClass().toString());
                queryStatement.setString(3, objective.getName());
                queryStatement.setString(4, objective.getDescription());
                queryStatement.setString(5,  ItemUtils.itemStackToBase64(objective.getItem()));
                queryStatement.setInt(6, objective.getObjectifValue());
                queryStatement.setString(7, objective.getType());


                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new objective in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }

        return true;
    }
    public static boolean setObjectiveDescription(Objective objective){

        GeneralMethods.sendPacket(new ObjectivePacket(objective, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oObjective " +
                "SET description = ? " +
                "WHERE rankId=? AND rankType=? AND name=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, objective.getDescription());
                queryStatement.setInt(2, objective.getRank().getId());
                queryStatement.setString(3, objective.getRank().getClass().toString());
                queryStatement.setString(4, objective.getName());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the objective description to '" + objective.getDescription() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setObjectiveValue(Objective objective){

        GeneralMethods.sendPacket(new ObjectivePacket(objective, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oObjective " +
                "SET objectifValue=? " +
                "WHERE rankId=? AND rankType=? AND name=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, objective.getObjectifValue());
                queryStatement.setInt(2, objective.getRank().getId());
                queryStatement.setString(3, objective.getRank().getClass().toString());
                queryStatement.setString(4, objective.getName());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the objective value to '" + objective.getObjectifValue() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;

    }
    public static boolean deleteRankObjectif(Objective objective) {

        GeneralMethods.sendPacket(new ObjectivePacket(objective, OrganisationPacket.Action.REMOVE));
        final String queryDeleteAllRankObjectifs = "DELETE FROM oObjective " +
                "WHERE rankId=? AND rankType=? AND name=?;";
        final String queryDeleteObjectiveProgression= "DELETE FROM oObjectiveProgression " +
                "WHERE rankId=? AND rankType=? AND objectiveName =?;";

        try (Connection connection = DBConnection.sql.getConnection()) {

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteObjectiveProgression)) {
                queryStatement.setInt(1, objective.getRank().getId());
                queryStatement.setString(2, objective.getRank().getClass().toString());
                queryStatement.setString(3, objective.getName());
                queryStatement.execute();
            }

            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteAllRankObjectifs)) {
                queryStatement.setInt(1, objective.getRank().getId());
                queryStatement.setString(2, objective.getRank().getClass().toString());
                queryStatement.setString(3, objective.getName());
                queryStatement.execute();
            }

        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete objectif "+ objective.getName() +" in " + objective.getRank().getName() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean restoreRankObjectif() {
        final String query = "SELECT * FROM oObjective;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (ResultSet rs = DBConnection.sql.readQuery(connection, query)) {
                while (rs.next()) {

                    final int rankId = rs.getInt("rankId");
                    final String rankType = rs.getString("rankType");
                    final String name = rs.getString("name");
                    final String description = rs.getString("description");
                    final String itembase64 = rs.getString("item");
                    final int objectifValue = rs.getInt("objectifValue");
                    final String objectifType = rs.getString("objectifType");

                    Rank rank = null;
                    if (rankType.equals("class fr.avatar_returns.organisation.rank.GroupRank")) {
                        rank = GroupRank.getById(rankId);
                    }
                    else if (rankType.equals("class fr.avatar_returns.organisation.rank.PlayerRank")) {
                        rank = PlayerRank.getById(rankId);
                    }
                    if (rank == null) continue;

                    ItemStack item =  ItemUtils.itemStackFromBase64(itembase64);
                    rank.addObjectif(name, description, objectifValue, item, objectifType, false);
                }
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean addObjectiveProgression(GroupObjective groupObjective) {

        GeneralMethods.sendPacket(new GroupObjectivePacket(groupObjective, OrganisationPacket.Action.CREATE));
        final String query = "INSERT INTO oObjectiveProgression (rankId, groupId, rankType, objectiveName, currentObjectiveValue) " +
                "VALUES (?,?,?,?,?);";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, groupObjective.getObjective().getRank().getId());
                queryStatement.setInt(2, groupObjective.getGroup().getId());
                queryStatement.setString(3, groupObjective.getObjective().getRank().getClass().toString());
                queryStatement.setString(4, groupObjective.getObjective().getName());
                queryStatement.setInt(5, 0);

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new oObjectiveProgression in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }

        return true;
    }
    public static boolean updateObjectiveProgression(int currentObjectiveValue, GroupObjective groupObjective) {

        GeneralMethods.sendPacket(new GroupObjectivePacket(groupObjective, OrganisationPacket.Action.UPDATE));
        final String query = "UPDATE oObjectiveProgression " +
                             "SET currentObjectiveValue = ? " +
                             "WHERE rankId=? AND groupId=? AND rankType=? AND objectiveName=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, currentObjectiveValue);
                queryStatement.setInt(2, groupObjective.getObjective().getRank().getId());
                queryStatement.setInt(3, groupObjective.getGroup().getId());
                queryStatement.setString(4, groupObjective.getObjective().getRank().getClass().toString());
                queryStatement.setString(5, groupObjective.getObjective().getName());


                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to update new oObjectiveProgression in database");
            Organisation.log.severe(e.getMessage());
            return false;
        }

        return true;
    }
    public static ArrayList<GroupObjective> getGroupRankObjective(Group group) {

        ArrayList<GroupObjective> lstGroupObjective = new ArrayList<>();

        final String query = "SELECT * FROM oObjectiveProgression WHERE rankId=? AND groupId=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, group.getRank().getId());
                queryStatement.setInt(2, group.getId());

                try (ResultSet rs = queryStatement.executeQuery()) {
                    while (rs.next()) {

                        final String objectiveName = rs.getString("objectiveName");
                        final int currentObjectiveValue = rs.getInt("currentObjectiveValue");

                        Objective objective = null;
                        for(Objective tmpObjective : group.getRank().getLstObjectif()){
                            if(tmpObjective.getName().equalsIgnoreCase(objectiveName)){
                                objective = tmpObjective;
                                break;
                            }
                        }
                        if(objective == null)continue;

                        lstGroupObjective.add(new GroupObjective(group, objective, currentObjectiveValue));
                    }
                }
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to get getGroupRankObjective progression in database");
            Organisation.log.severe(e.getMessage());
        }

        return lstGroupObjective;
    }

    public static int addEvent(Event event) {
        final String query = "INSERT INTO oEvents (id, name, date, timeRepetition, repeat, groupWinnerId, playerWinnerUUID) VALUES (?, ?, ?, ?, ?, ?, ?);";

        int id = 0;
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

                queryStatement.setInt(1, event.getId());
                queryStatement.setString(2, event.getName());
                queryStatement.setString(3, event.getDate());
                queryStatement.setString(4, event.getTimeRepetition());
                queryStatement.setBoolean(5, event.isRepeat());
                queryStatement.setInt(6, -1);
                queryStatement.setString(7, "");

                queryStatement.execute();
                ResultSet generatedKeys = queryStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    id = generatedKeys.getInt("insert_id");
                }
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to create new Event in database");
            Organisation.log.severe(e.getMessage());
            return -1;
        }

        return id;
    }
    public static boolean setEventName(Event event) {


        final String query = "UPDATE oEvents " +
                "SET name=? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, event.getName());
                queryStatement.setInt(2, event.getId());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the Event name to '" + event.getName() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setEventDate(Event event) {


        final String query = "UPDATE oEvents " +
                "SET date=? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, event.getDate());
                queryStatement.setInt(2, event.getId());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the Event date to '" + event.getDate() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setEventTimeRepetition(Event event) {


        final String query = "UPDATE oEvents " +
                "SET timeRepetition=? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setString(1, event.getTimeRepetition());
                queryStatement.setInt(2, event.getId());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the Event timeRepetition to '" + event.getTimeRepetition() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setEventTimeRepeat(Event event) {


        final String query = "UPDATE oEvents " +
                "SET repeat=? " +
                "WHERE id=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setBoolean(1, event.isRepeat());
                queryStatement.setInt(2, event.getId());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the Event repeat to '" + event.getTimeRepetition() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean removeEvent(Event event){ return true; }
    public static boolean restoreEvent(){ return true; }

    public static boolean addPhase(Phases phase) {
        final String query = "INSERT INTO oPhases (idEvent, name, order, timeElapsed, maxTimeDuration) VALUES (?, ?, ?, ?, ?);";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, phase.getEventId());
                queryStatement.setString(2, phase.getName());
                queryStatement.setInt(3, phase.getOrder());
                queryStatement.setLong(4, phase.getTimeElapsed());
                queryStatement.setLong(5, phase.getMaxTimeDuration());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to insert new Phase into the database");
            Organisation.log.severe(e.getMessage());
            return false;
        }

        return true;
    }
    public static boolean setPhaseOrder(Phases phase) {


        final String query = "UPDATE oPhases " +
                "SET name=? " +
                "WHERE idEvent=? AND name=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, phase.getOrder());
                queryStatement.setInt(2, phase.getEventId());
                queryStatement.setString(3, phase.getName());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the Phase order to '" + phase.getOrder() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setPhaseTimeElapsed(Phases phase) {


        final String query = "UPDATE oPhases " +
                "SET timeElapsed=? " +
                "WHERE id=? AND name=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, phase.getTimeElapsed());
                queryStatement.setInt(2, phase.getEventId());
                queryStatement.setString(3, phase.getName());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the Phase timeElapsed to '" + phase.getTimeElapsed() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }
    public static boolean setPhaseMaxTimeDuration(Phases phase) {

        final String query = "UPDATE oPhases " +
                "SET maxTimeDuration=? " +
                "WHERE id=? AND name=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setLong(1, phase.getMaxTimeDuration());
                queryStatement.setInt(2, phase.getEventId());
                queryStatement.setString(3, phase.getName());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to set the Phase maxTimeDuration to '" + phase.getMaxTimeDuration() +  "' in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }

        return true;
    }
    public static boolean removePhase(Phases phase){
        final String queryDeletePhasesRewards = "DELETE FROM oRewards WHERE idEvent=? AND phaseName=?;";
        final String queryDeletePhase = "DELETE FROM oPhases WHERE idEvent=? AND name=?;";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeletePhasesRewards)) {
                queryStatement.setInt(1, phase.getEventId());
                queryStatement.setString(2, phase.getName());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete Phase "+ phase.getName()+ " in event " + phase.getEventId() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }

//
    public static boolean addReward(Rewards reward) {
        final String query = "INSERT INTO oRewards (idEvent, phaseName,name, item, amount) VALUES (?,? ?, ?, ?);";

        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(query)) {

                queryStatement.setInt(1, reward.getEventId());
                queryStatement.setString(2, reward.getPhaseName());
                queryStatement.setString(3, reward.getName());
                queryStatement.setString(4, ItemUtils.itemStackToBase64(reward.getItem())); // assuming you have this method
                queryStatement.setInt(5, reward.getAmout());

                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to insert new Reward into the database");
            Organisation.log.severe(e.getMessage());
            return false;
        }

        return true;
    }
    public static boolean removeReward(Rewards reward) {

        final String queryDeleteRewards = "DELETE FROM oRewards WHERE idEvent=? AND phaseName=? AND name=?;";
        try (Connection connection = DBConnection.sql.getConnection()) {
            try (PreparedStatement queryStatement = connection.prepareStatement(queryDeleteRewards)) {
                queryStatement.setInt(1, reward.getEventId());
                queryStatement.setString(2, reward.getPhaseName());
                queryStatement.setString(3, reward.getName());
                queryStatement.execute();
            }
        } catch (SQLException e) {
            Organisation.log.severe("Unable to delete Reward "+ reward.getName()+ " in phase " + reward.getPhaseName() + " for event " + reward.getEventId() + " in database.");
            Organisation.log.severe(e.getMessage());
            return false;
        }
        return true;
    }

}
