package fr.avatar_returns.organisation.storage;

import com.zaxxer.hikari.HikariDataSource;
import fr.avatar_returns.organisation.Organisation;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.*;
import java.util.logging.Logger;

public class Database {

    public          String           type;
    protected final Logger           log;
    protected final String           prefix;
    protected final String           dbprefix;
    private         Connection       connection;
    protected       HikariDataSource hikari;

    public Database(String type, final Logger log, final String prefix, final String dbprefix, HikariDataSource hikari) {
        this.type = type;
        this.log = log;
        this.prefix = prefix;
        this.dbprefix = dbprefix;
        this.hikari = hikari;
    }

    /**
     * Print information to console.
     *
     * @param message The string to print to console
     */
    protected void printInfo(final String message) {
        this.log.info(this.prefix + this.dbprefix + message);
    }

    /**
     * Print error to console.
     *
     * @param message The string to print to console
     * @param severe  If {@param severe} is true print an error, else print a
     *                warning
     */
    protected void printErr(final String message, final boolean severe) {
        if (severe) {
            this.log.severe(this.prefix + this.dbprefix + message);
        } else {
            this.log.warning(this.prefix + this.dbprefix + message);
        }
    }

    /**
     * Returns the current Connection.
     *
     * @return Connection if exists, else null
     */
    public Connection getConnection() {
        try {
            this.connection = this.hikari.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return this.connection;
    }


    /**
     * Close connection to Database.
     */
    public void close() {
        if (this.hikari != null) {
            this.hikari.close();
            this.hikari = null;
        } else {
            this.printErr("There was no SQL connection open.", false);
        }
    }

    /**
     * Queries the Database, for queries which modify data. Run async by
     * default.
     *
     * @param query Query to run
     */
    @Deprecated
    public void modifyQuery(final String query) {
        this.modifyQuery(this.getConnection(), query, true);
    }

    /**
     * Queries the Database, for queries which modify data. Run async by
     * default.
     *
     * @param connection Connection to use
     * @param query      Query to run
     */
    public void modifyQuery(Connection connection, final String query) {
        this.modifyQuery(connection, query, true);
    }

    /**
     * Queries the Databases, for queries which modify data.
     *
     * @param query Query to run
     * @param async If to run asynchronously
     */
    @Deprecated
    public void modifyQuery(final String query, final boolean async) {
        this.modifyQuery(this.getConnection(), query, async);
    }

    /**
     * Queries the Databases, for queries which modify data.
     *
     * @param connection Connection to use
     * @param query      Query to run
     * @param async      If to run asynchronously
     */
    public void modifyQuery(Connection connection, final String query, final boolean async) {
        if (async) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    Database.this.doQuery(connection, query);
                }
            }.runTaskAsynchronously(Organisation.plugin);
        } else {
            this.doQuery(connection, query);
        }
    }

    /**
     * Queries the Database, for queries which return results.
     *
     * @param query Query to run
     * @return Result set of ran query
     */
    @Deprecated
    public ResultSet readQuery(final String query) {
        try (Connection connection = this.getConnection()) {
            return this.readQuery(connection, query);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Queries the Database, for queries which return results.
     *
     * @param query Query to run
     * @return Result set of ran query
     */
    public ResultSet readQuery(Connection connection, final String query) {
        try {
            if (connection == null || connection.isClosed())
                connection = this.getConnection();
            return connection.prepareStatement(query).executeQuery();
        } catch (final SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Check database to see if a table exists.
     *
     * @param table Table name to check
     * @return true if table exists, else false
     */
    public boolean tableExists(final String table) {
        try (Connection connection = this.getConnection()) {
            return this.tableExists(connection, table);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Check database to see if a table exists.
     *
     * @param table Table name to check
     * @return true if table exists, else false
     */
    public boolean tableExists(Connection connection, final String table) {
        try {
            if (connection == null || connection.isClosed())
                return this.tableExists(table);
            final DatabaseMetaData dmd = connection.getMetaData();
            try (ResultSet rs = dmd.getTables(null, null, table, null)) {
                return rs.next();
            }
        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Check database to see if column exists within table.
     *
     * @param table  Table name to check
     * @param column Column name to check
     * @return true if column exists within table, else false
     */
    public boolean columnExists(final String table, final String column) {
        try (Connection connection = this.getConnection()) {
            return this.columnExists(connection, table, column);
        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Check database to see if column exists within table.
     *
     * @param table  Table name to check
     * @param column Column name to check
     * @return true if column exists within table, else false
     */
    public boolean columnExists(Connection connection, final String table, final String column) {
        try {
            if (connection == null || connection.isClosed())
                return this.columnExists(table, column);
            final DatabaseMetaData dmd = connection.getMetaData();
            try (ResultSet rs = dmd.getColumns(null, null, table, column)) {
                return rs.next();
            }
        } catch (final Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private synchronized void doQuery(final String query) {
        try (Connection connection = this.getConnection()) {
            this.doQuery(connection, query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private synchronized void doQuery(Connection connection, final String query) {
        try {
            if (connection == null || connection.isClosed()) {
                this.doQuery(query);
                return;
            }
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.execute();
            }
        } catch (final SQLException e) {
            e.printStackTrace();
        }
    }


}
