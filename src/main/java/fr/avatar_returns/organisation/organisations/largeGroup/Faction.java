package fr.avatar_returns.organisation.organisations.largeGroup;

import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.group.FactionPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.group.GuildePacket;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Faction extends LargeGroup {

    public Faction(String name){
        super(name,GeneralMethods.getIntConf("Organisation.Faction.maxPlayer"), GeneralMethods.getIntConf("Organisation.Faction.minPlayer"), GeneralMethods.getIntConf("Organisation.Faction.maxLand"),GeneralMethods.getIntConf("Organisation.Faction.maxRaid"));
        GeneralMethods.sendPacket(new FactionPacket(this, OrganisationPacket.Action.CREATE));
    }
    public Faction(int id, String name, int maxPlayer, int minPlayer, int maxLand, int maxRaid, String description, String connexionMessage, GroupRank rank, ItemStack flagItem, boolean isVisible, int daily, long nextimeAllowedWar, boolean canUnClaim){
        super(id,name,maxPlayer, minPlayer, maxLand, maxRaid, description, connexionMessage, rank, flagItem, isVisible, daily, nextimeAllowedWar, canUnClaim);
    }

    //Getter
    public static ArrayList<Faction> getLstFaction(){
        return Faction.getLstFaction(true);
    }
    public static ArrayList<Faction> getLstFaction(boolean displayHide) {
        ArrayList<Faction> lstFaction = new ArrayList<>();

        for(Group group : Group.getLstGroup(displayHide)){
            if(group instanceof Faction) lstFaction.add((Faction)group);
        }

        return lstFaction;
    }

}
