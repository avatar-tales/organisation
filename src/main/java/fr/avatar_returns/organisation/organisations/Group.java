package fr.avatar_returns.organisation.organisations;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.exception.OrganisationGroupPlayerException;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.largeGroup.Faction;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.organisations.utils.Permission;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.rank.GroupObjective;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.view.GeneralItem;
import fr.avatar_returns.organisation.view.ui.objectiveUI.ObjectiveUI;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scoreboard.Team;
import org.jetbrains.annotations.NotNull;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public abstract class Group {

    private static ArrayList<Group> lstGroupe = new ArrayList<>();

    protected int id;
    protected String name;
    protected String description = "";
    protected int maxPlayer;
    protected int minPlayer;
    private GroupRank rank;
    private boolean isVisible;

    protected ArrayList<OrganisationMember> lstMember;
    protected ArrayList<Role> lstRole;
    private String connexionMessage;
    private transient @NotNull Team team;
    private transient ItemStack flag;
    private ArrayList<GroupObjective> lstGroupObjective = null;

    private boolean canUnClaim;

    public Group(String name,int maxPlayer, int minPlayer, GroupRank groupRank){

        this.name = name;
        this.description = "";
        this.connexionMessage = "";
        this.maxPlayer = maxPlayer;
        this.minPlayer = minPlayer;
        this.rank = groupRank;
        this.isVisible = true;
        this.canUnClaim = true;

        this.setTeam();

        this.lstMember = new ArrayList<>();
        this.lstRole = new ArrayList<>();
        this.lstGroupObjective = new ArrayList<>();

        Group.lstGroupe.add(this);
    }
    public Group(int id, String name, int maxPlayer, int minPlayer, String description, String connexionMessage, GroupRank groupRank, ItemStack flag, boolean isVisible, boolean canUnClaim){

        this.id = id;
        this.name = name;
        this.description = description;
        this.maxPlayer = maxPlayer;
        this.minPlayer = minPlayer;
        this.lstMember = new ArrayList<>();
        this.lstRole = new ArrayList<>();
        this.connexionMessage = connexionMessage;
        this.rank = groupRank;
        this.flag = flag;
        this.isVisible = isVisible;
        this.lstGroupObjective = new ArrayList<>();
        this.canUnClaim = canUnClaim;

        this.team = Organisation.board.getTeam(this.name);
        if(this.team == null){
            Organisation.log.info("Unable to load '"+this.name+"' team name. We proceed to a new team registration");
        }
        this.setTeam();

        Group.lstGroupe.add(this);
    }

    public void initDefaultRole(){

        // Generate default role
        Role recrueRole = new Role("Recrue",-1,this);
        Role memberRole = new Role("Membre",1000,this);
        Role officerRole = new Role("Officier",10,this);
        Role leaderRole = new Role("Chef",0,this);

        // Generate Leader default permissions.
        leaderRole.setLstPermission(Permission.getAllPermissions());

        // Generate officier default permissions.
        ArrayList<String> officerPermission = new ArrayList<>();
        officerPermission.addAll(Permission.getDiplomatiePermissions());
        officerPermission.addAll(Permission.getInteractPermissions());
        officerPermission.addAll(Permission.getBuildPermissions());
        officerPermission.addAll(Permission.getLandPermissions());
        officerRole.setLstPermission(officerPermission);

        // Generate member's default permissions.
        ArrayList<String> memberPermission = new ArrayList<>();
        memberPermission.addAll(Permission.getBuildPermissions());
        memberPermission.addAll(Permission.getInteractPermissions());
        memberRole.setLstPermission(memberPermission);

        //Generate recrue's default permissions.
        ArrayList<String> recruePermission = new ArrayList<>();
        recruePermission.addAll(Permission.getInteractPermissions());
        recrueRole.setLstPermission(recruePermission);

        //Add roles
        this.lstRole.add(leaderRole);
        this.lstRole.add(officerRole);
        this.lstRole.add(memberRole);
        this.lstRole.add(recrueRole);
    }

    public boolean addRole(Role role){
        return this.addRole(role, true);
    }
    public boolean addRole(Role role, boolean storeDb){


        //Check if role currently exist.
        for (Role groupRole : this.getLstRole()){
            if(groupRole.equals(role)){
                return false;
            }

            if(groupRole.getRolePower() == 0 && role.getRolePower() == 0)return false; //Max one leader group.
            if(groupRole.getRolePower() < 0 && role.getRolePower() < 0)return false;   //Max one default group.

        }

        this.lstRole.add(role);
        if(storeDb)DBUtils.addRole(role);
        return true;
    }

    public boolean removeRole(Role role){return this.removeRole(role, true);}
    public boolean removeRole(Role role, boolean storeDb){


        if(role.isLeader() || role.isRecrue())return false;

        if(this.lstRole.contains(role)) {

            for(OrganisationMember roleMember: this.getLstMember()){
                //Move player of delete role to the default role.
                if(roleMember.getRole().equals(role))roleMember.setRole(this.getDefaultRole());
            }

            this.lstRole.remove(role);
            if(storeDb)DBUtils.deleteRole(role);
            return true;
        }
        return false;
    }

    public boolean addMember(OfflinePlayer player) throws OrganisationGroupPlayerException {

        OrganisationPlayer organisationPlayer;
        try {
            organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException e) {
            throw new OrganisationGroupPlayerException(this, player);
        }
        return this.addMember(organisationPlayer);
    }
    public boolean addMember(OrganisationPlayer organisationPlayer){

        if(!organisationPlayer.canJoinOrganisation(this))return false;

        Role newMemberRole = (this.getLstMember().size() == 0)? this.getLeadertRole() : this.getDefaultRole();
        OrganisationMember newMember = new OrganisationMember(this,organisationPlayer, newMemberRole);
        if(organisationPlayer.getOfflinePlayer() != null) {
            if(this instanceof LargeGroup) this.team.addPlayer(organisationPlayer.getOfflinePlayer());
        }

        this.lstMember.add(newMember);
        return true;
    }

    public boolean hasPermission(OfflinePlayer player,Enum perm){
        return hasPermission(player,perm.toString());
    }
    public boolean hasPermission(OfflinePlayer player,String perm){

        OrganisationMember member = null;
        for (OrganisationMember organisationMember: this.getLstMember()){
            if (organisationMember.getOfflinePlayer().getUniqueId().equals(player.getUniqueId())) {
                member = organisationMember;
            }
        }

        if (member == null)return false;
        return member.hasPermission(perm);
    }

    public boolean removeMember(OfflinePlayer player) throws OrganisationGroupPlayerException {return this.removeMember(player, true);}
    public boolean removeMember(OfflinePlayer player, boolean storeDb) throws OrganisationGroupPlayerException {

        OrganisationPlayer organisationPlayer;
        try {
            organisationPlayer = OrganisationPlayer.getOrganisationPlayer(player);
        } catch (OrganisationPlayerException e) {
            throw new OrganisationGroupPlayerException(this, player);
        }

        return this.removeMember(organisationPlayer, storeDb);
    }

    public boolean removeMember(OrganisationPlayer organisationPlayer){return this.removeMember(organisationPlayer, true);}
    public boolean removeMember(OrganisationPlayer organisationPlayer, boolean storeDb) {

        for(OrganisationMember member : this.getLstMember()){
            if(organisationPlayer.equals(member.getOrganisationPlayer())){

                this.lstMember.remove(member);
                if(member.getOfflinePlayer() != null) {
                    try {
                        Organisation.getDefaultTeam().addPlayer(member.getOfflinePlayer());
                    }
                    catch (Exception e) {}
                }
                if(storeDb) DBUtils.deleteGroupMember(this, member);

                return true;
            }
        }
        return false;
    }

    //Setter
    public void setName(String name){this.setName(name, true);}
    public void setName(String name, boolean storeDb){

        if(this.getTeam() != null) this.getTeam().unregister();
        if(this instanceof LargeGroup) {
            this.team = Organisation.board.registerNewTeam(name);

            for (OfflinePlayer offlinePlayer : this.getLocalPlayers()) {
                if (offlinePlayer == null) this.team.addPlayer(offlinePlayer);
            }
        }

        this.name= name;
        if(storeDb)DBUtils.setGroupName(this, name);
    }

    public void setDescription(String description) {
        this.setDescription(description, true);
    }
    public void setDescription(String description, boolean storeDb) {
        this.description = description;
        if(storeDb)DBUtils.setGroupDesc(this);
    }

    public void setConnexionMessage(String connexionMessage) {
       this.setConnexionMessage(connexionMessage, true);
    }
    public void setConnexionMessage(String connexionMessage, boolean storeDb) {
        this.connexionMessage = connexionMessage;
        if(storeDb)DBUtils.setConnexionMessage(this);
    }

    public void setMaxPlayer(int maxPlayer) {this.setMaxPlayer(maxPlayer, true);}
    public void setMaxPlayer(int maxPlayer, boolean storeDb) {

        this.maxPlayer = maxPlayer;
        if(storeDb)DBUtils.setGroupMaxPlayer(this);
    }

    public void setMinPlayer(int minPlayer) {this.setMinPlayer(minPlayer, false);}
    public void setMinPlayer(int minPlayer, boolean storeDb){
        this.minPlayer = minPlayer;
        if(storeDb)DBUtils.setGroupMinPlayer(this);
    }

    public boolean setRank(GroupRank rank){
        return this.setRank(rank, true);
    }
    public boolean setRank(GroupRank rank, boolean storeDb){

        this.lstGroupObjective = GroupObjective.resetObjective(this);
        this.rank = rank;
        if(this.getMaxPlayer() < rank.getMaxPlayer())this.setMaxPlayer(rank.getMaxPlayer(), storeDb);
        this.lstGroupObjective = GroupObjective.resetObjective(this);
        if(storeDb)return DBUtils.setGroupRank(this);
        return true;
    }

    public boolean setCanUnClaim(boolean canUnClaim){return this.setCanUnClaim(canUnClaim, true);}
    public boolean setCanUnClaim(boolean canUnClaim, boolean storeDB){

        this.canUnClaim = canUnClaim;
        if(storeDB)DBUtils.setGroupCanUnClaim(this);

        return true;
    }

    public void setIsVisible(boolean isVisible){
        this.setIsVisible(isVisible, true);
    }
    public void setIsVisible(boolean isVisible, boolean storeDb){
        this.isVisible = isVisible;
        if(storeDb)DBUtils.setIsVisible(this);
    }

    public void setFlag(ItemStack flag){
        this.setFlag(flag, true);
    }
    public void setFlag(ItemStack flag, boolean storeDb){
        this.flag = flag;
        if(storeDb) DBUtils.setFlagItem(this);

    }

    public void setLstGroupObjective(ArrayList<GroupObjective> lstGroupObjective){
        this.lstGroupObjective = lstGroupObjective;
    }

    public int getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
    public String getConnexionMessage() { return this.connexionMessage; }
    public String getDescription() {
        return this.description;
    }
    public int getMinPlayer() {
        return this.minPlayer;
    }
    public int getMaxPlayer() { return this.maxPlayer; }
    public boolean canUnClaim(){return this.canUnClaim;}
    public GroupRank getRank(){return this.rank;}
    @Nullable public ItemStack getFlag() {
        return this.flag;
    }
    @Nullable private Team setTeam(){

        if(!(this instanceof LargeGroup)){
            this.team = null;
            return null;
        }

        Team localTeam = Organisation.board.getTeam(this.name);
        this.team = (localTeam == null)?Organisation.board.registerNewTeam(this.name) : localTeam;

        if(GeneralMethods.getBooleanConf("Organisation.Faction.AllowFriendlyFire"))this.team.setAllowFriendlyFire(true);
        else this.team.setAllowFriendlyFire(false);

        this.team.setOption(Team.Option.NAME_TAG_VISIBILITY,Team.OptionStatus.FOR_OTHER_TEAMS);

        return this.team;
    }
    @Nullable public Team getTeam(){
        return this.team;
    }
    public boolean isVisible(){
        return isVisible;
    }
    public ArrayList<OrganisationMember> getLstMember() {
        return lstMember;
    }

    @Nullable public OrganisationMember getOrganisationMember(OrganisationPlayer organisationPlayer){
        for(OrganisationMember organisationMember : this.getLstMember()){
            if(organisationMember.getOrganisationPlayer().equals(organisationPlayer)) return organisationMember;
        }
        return null;
    }
    @Nullable public OrganisationMember getOrganisationMember(OfflinePlayer player){
        for(OrganisationMember organisationMember : this.getLstMember()){
            if(organisationMember.getOfflinePlayer().equals(player)) return organisationMember;
        }
        return null;
    }
    public ArrayList<OfflinePlayer> getLocalOnlinePlayers(){
        /***
         * Get only local online player of the group
         */

        ArrayList<OfflinePlayer> lstOnlinePlayer = new ArrayList<OfflinePlayer>();

        for(OrganisationMember organisationMember : this.getLstMember() ){
            if(organisationMember.getOfflinePlayer().isOnline())lstOnlinePlayer.add(organisationMember.getOfflinePlayer());
        }
        return lstOnlinePlayer;
    }
    public ArrayList<OfflinePlayer> getLocalOffLinePlayers(){
        /***
         * Get only local offline player of the group
         */

        ArrayList<OfflinePlayer> lstOffLinePlayer = new ArrayList<OfflinePlayer>();

        for(OrganisationMember organisationMember : this.getLstMember() ){
            if(!organisationMember.getOfflinePlayer().isOnline())lstOffLinePlayer.add(organisationMember.getOfflinePlayer());
        }
        return lstOffLinePlayer;
    }
    public ArrayList<OfflinePlayer> getLocalPlayers() {
        /***
         * Get all group player (offline and online)
         */

        ArrayList<OfflinePlayer> lstPlayers = new ArrayList<OfflinePlayer>();
        for(OrganisationMember organisationMember : this.getLstMember()){
            lstPlayers.add(organisationMember.getOfflinePlayer());
        }

        return lstPlayers;
    }

    public ArrayList<OrganisationPlayer> getLstGroupOrganisationPlayer() {
        ArrayList<OrganisationPlayer> lstOrganisationPlayer = new ArrayList<>();
        for (OrganisationMember organisationMember : this.getLstMember()) lstOrganisationPlayer.add(organisationMember.getOrganisationPlayer());
        return lstOrganisationPlayer;
    }
    public ArrayList<OrganisationPlayer> getLstGroupConnectedOrganisationPlayer(){
        ArrayList<OrganisationPlayer> lstConnectedOrganisationPlayer = new ArrayList<>();
        for (OrganisationMember organisationMember : this.getLstGroupConnectedMember()) {
            lstConnectedOrganisationPlayer.add(organisationMember.getOrganisationPlayer());
        }
        return lstConnectedOrganisationPlayer;
    }
    public ArrayList<OrganisationMember> getLstGroupConnectedMember(){
        ArrayList<OrganisationMember> lstConnectedOrganisationMember = new ArrayList<>();
        for (OrganisationMember organisationMember : this.getLstMember()) {
            if(organisationMember.isConnected()) lstConnectedOrganisationMember.add(organisationMember);
        }
        return lstConnectedOrganisationMember;
    }

    public ArrayList<OrganisationMember> getLstGroupNotConnectedMember(){
        ArrayList<OrganisationMember> lstNotConnectedOrganisationMember = new ArrayList<>();
        for (OrganisationMember organisationMember : this.getLstMember()) {
            if(!organisationMember.isConnected()) lstNotConnectedOrganisationMember.add(organisationMember);
        }
        return lstNotConnectedOrganisationMember;
    }

    @Nullable public Role getDefaultRole(){
        for(Role role : this.getLstRole()){
            if(role.getRolePower() == -1)return role;
        }
        return null;
    }
    @Nullable public Role getLeadertRole(){
        for(Role role : this.getLstRole()){
            if(role.getRolePower() == 0)return role;
        }
        Organisation.log.severe("No leader role find");
        return null;
    }
    public ArrayList<Role> getLstRole() {
        Collections.sort(this.lstRole, (a, b) -> b.compareTo(a));
        return this.lstRole;
    }
    public ArrayList<GroupObjective> getLstGroupObjective(){
        return this.lstGroupObjective;
    }

    //Static Method
    public static boolean remove(Group group){return Group.remove(group, true);}
    public static boolean remove(Group group, boolean storeDb)  {

        //Remove the group from the global group list
        for(OfflinePlayer player: group.getLocalPlayers()){
            if(player != null)Organisation.getDefaultTeam().addPlayer(player);
        }

        if(group instanceof LargeGroup){
            for (LargeGroup vassal : ((LargeGroup) group).getLstVassal())vassal.setLord(null, storeDb);

            //Update Land.
            for(Land land: Land.getLstLandByGroup(group)){
                land.setOwner(null, storeDb);
            }
        }

        if(group.getTeam() != null) group.getTeam().unregister();
        Group.getLstGroup().remove(group);

        if(storeDb)DBUtils.deleteGroup(group);
        return true;
    }
    public static int getNbGroup() {return lstGroupe.size();}
    public static ArrayList<Group> getLstGroup() {return lstGroupe;}
    public static ArrayList<Group> getLstGroup(boolean displayHide) {

        if(displayHide)return Group.getLstGroup();

        ArrayList<Group> lstHidenGroupe = new ArrayList<>();
        for(Group group : Group.getLstGroup()){
            if(group.isVisible())lstHidenGroupe.add(group);
        }
        return lstHidenGroupe;
    }

    @Nullable public static Group getGroupByName(String name){

        for(Group group : Group.getLstGroup()){
            if (group.getName().equalsIgnoreCase(name)){
                return group;
            }
        }
        return null;
    }
    @Nullable public static Group getGroupById(int id){
        for(Group group : Group.getLstGroup()){
            if (group.getId() == id){
                return group;
            }
        }
        return null;
    }

    //Override
    @Override public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", maxPlayer=" + maxPlayer +
                ", minPlayer=" + minPlayer +
                ", lstMember=" + lstMember +
                '}';
    }
    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;
        Group group = (Group) o;
        return getId() == group.getId() &&
                getMaxPlayer() == group.getMaxPlayer() &&
                minPlayer == group.minPlayer &&
                Objects.equals(getName(), group.getName()) &&
                Objects.equals(getLstMember(), group.getLstMember());
    }
    @Override public int hashCode() {
        return Objects.hash(getId(), getName(), getMaxPlayer(), minPlayer, getLstMember());
    }

    public Inventory getBoard(OrganisationPlayer oPlayer ) { return new ObjectiveUI(this, oPlayer).getInv();}
    public ItemStack getItemRank(){
        ItemStack rank = (this.getFlag() != null)?this.getFlag().clone() : new ItemStack(Material.RED_BANNER);
        GeneralItem.cleanItemMeta(rank);

        ItemMeta rankMeta = rank.getItemMeta();
        rankMeta.setDisplayName("§6§l"+this.getName().replace("_"," "));
        ArrayList<Component> groupDesc = new ArrayList<>();
        groupDesc.add(Component.text("§8Rang : §7" + this.getRank().getName()));
        groupDesc.add(Component.text("§8Membres : §7" + this.getLstMember().size()+"/"+this.getMaxPlayer()));
        if(this instanceof LargeGroup){
            LargeGroup largeGroup = (LargeGroup) this;
            groupDesc.add(Component.text("§8Land : §7"+largeGroup.getLstVland().size() + "/" + largeGroup.getMaxLand()));
            if(this.getRank().isCanVassalize()) groupDesc.add(Component.text("§8Nombre vassaux : §7"+largeGroup.getLstVassal().size() + "/" + ((LargeGroup) this).getMaxVassal()));
        }
        rankMeta.lore(groupDesc);

        rank.setItemMeta(rankMeta);
        return rank.clone();
    }

    public ArrayList<Invitation> getLstInvitation() {
        ArrayList<Invitation> lstGroupInvitation = new ArrayList<>();
        for(Invitation invitation : InviteCommand.lstInvitation){
            if(invitation.getGroupHost().getName().equals(this.getName())){
                lstGroupInvitation.add(invitation);
            }
        }
        return lstGroupInvitation;
    }
}
