package fr.avatar_returns.organisation.organisations.simpleGroup;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.largeGroup.Faction;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.group.FactionPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.group.GuildePacket;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Guilde extends Group {

    private static int maxMultiGuilde =  GeneralMethods.getIntConf("Organisation.Guilde.maxMultiGuilde");

    public Guilde(String name) {
        super(name, GeneralMethods.getIntConf("Organisation.Guilde.maxPlayer"), GeneralMethods.getIntConf("Organisation.Guilde.minPlayer"), Guilde.getDefaultGroupRank());
        this.id = DBUtils.addGroup(this);
        GeneralMethods.sendPacket(new GuildePacket(this, OrganisationPacket.Action.CREATE));
    }

    public Guilde(int id, String name, int maxPlayer, int minPlayer, String description, String connexionMessage, GroupRank rank, ItemStack flagItem, boolean isVisible, boolean canUnClaim) {
        super(id, name, maxPlayer, minPlayer, description, connexionMessage, rank, flagItem, isVisible, canUnClaim);
    }

    public static GroupRank getDefaultGroupRank(){
        for (GroupRank groupRank : GroupRank.getLstGroupRank()){
            if(groupRank.getGroupType().equalsIgnoreCase("GUILDE") && groupRank.getOrder() == 0){
                return groupRank;
            }
        }

        Organisation.log.warning("Unable to find Guilde default GroupRank : I created it");
        GroupRank defaultGroupRank = new GroupRank("Guilde",0, "GUILDE", 0,0,7, false);
        return defaultGroupRank;
    }

    public static ArrayList<Guilde> getLstGuilde() {
        return Guilde.getLstGuilde(true);
    }
    public static ArrayList<Guilde> getLstGuilde(boolean displayHide) {
       ArrayList<Guilde> lstGuilde = new ArrayList<>();
       for(Group group: Group.getLstGroup(displayHide)) {
            if(group instanceof Guilde) lstGuilde.add((Guilde) group);
       }
       return lstGuilde;
    }

    public static int getMaxMultiGuilde() {
        return maxMultiGuilde;
    }
}
