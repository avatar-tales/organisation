package fr.avatar_returns.organisation.organisations;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.view.GeneralItem;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;
import org.bukkit.OfflinePlayer;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;


public class OrganisationMember{

    private OrganisationPlayer organisationPlayer;

    private Role role;
    private final long firstTimeConnexion;
    private long lastEffectivePlayedTime;
    private long lastTimeConnexionTime;

    public OrganisationMember(Group group,OrganisationPlayer organisationPlayer,Role role) {

        this.setOrganisationPlayer(organisationPlayer);

        this.firstTimeConnexion = System.currentTimeMillis();
        this.lastEffectivePlayedTime = 0;
        this.lastTimeConnexionTime = System.currentTimeMillis();

        this.role = role;
        DBUtils.addGroupMember(group, this);
    }
    public OrganisationMember(Group group,OrganisationPlayer organisationPlayer,Role role, long firstTimeConnexion, long effectivePlayedTime) {

        this.setOrganisationPlayer(organisationPlayer);

        this.firstTimeConnexion = firstTimeConnexion;
        this.lastEffectivePlayedTime = effectivePlayedTime;
        this.lastTimeConnexionTime = System.currentTimeMillis();

        this.role = role;
    }

    public OfflinePlayer getOfflinePlayer(){
        return getOrganisationPlayer().getOfflinePlayer();
    }
    public Role getRole() {
        return role;
    }
    public String getName(){return this.getOrganisationPlayer().getName();}
    public OrganisationPlayer getOrganisationPlayer() {
            return organisationPlayer;
    }
    @Nullable public Group getGroup() {
        for(Group group : Group.getLstGroup()){
            for(OrganisationMember organisationMember: group.getLstMember()){
                if(organisationMember.equals(this))return group;
            }
        }
        return null;
    }

    private void setOrganisationPlayer(OrganisationPlayer organisationPlayer) {
        this.organisationPlayer = organisationPlayer;
    }

    public void setRole(Role role) {this.setRole(role, true);}
    public void setRole(Role role, boolean storeDb) {
        this.role = role;
        if(storeDb)DBUtils.changeRoleGroupMember(role,this);
    }

    public boolean isLeader() {
        return this.getRole().isLeader();
    }
    public boolean isRecrue(){
        return this.getRole().isRecrue();
    }
    public boolean isConnected(){return this.organisationPlayer.isConnected();}

    public boolean hasPermission(Enum permission) {
        return this.hasPermission(permission.toString());
    }
    public boolean hasPermission(String permission) {
        if (this.getOfflinePlayer().isOp())return true;
        if(this.getOfflinePlayer().getPlayer() != null){
            if(this.getOfflinePlayer().getPlayer().hasPermission("organisation.command.role.admin")) return true;
        }

        return this.getRole().hasPermission(permission);
    }

    public final long getFirstTimeConnexion(){return this.firstTimeConnexion;}
    public final long getLastEffectivePlayedTime(){return this.lastEffectivePlayedTime;}
    public long getEffectivePlayedTime() {
        long currentTime = System.currentTimeMillis();
        this.lastEffectivePlayedTime += (currentTime - this.getLastTimeConnexion());

        //Secure way to be sure an past time will not be count two times.
        this.setLastConnexionTime();
        return  this.lastEffectivePlayedTime;
    }
    private long getLastTimeConnexion() {
        return this.lastTimeConnexionTime;
    }
    public void setLastConnexionTime() {
        this.lastTimeConnexionTime = System.currentTimeMillis();
    }

    public ItemStack getOMemberItem(){
        ItemStack item = this.organisationPlayer.getHead();

        item = GeneralItem.renameItemStackWithName(item, "§8["+this.getRole().getName()+"§8] §7" + this.getName());
        String timeInGroup = "";

        long effectivePlayedTime = this.getEffectivePlayedTime();
        long seconds = effectivePlayedTime / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        long remainingDays = days % 365;
        long remainingHours = hours % 24;
        long remainingMinutes = minutes % 60;
        long remainingSeconds = seconds % 60;

        String min_str = (remainingMinutes < 9)? "0"+remainingMinutes : ""+remainingMinutes;
        String sec_str = (remainingSeconds < 9)? "0"+remainingSeconds : ""+remainingSeconds;

        timeInGroup = remainingHours+"h" + min_str + ":" + sec_str;
        if(remainingDays > 0) timeInGroup = remainingDays + " jours, " + timeInGroup;

        Date date = new Date(this.getFirstTimeConnexion());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy  HH:mm:ss");
        String formattedDate = sdf.format(date);

        GeneralItem.setLoreFromList(item, Arrays.asList(
                "§8Role : §7" + this.getRole().getName(),
                "§8Temps de jeux dans le groupe : §7" + timeInGroup,
                "§8Arrivé dans le groupe : §7" + formattedDate));

        return item;
    }
}
