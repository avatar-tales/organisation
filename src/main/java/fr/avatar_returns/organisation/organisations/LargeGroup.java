package fr.avatar_returns.organisation.organisations;


import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.organisations.largeGroup.Faction;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.group.FactionPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.group.GuildePacket;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.rank.Rank;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.territory.Territory;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.block.Banner;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Date;

public abstract class LargeGroup extends Group {

    private ArrayList<Territory> lstTerritory;
    private int maxLand;
    private int maxRaid;
    private int dailyRaid;
    private long nextimeAllowedWar;
    private LargeGroup lord;


    public LargeGroup(String name, int maxPlayer, int minPlayer, int maxLand, int maxRaid) {

        super(name, maxPlayer, minPlayer, LargeGroup.getDefaultGroupRank());
        this.maxLand = maxLand;
        this.maxRaid = maxRaid;
        this.dailyRaid = 0;
        this.nextimeAllowedWar = 0;

        this.id = DBUtils.addGroup(this);

        lstTerritory = new ArrayList<>();
    }
    public LargeGroup(int id, String name, int maxPlayer, int minPlayer, int maxLand, int maxRaid, String description, String connexionMessage, GroupRank rank, ItemStack flagItem, boolean isVisible, int daily, long nextimeAllowedWar, boolean canUnClaim) {
        super(id, name, maxPlayer, minPlayer, description, connexionMessage, rank, flagItem, isVisible, canUnClaim);
        this.maxLand = maxLand;
        this.maxRaid = maxRaid;
        this.dailyRaid = daily;
        this.nextimeAllowedWar = nextimeAllowedWar;

        lstTerritory = new ArrayList<>();
    }

    public static GroupRank getDefaultGroupRank(){
        for (GroupRank groupRank : GroupRank.getLstGroupRank()){
            if(groupRank.getGroupType().equalsIgnoreCase("FACTION") && groupRank.getOrder() == 0){
                return groupRank;
            }
        }

        Organisation.log.warning("Unable to find LargeGroup default GroupRank : I created it");
        GroupRank defaultGroupRank = new GroupRank("Tribu",0, "FACTION", 1,0,5, false);
        return defaultGroupRank;
    }
    public static ArrayList<LargeGroup> getLstLargeGroup() {
        ArrayList<LargeGroup> lstLargeGroup = new ArrayList<>();
        for (Group group : Group.getLstGroup()){
            if(group instanceof LargeGroup) lstLargeGroup.add((LargeGroup)group);
        }
        return lstLargeGroup;
    }
    public ArrayList<Territory> getLstTerritory() {
        return lstTerritory;
    }

    public ArrayList<VLand> getLstVland(){

        ArrayList<VLand> groupLstVland = new ArrayList<>();
        for(VLand vLand: VLand.getLstVland()){
            if(vLand.getOwnerId() == this.getId())groupLstVland.add(vLand);
        }
        return groupLstVland;
    }
    public ArrayList<Land> getLstLand(){

        ArrayList<Land> lstLand = new ArrayList<>();

        for(Land land :Land.getAllLand()){
            if(land.getOwner() != null){
                if(land.getOwner().equals(this))lstLand.add(land);
            }
        }
        return lstLand;
    }

    public int getMaxLand(){return this.maxLand;}
    public int getMaxRaid(){return this.maxRaid;}
    public int getMaxVassal(){return this.getRank().getMaxVassal();}
    public boolean isCanVassalize(){return this.getRank().isCanVassalize();}

    public boolean setLord(LargeGroup largeGroup){return this.setLord(largeGroup, true);}
    public boolean setLord(LargeGroup largeGroup, boolean storeDb){
        this.lord = largeGroup;
        if(storeDb) return DBUtils.setGroupLord(largeGroup);
        return true;
    }

    @Nullable public LargeGroup getLord(){
        return this.lord;
    }
    public ArrayList<LargeGroup> getLstVassal(){
        ArrayList<LargeGroup> lstVassal = new ArrayList<>();

        for (LargeGroup largeGroup : LargeGroup.getLstLargeGroup()) {
            if (largeGroup.getLord() == this) lstVassal.add(largeGroup);
        }

        return lstVassal;
    }


    @Nullable public static LargeGroup getLargeGroupByName(String name){
        Group  group = Group.getGroupByName(name);
        if(group instanceof LargeGroup)return (LargeGroup) group;
        return null;
    }
    @Nullable public Land getMainLand() {
        for(Land land: this.getLstLand()){
            if(land.isMainLand())return land;
        }
        return null;
    }

    public int getDailyRaid() {
        return dailyRaid;
    }
    public long getNextimeAllowedWar() {
        return this.nextimeAllowedWar;
    }

    public boolean joinLand(Land land){

        ArrayList<Land> lstLargeGroupLand = this.getLstLand();
        if(lstLargeGroupLand.contains(land))return true;
        if(!land.isOwnableLand())return false;
        if(lstLargeGroupLand.size()>=this.getMaxLand())return false;

        return land.setOwner(this);
    }
    public boolean leaveLand(Land land){
        ArrayList<Land> lstLargeGroupLand = this.getLstLand();
        if(land.isMainLand())return false;

        return land.setOwner(null);
    }

    public void setDailyRaid(int dailyRaid){this.setDailyRaid(dailyRaid, true);}
    public void setDailyRaid(int dailyRaid, boolean storeDb) {
        this.dailyRaid = dailyRaid;
        if(storeDb) DBUtils.setDailyRaid(this);
    }

    public void setNextimeAllowedWar(long nextimeAllowedWar){this.setNextimeAllowedWar(nextimeAllowedWar, true);}
    public void setNextimeAllowedWar(long nextimeAllowedWar, boolean storeDb) {
        this.nextimeAllowedWar = nextimeAllowedWar;
        if(storeDb) DBUtils.setNextimeAllowedWar(this);
    }

    public void setMaxRaid(int maxRaid) {
        this.setMaxRaid(maxRaid, true);
    }
    public void setMaxRaid(int maxRaid, boolean storeDb) {
        this.maxRaid = maxRaid;
        if(storeDb)DBUtils.setLargeGroupMaxRaid(this);
    }

    public void setMaxLand(int maxLand) {
        this.setMaxLand(maxLand, true);
    }
    public void setMaxLand(int maxLand, boolean storeDb) {
        if(storeDb)DBUtils.setLargeGroupMaxLand(this);
    }

    @Override public boolean setRank(GroupRank groupRank, boolean storeDb){
        boolean result = super.setRank(groupRank, storeDb);
        if(this.getMaxLand() < this.getRank().getMaxLand()) this.setMaxLand(this.getRank().getMaxLand(),storeDb);
        return result;
    }

    public void updateBanner(){
        for(Land land : this.getLstLand()){
            for(PositionPointInterest positionPointInterest : land.getPositionPointOfInterest()){
                PositionPointInterest.setBanner(positionPointInterest,this);
            }
        }
    }

}
