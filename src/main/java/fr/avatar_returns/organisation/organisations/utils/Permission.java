package fr.avatar_returns.organisation.organisations.utils;

import fr.avatar_returns.organisation.view.GeneralItem;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public abstract class Permission {

    private static final String organisation = "organisation";
    private static final String interactAlias = organisation + ".interact";
    private static final String diplomatieAlias = organisation + ".diplomatie";
    private static final String landAlias = organisation + ".land";
    private static final String buildAlias = organisation + ".build";
    private static final String  managementAlias = organisation + ".management";

    public enum management {
        // Management permissions

        disband(managementAlias + ".disband"),
        kick(managementAlias + ".kick"),
        invite(managementAlias + ".invite"),
        promote(managementAlias + ".promote"),
        createRole(managementAlias + ".createRole"),
        renameRole(managementAlias + ".renameRole"),
        removeRole(managementAlias + ".removeRole"),
        setPower(managementAlias + ".setPower"),
        setPermission(managementAlias + ".setPermission"),
        setDescription(managementAlias + ".setDescription"),
        setConnexionMessage(managementAlias + ".setConnexionMessage"),
        mute(managementAlias + ".mute");

        private String permission;

        management(String permission) {
            this.permission = permission;
        }

        public String getPermission(){return this.permission;}
        public boolean equals (String perm){ return perm.equals(this.permission);}
        public boolean equalsIgnoreCase(String perm) {return perm.equalsIgnoreCase(this.permission);}

        @Override
        public String toString() {
            return this.getPermission();
        }
    }
    public enum build {
        // Construction permissions

        placeBlock(buildAlias + ".placeBlock"),
        breakBlock(buildAlias + ".BreakBlock");

        private String permission;

        build(String permission) {
            this.permission = permission;
        }

        public String getPermission(){return this.permission;}
        public boolean equals (String perm){ return perm.equals(this.permission);}
        public boolean equalsIgnoreCase(String perm) {return perm.equalsIgnoreCase(this.permission);}

        @Override
        public String toString() {
            return this.getPermission();
        }
    }
    public enum interact {
        // Interaction permissions

        openIronDor(interactAlias + ".openIronDor"),
        openWoodDor(interactAlias + ".openWoodDor"),
        openIronTrap(interactAlias + ".openIronTrap"),
        openWoodTrap(interactAlias + ".openWoodTrap"),
        openChest(interactAlias + ".openChest"),
        openFurnace(interactAlias + ".openFurnace"),
        useWoodButton(interactAlias + ".useWoodButton"),
        useStoneButton(interactAlias + ".useStoneButton"),
        userLever(interactAlias + ".useLever"),
        getItemInMine(interactAlias + ".getItemInMine");

        private String permission;

        interact(String permission) {
            this.permission = permission;
        }

        public String getPermission(){return this.permission;}
        public boolean equals (String perm){ return perm.equals(this.permission);}
        public boolean equalsIgnoreCase(String perm) {return perm.equalsIgnoreCase(this.permission);}

        @Override
        public String toString() {
            return this.getPermission();
        }
    }

    public enum diplomatie {
        // Diplomatic permissions

        delacreWar(diplomatieAlias + ".declrareWar"),
        launchRaid(diplomatieAlias + ".launchRaid"),
        surrend(diplomatieAlias + ".surrend"),
        setVassality(diplomatieAlias + ".setDiplomatie");

        private String permission;

        diplomatie(String permission) {
            this.permission = permission;
        }

        public String getPermission(){return this.permission;}
        public boolean equals (String perm){ return perm.equals(this.permission);}
        public boolean equalsIgnoreCase(String perm) {return perm.equalsIgnoreCase(this.permission);}
        @Override
        public String toString() {
            return this.getPermission();
        }
    }

    public enum land {
        //Land permissions

        leaveLand(landAlias + ".leaveLand"),
        taxExept(landAlias + ".taxExcept"),
        claim(landAlias + ".claim");

        private String permission;

        land(String permission) {
            this.permission = permission;
        }

        public String getPermission(){return this.permission;}
        public boolean equals (String perm){ return perm.equals(this.permission);}
        public boolean equalsIgnoreCase(String perm) {return perm.equalsIgnoreCase(this.permission);}
        @Override
        public String toString() {
            return this.getPermission();
        }
    }

    public static ArrayList<String> getInteractPermissions(){
        ArrayList<String> lstPermission = new ArrayList<>();

        for (interact perm : interact.values()){
            lstPermission.add(perm.getPermission());
        }
        return lstPermission;
    }

    public static ArrayList<String> getDiplomatiePermissions(){
        ArrayList<String> lstPermission = new ArrayList<>();

        for (diplomatie perm : diplomatie.values()){
            lstPermission.add(perm.getPermission());
        }
        return lstPermission;
    }

    public static ArrayList<String> getLandPermissions(){
        ArrayList<String> lstPermission = new ArrayList<>();

        for (land perm : land.values()){
            lstPermission.add(perm.getPermission());
        }
        return lstPermission;
    }

    public static ArrayList<String> getBuildPermissions(){
        ArrayList<String> lstPermission = new ArrayList<>();

        for (build perm : build.values()){
            lstPermission.add(perm.getPermission());
        }
        return lstPermission;
    }

    public static ArrayList<String> getManagementPermissions(){
        ArrayList<String> lstPermission = new ArrayList<>();

        for (management perm : management.values()){
            lstPermission.add(perm.getPermission());
        }
        return lstPermission;
    }

    public static ArrayList<String> getAllPermissions(){
        ArrayList<String> lstAllPermissions = new ArrayList<>();

        lstAllPermissions.addAll(getManagementPermissions());
        lstAllPermissions.addAll(getInteractPermissions());
        lstAllPermissions.addAll(getLandPermissions());
        lstAllPermissions.addAll(getDiplomatiePermissions());
        lstAllPermissions.addAll(getBuildPermissions());

        return lstAllPermissions;
    }

    public static ItemStack getPermissionItem(Role role, Permission permission){
        return Permission.getPermissionItem(role, permission.toString());
    }

    public static ItemStack getPermissionItem(Role role, String permission){

        ItemStack permissionItem = null;

        if(permission.equalsIgnoreCase(management.disband.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.FIRE_CHARGE),"§6Disband");
        else if (permission.equalsIgnoreCase(management.kick.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.STICK),"§6Kick");
        else if (permission.equalsIgnoreCase(management.invite.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.PAPER),"§6Inviter");
        else if (permission.equalsIgnoreCase(management.promote.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.GOLD_INGOT),"§6Promouvoir");
        else if (permission.equalsIgnoreCase(management.createRole.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.GOLDEN_HELMET),"§6Créer un rôle");
        else if (permission.equalsIgnoreCase(management.renameRole.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.FEATHER),"§6Renommer un rôle");
        else if (permission.equalsIgnoreCase(management.removeRole.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.NETHERITE_HELMET),"§6Supprimer un rôle");
        else if (permission.equalsIgnoreCase(management.setPower.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.GUNPOWDER),"§6Modifier le power d'un rôle");
        else if (permission.equalsIgnoreCase(management.setPermission.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.HONEYCOMB),"§6Définir les permissions d'un rôle");
        else if (permission.equalsIgnoreCase(management.setDescription.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.GLOBE_BANNER_PATTERN),"§6Définir la description de l'organisation");
        else if (permission.equalsIgnoreCase(management.setConnexionMessage.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.MUSIC_DISC_11),"§6Définir le message a la connexion");
        else if (permission.equalsIgnoreCase(build.placeBlock.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.BRICK),"§6Poser des bocks");
        else if (permission.equalsIgnoreCase(build.breakBlock.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.IRON_PICKAXE),"§6Casser des blocks");
        else if (permission.equalsIgnoreCase(interact.openIronDor.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.IRON_DOOR),"§6Ouvrir une porte en fer");
        else if (permission.equalsIgnoreCase(interact.openWoodDor.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.DARK_OAK_DOOR),"§6Ouvrir une porte en bois");
        else if (permission.equalsIgnoreCase(interact.openIronTrap.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.IRON_TRAPDOOR),"§6Ouvrir une trappe en fer");
        else if (permission.equalsIgnoreCase(interact.openWoodTrap.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.DARK_OAK_TRAPDOOR),"§6Ouvrir une trappe en bois");
        else if (permission.equalsIgnoreCase(interact.openChest.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.CHEST),"§6Ouvrir un coffre");
        else if (permission.equalsIgnoreCase(interact.openFurnace.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.FURNACE),"§6Ouvrir un four");
        else if (permission.equalsIgnoreCase(interact.useWoodButton.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.ACACIA_BUTTON),"§6Utiliser un bouton en bois");
        else if (permission.equalsIgnoreCase(interact.useStoneButton.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.STONE_BUTTON),"§6Utiliser un bouton en pierre");
        else if (permission.equalsIgnoreCase(interact.userLever.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.LEVER),"§6Utiliser un levier");
        else if (permission.equalsIgnoreCase(interact.getItemInMine.toString()))permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.CHEST_MINECART),"§6Prendre une ressource dans une mine");
        else if (permission.equalsIgnoreCase(diplomatie.delacreWar.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.DIAMOND_SWORD),"§6Déclarer la guerre");
        else if (permission.equalsIgnoreCase(diplomatie.launchRaid.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.GOLDEN_SWORD),"§6Lancer un raid");
        else if (permission.equalsIgnoreCase(diplomatie.surrend.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.WHITE_BANNER),"§6Abandonner une guerre");
        else if (permission.equalsIgnoreCase(diplomatie.setVassality.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.BOOK),"§6Se définir vassal d'une organisation");
        else if (permission.equalsIgnoreCase(land.leaveLand.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.BIRCH_BOAT),"§6Unclaim un land");
        else if (permission.equalsIgnoreCase(land.taxExept.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.NAME_TAG),"§6Ne pas payer de taxe");
        else if (permission.equalsIgnoreCase(land.claim.toString())) permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.LEAD),"§6Claim un land");
        else permissionItem = GeneralItem.renameItemStackWithName(new ItemStack(Material.BEDROCK),"TODO");

        ItemMeta meta = permissionItem.getItemMeta();
        ArrayList<Component> lstLore = new ArrayList<>();

        if(role.hasPermission(permission)) {
            meta.addEnchant(Enchantment.DIG_SPEED,1, true);
            lstLore.add(Component.text("§8Autorisation : §aOui"));
        }
        else{
            lstLore.add(Component.text("§8Autorisation : §cNon"));
        }

        meta.lore(lstLore);
        permissionItem.setItemMeta(meta);
        GeneralItem.cleanItemMeta(permissionItem);

        return permissionItem;
    }





}
