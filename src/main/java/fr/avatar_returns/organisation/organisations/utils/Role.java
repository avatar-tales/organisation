package fr.avatar_returns.organisation.organisations.utils;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.view.GeneralItem;
import net.kyori.adventure.text.Component;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Objects;

public class Role implements Comparable{

    private String name;
    private int rolePower;
    private int groupId;
    private ArrayList<String> lstPermission;

    public Role(String name, int rolePower, Group group){
        this.groupId = group.getId();
        this.name = name.replace("&","§");
        this.rolePower = rolePower;
        this.setLstPermission(new ArrayList<>());
        DBUtils.addRole(this);
    }
    public Role(String name, int rolePower, Group group, boolean fromDB){
        this.groupId = group.getId();
        this.name = name.replace("&","§");
        this.rolePower = rolePower;
        this.setLstPermission(new ArrayList<>());
        if(!fromDB){DBUtils.addRole(this);}
    }

    //Setter
    public void setName(String newname) { this.setName(newname, true);}
    public void setName(String newname, boolean storeDb) {

        newname = newname.replace("&","§");

        if(storeDb)DBUtils.renameRole(this, newname);
        this.name = newname;
    }

    public void setRolePower(int newpower) { this.setRolePower(newpower, true);}
    public void setRolePower(int newpower, boolean storeDb) {
        DBUtils.setPowerRole(this, newpower);
        this.rolePower = newpower;
    }

    public void setLstPermission(ArrayList<String> lstPermission){this.setLstPermission(lstPermission, true);}
    public void setLstPermission(ArrayList<String> lstPermission, boolean storeDb) {
        this.lstPermission = lstPermission;
        if(storeDb) {
            for (String permission : this.lstPermission) {
                DBUtils.addRolePermission(this, permission);
            }
        }
    }

    //Getters
    public String getName() {
        return name;
    }
    @Nullable public Group getGroup(){
        return Group.getGroupById(this.groupId);
    }
    public int getRolePower() {return rolePower;}
    public ArrayList<String> getLstPermission() {
        return lstPermission;
    }

    public boolean hasPermission(String permission){

        for (String perm : this.getLstPermission()){
            if(perm.equalsIgnoreCase(permission)){
                return true;
            }
        }

        if(this.isLeader())return true;
        return false;
    }
    public boolean hasPermission(Enum permission){
        return this.hasPermission(permission.toString());
    }

    @Override public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Role)) return false;
        Role role = (Role) o;
        return role.getName().equalsIgnoreCase(this.name);
    }
    public int compareTo(Role anotherRole) {
        if ( this.getRolePower() > anotherRole.getRolePower() ){
            return -1;
        }
        else {
            return 1;
        }
    }
    @Override public int compareTo(@NotNull Object o) {

        if( o instanceof Role) {

            Role anotherRole = (Role) o;
            if (this.getRolePower() < anotherRole.getRolePower()) {
                return -1;
            } else {
                return 1;
            }
        }
        return -2;
    }

    public boolean isLeader() {
        return this.getRolePower() == 0;
    }
    public boolean isRecrue(){
        return this.getRolePower() == -1;
    }
    public static boolean canExecuteOnOtherRole(OrganisationPlayer inititatorOrganisationPlayer, Role targetRole){

        if(inititatorOrganisationPlayer.getOfflinePlayer().isOp())return true;

        OrganisationMember initiatorGroupMember = targetRole.getGroup().getOrganisationMember(inititatorOrganisationPlayer.getOfflinePlayer());
        if(initiatorGroupMember == null)return false;
        return canExecuteOnOtherRole(initiatorGroupMember, targetRole);
    }
    public static boolean canExecuteOnOtherRole(OrganisationPlayer inititatorOrganisationPlayer, Role targetRole, Enum permission){

        if(inititatorOrganisationPlayer.getOfflinePlayer().isOp())return true;
        OrganisationMember initiatorGroupMember = targetRole.getGroup().getOrganisationMember(inititatorOrganisationPlayer.getOfflinePlayer());
        if(initiatorGroupMember == null)return false;

        return canExecuteOnOtherRole(initiatorGroupMember, targetRole, permission);
    }
    public static boolean canExecuteOnOtherRole(OrganisationMember initiatorGroupMember, Role targetRole, Enum permission){

        return canExecuteOnOtherRole(initiatorGroupMember, targetRole) && initiatorGroupMember.hasPermission(permission);
    }
    public static boolean canExecuteOnOtherRole(OrganisationMember initiatorGroupMember, Role targetRole){

        if (initiatorGroupMember.getOfflinePlayer().isOp()) return true;
        if (initiatorGroupMember.getGroup() != targetRole.getGroup()) return false;
        if (!initiatorGroupMember.isRecrue() && targetRole.isRecrue()) return true;

        return initiatorGroupMember.getRole().getRolePower() < targetRole.getRolePower();
    }

    public ItemStack getRoleItem(){

        ItemStack roleItem = null;
        if(this.isLeader())roleItem = new ItemStack(Material.NETHERITE_HELMET);
        else if(this.isRecrue()) roleItem = new ItemStack(Material.TURTLE_HELMET);
        else if(this.getRolePower() <= 10) roleItem = new ItemStack(Material.DIAMOND_HELMET);
        else if(this.getRolePower() <= 100) roleItem = new ItemStack(Material.GOLDEN_HELMET);
        else if(this.getRolePower() <= 500) roleItem = new ItemStack(Material.IRON_HELMET);
        else if(this.getRolePower() <= 1000) roleItem = new ItemStack(Material.CHAINMAIL_HELMET);
        else roleItem = new ItemStack(Material.LEATHER_HELMET);

        GeneralItem.cleanItemMeta(roleItem);
        roleItem = GeneralItem.renameItemStackWithName(roleItem, "§f"+this.getName());

        ArrayList<Component> lstLore = new ArrayList<>();
        if(!(this.isLeader() || this.isRecrue())) lstLore.add(Component.text("§8Power : §7" + this.getRolePower()));
        lstLore.add(Component.text("§8Permissions : §7" + this.getLstPermission().size()));

        ItemMeta meta = roleItem.getItemMeta();
        Group group = this.getGroup();
        if(group != null){
            int cptRoleMember = 0;
            for(OrganisationMember oMember : group.getLstMember()){
                if(oMember.getRole().getName().equals(this.getName()))cptRoleMember++;
            }
            lstLore.add(Component.text("§8Membres avec ce role : §7" + cptRoleMember));
        }

        meta.lore(lstLore);
        roleItem.setItemMeta(meta);

        return roleItem;
    }
}
