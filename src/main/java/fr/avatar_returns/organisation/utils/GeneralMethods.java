package fr.avatar_returns.organisation.utils;

import fr.alessevan.api.connections.exceptions.NoCodecFound;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.Packet;
import fr.avatar_returns.organisation.rabbitMQPackets.StoredInventoryPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.player.SimpleChatPacket;
import fr.avatar_returns.organisation.storage.DBConnection;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.storedInventory.MineUtils;
import fr.avatar_returns.organisation.storedInventory.StoredInventory;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import fr.avatar_returns.organisation.utils.teleportation.MultiServerTPMethodimplements;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.BlockIterator;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static fr.avatar_returns.organisation.Organisation.plugin;

public class GeneralMethods {


    private static String name;
    private static final ExecutorService executor = Executors.newCachedThreadPool();
    public GeneralMethods(){
        name = getStringConf("Default.Name");
    }

    public static String getServerId(){ return ConfigManager.getServerId(); }
    public static String getStringConf(String path){
        return ConfigManager.defaultConfig.get().getString(path);
    }
    public static String getLanguage(String path) {return ConfigManager.language.get().getString(path);}
    public static long getLongConf(String path){
        return ConfigManager.defaultConfig.get().getLong(path);
    }
    public static int getIntConf(String path){
        return ConfigManager.defaultConfig.get().getInt(path);
    }
    public static double getDoubleConf(String path){
        return ConfigManager.defaultConfig.get().getDouble(path);
    }
    public static boolean getBooleanConf(String path){
        return ConfigManager.defaultConfig.get().getBoolean(path);
    }
    public static Float getFloatConf(String path){
        Double doubleValue = ConfigManager.defaultConfig.get().getDouble(path);
        return doubleValue.floatValue();
    }

    public static void teleportSpawnServer(Player player){
        String spawnServer = GeneralMethods.getStringConf("Organisation.Spawn.Config.ServerId");
        String worldName = GeneralMethods.getStringConf("Organisation.Spawn.Config.world");

        int x = GeneralMethods.getIntConf("Organisation.Spawn.Config.x");
        int y = GeneralMethods.getIntConf("Organisation.Spawn.Config.y");
        int z = GeneralMethods.getIntConf("Organisation.Spawn.Config.z");

        if (spawnServer.equals(ConfigManager.getServerId())) {

            World world = Organisation.plugin.getServer().getWorld(worldName);
            if (world == null) return;
            player.teleport(new Location(world, x, y, z));
        } else {
            MultiServerTPMethodimplements.sendTPCommand(spawnServer, player, x, y, z, worldName);
        }
    }

    public static void sendPlayerMessage(OrganisationPlayer organisationPlayer, String text){
        if(organisationPlayer.getOfflinePlayer() != null) sendPlayerMessage(organisationPlayer.getOfflinePlayer(), text);
        else GeneralMethods.sendPacket(new SimpleChatPacket(organisationPlayer, name + "§6" + text));
    }
    public static void sendPlayerMessage(OrganisationPlayer organisationPlayer, String path, boolean fromConfig){
        if(organisationPlayer.getOfflinePlayer() != null) sendPlayerMessage(organisationPlayer.getOfflinePlayer(), path, fromConfig);
        else GeneralMethods.sendPacket(new SimpleChatPacket(organisationPlayer, name + "§6" + ((fromConfig)? getLanguage(path) : path)));
    }
    public static void sendPlayerMessage(OfflinePlayer player, String text){
        try {
            if (player.getPlayer() == null)
                GeneralMethods.sendPacket(new SimpleChatPacket(OrganisationPlayer.getOrganisationPlayer(player), name + "§6" + text));
            else sendPlayerMessage(player.getPlayer(), text);
        }catch (OrganisationPlayerException e){};
    }
    public static void sendPlayerMessage(Player player, String text){
        player.sendMessage(name+" §6"+text);
    }
    public static void sendPlayerMessage(OfflinePlayer player, String path, boolean fromConfig){
        try{
            if(player.getPlayer() == null) GeneralMethods.sendPacket(new SimpleChatPacket(OrganisationPlayer.getOrganisationPlayer(player),name +"§6"+ ((fromConfig)?GeneralMethods.getLanguage(path) : path)));
            else sendPlayerMessage(player.getPlayer(),path,fromConfig);
        }catch (OrganisationPlayerException e){};
    }
    public static void sendPlayerMessage(Player player, String path, boolean fromConfig){
        if(!fromConfig)player.sendMessage(name+" §6"+path);
        else{
            player.sendMessage(name+" §6"+getLanguage(path));
        }
    }
    public static void sendPlayerErrorMessage(Player player, String text){
        player.sendMessage(name+" §c"+text);
    }
    public static void sendPlayerErrorMessage(OfflinePlayer player, String text){
        try{
            if(player.getPlayer() == null) GeneralMethods.sendPacket(new SimpleChatPacket(OrganisationPlayer.getOrganisationPlayer(player),name +"§c"+ text));
            else sendPlayerErrorMessage(player.getPlayer(),text);
        }catch (OrganisationPlayerException e){};
    }
    public static void sendPlayerErrorMessage(OfflinePlayer player, String path,  boolean fromConfig){
        try{
            if(player.getPlayer() == null) GeneralMethods.sendPacket(new SimpleChatPacket(OrganisationPlayer.getOrganisationPlayer(player),name +"§c"+ ((fromConfig)?GeneralMethods.getLanguage(path) : path)));
            else sendPlayerErrorMessage(player.getPlayer(),path,fromConfig);
        }catch (OrganisationPlayerException e){};
    }
    public static void sendPlayerErrorMessage(Player player, String path, boolean fromConfig){
        if(!fromConfig)player.sendMessage(name+" §c"+path);
        else{
            player.sendMessage(name+" §c"+getLanguage(path));
        }
    }

    public static @Nullable OfflinePlayer getOfflinePlayerFromName(String playerName) {
        UUID uuid = plugin.getServer().getPlayerUniqueId(playerName);
        return getOfflinePlayerFromUUID(uuid);
    }
    public static @Nullable OfflinePlayer getOfflinePlayerFromUUID(UUID uuid){
        return plugin.getServer().getOfflinePlayer(uuid);
    }

    public static final Block getPlayerTargetBlock(Player player, Integer range, boolean lastAirBlock) {
        BlockIterator bi= new BlockIterator(player, range);
        Block lastBlock = bi.next();
        ArrayList<Block> lstBlock = new ArrayList<>();

        while (bi.hasNext()) {

            lastBlock = bi.next();
            if (lastBlock.getType() == Material.AIR) {
                lstBlock.add(lastBlock);
                continue;
            }

            break;
        }
        if(lastAirBlock){
            if(lstBlock.size() == 0)return player.getLocation().getBlock();
            return lstBlock.get(lstBlock.size() - 1);
        }
        return lastBlock;
    }

    public static void stopPlugin() {
        plugin.getServer().getPluginManager().disablePlugin(plugin);
    }
    public static void reloadPlugin(){

        if (DBConnection.sql != null)
            DBConnection.sql.close();

        if (!DBConnection.init()) {
            Organisation.log.severe("Unable to enable Organisation plugin due to the database not being open");
            stopPlugin();
        }
    }

    public static int getItemAmountInInventory(Inventory inventory){
        int amount = 0;
        for(ItemStack item : inventory.getContents()) amount += (item == null)? 0 : item.getAmount();
        return amount;
    }

    public static void restoreFromDb(){

        DBUtils.restoreServer();
        DBUtils.restoreOrganisationPlayer();
        DBUtils.restoreRank();
        DBUtils.restoreRankObjectif();
        DBUtils.restoreGroup();
        DBUtils.restoreRole();
        DBUtils.restorePermissions();
        DBUtils.restoreMember();
        DBUtils.restoreInvitation();
        DBUtils.restoreProvince();
        DBUtils.restoreLand();
        DBUtils.restoreLandSubTerritory();
        DBUtils.restoreStoredInventory();
        DBUtils.restorePositionInterest();
        DBUtils.restoreRelation();

        GeneralMethods.prepareMinesMechanismes();
    }

    private static void prepareMinesMechanismes(){

        boolean createDefaultMine = true;
        for(StoredInventory storedInventory : StoredInventory.getLstStoredInventoryType(StoredInventory.Type.MINE)){
            if(storedInventory.getId() == 1) {
                createDefaultMine = false;
                break;
            }
        }

        if(createDefaultMine){
            Organisation.log.info("Creation of default Mine");
            Inventory defaultInv = Bukkit.createInventory(null, 54, "Mine");

            StoredInventory defaultMine = new StoredInventory(1, "§3§lMine",defaultInv, 0.0D,0,StoredInventory.Type.MINE.getStrType());
            DBUtils.addStoredInventory(defaultMine);
            GeneralMethods.sendPacket(new StoredInventoryPacket(defaultMine, OrganisationPacket.Action.CREATE));
        }

        MineUtils.prepareMineMechanismes();
    }
    public static void stopMine(){
        Organisation.log.info("Closes all mines.");
        for(Player player : Bukkit.getOnlinePlayers())player.closeInventory();
    }

    public static boolean cancelEvent(Event e){
        if(e instanceof Cancellable)((Cancellable) e).setCancelled(true);
        return true;
    }

    public static String convertTimeWithTimeZome(long time){
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
        cal.setTimeInMillis(time);

        String day = (cal.get(Calendar.DAY_OF_MONTH) < 10) ? "0"+ cal.get(Calendar.DAY_OF_MONTH) : "" + cal.get(Calendar.DAY_OF_MONTH);

        return ( day+ "/" + (cal.get(Calendar.MONTH) + 1) + "/" + cal.get(Calendar.YEAR)
                + " " + cal.get(Calendar.HOUR_OF_DAY) + ":"
                + cal.get(Calendar.MINUTE));

    }

    private static String encodeB64(String stringToEncode){
        return new String(Base64.getEncoder().encode(stringToEncode.getBytes()));
    }
    private static String decodeB64(String stringToDecode){
        return new String(Base64.getDecoder().decode(stringToDecode));
    }

    public static void sendPacket(Packet packet){
        GeneralMethods.sendPacket(packet,"*");
    }
    public static void sendPacket(Packet packet, String target){
        executor.submit(() -> {
            try {
                plugin.getMessaging().send(packet, target);
            } catch (NoCodecFound e) {}
        });
    }

}
