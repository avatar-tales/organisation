	package fr.avatar_returns.organisation.utils.configuration;

	import fr.avatar_returns.organisation.utils.GeneralMethods;
	import org.bukkit.configuration.file.FileConfiguration;

	import java.io.File;
	import java.util.*;

	public class ConfigManager {

	public static Config defaultConfig;
	public static Config language;
	private static String serverId;

	public ConfigManager() {

		defaultConfig = new Config(new File("config.yml"));
		language = new Config(new File("language.yml"));
		configCheck(ConfigType.LANGUAGE);
		configCheck(ConfigType.DEFAULT);
		ConfigManager.serverId = getConfig().getString("Default.ServerId");
	}

	public static String getServerId() {
		return ConfigManager.serverId;
	}

	public static void configCheck(final ConfigType type) {
		FileConfiguration config;
		 if (type == ConfigType.DEFAULT) {
			config = defaultConfig.get();

			config.addDefault("Default.Name", "§8[§9Organisation§8]§6");
			config.addDefault("Default.ServerId", UUID.randomUUID().toString());
			config.addDefault("Default.Chat.isOrganisationChatEnabled", true);
			config.addDefault("Default.OrganisationWorld", "world");
			config.addDefault("Commands.Disband.TokenDuration",  5*60*1000*1000);

			//Organisation configuration
			config.addDefault("Organisation.Player.CanCreatePortal",false);
			config.addDefault("Organisation.Player.CanExplodeBlock",false);
			config.addDefault("Organisation.Player.CanDefineIfGroupMemberCantOpenMine",true);
			config.addDefault("Organisation.Player.DisplayWorldWideTitle",false);
			config.addDefault("Organisation.Player.timeToBeExperimentedAsPlayer", 24*60*60*1000);

			config.addDefault("Organisation.Guilde.minPlayer", 0);
			config.addDefault("Organisation.Guilde.maxPlayer", 12);
			config.addDefault("Organisation.Guilde.maxMultiGuilde", 1);

			config.addDefault("Organisation.Faction.minPlayer", 0);
			config.addDefault("Organisation.Faction.maxPlayer", 12);
			config.addDefault("Organisation.Faction.maxLand", 3);
			config.addDefault("Organisation.Faction.maxRaid", 2);
			config.addDefault("Organisation.Faction.AllowFriendlyFire", false);

			config.addDefault("Organisation.LargeGroup.DamageReduction.value", 0.1);

			 config.addDefault("Organisation.Disable.LandDisplay", false);
			 config.addDefault("Organisation.Disable.Protection.Interaction", false);
			 config.addDefault("Organisation.Disable.Protection.Build", false);
			 config.addDefault("Organisation.Disable.GamemodeAutoChange", false);

			 config.addDefault("Organisation.board.canSeeOther", false);

			 config.addDefault("Organisation.Spawn.Config.EnableRespawnOnOtherServer", true);
			 config.addDefault("Organisation.Spawn.Config.ServerId", "conquete-spawn");
			 config.addDefault("Organisation.Spawn.Config.x", -176);
			 config.addDefault("Organisation.Spawn.Config.y", 75);
			 config.addDefault("Organisation.Spawn.Config.z", 210);
			 config.addDefault("Organisation.Spawn.Config.world", "ukiyo");
			 config.addDefault("Organisation.Spawn.Teleportation.Duration", 10);
			 config.addDefault("Organisation.Spawn.Teleportation.Cost", 10);

			 config.addDefault("Organisation.Home.Teleportation.Duration", 10);
			 config.addDefault("Organisation.Home.Teleportation.Cost", 10);

			 config.addDefault("Organisation.Prespawn.AllowMultipleTeleportation", false);
			 config.addDefault("Organisation.Prespawn.MaxMultipleTeleportation", 2);
			 config.addDefault("Organisation.Prespawn.Teleportation.Duration", 10);

			 config.addDefault("Organisation.FightMode.Duration", 60);
			 config.addDefault("Organisation.FightMode.IsEnabled", true);

			 config.addDefault("Organisation.Raid.matchMakingMinuteDuration", 5);
			 config.addDefault("Organisation.Raid.raidMinuteDuration", 20);
			 config.addDefault("Organisation.Raid.secondeToCapture", 10);
			 config.addDefault("Organisation.Raid.captureRayAroundTotem", 0.75);
			 config.addDefault("Organisation.Raid.timeToBeExperimentedAsPlayer", 24*60*60*1000);
			 config.addDefault("Organisation.Raid.timeToBeExperimentedAsOrganisationMember", 1*24*60*60*1000);
			 config.addDefault("Organisation.Raid.acceptableMultiplicatorAttackerExperimentedPlayer", 1.5);
			 config.addDefault("Organisation.Raid.howManyNewsPlayerAsToBeConsiderAsExperimtentedPlayer", 2);

			 config.addDefault("Organisation.MythicsMobs.Land.AllowHostileMob",true);
			 config.addDefault("Organisation.MythicsMobs.Land.WhiteList", Arrays.asList("Appa","Bandit"));
			 config.addDefault("Organisation.MythicsMobs.Land.BlackList", Arrays.asList("DUMMY","Arachnid"));
			 config.addDefault("Organisation.MythicsMobs.MainLand.AllowHostileMob",true);
			 config.addDefault("Organisation.MythicsMobs.MainLand.WhiteList", Arrays.asList("Appa","Bandit"));
			 config.addDefault("Organisation.MythicsMobs.MainLand.BlackList", Arrays.asList("DUMMY","Arachnid"));
			 config.addDefault("Organisation.MythicsMobs.ZoneRP.AllowHostileMob",true);
			 config.addDefault("Organisation.MythicsMobs.ZoneRP.WhiteList", Arrays.asList("Appa","Bandit"));
			 config.addDefault("Organisation.MythicsMobs.ZoneRP.BlackList", Arrays.asList("DUMMY","Arachnid"));

			 config.addDefault("Organisation.Mine.DayOfWeekRotate", 7);
			 config.addDefault("Organisation.Mine.PeriodToFillSecond", 60*60*1);

			 config.addDefault("Organisation.Mine.DayOfWeekRotate", 7);
			 config.addDefault("Organisation.Mine.DayOfWeekRotate", 7);

			 config.addDefault("Organisation.chat.enable", true);
			 config.addDefault("Organisation.chat.multi-server.enable", true);
			 config.addDefault("Organisation.chat.local-chat.enable", true);
			 config.addDefault("Organisation.chat.local-chat.default-token", "!");
			 config.addDefault("Organisation.chat.local-chat.radius", 50);
			 config.addDefault("Organisation.chat.local-chat.format.normal", "<green>[L]</green> {prefix_{player}}{suffix_{player}} {group_name}{name_color}{player}<reset><dark_gray>: </dark_gray><reset>{message}");
			 config.addDefault("Organisation.chat.local-chat.format.spy", "<gray><i>[Spy] {prefix_{player}}{suffix_{player}} {group_name}{player} <gray>: {message}");

			 config.addDefault("Organisation.chat.local-chat.megaphone.enable", true);
			 config.addDefault("Organisation.chat.local-chat.megaphone.radius", 100);
			 config.addDefault("Organisation.chat.local-chat.megaphone.item", "SPYGLASS");
			 config.addDefault("Organisation.chat.local-chat.megaphone.name", "&6&lMégaphone");
			 config.addDefault("Organisation.chat.local-chat.megaphone.craft.enable", true);
			 config.addDefault("Organisation.chat.local-chat.megaphone.craft.recipe", List.of(" I ","ISI"," I "));
			 config.addDefault("Organisation.chat.local-chat.megaphone.craft.ingredients", Map.of("I","IRON_INGOT","S", "SPYGLASS"));

			 Map<String, String> remap = new HashMap<>();
			 remap.put("(:","\uEE05");
			 remap.put(":*","\uEE07");
			 remap.put(";)","\uEE09");
			 remap.put(":)","\uEE22");
			 remap.put(":o","\uEE30");
			 remap.put(":0","\uEE30");
			 remap.put(">:)","\uEE35");
			 remap.put("</3","\uEE40");
			 remap.put("<3","\uEE46");
			 remap.put(":3","\uEE50");
			 remap.put(":(","\uEE14");
			 remap.put(">:(","\uEE20");
			 remap.put(":/","\uEE54");

			 config.addDefault("Organisation.chat.settings.format", "{prefix_{player}}{suffix_{player}} {group_name}{name_color}{player}<reset><dark_gray>: </dark_gray><reset>{message}");
			 config.addDefault("Organisation.chat.settings.forbiddenChars", "ꨁꨂꨃꨄꨅꨆꨇꨈꨉꨐꨑꨒ\uE07A");
			 config.addDefault("Organisation.chat.settings.remap", remap);


			 config.addDefault("Storage.engine", "sqlite");
			 config.addDefault("Storage.MySQL.host", "localhost");
			 config.addDefault("Storage.MySQL.port", 3306);
			 config.addDefault("Storage.MySQL.pass", "");
			 config.addDefault("Storage.MySQL.db", "organisationdb");
			 config.addDefault("Storage.MySQL.user", "organisationUser");

			 config.addDefault("Messaging.engine", "local");
			 config.addDefault("Messaging.host", "localhost");
			 config.addDefault("Messaging.port", 20000);
			 config.addDefault("Messaging.user", "organisationUser");
			 config.addDefault("Messaging.pass", "");


			defaultConfig.save();
		}
		 if (type == ConfigType.LANGUAGE){
			 config = language.get();

			 //Error from plugin message :
			 config.addDefault("Error.OrganisationPlayerDoesntExist", "Erreur 0 : Contacter un administrateur");

			 //Commands Message
			 config.addDefault("Organisation.Muted", "Vous ne pouvez pas parler. Vous êtes en mode : MUTE");
			 config.addDefault("Commands.NoPermission", "Tu n'as pas la permission de faire cela");
			 config.addDefault("Commands.MustBePlayer", "Tu dois être un joueur pour faire cette action");
			 config.addDefault("Commands.NoOrganisationFound", "Vous n'avez pas d'organisation. Impossible d'effectuer cette commande.");
			 config.addDefault("Commands.ToManyOrganisation", "Veuillez indiquer l'organisation...");
			 config.addDefault("Commands.BadGroup", "Vous ne pouvez effectuer cette commande sur une organisation qui ne vous appartient pas.");
			 config.addDefault("Organisation.TP.CantTPFromSafeZone", "Vous ne pouvez pas vous téléporter depuis une zone protégée.");

			 config.addDefault("Commands.Create.Desc", "Créer une nouvelle organisation");
			 config.addDefault("Commands.Create.AlreadyExist", "L'organisation existe déja");
			 config.addDefault("Commands.Create.Successful", "L'organisation'a bien été créée");
			 config.addDefault("Commands.Create.Max_Guilde_Join", "Vous avez déja rejoins trop de guildes");
			 config.addDefault("Commands.Create.Max_Guilde_Join", "Vous avez rejoins trop de guildes");
			 config.addDefault("Commands.Create.Max_LargeOrganisation_Join", "Vous êtes déja dans une grande organisation");

			 config.addDefault("Commands.edit.Desc","Editer une organisation existante");
			 config.addDefault("Commands.edit.NoPermission","Impossible d'éditer l'organisation, vous n'avez pas la permission");
			 config.addDefault("Commands.edit.rename.Successful","L'organisation a bien été renommée.");
			 config.addDefault("Commands.edit.ValueError.Int","Impossible d'editer l'organisation : veuillez saisir un nombre supérieur à 1");
			 config.addDefault("Commands.edit.ValueError.Bool","Impossible d'editer l'organisation : veuilleez saisir <true|false>");
			 config.addDefault("Commands.edit.rename.Successful","L'organisation a bien été renommée.");
			 config.addDefault("Commands.edit.maxPlayer.Successful","Le nombre maximum de joueur de l'organisation a bien été édité.");
			 config.addDefault("Commands.edit.maxPlayer.Error","Le nombre de joueur de maximum doit être strictement supérieur au nombre de joueur minimum et au nombre de joueurs actuel");
			 config.addDefault("Commands.edit.minPlayer.Successful","Le nombre minimum de joueur de l'organisation a bien été édité.");
			 config.addDefault("Commands.edit.minPlayer.Error","Impossible d'imposer un nombre minimum de joueur supérieur au nombre de joueurs actuels");
			 config.addDefault("Commands.edit.emptyValue","Attention : La valeur indiquée est vide");
			 config.addDefault("Commands.edit.desc.Successful","La description a bien été modifiée");
			 config.addDefault("Commands.edit.connexionMessage.Successful","La message d'accueil a bien été éditée");
			 config.addDefault("Commands.edit.rank.Successful", "Le rang a été édité avec succès.");
			 config.addDefault("Commands.edit.rank.BadRank", "Impossible de donner ce rang au groupe, il n'est pas du bon type.");
			 config.addDefault("Commands.edit.rank.BadId", "L'id du rang donné n'existe pas.");
			 config.addDefault("Commands.edit.maxRaid.Error", "Le nombre maximum de raid journalier est forcément positif.");
			 config.addDefault("Commands.edit.maxRaid.Successful", "Le nombre maximum de raid journalier a été édité avec succès.");
			 config.addDefault("Commands.edit.maxLand.Error", "Le nombre maximum de Land est forcément positif.");
			 config.addDefault("Commands.edit.maxLand.Successful", "Le nombre maximum de Land a été édité avec succès.");
			 config.addDefault("Commands.edit.dailyRaid.Error", "Le nombre de raid journalier est forcément positif.");
			 config.addDefault("Commands.edit.dailyRaid.Successful", "Le nombre de raid journalier a été édité avec succès.");
			 config.addDefault("Commands.edit.lord.ToMutchVassal", "Le lord a déja atteind son nombre maximum de vassals.");
			 config.addDefault("Commands.edit.lord.CantHaveLordWithLowerRank", "Le vassal a un rang supérieur au Lord.");
			 config.addDefault("Commands.edit.lord.LordNotFound", "Le lord n'a pas été trouvé.");
			 config.addDefault("Commands.edit.lord.NotLargeGroup", "La groupe choisi n'est pas un large groupe. Elle n'est donc pas compatible avec le système de vassalité");
			 config.addDefault("Commands.edit.lord.EqualGroup", "Une organisation ne peut pas être le vassal d'elle même.");
			 config.addDefault("Commands.edit.lord.Successful", "Le lord a bien été mis a jour.");
			 config.addDefault("Commands.edit.isVisible.Successful", "La visibilité du groupe a bien été mise a jour.");
			 config.addDefault("Commands.edit.canUnClaim.Successful", "La possibilité d'unclaim du groupe a bien été mise a jour.");

			 config.addDefault("Commands.Teleportation.YoungDontPay", "Le voyage est offert aux nouveaux arrivants.");
			 config.addDefault("Commands.Spawn.Teleportation.Cancel", "La téléportation a bien été annulée");
			 config.addDefault("Commands.Spawn.Teleportation.NotEnouthMoney", "Vous n'avez pas suffisement d'argent pour pouvoir aller au spawn");
			 config.addDefault("Commands.Spawn.Teleportation.MoveCancel", "Vous avez bougé, téléportation au spawn annulée.");
			 config.addDefault("Commands.Spawn.Teleportation.Successful", "La téléportation au spawn aura lieu dans 10 secondes");

			 config.addDefault("Commands.Home.Teleportation.Cancel", "La téléportation a votre organisation bien été annulée");
			 config.addDefault("Commands.Home.Teleportation.NoHomeFound", "Votre organisation n'a pas de home");
			 config.addDefault("Commands.Home.Teleportation.NoMainLandFound", "Votre organisation n'a pas de land principal");
			 config.addDefault("Commands.Home.NoOrganisationFound.Admin","L'organisation demandé n'existe pas.");
			 config.addDefault("Commands.Home.NoOrganisationFound.Player","Vous n'avez pas d'organisation");
			 config.addDefault("Commands.Home.Teleportation.NotEnouthMoney", "Vous n'avez pas suffisement d'argent pour téléporter a votre organisation");
			 config.addDefault("Commands.Home.Teleportation.MoveCancel", "Vous avez bougé, téléportation a votre organisation a été annulée.");
			 config.addDefault("Commands.Home.Teleportation.Successful", "La téléportation a votre organisation aura lieu dans 10 secondes");

			 config.addDefault("Commands.Prespawn.Target.NotFound", "Le joueur demandé n'existe pas...");
			 config.addDefault("Commands.Prespawn.ToMutchSupportTeleportation", "Trop de support sont en train de se téléporter...");
			 config.addDefault("Commands.Prespawn.Target.NotConnected", "Le joueur demandé n'est pas connecté...");
			 config.addDefault("Commands.Prespawn.Target.NotInPrespawn", "Le joueur demandé n'est pas au prespawn...");
			 config.addDefault("Commands.Prespawn.IsInFightMode", "Vous ne pouvez pas vous téléporter au prespawn quand vous êtes en mode combat.");
			 config.addDefault("Commands.Prespawn.Teleportation.Successful", "La téléportation au prespawn aura lieu dans 10 secondes");
			 config.addDefault("Commands.Prespawn.Teleportation.Inform.Successful", "Un support vas se téléporter sur vous dans 10 secondes.");
			 config.addDefault("Commands.Prespawn.Teleportation.Inform.Cancel", "Un support a annulé sa téléportation sur vous.");
			 config.addDefault("Organisation.Prespawn.CantExecuteCommand", "Vous ne pouvez pas effectuer cette commande au prespawn");

			 config.addDefault("Organisation.FightMod.CantExecuteCommand", "Vous ne pouvez pas effectuer cette commande en combat");
			 config.addDefault("Organisation.FightMod.Alert.Start", "Vous êtes désormais en mode combat. Certaines zones vous sont restreintes et vous ne pouvez plus vous téléporter.");
			 config.addDefault("Organisation.FightMod.Alert.End", "Vous n'êtes plus en mode combat.");

			 config.addDefault("Commands.Info.Desc", "Donne des informations sur son/ses organisation(s)");
			 config.addDefault("Commands.Info.GroupDoesntExist", "Le group saisie, n'existe pas");

			 config.addDefault("Commands.List.Desc", "Liste les organisations existante");

			 config.addDefault("Commands.Invite.Desc", "Ajoute un nouveau membre a l'organisation");
			 config.addDefault("Commands.Invite.SameGroupError", "Impossible d'inviter quelqu'un qui est déja dans cette organisation");
			 config.addDefault("Commands.Invite.InvitationSend", "Invitation envoyée avec succès");
			 config.addDefault("Commands.Invite.InvitationRemove","L'invitation a bien été suprimée");

			 config.addDefault("Commands.Kick.Desc", "Ajoute retire un membre à l'organisation");
			 config.addDefault("Commands.Kick.PlayerDontExistInGroup", "Le joueur n'appartient pas à ce groupe ...");
			 config.addDefault("Commands.Kick.PlayerNoPermission", "Vous n'avez pas la permission de retier un joueur de ce groupe");
			 config.addDefault("Commands.Kick.KickProceed", "Le joueur à été retiré avec succès");

			 config.addDefault("Commands.Leave.Desc", "Quitter une organisation.");

			 config.addDefault("Commands.Join.Desc", "Rejoindre une organisation qui vous a invitée");
			 config.addDefault("Commands.Join.SuccessJoin", "Vous avez bien rejoins l'organisation");
			 config.addDefault("Commands.Join.PlayerOrGroupNotFound", "Vous n'avez pas reçu d'invitation pour ce group");
			 config.addDefault("Commands.Join.ToMutchPlayer", "Impossible de rejoindre. Cette organisation a atteind son nombre max de joueurs");
			 config.addDefault("Commands.Join.CanNotJoinYourGroup", "Vous êtes déja dans ce groupe...");

			 config.addDefault("Commands.Disband.Desc", "Supression d'une organisation");
			 config.addDefault("Commands.Disband.Success", "L'organisation a bien été supprimée.");
			 config.addDefault("Commands.Disband.TokenExpiration", "Impossible de disband. La demande est arrivé expiration, ou ne peut être validée.");
			 config.addDefault("Commands.Disband.Confirm", "Etes vous sûr de vouloir détruire cette organisation ?\n Cette modification est iréversible.");

			 config.addDefault("Commands.Role.Desc", "Commande de gestion des role de votre organisation");
			 config.addDefault("Commands.Role.ReservedPower", "Ce niveau de role est réservé.");
			 config.addDefault("Commands.Role.RoleNotFound", "Le role indiqué n'a pas été trouvée");

			 config.addDefault("Commands.Role.SetPower.RoleExist", "Impossible d'éditer le power d'un role qui n'existe pas...");
			 config.addDefault("Commands.Role.SetPower.PermissionError", "Impossible de créer le role : vous n'avez pas la permission");
			 config.addDefault("Commands.Role.SetPower.SetPower", "Le niveau du role à bien été changé.");
			 config.addDefault("Commands.Role.SetPower.NotEnafRolePowerError", "Vous ne pouvez pas changer le niveau d'un role plus du même niveau ou plus élevé que le votre: Permission insufisante");
			 config.addDefault("Commands.Role.SetPower.CantSetMorePower", "Vous ne pouvez pas donner un niveau de role plus élevé ou égale au votre: Permission insufisante");
			 config.addDefault("Commands.Role.SetPower.CantChangeLeader", "Vous ne pouvez pas modifier le power du role Leader");
			 config.addDefault("Commands.Role.SetPower.CantChangeRecrue", "Vous ne pouvez pas modifier le power du role recrue");

			 config.addDefault("Commands.Role.Create.RoleExist", "Impossible de créer un role qui existe.");
			 config.addDefault("Commands.Role.Create.PermissionError", "Impossible de créer le role : vous n'avez pas la permission");
			 config.addDefault("Commands.Role.Create.RoleCreate", "Le role demandée a bien été créé.");
			 config.addDefault("Commands.Role.Create.NotEnafRolePowerError", "Vous ne pouvez pas créer un role plus du même niveau ou plus élevé que le votre: Permission insufisante");

			 config.addDefault("Commands.Role.Rename.PermissionError", "Impossible de renomer le role : vous n'avez pas la permission");
			 config.addDefault("Commands.Role.Rename.NewNameExistingError", "Impossible de renomer le role : Le novueau nom existe déja.");
			 config.addDefault("Commands.Role.Rename.RoleRename", "Le role demandé a bien été renommé.");
			 config.addDefault("Commands.Role.Rename.NotEnafRolePowerError", "Vous ne pouvez pas renommer ce role : Permission insufisante");

			 config.addDefault("Commands.Role.Remove.PermissionError", "Impossible de supprimer le role : vous n'avez pas la permission.");
			 config.addDefault("Commands.Role.Remove.RoleRemove", "Le role demandé a bien été supprimé.");
			 config.addDefault("Commands.Role.Remove.RoleDoesntExist", "Le role n'existe pas. Impossible de supprimer.");
			 config.addDefault("Commands.Role.Remove.CantRemoveLeaderRole", "Impossible de supprimer le role de leader");
			 config.addDefault("Commands.Role.Remove.CantRemoveRecrueRole", "Impossible de supprimer le role de recrue");
			 config.addDefault("Commands.Role.Remove.CantRemoveRecrueRole", "Impossible de supprimer le role de recrue");
			 config.addDefault("Commands.Role.Remove.NotEnafRolePowerError", "Vous ne pouvez pas supprimer ce role : Permission insufisante");

			 config.addDefault("Commands.Role.Update.RoleDoesntExist", "Impossible d'éditer les permission du role, : Le role demandé n'existe pas.");
			 config.addDefault("Commands.Role.Update.PermissionError", "Impossible d'éditer les permission du role, vous n'avez pas la permission");
			 config.addDefault("Commands.Role.Update.RemoveLeaderSetError", "Impossible de retirer la permission d'édition des permissions au role Leader");
			 config.addDefault("Commands.Role.Update.PermissionDoesntExist", "La permission demandée n'exite pas...");
			 config.addDefault("Commands.Role.Update.PermissionRemove", "La permission demandée à bien été retirée");
			 config.addDefault("Commands.Role.Update.PermissionNotGiven", "Vous ne pouvez pas modifier une permission que vous n'avez pas");
			 config.addDefault("Commands.Role.Update.PermissionAdd", "La permission demandée à bien été ajoutée");
			 config.addDefault("Commands.Role.Update.NotEnafRolePowerError", "Vous ne pouvez pas mettre à jour ce role : Permission insufisante");

			 config.addDefault("Commands.Role.Set.PermissionError", "Vous n'avez pas les permissions nécéssaire pour donner ce role à quelqu'un.");
			 config.addDefault("Commands.Role.Set.TargetUserIsNotInTargetGroup", "Le pseudo demandé n'appartient pas au groupe : Impossible de donner ce role à l'utilisateur");
			 config.addDefault("Commands.Role.Set.SenderPowerToLowError", "Vous ne pouvez pas donner un role à quelqu'un qui à un role plus élevé que le votre.");
			 config.addDefault("Commands.Role.Set.NotEnafRolePowerError", "Vous ne pouvez pas donner un role plus élevé que le votre.");
			 config.addDefault("Commands.Role.Set.SameRoleError", "Vous ne pouvez pas donner votre role à quelqu'un.");
			 config.addDefault("Commands.Role.Set.SameRole", "Le joueur à déja ce role.");
			 config.addDefault("Commands.Role.Set.OneLeaderNeededError", "Le groupe à besoin d'au moin un leader pour fonctionner...");
			 config.addDefault("Commands.Role.Set.SuccessRole", "Le role à bien été donné à l'utilisateur");

			 config.addDefault("Commands.Province.ProvinceNotFound", "L'id province donné n'existe pas.");
			 config.addDefault("Commands.Province.ProvinceAlreadyExist", "Une province existe déja avec cette id");
			 config.addDefault("Commands.Province.AddLandSuccess", "Le land à bien été ajoutée à la province");
			 config.addDefault("Commands.Province.RemoveLandSuccess", "Le land à bien été retirée de la province");
			 config.addDefault("Commands.Province.EditNameSuccess", "La province a bien été renomée.");
			 config.addDefault("Commands.Province.EditDescriptionSuccess", "La description de la province a bien été éditée");
			 config.addDefault("Commands.Province.unregisterSuccess", "Province suprimée avec succès.");
			 config.addDefault("Commands.Province.registerSuccess", "Province enregistré avec succès.");
			 config.addDefault("Commands.Province.LandNotInThisProvince", "La land indiquée n'appartient pas à cette province...");

			 config.addDefault("Commands.Land.LandNotFound", "L'id land donné n'existe pas.");
			 config.addDefault("Commands.Land.TerritoryAlreadyUsed", "Impossible de créer ce land, la region worldguard est déja utilisé pour un territoire");
			 config.addDefault("Commands.Land.AddSubTerritorySuccess", "Le sous territoire à bien été ajoutée au land");
			 config.addDefault("Commands.Land.RemoveSubTerritorySuccess", "Le sous territoire à bien été retirée du land");
			 config.addDefault("Commands.Land.EditNameSuccess", "La land a bien été renomée.");
			 config.addDefault("Commands.Land.unregisterSuccess", "Land suprimée avec succès.");
			 config.addDefault("Commands.Land.setProvinceSuccess", "Le land à bien été ajouté à la province");
			 config.addDefault("Commands.Land.CantRegisterLandOnOtherTerritory", "Impossible d'enregistrer ce land. La région worldguard choisi se superpose a un Territoire existant. Il y aurait donc un conflit. Vous devez refaire la zone worldguard.");

			 config.addDefault("Commands.Land.registerSuccess", "Land enregistré avec succès.");
			 config.addDefault("Commands.Land.NoOwnerFoundError", "Aucun owner n'a été trouvé pour ce land ...");
			 config.addDefault("Commands.Land.setOwnerSuccessFull", "L'owner du land à bien été mis à jour.");
			 config.addDefault("Commands.Land.LandNotInThisProvince", "La land indiquée n'appartient pas à cette province...");
			 config.addDefault("Commands.Land.BuildingLand", "Impossible d'affecter un land à un groupe quand celui-ci n'est pas finis.");
			 config.addDefault("Commands.Land.OnlyLargeGroupCanOwnLand","Seul les Factions et les Etats peuvent posséder un land.");
			 config.addDefault("Commands.Land.SetMainLand.AlreadyMainLand","La faction a déja un land principal...");
			 config.addDefault("Commands.Land.SetMainLand.InvalidArg","Seul les valeurs 'true' ou 'false' peuvent être utilisée pour définir si un Land est principal ou non.");
			 config.addDefault("Commands.Land.SetMain.Success.False","Le land n'est désormais plus le land principal de l'organisation qui l'occupe");
			 config.addDefault("Commands.Land.SetMainLand.Success.True","Le land à bien été définis comme le land principal de l'organisation qui l'occupe");
			 config.addDefault("Commands.Land.SetIsActive.InvalidArg","Seul les valeurs 'true' ou 'false' peuvent être utilisée pour définir si un Land est actif ou non.");
			 config.addDefault("Commands.Land.SetIsActive.Success.False","Le land est désormais inactif.");
			 config.addDefault("Commands.Land.SetIsActive.Success.True","Le land est désormais actif. Attention cet état est pris en compte uniquement si une Mine et un Totem son définis");
			 config.addDefault("Commands.Land.SetCanOtherGoIn.InvalidArg","Seul les valeurs 'true' ou 'false' peuvent être utilisée pour définir si des membres d'autres groupent peuvent rejoindre un Land.");
			 config.addDefault("Commands.Land.SetCanOtherGoIn.Success.False","Le land est désormais inaccessible par les joueurs qui ne possède pas le land.");
			 config.addDefault("Commands.Land.SetCanOtherGoIn.Success.True","Le land est désormais accessible par tout le monde");

			 config.addDefault("Commands.Claim.nullLand","Vous n'etes pas dans un Land valide");
			 config.addDefault("Commands.Claim.MaxLand","Impossible de claim : vous avez atteind la limite de land.");
			 config.addDefault("Commands.Claim.nonOwnableLand","Vous ne pouvez pas claim ce Land");
			 config.addDefault("Commands.Claim.success","Vous avez bien claim ce Land");
			 config.addDefault("Commands.Claim.error","Vous ne pouvez pas rejoindre ce land");

			 config.addDefault("Commands.UnClaim.nullLand","Vous n'etes pas dans un Land valide");
			 config.addDefault("Commands.UnClaim.notYourLand","Vous ne pouvez pas unclaim ce Land");
			 config.addDefault("Commands.UnClaim.MaxUnClaim","Vous ne pouvez pas unclaim plus d'une fois par jour.");
			 config.addDefault("Commands.UnClaim.success","Vous avez bien unclaim ce Land");
			 config.addDefault("Commands.UnClaim.error","Vous ne pouvez pas quitter ce land");

			 config.addDefault("Commands.LandSubTerritory.LandSubTerritoryNotFound", "L'id SubTerritory donné n'existe pas.");
			 config.addDefault("Commands.LandSubTerritory.register.TerritoryAlreadyUsed", "Impossible de créer le Sous-Land. Un territoire utilise déja cette région world guard.");
			 config.addDefault("Commands.LandSubTerritory.register.LandSubTerritoryNotIncludeInLand", "Impossible de créer le Sous-Land. La région worldguard indiqué n'est pas comprrise intégralement dans le Land");
			 config.addDefault("Commands.LandSubTerritory.register.InvalidType", "Impossible de créer le Sous-Land. La région worldguard indiqué n'est pas comprrise intégralement dans le Land");
			 config.addDefault("Commands.LandSubTerritory.register.Success.Mine", "La Mine a bien été enregistrée");
			 config.addDefault("Commands.LandSubTerritory.register.Success.Totem", "Le Totem a bien été enregistré");
			 config.addDefault("Commands.LandSubTerritory.register.Success.RP", "Le sous-land RP a bien été enregistré");
			 config.addDefault("Commands.LandSubTerritory.register.Success.Contest", "Le sous-land Contest a bien été enregistré");
			 config.addDefault("Commands.LandSubTerritory.SubTerritoryUnregisterSuccess", "Sous-land supprimé avec succès.");
			 config.addDefault("Commands.LandSubTerritory.SubTerritorySetNameSuccess", "Sous-land renommé avec succès.");
			 config.addDefault("Commands.LandSubTerritory.setCanOtherGoInSuccess.True", "Le Sous-land est désormais inaccessible par les joueurs qui ne possède pas le Sous-Land.");
			 config.addDefault("Commands.LandSubTerritory.setCanOtherGoInSuccess.False", "Le Sous-land est désormais accessible par tout le monde");
			 config.addDefault("Commands.LandSubTerritory.CanOtherGoInError", "Seul les valeurs 'true' ou 'false' peuvent être utilisée pour définir si des membres d'autres groupent peuvent rejoindre un Sous-Land.");

			 config.addDefault("Commands.Position.NotFound","Aucun point d'intérêt n'a été trouvé avec cet ID");
			 config.addDefault("Commands.Position.InvalidArgs.Int","Invalid id, veuillez saisir un nombre entier");
			 config.addDefault("Commands.Position.Add.Position.Context.Unknow","Impossible d'ajouter ce point d'intérêt. Son context n'est pas connu.");
			 config.addDefault("Commands.Position.Add.Position.Context.MineOnly","Impossible d'ajouter ce point d'intérêt. Le context choisi ne peut être utilisé que dans une zone Mine.");
			 config.addDefault("Commands.Position.Add.Position.Context.TotemOnly","Impossible d'ajouter ce point d'intérêt. Le context choisi ne peut être utilisé que dans une zone Totem.");
			 config.addDefault("Commands.Position.Add.Position.Context.LandOnly","Impossible d'ajouter ce point d'intérêt. Le context choisi ne peut être utilisé que dans un Land et en dehors de toute sous-zones.");
			 config.addDefault("Commands.Position.Add.Position.BadTerritory","Impossible d'ajouter ce point d'intérêt. Le context choisi ne peut être utilisé que dans un Land et en dehors de toute sous-zones.");
			 config.addDefault("Commands.Position.Add.Success","Le point d'intérêt a bien été ajouté.");
			 config.addDefault("Commands.Position.Add.Remove","Le point d'intérêt a bien été suprimé.");
			 config.addDefault("Commands.Position.Add.Teleport","Téléportation effectué avec succès.");
			 config.addDefault("Commands.Position.Add.AlreadyExist","Impossible d'ajouter ce point d'intérêt : Il y en  a déja un.");

			 config.addDefault("Commands.Inventory.Create.InvalidArgs","Impossible de créer cet inventaire, la valeur de ratio ne corespond ni a un double, ni a un int");
			 config.addDefault("Commands.Inventory.Create.InvalidType","Impossible de créer cet inventaire, le type choisi n'existe pas.");
			 config.addDefault("Commands.Inventory.Create.Success","Inventaire créé avec succès.");
			 config.addDefault("Commands.Inventory.Remove.InvalidInt","Impossible de supprimer cet inentaire, L'id choisi ne corespond n'est pas un nombre entier");
			 config.addDefault("Commands.Inventory.Remove.InventoryNotFound","Impossible de supprimer cet inentaire. L'id choisi ne, corespond a aucun inventaire existent.");
			 config.addDefault("Commands.Inventory.Remove.CantRemoveDefaultMine","Impossible de supprimer la mine par défaut");
			 config.addDefault("Commands.Inventory.Remove.Success","L'inventaire a été supprimé avec succès.");
			 config.addDefault("Commands.Inventory.Edit.InvalidInt","Impossible d'éditer cet inventaire. L'id choisi ne corespond n'est pas un nombre entier");
			 config.addDefault("Commands.Inventory.Edit.InventoryNotFound","Impossible d'éditer cet inventaire. L'id choisi ne, corespond a aucun inventaire existent.");
			 config.addDefault("Commands.Inventory.Edit.Inventory.Open","L'inventaire a bien été ouvert pour l'édition.");
			 config.addDefault("Commands.Inventory.Edit.Name.Success","L'inventaire a bien été renommé.");
			 config.addDefault("Commands.Inventory.Edit.Ratio.InvalidArgs","Impossible d'éditer cet inventaire, la valeur de ratio ne corespond ni a un double, ni a un int");
			 config.addDefault("Commands.Inventory.Edit.Ratio.Success","Le ratio de cet inventaire a bien été mis à jour.");
			 config.addDefault("Commands.Inventory.Save","Inventaire sauvegardé avec succès.");
			 config.addDefault("Commands.Inventory.RotateMine","Rotation des mines effectuée avec succès.");
			 config.addDefault("Commands.Inventory.Link.Position.NotFound","Impossible de lier l'inventaire à la position : l'id position n'existe pas.");
			 config.addDefault("Commands.Inventory.Link.Position.NotMineFixe","Impossible de lier l'inventaire à la position : La position n'est pas une position MINE_FIXE");
			 config.addDefault("Commands.Inventory.Link.StoredInventory.NotMineFixe","Impossible de lier l'inventaire à la position : L'inventaire n'est pas un inventaire MINE_FIXE");
			 config.addDefault("Commands.Inventory.Link.StoredInventory.NotFound","Impossible de lier l'inventaire à la position : l'id inventaire n'existe pas.");
			 config.addDefault("Commands.Inventory.Link.Mine.Success","L'inventaire de type Mine a bien été relié à la position Mine");

			 config.addDefault("Commands.Rank.Create.Success","Rang créé avec succès.");
			 config.addDefault("Commands.Rank.Edit.Success","Rang édité avec succès.");
			 config.addDefault("Commands.Rank.Create.NotValidGroupType","Le type de groupe selectionné n'est pas valide.");
			 config.addDefault("Commands.Rank.Remove.Success","Rang supprimé avec succès.");
			 config.addDefault("Commands.Rank.InvalidInt","Certain attribut doivent être des entiers (0-9)");
			 config.addDefault("Commands.Rank.NullItem","Impossible d''enregistrer un objectif de ressources sur un item null.");
			 config.addDefault("Commands.Rank.NotExist","Le rang demandé n'existe pas.");
			 config.addDefault("Commands.Rank.UnLockLeftHand.Error","UnLockLeftHand = true or false");
			 config.addDefault("Commands.Rank.CantDefault","Vous ne pouvez pas supprimer le rang par défaut.");
			 config.addDefault("Commands.Rank.Objective.Create.Success","Objectif créé avec succès");
			 config.addDefault("Commands.Rank.Objective.Create.Fail","Impossible de créer deux objectifs avec les même nom");
			 config.addDefault("Commands.Rank.Objective.Remove.Success","Objectif supprimé avec succès");
			 config.addDefault("Commands.Rank.Objective.Remove.Fail","Impossible de supprimé l'objectif, le nom donné n'existe pas.");
			 config.addDefault("Commands.Rank.Objective.Edit.Success.NbRessources","Nombre de ressource a obtenir pour valider l'objectif éditée avec succès.");
			 config.addDefault("Commands.Rank.Objective.Edit.Success.MinVassal","Le nombre minimum de vassal a été éditée avec succès");
			 config.addDefault("Commands.Rank.Objective.Edit.Success.MinPlayer","Le nombre minimum de joueurs a été édité avec succès");
			 config.addDefault("Commands.Rank.Objective.Edit.Success.MinLand","Le nombre minimum de land a été éditée avec succès");
			 config.addDefault("Commands.Rank.Objective.Edit.Success.Desc","La description de l'objectif a été éditée avec succès.");
			 config.addDefault("Commands.Rank.Objective.Edit.Fail","Impossible d'éditer l'objectif : l'objectif demandé n'existe pas.");

			 config.addDefault("Commands.Raid.NullLand","Vous n'êtes pas sur un land, impossible de lancer le raid.");
			 config.addDefault("Commands.Raid.EmptyLand","Ce land n'a pas de propriétaire, impossible de le raid.");
			 config.addDefault("Commands.Raid.MainLand","Impossible de Raid un mainland...");
			 config.addDefault("Commands.Raid.YourLand","Impossible de raid votre land...");
			 config.addDefault("Commands.Raid.MaxDailyRaidReach","Vous ne pouvez pas raid plus aujourd'hui. Vous avez atteind votre quotat");
			 config.addDefault("Commands.Raid.AttackerAlreadyInRaid","Vous êtes déja en raid, impossible d'en lancer un nouveau.");
			 config.addDefault("Commands.Raid.DefenderAlreadyInRaid","Le détenteur du land est déja en raid. Vous ne pouvez pas le raid de suite.");
			 config.addDefault("Commands.Raid.NotEnafExperimentedPlayer","Impossible de lancer le raid. Le defendeur n'a pas assez de joueur expérimenté connecté");
			 config.addDefault("Commands.Raid.EnableRoad","Les routes du land sont de nouveau fonctionnelles");
			 config.addDefault("Commands.Raid.DisableRoad","Pendant le raid, toutes les routes du lands sont désactivée.");
			 config.addDefault("Commands.Raid.MaxLand","Impossible de raid : vous avez atteind la limite de land.");

			 config.addDefault("Commands.Raid.YouAreNotInRaid","Vous ne pouvez pas executer cette commande : Vous nêtes pas en raid.");
			 config.addDefault("Commands.Raid.NotMatchMaking","Cette commande ne peut pas être exeuté en dehors du matchmaking.");
			 config.addDefault("Commands.Raid.Interrupt","Vous avez bien arrêté le raid.");
			 config.addDefault("Commands.Raid.Add.Success","Le joueur a bien été ajouté au raid.");
			 config.addDefault("Commands.Raid.Add.ToMutchAttacker","Vous ne pouvez pas ajouter plus joueur au raid. Sinon il risque d'être trop déséquilibré.");
			 config.addDefault("Commands.Raid.Add.PlayerAlreadyInRaid","Impossible d'ajouter ce joueur au raid, le joueur est déja dans le raid.");
			 config.addDefault("Commands.Raid.Remove.Success","Le joueur a bien été retiré du raid.");
			 config.addDefault("Commands.Raid.Remove.PlayerNotInRaid","Impossible de retirer un joueur qui n'est pas dans ce raid.");

			 config.addDefault("Organisation.BreakBlockEvent.CantBreakHere","Vous ne pouvez pas détruire ici");
			 config.addDefault("Organisation.BreakBlockEvent.CantBreakOtherLand","Vous ne pouvez pas détuire un block dans un land qui n'est pas votre land principale");
			 config.addDefault("Organisation.BreakBlockEvent.CantBreakPositionPointOfInterestBlock","Vous ne pouvez pas détuire un point d'intérêt.");
			 config.addDefault("Organisation.BreakBlockEvent.NoPermission","Votre, organisation vous a retiré la permission de casser des blocks ici");

			 config.addDefault("Organisation.PlaceBlockEvent.CantPlaceHere","Vous ne pouvez pas placer des bloques ici");
			 config.addDefault("Organisation.PlaceBlockEvent.CantPlaceOtherLand","Vous ne pouvez pas poser un block dans un land qui n'est pas votre land principale");
			 config.addDefault("Organisation.PlaceBlockEvent.NoPermission","Votre, organisation vous a retiré la permission de poser des bloques");

			 config.addDefault("Organisation.InteractEvent.CantInteractHere","Vous ne pouvez pas interagir ici");
			 config.addDefault("Organisation.InteractEvent.CantInteractOtherLand","Vous ne pouvez pas interagir dans un land qui ne vous appartient pas.");
			 config.addDefault("Organisation.InteractEvent.NoPermission","Votre, organisation vous a retiré la permission d'interagir avec ce block");

			 config.addDefault("Organisation.WorldGuard.NotFound", "La région worldguard indiquée n'existe pas");
			 config.addDefault("Organisation.Raid.CantJoinRaidLand", "Ce land subit un raid. Vous n'êtes pas autorisé a rentrer dedans.");
			 config.addDefault("Organisation.LargeGroup.DamageReduction.message", "Vous subissez des dégats réduits sur vos terres.");
			 config.addDefault("Organisation.Raid.CantDamage", "Vous n'êtes pas dans le raid. Vous ne pouvez pas taper un joueur subissant un raid sur le land attaqué.");
			 config.addDefault("Organisation.Mine.NoPermission","Votre organisation vous a retirer le droit d'ouvrir les mines.");
			 config.addDefault("Organisation.Mine.CantInteract","Vous n'avez pas la permission d'intéragir avec une mine qui ne vout appartient pas.");

			 language.save();
		 }
	}

	public static FileConfiguration getConfig() {
		return ConfigManager.defaultConfig.get();
	}

	}
