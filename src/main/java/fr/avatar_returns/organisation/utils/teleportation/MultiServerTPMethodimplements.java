package fr.avatar_returns.organisation.utils.teleportation;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

public class MultiServerTPMethodimplements implements PluginMessageListener {

    public void onPluginMessageReceived(String channel, Player player, byte[] message) {


        if (!channel.equalsIgnoreCase("tp:velocity")) return;

        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String subchannel = in.readUTF();

        if (subchannel.equalsIgnoreCase("TPData")) {
            final String somedata = in.readUTF();

            String playerName = somedata.split(",")[0];
            Player p = Bukkit.getPlayer(playerName);

            boolean mustDelay = false;
            if(p == null) mustDelay = true;
            else if (!p.isOnline()) mustDelay = true;

            if(mustDelay) {

                (new BukkitRunnable() {
                    public void run() {
                        MultiServerTPMethodimplements.tpplayer(somedata);
                    }
                }).runTaskLater(Organisation.plugin, 30L);
            }
            else{
                MultiServerTPMethodimplements.tpplayer(somedata);
            }
        }
    }

    public static void tpplayer(String v) {

        World w;
        String[] values = v.split("@")[0].split(",");
        if(v.split("@").length > 1){
            if(v.split("@")[1].equals(GeneralMethods.getServerId()) && !values[4].startsWith("#tpTo:")){
                Organisation.log.warning("Cancel teleportation over proxy from this server to this server.");
                return;
            }
        }

        Player p = Bukkit.getPlayer(values[0]);
        if(p == null)return;

        try {

            float x, y, z;
            if(values[4].startsWith("#tpTo:")){

                UUID playerUUID = UUID.fromString(values[4].replace("#tpTo:",""));
                Player playerToTp = Bukkit.getPlayer(playerUUID);

                if(playerToTp == null)return;

                w = playerToTp.getWorld();
                x = playerToTp.getLocation().getBlockX();
                y = playerToTp.getLocation().getBlockY();
                z = playerToTp.getLocation().getBlockZ();
            }
            else {

                if (Bukkit.getWorld(values[4]) != null) {
                    w = Bukkit.getWorld(values[4]);
                } else {
                    w = Bukkit.getWorld("world");
                }
                x = Integer.parseInt(values[1]);
                y = Integer.parseInt(values[2]);
                z = Integer.parseInt(values[3]);
            }

            Location loc = new Location(w, x, y, z);

            for(PotionEffect potionEffect : p.getActivePotionEffects())p.removePotionEffect(potionEffect.getType());
            p.teleport(loc);
        }
        catch (IllegalArgumentException  exception){}
    }

    public static void sendTPCommand(String serverTarget, Player playertarget, int x, int y, int z, String world) {

        for(PotionEffect potionEffect : playertarget.getActivePotionEffects())playertarget.removePotionEffect(potionEffect.getType());

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        String values = serverTarget + "," + playertarget.getName() + "," + x + "," + y + "," + z + "," + world+"@"+GeneralMethods.getServerId();

        try {
            out.writeUTF("TPPaper");
            out.writeUTF(values);
            playertarget.sendPluginMessage(Organisation.plugin, "tp:velocitypaper", b.toByteArray());
        } catch (IOException var8) {
            var8.printStackTrace();
        }
    }

    public static void sendTPCommand(String serverTarget, String playerTarget, int x, int y, int z, String world) {

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        String values = serverTarget + "," + playerTarget + "," + x + "," + y + "," + z + "," + world +"@"+GeneralMethods.getServerId();

        Player player = Bukkit.getPlayer(playerTarget);
        if(player != null)for(PotionEffect potionEffect : player.getActivePotionEffects())player.removePotionEffect(potionEffect.getType());

        try {
            out.writeUTF("TPPaper");
            out.writeUTF(values);
            Bukkit.getPlayer(playerTarget).sendPluginMessage(Organisation.plugin, "tp:velocitypaper", b.toByteArray());
        } catch (IOException var8) {
            var8.printStackTrace();
        }
    }
}
