package fr.avatar_returns.organisation.storedInventory;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.StoredInventoryPacket;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.territory.VMine;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.ItemUtils;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.jetbrains.annotations.Nullable;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.Arrays;

public class StoredInventory {

    public enum Type {

        MINE("MINE"),
        MINE_FIXE("MINE_FIXE"),
        RANK_OBJECTIF("RANK_OBJECTIF");

        private String context;

        Type(String context) {
            this.context = context;
        }

        public String getStrType(){return this.context;}
        public boolean equals (String context){ return context.equals(this.context);}
        public boolean equalsIgnoreCase(String context) {return context.equalsIgnoreCase(this.context);}
        public static boolean isValidType(String type) {
            return type.equalsIgnoreCase(MINE.toString()) || type.equalsIgnoreCase(MINE_FIXE.toString()) || type.equalsIgnoreCase(RANK_OBJECTIF.toString());
        }


        @Override public String toString() {
            return this.getStrType();
        }
    }

    private final int id;
    private String name;
    private transient Inventory inventory;
    private double ratio;
    private int maxNumber;
    private final String type;
    private long nextFillTime;

    private static ArrayList<StoredInventory> lstStoredInventory = new ArrayList<>();

    public StoredInventory(String name, Player player, double ratio, int maxNumber, final String type){

        this.name = name;

        this.inventory = Bukkit.createInventory(null, 54, "§3§l" + this.name);

        // Clone inventory
        for(int i = 0; i< player.getInventory().getSize(); i++){
            if(player.getInventory().getItem(i) != null)this.inventory.setItem(i, player.getInventory().getItem(i));
        }

        this.ratio = ratio;
        this.maxNumber = maxNumber;
        this.type = type;
        this.nextFillTime = 0;

        this.id = DBUtils.addStoredInventory(this);
        GeneralMethods.sendPacket(new StoredInventoryPacket(this, OrganisationPacket.Action.CREATE));
        StoredInventory.lstStoredInventory.add(this);
    }
    public StoredInventory(final int id, String name, final Inventory inventory, double ratio, int maxNumber, final String type){

        this.id = id;

        this.name = name;
        this.inventory = inventory;
        this.ratio = ratio;
        this.maxNumber = maxNumber;
        this.type = type;
        this.nextFillTime = 0;

        StoredInventory.lstStoredInventory.add(this);
    }

    public void setName(String name){this.setName(name,true);}
    public void setName(String name, boolean storeDb) {

        this.name = name;
        if(storeDb) DBUtils.setNameStoredInventory(this, name);

        Inventory tmpInventory = Bukkit.createInventory(null, 54, "§3§l" + name);
        for(int i = 0; i< this.getInventory().getSize(); i++){
            if(this.getInventory().getItem(i) != null)tmpInventory.setItem(i, this.getInventory().getItem(i));
        }

        this.inventory = tmpInventory;
        if(storeDb) this.save();
    }

    public void setMaxNumber(int maxNumber){this.setMaxNumber(maxNumber,true);}
    public void setMaxNumber(int maxNumber, boolean storeDb) {
        if(storeDb) DBUtils.setMaxNumberStoredInventory(this, maxNumber);
        this.maxNumber = maxNumber;
    }

    public void setRatio(double ratio){this.setRatio(ratio,true);}
    public void setRatio(double ratio, boolean storeDb) {
        if(storeDb) DBUtils.setRatioStoredInventory(this, ratio);
        this.ratio = ratio;
    }

    public int getId() {return id;}
    public String getName() {return name;}
    public double getRatio() {return ratio;}
    public int getMaxNumber() {return maxNumber;}
    public Inventory getInventory() {return inventory;}
    public String getType() {return type;}
    public String getStringFromStoredInventory(){
        return StoredInventory.getStringFromInventory(this.getInventory());
    }

    public static ArrayList<StoredInventory> getLstStoredInventory() {return lstStoredInventory;}
    public static ArrayList<StoredInventory> getLstStoredInventoryType(Type type){
        return StoredInventory.getLstStoredInventoryType(type.getStrType());
    }
    public static ArrayList<StoredInventory> getLstStoredInventoryType(String type) {

        ArrayList<StoredInventory> tmpLstStoredInventoryType = new ArrayList<>();
        for(StoredInventory storedInventory : StoredInventory.getLstStoredInventory()){
            if(storedInventory.getType().equalsIgnoreCase(type))tmpLstStoredInventoryType.add(storedInventory);
        }
        return tmpLstStoredInventoryType;
    }

    @Nullable public static StoredInventory getStoredInventoryById(int id){
        for(StoredInventory storedInventory: StoredInventory.getLstStoredInventory()){
            if(storedInventory.getId() == id)return storedInventory;
        }

        return null;
    }
    @Nullable public static StoredInventory getStoredInventoryByInventory(Inventory inventory){
        for(StoredInventory storedInventory: StoredInventory.getLstStoredInventory()){
            if(storedInventory.getInventory().equals(inventory))return storedInventory;
        }

        return null;
    }
    @Nullable public static StoredInventory getStoredInventoryByName(String name){
        for(StoredInventory storedInventory: StoredInventory.getLstStoredInventory()){
            if(storedInventory.getName().equalsIgnoreCase(name))return storedInventory;
        }

        return null;
    }

    public static String getStringFromInventory(Inventory inventory){

        String stringInventory = ItemUtils.toBase64(inventory);
        return stringInventory;
    }
    @Nullable public static Inventory getInventoryFromString(String inventoryString){

        Inventory inventory = null;
        try {
            inventory = ItemUtils.fromBase64(inventoryString);
        } finally { return inventory; }
    }

    public int getUsage() {

        int usage = 0;
//        for(PositionPointInterest positionPointInterest : PositionPointInterest.getByContext(PositionPointInterest.Type.MINE)){
//            if (positionPointInterest.getOptionalValueOne().equals(""+this.getId()))usage ++;
//        }

        for(VMine vMine: VMine.getLstVMine()){
            if(vMine.getStoredInventoryId().equalsIgnoreCase(""+this.getId())) usage++;
        }

        return usage;
    }
    public long numberItemToFill(){return this.numberItemToFill(false);}
    public long numberItemToFill(boolean debug){


        long currentTimeSecond = System.currentTimeMillis()/1000;

        // Organisation.log.info("==============================");
        if(debug) Organisation.log.info( "["+this.getName()+"] Time : " + currentTimeSecond);
        if(debug) Organisation.log.info("["+this.getName()+"] NextFillTime : " + nextFillTime);

        if(currentTimeSecond < nextFillTime) return 0;

        long fillDelay = 1;
        long itemToFill = 1;

        int totalItemToFill = GeneralMethods.getItemAmountInInventory(this.getInventory());
        long secondPeriod = ConfigManager.getConfig().getInt("Organisation.Mine.PeriodToFillSecond");
        if(debug) Organisation.log.info("["+this.getName()+"] TotalItemToFill : " + totalItemToFill);
        if(debug) Organisation.log.info("["+this.getName()+"] secondPeriod : " + secondPeriod);

        if(totalItemToFill == 0 || secondPeriod == 0)return 0;

        if(debug)Organisation.log.info("["+this.getName()+"] "+ secondPeriod + " > " + totalItemToFill );
        if(debug)Organisation.log.info("-----------------------------------");

        if(secondPeriod > totalItemToFill){
            fillDelay = secondPeriod / totalItemToFill;
        }
        else if( secondPeriod < totalItemToFill){
            itemToFill =  totalItemToFill / secondPeriod;
        }
        else{
            return 1;
        }

        nextFillTime =  currentTimeSecond + fillDelay;

        //Organisation.log.info("Delay = " + fillDelay);
        //Organisation.log.info("Item to fill = " + itemToFill);

        return itemToFill;
    }

    public void save() {
        DBUtils.setInventoryStoredInventory(this);
    }

    public void remove(){this.remove(true);}
    public void remove(boolean storeDb){
        lstStoredInventory.remove(this);
        if(storeDb)DBUtils.removeStoredInventory(this);
    }

}
