package fr.avatar_returns.organisation.storedInventory;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.PositionPointInterest;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.territory.VMine;
import fr.avatar_returns.organisation.territory.landSubTerritory.rpSubTerritory.Mine;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.jetbrains.annotations.Nullable;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


import java.util.*;

public abstract class MineUtils {

    private static HashMap<VLand, Inventory> mapMineMainLand = new HashMap<>();
    private static HashMap<VMine, Inventory> mapMineInventory = new HashMap<>();
    private static HashMap<VMine, Inventory> mapMineStatic = new HashMap<>();

    private static HashMap<StoredInventory, Integer> mapProgressionStoredInventory = new HashMap<>();
    private static HashMap<StoredInventory, Integer> mapAmountStoredInventory = new HashMap<>();

    public static void weeklyAutoRotate(){

        Calendar cal = GregorianCalendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());

        int day = cal.get(Calendar.DAY_OF_WEEK);
        // int month = cal.get(Calendar.MONTH);
        // int year = cal.get(Calendar.YEAR);

        if(day == ConfigManager.getConfig().getInt("Organisation.Mine.DayOfWeekRotate")){
            Organisation.log.info("Launching auto rotation of mines");
            MineUtils.rotateMine();
        }
    }
    public static void rotateMine(){

        ArrayList<StoredInventory> lstStoredMine = StoredInventory.getLstStoredInventoryType(StoredInventory.Type.MINE);

        // Get limited Mine
        ArrayList<StoredInventory> lstStoredMineLimited = new ArrayList<>();
        for(StoredInventory storedMine : lstStoredMine) {
            if (storedMine.getMaxNumber() > 0)lstStoredMineLimited.add(storedMine);
        }
        Collections.sort(lstStoredMineLimited, new Comparator<StoredInventory>() {
            @Override
            public int compare(StoredInventory o1, StoredInventory o2) {
                return o1.getMaxNumber()- o2.getMaxNumber();
            }
        });

        // Get ratio Mine and sort from highter ratio to lower ratio.
        ArrayList<StoredInventory> lstStoredMineRatio = new ArrayList<>();

        // Reset mine's inventory.
        mapMineMainLand = new HashMap<>();
        mapMineInventory = new HashMap<>();

        for(StoredInventory storedMine : lstStoredMine) {
            if (storedMine.getRatio() > 0.0D)lstStoredMineRatio.add(storedMine);
        }
        /*
        Collections.sort(lstStoredMineRatio, new Comparator<StoredInventory>() {
            @Override
            public int compare(StoredInventory o1, StoredInventory o2) {
                return (int)(o1.getRatio()*100) - (int)(o2.getRatio() *100);
            }
        });*/
        Collections.shuffle(lstStoredMineRatio);
        Collections.shuffle(lstStoredMineLimited);

        ArrayList<VMine> lstMineShuffle = VMine.getLstVMine(PositionPointInterest.Type.MINE);
        Collections.shuffle(lstMineShuffle);

        //Reset VMine before affect Mine
        for(VMine mine : lstMineShuffle){
            mine.setOptionalValueOne("1"); //We link the position point of interest to the default Mine
        }

        //System.out.println("Mine RESET");

        for(VMine mine : lstMineShuffle){

            VLand mineLand = mine.getVLand();
            if (mineLand.isMainLand()) {
                    mine.setOptionalValueOne("1"); //We link the position point of interest to the default Mine
                    if(!MineUtils.mapMineMainLand.containsKey(mineLand)){
                        // Set Mine to default Mine.
                        mapMineMainLand.put(mineLand, Bukkit.createInventory(null, 54, "§3§lMine"));
                    }
                    continue;
            }
            //System.out.println("DEFINE MINE");

            boolean continueLoop = false;
            for(StoredInventory storedMine : lstStoredMineLimited){

                if(storedMine.getUsage() < storedMine.getMaxNumber()){

                    mine.setOptionalValueOne(""+storedMine.getId());
                    mapMineInventory.put(mine, Bukkit.createInventory(null, 54, "§3§lMine"));
                    continueLoop = true;

                    //System.out.println("ADD (" + storedMine.getId()+ ")  "+ storedMine.getName() + " --> " + storedMine.getUsage() +"/" + storedMine.getMaxNumber());
                    break;
                }
                else{
                    //System.out.println("NEXT (" + storedMine.getId()+ ")  "+ storedMine.getName() + " --> " + storedMine.getUsage() +"/" + storedMine.getMaxNumber());
                }
            }

            if(continueLoop)continue;

            for(StoredInventory storedMine : lstStoredMineRatio){
                if(storedMine.getUsage() < storedMine.getRatio()){
                    mine.setOptionalValueOne(""+storedMine.getId());
                    mapMineInventory.put(mine, Bukkit.createInventory(null, 54, "§3§lMine"));
                    continueLoop = true;
                    break;
                }
            }
            if(continueLoop)continue;

            mine.setOptionalValueOne("1"); //We link the position point of interest to the default Mine
            mapMineInventory.put(mine, Bukkit.createInventory(null, 54, "§3§lMine"));
        }

        //System.out.println("\nNB MINES : " + lstMineShuffle.size());
        //System.out.println("\nNB VMINES : " + VMine.getLstVMine().size());
    }
    public static boolean autoLinkMineToInventory(VMine mine){


        // Set Mine of Main Land to default Mine
        VLand mineLand = mine.getVLand();
        if(mineLand.isMainLand()){
            mine.setOptionalValueOne("1"); //We link the position point of interest to the default Mine
            if(!MineUtils.mapMineMainLand.containsKey(mineLand)){
                mapMineMainLand.put(mineLand, Bukkit.createInventory(null, 54, "§3§lMine"));
            }
            return true;
        }

        ArrayList<StoredInventory> lstStoredMine = StoredInventory.getLstStoredInventoryType(StoredInventory.Type.MINE);

        // Get limited Mine
        ArrayList<StoredInventory> lstStoredMineLimited = new ArrayList<>();
        for(StoredInventory storedMine : lstStoredMine) {
            if (storedMine.getMaxNumber() > 0)lstStoredMineLimited.add(storedMine);
        }
        Collections.sort(lstStoredMineLimited, new Comparator<StoredInventory>() {
            @Override
            public int compare(StoredInventory o1, StoredInventory o2) {
                return o1.getMaxNumber()- o2.getMaxNumber();
            }
        });

        // Get ratio Mine and sort from highter ratio to lower ratio.
        ArrayList<StoredInventory> lstStoredMineRatio = new ArrayList<>();
        for(StoredInventory storedMine : lstStoredMine) {
            if (storedMine.getRatio() > 0.0D)lstStoredMineRatio.add(storedMine);
        }
        Collections.sort(lstStoredMineRatio, new Comparator<StoredInventory>() {
            @Override
            public int compare(StoredInventory o1, StoredInventory o2) {
                return (int)(o1.getRatio()*100) - (int)(o2.getRatio() *100);
            }
        });

        for(StoredInventory storedMine : lstStoredMineLimited){
            if(storedMine.getUsage() < storedMine.getMaxNumber()){
                mine.setOptionalValueOne(""+storedMine.getId());
                mapMineInventory.put(mine, Bukkit.createInventory(null, 54, "§3§lMine"));
                return true;
            }
        }
        for(StoredInventory storedMine : lstStoredMineRatio){
            if(storedMine.getUsage() < storedMine.getRatio()){
                mine.setOptionalValueOne(""+storedMine.getId());
                mapMineInventory.put(mine, Bukkit.createInventory(null, 54, "§3§lMine"));
                return true;
            }
        }

        mine.setOptionalValueOne("1"); //We link the position point of interest to the default Mine
        mapMineInventory.put(mine, Bukkit.createInventory(null, 54, "§3§lMine"));

        return true;
    }

    public static void prepareMineMechanismes() {

        mapMineMainLand = new HashMap<>();
        mapMineInventory = new HashMap<>();

        ArrayList<VMine> lstMineShuffle = VMine.getLstVMine(PositionPointInterest.Type.MINE);
        Collections.shuffle(lstMineShuffle);

        for(VMine mine: lstMineShuffle){

            int expectedStoredInventoryId = 0;
            try {
                expectedStoredInventoryId = Integer.parseInt(mine.getStoredInventoryId());
            }
            catch(NumberFormatException e){
                MineUtils.autoLinkMineToInventory(mine);
                continue;
            }

            VLand mineLand = mine.getVLand();
            if (mineLand.isMainLand()) {
                if(!MineUtils.mapMineMainLand.containsKey(mineLand)){
                    mapMineMainLand.put(mineLand, Bukkit.createInventory(null, 54, "§3§lMine"));
                }
            }
            else{

                StoredInventory linkedStoredIventory = StoredInventory.getStoredInventoryById(expectedStoredInventoryId);
                if(linkedStoredIventory == null){
                    MineUtils.autoLinkMineToInventory(mine);
                    continue;
                }

                mapMineInventory.put(mine, Bukkit.createInventory(null, 54, "§3§lMine"));
            }
        }

        ArrayList<VMine> lstMineStatic = VMine.getLstVMine(PositionPointInterest.Type.MINE_FIXE);
        for(VMine mineStatic : lstMineStatic){
            int expectedStoredInventoryId = 1;
            try {
                expectedStoredInventoryId = Integer.parseInt(mineStatic.getStoredInventoryId());
            }
            catch(NumberFormatException e){
                continue;
            }
            if(!MineUtils.mapMineStatic.containsKey(mineStatic)){
                mapMineStatic.put(mineStatic, Bukkit.createInventory(null, 54, "§3§lMine"));
            }
        }
    }
    public static void filleMines(){

        for(StoredInventory storedInventory: StoredInventory.getLstStoredInventory()){

            if(storedInventory == null)continue;
            int id = storedInventory.getId();

            HashMap<VMine, Inventory> baseMap;
            HashMap<Object, Inventory> requestMap = ( storedInventory.getId() == 1)? new HashMap<>(mapMineMainLand): new HashMap<>();
            ArrayList<VMine> lstLinkedMine = MineUtils.getLinkedMines(storedInventory);

            if(lstLinkedMine.isEmpty())continue;


            if(storedInventory.getType().equals(StoredInventory.Type.MINE.toString()))baseMap = mapMineInventory;
            else if(storedInventory.getType().equals(StoredInventory.Type.MINE_FIXE.toString()))baseMap = mapMineStatic;
            else {
                Organisation.log.severe(storedInventory.getType());
                continue;
            }

            for(VMine vMine : baseMap.keySet()){
                if(lstLinkedMine.contains(vMine))requestMap.put((Object) vMine, baseMap.get(vMine));
            }

            if(requestMap.isEmpty())continue;
            MineUtils.fillStoredInventory(storedInventory, requestMap);
        }
    }

    private static ArrayList<VMine> getLinkedMines(StoredInventory storedInventory){

        ArrayList<VMine> lstLinkedMine = new ArrayList<>();
        String storedInventoryId = ""+storedInventory.getId();

        for(VMine vMine: VMine.getLstVMine()){
            if(!vMine.getStoredInventoryId().equalsIgnoreCase(storedInventoryId))continue;

            if(mapMineInventory.keySet().contains(vMine))lstLinkedMine.add(vMine);
            else if(mapMineStatic.keySet().contains(vMine))lstLinkedMine.add(vMine);
            else if(mapMineMainLand.containsKey(vMine.getVLand())) lstLinkedMine.add(vMine);
        }

//        OLD WAY
//        for(VMine mineInventory : mapMineInventory.keySet()){
//            if(mineInventory.getStoredInventoryId().equalsIgnoreCase(storedInventoryId))lstLinkedMine.add(mineInventory);
//        }
//        for(VMine mineStatic : mapMineStatic.keySet()){
//            if(mineStatic.getStoredInventoryId().equalsIgnoreCase(storedInventoryId))lstLinkedMine.add(mineStatic);
//        }

        return lstLinkedMine;
    }
    private static void fillStoredInventory(StoredInventory mine, HashMap<Object, Inventory> mapObjectInventory){
        MineUtils.fillStoredInventory(mine, mapObjectInventory, false);
    }
    private static void fillStoredInventory(StoredInventory mine, HashMap<Object, Inventory> mapObjectInventory, boolean debug){

        long itemToFill = mine.numberItemToFill(debug);
        if(debug) Organisation.log.info("["+mine.getName()+"] Remplissage de " + itemToFill + " items");

        if(itemToFill <= 0)return;

        if (!mapProgressionStoredInventory.containsKey(mine))mapProgressionStoredInventory.put(mine, 0);
        if (!mapAmountStoredInventory.containsKey(mine)) mapAmountStoredInventory.put(mine, 0);

        for( int nbItemToAdd = 0; nbItemToAdd < itemToFill; nbItemToAdd ++) {

            int position = mapProgressionStoredInventory.get(mine);
            while (  position < 54 && mine.getInventory().getItem(position) == null) {
                position++;
                mapProgressionStoredInventory.put(mine, position);
            }

            if(position >= 54){
                break;
            }
            if (mine.getInventory().getItem(position) != null) {
                for (Object object : mapObjectInventory.keySet()) {

                    ItemStack item = mapObjectInventory.get(object).getItem(position);

                    if(item == null){
                        item = mine.getInventory().getItem(position).clone();
                        item.setAmount(1);
                    }
                    else if(item.getType() != mine.getInventory().getItem(position).getType()){
                        item = mine.getInventory().getItem(position).clone();
                        item.setAmount(1);
                    }
                    else if (item.getAmount() < mine.getInventory().getItem(position).getAmount())item.setAmount(item.getAmount() + 1);

                    mapObjectInventory.get(object).setItem(position, item);
                }
            }

            mapAmountStoredInventory.put(mine, mapAmountStoredInventory.get(mine) + 1);

            if (mapAmountStoredInventory.get(mine) >= mine.getInventory().getItem(position).getAmount()) {
                mapProgressionStoredInventory.put(mine, position + 1);
                mapAmountStoredInventory.put(mine, 0);
            }
        }

        if (mapProgressionStoredInventory.get(mine) >= 54) {
            mapProgressionStoredInventory.put(mine, 0);
            mapAmountStoredInventory.put(mine, 0);
        }
    }

    @Nullable public static Inventory getInventoryOfMinePosition(PositionPointInterest positionPointInterest) {
        for(VMine vMine : VMine.getLstVMine()){
            if(vMine.getMine().equals(positionPointInterest))return getInventoryOfMinePosition(vMine);
        }
        return null;
    }
    @Nullable public static Inventory getInventoryOfMinePosition(VMine mine){

        if(!mine.getMine().getContext().equals(PositionPointInterest.Type.MINE)){

             if(mine.getMine().getContext().equals(PositionPointInterest.Type.MINE_FIXE)){
                 if(!MineUtils.mapMineStatic.containsKey(mine)){
                     try{
                         int idInventory = Integer.valueOf(mine.getStoredInventoryId());
                         mapMineStatic.put(mine, Bukkit.createInventory(null, 54, "§3§lMine"));
                     }
                     catch (NumberFormatException e){
                         mine.setOptionalValueOne("");
                         return null;
                     }
                 }
                 return mapMineStatic.get(mine);
             }
            return null;
        }

        VLand mineLand = mine.getVLand();
        if (mineLand.isMainLand()) {
            if(mapMineMainLand.containsKey(mine))return mapMineMainLand.get(mineLand);
            MineUtils.autoLinkMineToInventory(mine);
            return mapMineMainLand.get(mineLand);
        }

        if(mapMineInventory.containsKey(mine))return mapMineInventory.get(mine);
        MineUtils.autoLinkMineToInventory(mine);

        if(mapMineInventory.containsKey(mine))return mapMineInventory.get(mine);
        return mapMineMainLand.get(mineLand);
    }


    @Nullable public static Land getLandFromInventory(Inventory clickedInventory) {

        for(VLand vLand : mapMineMainLand.keySet()){
            if(mapMineMainLand.get(vLand).equals(clickedInventory))return Land.getLandById(vLand.getId());
        }

        for(VMine vMine : mapMineInventory.keySet()){
            if(mapMineInventory.get(vMine).equals(clickedInventory))return Land.getLandById(vMine.getVLand().getId());
        }

        for(VMine vMine : mapMineStatic.keySet()){
            if(mapMineStatic.get(vMine).equals(clickedInventory))return Land.getLandById(vMine.getVLand().getId());
        }

        return null;
    }
}
