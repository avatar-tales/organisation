package fr.avatar_returns.organisation.rabbitMQPackets.player;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.rabbitMQPackets.Packet;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import fr.avatar_returns.organisation.utils.messages.Title;
import org.bukkit.OfflinePlayer;

import java.util.HashMap;
import java.util.UUID;

public class TitlePacket implements Packet {

    UUID uuid;
    String title;
    String subTitle;
    int fadeIn;
    int fadeOut;
    int stay;

    public TitlePacket(OrganisationPlayer oPlayer, int fadeIn, int stay, int fadeOut, String title, String subTitle){
        this.uuid = oPlayer.getUuid();
        this.title = title;
        this.subTitle = subTitle;
        this.fadeIn = fadeIn;
        this.stay = stay;
        this.fadeOut = fadeOut;
    }

    public static Messaging.MessagingCodec<TitlePacket> getCodec(Organisation organisation){
        Messaging.MessagingCodec<TitlePacket> defaultCodec = organisation.getMessaging().getDefaultCodec(TitlePacket.class);
        defaultCodec.receive((packet, source) ->{
            try{
                if(source.equals(ConfigManager.getServerId()))return;
                OfflinePlayer offlinePlayer = GeneralMethods.getOfflinePlayerFromUUID(packet.uuid);
                if(offlinePlayer == null)return;
                if(offlinePlayer.getPlayer() == null)return;

                String title = packet.title;
                String subTitle = packet.subTitle;
                HashMap<String, String> mapSmiley = ConfigManager.getConfig().getObject("Organisation.chat.settings.remap", HashMap.class);
                for(String key : mapSmiley.keySet()) {
                    title = title.replace(key, ConfigManager.getConfig().getString("Organisation.chat.settings.remap." + key));
                    subTitle = subTitle.replace(key, ConfigManager.getConfig().getString("Organisation.chat.settings.remap." + key));
                }

                Title.sendFullTitle(offlinePlayer.getPlayer(),
                        packet.fadeIn,
                        packet.stay,
                        packet.fadeOut,
                        title,
                        subTitle);
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
