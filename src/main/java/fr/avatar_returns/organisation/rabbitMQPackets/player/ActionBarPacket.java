package fr.avatar_returns.organisation.rabbitMQPackets.player;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.rabbitMQPackets.Packet;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.OfflinePlayer;

import java.util.UUID;

public class ActionBarPacket implements Packet {

    UUID uuid;
    String message = "";

    public ActionBarPacket(OrganisationPlayer oPlayer, String message){

        this.message = message;
        this.uuid = oPlayer.getUuid();
    }

    public static Messaging.MessagingCodec<ActionBarPacket> getCodec(Organisation organisation){
        Messaging.MessagingCodec<ActionBarPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(ActionBarPacket.class);
        defaultCodec.receive((packet, source) ->{
            try{
                if(source.equals(ConfigManager.getServerId()))return;
                OfflinePlayer offlinePlayer = GeneralMethods.getOfflinePlayerFromUUID(packet.uuid);
                if(offlinePlayer == null)return;
                if(offlinePlayer.getPlayer() == null)return;

                offlinePlayer.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(packet.message));
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
