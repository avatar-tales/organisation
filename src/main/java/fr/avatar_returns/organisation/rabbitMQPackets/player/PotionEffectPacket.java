package fr.avatar_returns.organisation.rabbitMQPackets.player;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.rabbitMQPackets.Packet;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

import java.util.UUID;

public class PotionEffectPacket implements Packet {

    UUID uuid;
    PotionEffect potionEffect;

    public PotionEffectPacket(OrganisationPlayer oPlayer, PotionEffect potionEffect){

        this.potionEffect = potionEffect;
        this.uuid = oPlayer.getUuid();
    }

    public static Messaging.MessagingCodec<PotionEffectPacket> getCodec(Organisation organisation){
        Messaging.MessagingCodec<PotionEffectPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(PotionEffectPacket.class);
        defaultCodec.receive((packet, source) ->{
            try{
                if(source.equals(ConfigManager.getServerId()))return;
                OfflinePlayer offlinePlayer = GeneralMethods.getOfflinePlayerFromUUID(packet.uuid);
                if(offlinePlayer == null)return;
                if(offlinePlayer.getPlayer() == null)return;

                Player player = offlinePlayer.getPlayer();
                player.addPotionEffect(packet.potionEffect);
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
