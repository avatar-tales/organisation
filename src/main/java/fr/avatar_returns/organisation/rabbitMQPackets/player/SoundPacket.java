package fr.avatar_returns.organisation.rabbitMQPackets.player;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.rabbitMQPackets.Packet;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.UUID;

public class SoundPacket implements Packet {

    UUID uuid;
    Sound sound;

    public SoundPacket(OrganisationPlayer oPlayer, Sound sound){

        this.sound = sound;
        this.uuid = oPlayer.getUuid();
    }

    public static Messaging.MessagingCodec<SoundPacket> getCodec(Organisation organisation){
        Messaging.MessagingCodec<SoundPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(SoundPacket.class);
        defaultCodec.receive((packet, source) ->{
            try{
                if(source.equals(ConfigManager.getServerId()))return;
                OfflinePlayer offlinePlayer = GeneralMethods.getOfflinePlayerFromUUID(packet.uuid);
                if(offlinePlayer == null)return;
                if(offlinePlayer.getPlayer() == null)return;


                Player player = offlinePlayer.getPlayer();
                player.playSound(player.getLocation(), packet.sound, 1.0f, 1.0f);
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }

}
