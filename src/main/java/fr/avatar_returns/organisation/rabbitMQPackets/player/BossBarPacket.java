package fr.avatar_returns.organisation.rabbitMQPackets.player;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.rabbitMQPackets.Packet;
import fr.avatar_returns.organisation.listener.LandPlayerEventManager;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.bukkit.OfflinePlayer;

import java.util.UUID;

public class BossBarPacket implements Packet {

    UUID uuid;
    String message = "";

    public BossBarPacket(OrganisationPlayer oPlayer, String message){
        this.message = message;
        this.uuid = oPlayer.getUuid();
    }

    public static Messaging.MessagingCodec<BossBarPacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<BossBarPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(BossBarPacket.class);
        defaultCodec.receive((packet, source) ->{
            try{
                if(source.equals(ConfigManager.getServerId()))return;
                OfflinePlayer offlinePlayer = GeneralMethods.getOfflinePlayerFromUUID(packet.uuid);
                if(offlinePlayer == null)return;
                if(offlinePlayer.getPlayer() == null)return;

                LandPlayerEventManager.mapPlayerPriorBossBarMessage.put(offlinePlayer.getPlayer() ,packet.message);
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
