package fr.avatar_returns.organisation.rabbitMQPackets.player;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.rabbitMQPackets.Packet;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.TextReplacementConfig;
import org.bukkit.OfflinePlayer;

import java.util.HashMap;
import java.util.UUID;

public class SimpleChatPacket implements Packet {

    UUID uuid;
    String message = "";
    public SimpleChatPacket(OrganisationPlayer oPlayer, String message){
        this.message = message;
        this.uuid = oPlayer.getUuid();
    }

    public static Messaging.MessagingCodec<SimpleChatPacket> getCodec(Organisation organisation){
        Messaging.MessagingCodec<SimpleChatPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(SimpleChatPacket.class);
        defaultCodec.receive((packet, source) ->{
            try{
                if(source.equals(ConfigManager.getServerId()))return;
                OfflinePlayer offlinePlayer = GeneralMethods.getOfflinePlayerFromUUID(packet.uuid);
                if(offlinePlayer == null)return;
                if(offlinePlayer.getPlayer() == null)return;

                String msg = packet.message;
                HashMap<String, String> mapSmiley = ConfigManager.getConfig().getObject("Organisation.chat.settings.remap", HashMap.class);
                for(String key : mapSmiley.keySet()) msg = msg.replace(key, ConfigManager.getConfig().getString("Organisation.chat.settings.remap." + key));

                offlinePlayer.getPlayer().sendMessage(msg);
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
