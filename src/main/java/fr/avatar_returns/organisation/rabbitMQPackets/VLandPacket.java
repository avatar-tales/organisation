package fr.avatar_returns.organisation.rabbitMQPackets;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.territory.VMine;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

public class VLandPacket extends OrganisationPacket{

    public VLand vLand;

    public VLandPacket(VLand vLand, Action action) {
        super(action);
        this.vLand = vLand;
    }

    public static Messaging.MessagingCodec<VLandPacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<VLandPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(VLandPacket.class);
        defaultCodec.receive((packet, source) ->{

            try {
                if (source.equals(ConfigManager.getServerId())) return;
                if (packet.vLand == null) {
                    if (packet.action.equals(Action.GET_ALL)) {
                        for (VLand vLand1 : VLand.getLstVland()) {
                            if (vLand1.getServerId().equals(GeneralMethods.getServerId())) {
                                GeneralMethods.sendPacket(new VLandPacket(vLand1, Action.UPDATE), source);

                            }
                        }
                    }
                    return;
                }

                VLand localVland = VLand.getVlandById(packet.vLand.getId());
                if (localVland == null && packet.action.equals(Action.UPDATE)) packet.action = Action.CREATE;

                if (packet.action.equals(Action.CREATE) && localVland == null) {
                    VLand vland = new VLand(packet.vLand.getId(),
                            packet.vLand.getServerId(),
                            packet.vLand.getWgRegionName(),
                            packet.vLand.getOwnerId(),
                            packet.vLand.isActive(),
                            packet.vLand.isMainLand(),
                            packet.vLand.getOwnedTime(),
                            packet.vLand.canOtherGoIn(),
                            packet.vLand.getName(),
                            packet.vLand.getProvinceName()
                    );
                    if (!packet.vLand.getFhome().equals(""))vland.setFhome(packet.vLand.getFhome());

                } else if (packet.action.equals(Action.UPDATE)) {

                    localVland.setOwnerId(packet.vLand.getOwnerId());
                    localVland.setActive(packet.vLand.isActive());
                    localVland.setMainLand(packet.vLand.isMainLand());
                    localVland.setOwnedTime(packet.vLand.getOwnedTime());
                    localVland.setCanOtherGoIn(packet.vLand.canOtherGoIn());
                    localVland.setCanOtherGoIn(packet.vLand.canOtherGoIn());
                    localVland.setName(packet.vLand.getName());
                    localVland.setProvinceName(packet.vLand.getProvinceName());
                    localVland.setFhome(packet.vLand.getFhome(), false);

                } else if (packet.action.equals(Action.REMOVE) && localVland != null) {
                    localVland.remove();
                }
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
