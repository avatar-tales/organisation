package fr.avatar_returns.organisation.rabbitMQPackets.rank;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rank.*;
import fr.avatar_returns.organisation.utils.ItemUtils;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;

public class ObjectivePacket extends OrganisationPacket {

    public Objective objective;
    public String itemString;

    public ObjectivePacket(Objective objective, Action action) {

        super(action);
        this.objective = objective;

        ItemStack[] lstItem = {objective.getItem()};
        this.itemString = ItemUtils.itemStackArrayToBase64(lstItem);

    }

    public static Messaging.MessagingCodec<ObjectivePacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<ObjectivePacket> defaultCodec = organisation.getMessaging().getDefaultCodec(ObjectivePacket.class);
        defaultCodec.receive((packet, source) ->{

            if(source.equals(ConfigManager.getServerId()))return;
            if(packet.objective == null)return;

            Rank localRank = null;

            if(packet.objective.getRank() instanceof GroupRank)localRank = GroupRank.getById(packet.objective.getRank().getId());
            else if(packet.objective.getRank() instanceof PlayerRank)localRank = PlayerRank.getById(packet.objective.getRank().getId());
            else{
                Organisation.log.warning("Unable to sync GroupObjective, Rank type is unknow : consider update organisation");
                return;
            }
            if(localRank == null)return;

            Objective localObjective = localRank.getObjectiveByName(packet.objective.getName());
            if(!packet.action.equals(Action.REMOVE)) {

                ItemStack item = null;
                try {
                    ItemStack[] lstItem = ItemUtils.itemStackArrayFromBase64(packet.itemString);
                    item = (lstItem.length < 1) ? null : lstItem[0];

                }catch (IOException e){
                    return;
                }


                if (packet.action.equals(Action.CREATE) && localObjective == null) {
                    localRank.addObjectif(packet.objective.getName(), packet.objective.getDescription(), packet.objective.getObjectifValue(), item, packet.objective.getType(), false);
                } else if (packet.action.equals(Action.UPDATE) && localObjective != null) {
                    localObjective.setDescription(packet.objective.getDescription(), false);
                    localObjective.setObjectifValue(packet.objective.getObjectifValue(), false);
                }
            }
            else if(localObjective != null){
                localRank.deleteObjectif(localObjective.getName(), false);
            }

        });

        return defaultCodec;
    }
}
