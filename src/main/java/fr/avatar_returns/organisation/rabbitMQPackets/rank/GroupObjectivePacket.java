package fr.avatar_returns.organisation.rabbitMQPackets.rank;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rank.*;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

public class GroupObjectivePacket extends OrganisationPacket {

    public int groupId;
    public Objective objective;
    public int currentObjectiveValue;

    public GroupObjectivePacket(GroupObjective groupObjective, Action action) {

        super(action);
        this.groupId = groupObjective.getGroup().getId();
        this.objective = groupObjective.getObjective();
        this.currentObjectiveValue = groupObjective.getCurrentObjectiveValue();
    }

    public static Messaging.MessagingCodec<GroupObjectivePacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<GroupObjectivePacket> defaultCodec = organisation.getMessaging().getDefaultCodec(GroupObjectivePacket.class);
        defaultCodec.receive((packet, source) ->{

            if(source.equals(ConfigManager.getServerId()))return;
            if(packet.objective == null)return;

            Group localGroup = Group.getGroupById(packet.groupId);
            Rank localRank = null;

            if(packet.objective.getRank() instanceof GroupRank)localRank = GroupRank.getById(packet.objective.getRank().getId());
            else{
                Organisation.log.warning("Unable to sync GroupObjective, Rank type is unknow : consider update organisation");
                return;
            }
            if(localRank == null)return;

            Objective localObjective = localRank.getObjectiveByName(packet.objective.getName());
            if(localObjective == null)return;

            GroupObjective localGroupObjective = null;
            for(GroupObjective groupObjective : localGroup.getLstGroupObjective()){
                if(groupObjective.getObjective().equals(localObjective)){
                    localGroupObjective = groupObjective;
                    break;
                }
            }

            if(packet.action.equals(Action.CREATE) && localGroupObjective == null){
                localGroup.getLstGroupObjective().add(new GroupObjective(localGroup, localObjective, packet.currentObjectiveValue));
            }
            else if(packet.action.equals(Action.UPDATE) && localGroupObjective != null){
                localGroupObjective.update(packet.currentObjectiveValue, false);
            }
            else if(packet.action.equals(Action.REMOVE) && localGroupObjective != null){
                if(localGroup.getLstGroupObjective().contains(localGroupObjective))localGroup.getLstGroupObjective().remove(localGroupObjective);
            }
        });

        return defaultCodec;
    }
}
