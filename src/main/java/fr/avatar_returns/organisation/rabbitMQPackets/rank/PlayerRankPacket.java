package fr.avatar_returns.organisation.rabbitMQPackets.rank;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.rank.Objective;
import fr.avatar_returns.organisation.rank.PlayerRank;
import fr.avatar_returns.organisation.rank.Rank;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

import java.util.ArrayList;

public class PlayerRankPacket extends OrganisationPacket {

    public PlayerRank playerRank;

    public PlayerRankPacket(PlayerRank playerRank, Action action) {
        super(action);
        this.playerRank = playerRank;
    }

    public static Messaging.MessagingCodec<PlayerRankPacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<PlayerRankPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(PlayerRankPacket.class);
        defaultCodec.receive((packet, source) ->{


            if(source.equals(ConfigManager.getServerId()))return;
            if(packet.playerRank == null)return;

            PlayerRank localRank = PlayerRank.getById(packet.playerRank.getId());
            if(packet.action.equals(Action.CREATE) && localRank == null){

                new PlayerRank(packet.playerRank.getId(),
                        packet.playerRank.getName(),
                        packet.playerRank.getOrder(),
                        new ArrayList<Objective>(),
                        packet.playerRank.getMinHeart(),
                        packet.playerRank.getMaxSlotInv(),
                        packet.playerRank.isUnlockLeftHand());

            }
            else if(packet.action.equals(Action.UPDATE) && localRank != null){

                localRank.setName(packet.playerRank.getName(),false);
                localRank.setOrder(packet.playerRank.getOrder(),false);
                localRank.setMinHeart(packet.playerRank.getMinHeart(), false);
                localRank.setMaxSlotInv(packet.playerRank.getMaxSlotInv(), false);
                localRank.setUnLockLeftHand(packet.playerRank.isUnlockLeftHand(), false);

            }
            else if(packet.action.equals(Action.REMOVE) && localRank != null){
                localRank.remove(false);
            }
        });
        return defaultCodec;
    }
}
