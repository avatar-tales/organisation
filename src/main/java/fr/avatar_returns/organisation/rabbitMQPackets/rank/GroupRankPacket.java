package fr.avatar_returns.organisation.rabbitMQPackets.rank;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.rank.Objective;
import fr.avatar_returns.organisation.rank.PlayerRank;
import fr.avatar_returns.organisation.rank.Rank;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

import java.util.ArrayList;

public class GroupRankPacket extends OrganisationPacket {

    public GroupRank groupRank;
    public GroupRankPacket(GroupRank groupRank, Action action) {
        super(action);
        this.groupRank = groupRank;
    }

    public static Messaging.MessagingCodec<GroupRankPacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<GroupRankPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(GroupRankPacket.class);
        defaultCodec.receive((packet, source) ->{


            if(source.equals(ConfigManager.getServerId()))return;
            if(packet.groupRank == null)return;

            GroupRank localRank = GroupRank.getById(packet.groupRank.getId());

            if(packet.action.equals(Action.CREATE) && localRank == null) {

                new GroupRank(packet.groupRank.getId(),
                        packet.groupRank.getName(),
                        packet.groupRank.getOrder(),
                        packet.groupRank.getGroupType(),
                        packet.groupRank.getMaxLand(),
                        packet.groupRank.getMaxVassal(),
                        packet.groupRank.getMaxPlayer(),
                        packet.groupRank.isCanVassalize(),
                        new ArrayList<Objective>());

            }
            else if(packet.action.equals(Action.UPDATE) && localRank != null){

                localRank.setName(packet.groupRank.getName(),false);
                localRank.setOrder(packet.groupRank.getOrder(),false);
                localRank.setGroupType(packet.groupRank.getGroupType(), false);
                localRank.setMaxLand(packet.groupRank.getMaxLand(), false);
                localRank.setMaxVassal(packet.groupRank.getMaxVassal(), false);
                localRank.setMaxPlayer(packet.groupRank.getMaxPlayer(), false);
                localRank.setCanVassalize(packet.groupRank.isCanVassalize(), false);
            }
            else if(packet.action.equals(Action.REMOVE) && localRank != null){
                localRank.remove(false);
            }
        });
        return defaultCodec;
    }
}
