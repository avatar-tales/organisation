package fr.avatar_returns.organisation.rabbitMQPackets;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.territory.VMine;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

public class VMinePacket extends OrganisationPacket{

    public VMine vMine;

    public VMinePacket(VMine vMine, Action action) {
        super(action);
        this.vMine = vMine;
    }

    public static Messaging.MessagingCodec<VMinePacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<VMinePacket> defaultCodec = organisation.getMessaging().getDefaultCodec(VMinePacket.class);
        defaultCodec.receive((packet, source) ->{

            try{
                if(source.equals(ConfigManager.getServerId()))return;
                if(packet.vMine == null){
                    if(packet.action.equals(Action.GET_ALL)){
                        for(VMine vMine1 : VMine.getLstVMine()){
                            GeneralMethods.sendPacket(new VMinePacket(vMine1, Action.UPDATE), source);
                        }
                    }
                    return;
                }

                VMine localVMine = null;
                for(VMine vMine1 : VMine.getLstVMine()){
                    if(vMine1.getMine().getId() == packet.vMine.getMine().getId() && vMine1.getServerId().equals(packet.vMine.getServerId())){
                        localVMine = vMine1;
                        break;
                    }
                }

                if(localVMine == null && packet.action.equals(Action.UPDATE))packet.action = Action.CREATE;

                if(packet.action.equals(Action.CREATE) && localVMine == null){
                    new VMine(packet.vMine.getServerId(), packet.vMine.getVLand(), packet.vMine.getMinePosition());
                }
                else if(packet.action.equals(Action.UPDATE)){
                    localVMine.setOptionalValueOne(packet.vMine.getStoredInventoryId(), false);
                    localVMine.setOptionalValueTwo(packet.vMine.getOptionalValueTwo(), false);
                }
                else if(packet.action.equals(Action.REMOVE) && localVMine != null){
                    localVMine.remove();
                }
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
