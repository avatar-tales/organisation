package fr.avatar_returns.organisation.rabbitMQPackets.group;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

import java.util.UUID;

public class GroupMemberPacket extends OrganisationPacket {

    int groupId;
    UUID oPlayerUUID;
    String roleName;
    long firstTimeConnexion;
    long lastEffectivePlayedTime;

    public GroupMemberPacket(OrganisationMember member, Group group, Action action) {
        super(action);
        this.groupId = group.getId();
        this.oPlayerUUID = member.getOrganisationPlayer().getUuid();
        this.roleName = member.getRole().getName();
        this.firstTimeConnexion = member.getFirstTimeConnexion();
        this.lastEffectivePlayedTime = member.getLastEffectivePlayedTime();

    }

    public static Messaging.MessagingCodec<GroupMemberPacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<GroupMemberPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(GroupMemberPacket.class);
        defaultCodec.receive((packet, source) ->{
            try{
                if(source.equals(ConfigManager.getServerId()))return;
                if(packet.oPlayerUUID == null)return;

                Group localGroup = Group.getGroupById(packet.groupId);
                if(localGroup == null)return;

                OrganisationMember localMember = null;
                for(OrganisationMember member : localGroup.getLstMember()){
                    if(member.getOrganisationPlayer().getUuid().equals(packet.oPlayerUUID)){
                        localMember = member;
                    }
                }

                Role localRole = null;
                for(Role role: localGroup.getLstRole()){
                    if(role.getName().equals(packet.roleName)) localRole = role;
                }

                if(localRole == null)return;

                if(localMember == null){
                    if(packet.action.equals(Action.REMOVE))return;
                    packet.action = Action.CREATE;
                }
                if(packet.action.equals(Action.CREATE) && localMember == null){

                    try {
                        OrganisationPlayer oPlayer = OrganisationPlayer.getOrganisationPlayerByUUID(packet.oPlayerUUID);
                        localMember = new OrganisationMember(localGroup, oPlayer, localRole, packet.firstTimeConnexion, packet.lastEffectivePlayedTime);
                        localGroup.getLstMember().add(localMember);
                    }
                    catch (OrganisationPlayerException e) {};

                }
                else if(packet.action.equals(Action.UPDATE)){
                    localMember.setRole(localRole, false);
                }
                else if(packet.action.equals(Action.REMOVE)){
                    try {
                        OrganisationPlayer oPlayer = OrganisationPlayer.getOrganisationPlayerByUUID(packet.oPlayerUUID);
                        localGroup.removeMember(oPlayer, false);
                    }
                    catch (OrganisationPlayerException e) {};
                }
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
