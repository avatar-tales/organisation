package fr.avatar_returns.organisation.rabbitMQPackets.group;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.largeGroup.Faction;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

public class GuildePacket extends OrganisationPacket {

    Guilde guilde;

    public GuildePacket(Guilde guilde, Action action) {
        super(action);
        this.guilde = guilde;
    }

    public static Messaging.MessagingCodec<GuildePacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<GuildePacket> defaultCodec = organisation.getMessaging().getDefaultCodec(GuildePacket.class);
        defaultCodec.receive((packet, source) ->{
            try{
                if(source.equals(ConfigManager.getServerId()))return;
                if(packet.guilde == null)return;

                Group localGroup = Group.getGroupById(packet.guilde.getId());
                GroupRank localRank = GroupRank.getById(packet.guilde.getRank().getId());
                if(localRank == null)return;

                if(packet.action.equals(Action.CREATE) && localGroup == null){

                    new Guilde(packet.guilde.getId(),
                            packet.guilde.getName(),
                            packet.guilde.getMaxPlayer(),
                            packet.guilde.getMinPlayer(),
                            packet.guilde.getDescription(),
                            packet.guilde.getConnexionMessage(),
                            localRank,
                            (packet.guilde.getFlag() == null)? null : packet.guilde.getFlag().clone(),
                            packet.guilde.isVisible(),
                            packet.guilde.canUnClaim());
                }else{
                    if(!(localGroup instanceof Guilde))return;
                    Guilde localguilde = (Guilde) localGroup;

                    if(packet.action.equals(Action.UPDATE)){

                        localguilde.setName(packet.guilde.getName(), false);
                        localguilde.setConnexionMessage(packet.guilde.getConnexionMessage(), false);
                        localguilde.setDescription(packet.guilde.getDescription(), false);
                        localguilde.setFlag((packet.guilde.getFlag() == null)? null : packet.guilde.getFlag().clone(), false);
                        localguilde.setIsVisible(packet.guilde.isVisible(), false);
                        localguilde.setMaxPlayer(packet.guilde.getMaxPlayer(), false);
                        localguilde.setMinPlayer(packet.guilde.getMinPlayer(), false);
                        localguilde.setCanUnClaim(packet.guilde.canUnClaim(), false);

                        if(localRank.getId() != localguilde.getId()) localguilde.setRank(localRank, false);
                    }
                    else if(packet.action.equals(Action.REMOVE)){
                        Group.remove(localguilde, false);
                    }
                }
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
