package fr.avatar_returns.organisation.rabbitMQPackets.group;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

public class RolePacket extends OrganisationPacket {

    Role role;
    String newName;

    public RolePacket(Role role, Action action, String newName) {

        super(action);
        this.role = role;
        this.newName = newName;
    }

    public static Messaging.MessagingCodec<RolePacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<RolePacket> defaultCodec = organisation.getMessaging().getDefaultCodec(RolePacket.class);
        defaultCodec.receive((packet, source) ->{
            try{
                if(source.equals(ConfigManager.getServerId()))return;
                if(packet.role == null)return;

                Group localGroup = packet.role.getGroup();
                if(localGroup == null)return;

                Role localRole = null;

                for(Role role : localGroup.getLstRole()) {
                    if (role.getName().equals(packet.role.getName())) {
                        localRole = role;
                        break;
                    }
                }

                if(localRole == null) {
                    if(packet.action.equals(Action.REMOVE))return;
                    packet.action = Action.CREATE;
                }

                if(packet.action.equals(Action.CREATE)){
                    localGroup.addRole(packet.role, false);
                }
                else if(packet.action.equals(Action.UPDATE)){

                    localRole.setRolePower(packet.role.getRolePower(), false);
                    localRole.setName(packet.newName, false);
                    localRole.setLstPermission(packet.role.getLstPermission(), false);

                }
                else if(packet.action.equals(Action.REMOVE)){
                    localGroup.removeRole(localRole, false);
                }
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }


}
