package fr.avatar_returns.organisation.rabbitMQPackets.group;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

public class PermissionPacket extends OrganisationPacket {

    Role role;
    String permission;

    public PermissionPacket(Role role, String permission, Action action) {
        super(action);
        this.role = role;
        this.permission = permission;
    }

    public static Messaging.MessagingCodec<PermissionPacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<PermissionPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(PermissionPacket.class);
        defaultCodec.receive((packet, source) ->{
            try{
                if(source.equals(ConfigManager.getServerId()))return;
                if(packet.role == null)return;
                if(packet.permission == null)return;

                Group localGroup = Group.getGroupById(packet.role.getGroup().getId());
                if(localGroup == null)return;

                Role localRole = null;
                for(Role role : localGroup.getLstRole()) {
                    if (role.getName().equals(packet.role.getName())) {
                        localRole = role;
                        break;
                    }
                }
                if(localRole == null) return;

                if(packet.action.equals(Action.CREATE)){
                    if(!localRole.getLstPermission().contains(packet.permission))localRole.getLstPermission().add(packet.permission);
                }
                else if(packet.action.equals(Action.REMOVE)){
                    if(localRole.getLstPermission().contains(packet.permission))localRole.getLstPermission().remove(packet.permission);
                }
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
