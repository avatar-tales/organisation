package fr.avatar_returns.organisation.rabbitMQPackets.group;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.largeGroup.Faction;
import fr.avatar_returns.organisation.organisations.simpleGroup.Guilde;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rank.GroupRank;
import fr.avatar_returns.organisation.utils.ItemUtils;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;

public class FactionPacket extends OrganisationPacket {

    Faction faction;
    String flag = "";

    public FactionPacket(Faction faction, Action action) {
        super(action);
        this.faction = faction;
        ItemStack[] lstFlag = {faction.getFlag()};
        this.flag = ItemUtils.itemStackArrayToBase64(lstFlag);

    }

    public static Messaging.MessagingCodec<FactionPacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<FactionPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(FactionPacket.class);
        defaultCodec.receive((packet, source) ->{
            try {
                if(source.equals(ConfigManager.getServerId()))return;
                if(packet.faction == null)return;

                Group localGroup = Group.getGroupById(packet.faction.getId());
                GroupRank localRank = GroupRank.getById(packet.faction.getRank().getId());
                if(localRank == null)return;

                ItemStack flagItem = null;
                try {
                    ItemStack[] lstItem = ItemUtils.itemStackArrayFromBase64(packet.flag);
                    flagItem = (lstItem.length < 1) ? null : lstItem[0];

                }catch (IOException e){}

                if(packet.action.equals(Action.CREATE) && localGroup == null){

                    new Faction(packet.faction.getId(),
                            packet.faction.getName(),
                            packet.faction.getMaxPlayer(),
                            packet.faction.getMinPlayer(),
                            packet.faction.getMaxLand(),
                            packet.faction.getMaxRaid(),
                            packet.faction.getDescription(),
                            packet.faction.getConnexionMessage(),
                            localRank,
                            flagItem,
                            packet.faction.isVisible(),
                            packet.faction.getDailyRaid(),
                            packet.faction.getNextimeAllowedWar(),
                            packet.faction.canUnClaim());

                }else{
                    if(!(localGroup instanceof Faction))return;
                    Faction localFaction = (Faction) localGroup;
                    Faction lord = (packet.faction.getLord() == null)? null : (Faction) Group.getGroupById(packet.faction.getId());


                    if(packet.action.equals(Action.UPDATE)){

                        localFaction.setName(packet.faction.getName(), false);
                        localFaction.setConnexionMessage(packet.faction.getConnexionMessage(), false);
                        localFaction.setDescription(packet.faction.getDescription(), false);
                        localFaction.setDailyRaid (packet.faction.getDailyRaid(), false);
                        localFaction.setFlag(flagItem, false);
                        localFaction.setIsVisible(packet.faction.isVisible(), false);
                        localFaction.setMaxLand(packet.faction.getMaxLand(), false);
                        localFaction.setMaxPlayer(packet.faction.getMaxPlayer(), false);
                        localFaction.setMinPlayer(packet.faction.getMinPlayer(), false);
                        localFaction.setNextimeAllowedWar(packet.faction.getNextimeAllowedWar(), false);
                        localFaction.setMaxRaid(packet.faction.getMaxRaid(), false);
                        localFaction.setCanUnClaim(packet.faction.canUnClaim(), false);

                        if(localFaction.getLord() != lord)localFaction.setLord(lord, false);
                        if(localRank.getId() != localFaction.getId()) localFaction.setRank(localRank, false);
                    }
                    else if(packet.action.equals(Action.REMOVE)){

                        Group.remove(localFaction, false);
                    }
                }
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
