package fr.avatar_returns.organisation.rabbitMQPackets;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.commands.InviteCommand;
import fr.avatar_returns.organisation.commands.needClass.Invitation;
import fr.avatar_returns.organisation.exception.InvitationGroupException;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

import java.util.UUID;

public class InvitationPacket extends OrganisationPacket{

    UUID playerHostUUID;
    UUID playerInvitUUID;
    int groupId;

    public InvitationPacket(Invitation invitation, Action action) {
        super(action);
        this.playerInvitUUID = invitation.getOPlayerInvite().getUuid();
        this.playerHostUUID = invitation.getOPlayerHost().getUuid();
        this.groupId = invitation.getGroupHost().getId();
    }

    public static Messaging.MessagingCodec<InvitationPacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<InvitationPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(InvitationPacket.class);
        defaultCodec.receive((packet, source) ->{

            try{
                if(source.equals(ConfigManager.getServerId()))return;
                if(packet.playerHostUUID == null)return;
                if(packet.playerInvitUUID == null)return;

                Invitation localInvitation = null;

                for(Invitation invitation : InviteCommand.lstInvitation){

                    if(invitation.getGroupHost().getId() == packet.groupId
                            && invitation.getOPlayerInvite().getUuid().equals(packet.playerInvitUUID)
                            && invitation.getOPlayerHost().getUuid().equals(packet.playerHostUUID)){
                        localInvitation = invitation;
                        break;
                    }
                }

                if(packet.action.equals(Action.CREATE) && localInvitation == null){

                    try{
                        OrganisationPlayer localOPlayerInvite = OrganisationPlayer.getOrganisationPlayerByUUID(packet.playerInvitUUID);
                        OrganisationPlayer localOPlayerHost = OrganisationPlayer.getOrganisationPlayerByUUID(packet.playerHostUUID);
                        Group localGroup = Group.getGroupById(packet.groupId);

                        if(localGroup == null){
                            return;
                        }

                        InviteCommand.lstInvitation.add(new Invitation(localOPlayerHost, localGroup, localOPlayerInvite));

                    }
                    catch (OrganisationPlayerException | InvitationGroupException e){};

                }
                else if(packet.action.equals(Action.REMOVE) && localInvitation != null){
                    InviteCommand.lstInvitation.remove(localInvitation);
                }
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
