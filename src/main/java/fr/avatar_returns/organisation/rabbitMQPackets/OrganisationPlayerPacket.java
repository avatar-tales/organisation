package fr.avatar_returns.organisation.rabbitMQPackets;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;

import static fr.avatar_returns.organisation.listener.GlobalEventManager.mapOPlayerLeaving;

public class OrganisationPlayerPacket extends OrganisationPacket{

    OrganisationPlayer oPlayer;

    public OrganisationPlayerPacket(OrganisationPlayer organisationPlayer, Action action) {
        super(action);
        this.oPlayer = organisationPlayer;
    }

    public static Messaging.MessagingCodec<OrganisationPlayerPacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<OrganisationPlayerPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(OrganisationPlayerPacket.class);
        defaultCodec.receive((packet, source) ->{

            try {
                if (source.equals(ConfigManager.getServerId())) return;
                if (packet.oPlayer == null) {
                    return;
                }

                OrganisationPlayer localOPlayer = null;
                try {
                    localOPlayer = OrganisationPlayer.getOrganisationPlayerByUUID(packet.oPlayer.getUuid());
                } catch (OrganisationPlayerException e) {
                    packet.action = Action.CREATE;
                }

                if (localOPlayer != null && packet.action == Action.CREATE) {
                    packet.action = Action.UPDATE;
                }

                if (packet.action.equals(Action.CREATE)) {
                    if (localOPlayer == null) {
                        OrganisationPlayer.getLstOrganisationPlayer().add(packet.oPlayer);
                    }
                } else if (packet.action.equals(Action.UPDATE)) {

                    localOPlayer.setIsConnected(packet.oPlayer.isConnected(), false);
                    localOPlayer.setLastSeenName(packet.oPlayer.getLastSeenName(), false);
                    localOPlayer.setRank(packet.oPlayer.getRank(), false);
                    localOPlayer.setLastTimeConnexion(packet.oPlayer.getLastTimeConnexion(), false);
                    localOPlayer.setIsInPrespawnMod(packet.oPlayer.isInPrespawnMod(), false);
                    localOPlayer.setLastTimeFightMod(packet.oPlayer.getLastTimeFightMod(), false);
                    localOPlayer.setLastHeadSeen(packet.oPlayer.getLastHeadSeen(), false);
                    localOPlayer.setLastServerSeenName(packet.oPlayer.getLastServerSeenName(), false);
                    localOPlayer.setMobsKill(packet.oPlayer.getMobsKill(), false);
                    localOPlayer.setNbDeath(packet.oPlayer.getNbDeath(), false);
                    localOPlayer.setPlayerKill(packet.oPlayer.getPlayerKill(), false);
                    localOPlayer.setChatLocked(packet.oPlayer.getChatLocked(), false);
                    localOPlayer.setNickName(packet.oPlayer.getNickName(), false);

                    if(mapOPlayerLeaving.contains(localOPlayer)){
                        mapOPlayerLeaving.remove(localOPlayer);
                    }
                    //localOPlayer.setLastStrElementSeen(packet.oPlayer.getLastStrElementSeen(), false);
                }
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }

}
