package fr.avatar_returns.organisation.rabbitMQPackets;

import fr.alessevan.api.connections.messaging.Messaging;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.exception.OrganisationPlayerException;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.OrganisationMember;
import fr.avatar_returns.organisation.organisations.utils.Role;
import fr.avatar_returns.organisation.storedInventory.StoredInventory;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;

import java.util.UUID;

public class StoredInventoryPacket extends OrganisationPacket{

    public StoredInventory storedInventory;
    public String inv;

    public StoredInventoryPacket(StoredInventory storedInventory, Action action) {
        super(action);
        this.storedInventory = storedInventory;
        this.inv = storedInventory.getStringFromStoredInventory();
    }

    public static Messaging.MessagingCodec<StoredInventoryPacket> getCodec(Organisation organisation){

        Messaging.MessagingCodec<StoredInventoryPacket> defaultCodec = organisation.getMessaging().getDefaultCodec(StoredInventoryPacket.class);
        defaultCodec.receive((packet, source) ->{

            try {
                if (source.equals(ConfigManager.getServerId())) return;
                if (packet.storedInventory == null) return;

                StoredInventory localStoredInventory = StoredInventory.getStoredInventoryById(packet.storedInventory.getId());
                if (packet.action.equals(Action.UPDATE) && localStoredInventory == null) packet.action = Action.CREATE;

                if (!packet.action.equals(Action.REMOVE)) {

                    Inventory tmpInventory = StoredInventory.getInventoryFromString(packet.inv);
                    if (tmpInventory == null) return;


                    Inventory inventory = Bukkit.createInventory(null, 54, "§3§l" + packet.storedInventory.getName());
                    for (int i = 0; i < tmpInventory.getSize(); i++) {
                        if (tmpInventory.getItem(i) != null) inventory.setItem(i, tmpInventory.getItem(i));
                    }

                    if (packet.action.equals(Action.CREATE) && localStoredInventory == null) {

                        new StoredInventory(packet.storedInventory.getId(),
                                packet.storedInventory.getName(),
                                inventory, packet.storedInventory.getRatio(),
                                packet.storedInventory.getMaxNumber(),
                                packet.storedInventory.getType());


                    } else if (packet.action.equals(Action.UPDATE)) {

                        localStoredInventory.setName(packet.storedInventory.getName(), false);
                        localStoredInventory.setMaxNumber(packet.storedInventory.getMaxNumber(), false);
                        localStoredInventory.setRatio(packet.storedInventory.getRatio(), false);

                        for (int i = 0; i < localStoredInventory.getInventory().getSize(); i++) {
                            if (i >= inventory.getSize()) continue;
                            if (inventory.getItem(i) != null)
                                localStoredInventory.getInventory().setItem(i, inventory.getItem(i));
                        }
                    }
                } else if (localStoredInventory != null) localStoredInventory.remove(false);
            }
            catch (Exception e) {
                Organisation.log.severe("Impossible de synchroniser : " + e.getMessage());
                Organisation.log.severe(e.getStackTrace().toString());
            }
        });

        return defaultCodec;
    }
}
