package fr.avatar_returns.organisation.rabbitMQPackets;

public abstract class OrganisationPacket implements Packet {

    public enum Action {

        CREATE("CREATE"),
        UPDATE("UPDATE"),
        REMOVE("REMOVE"),
        GET_ALL("GET_ALL");

        private String action;

        Action(String action) {
            this.action = action;
        }

    }

    public Action action;

    public OrganisationPacket(Action action){
        this.action = action;
    }


}
