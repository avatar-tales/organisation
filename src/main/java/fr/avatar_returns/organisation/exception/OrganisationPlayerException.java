package fr.avatar_returns.organisation.exception;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class OrganisationPlayerException extends OrganisationException {

    protected OfflinePlayer player;

    public OrganisationPlayerException(String playerName){

        super(playerName);
    }


    public OrganisationPlayerException(Player player){

        super("["+player.getUniqueId()+"] "+player.getName());
    }

    public OrganisationPlayerException(OfflinePlayer player){

        super("["+player.getUniqueId()+"] "+player.getName());
    }


    @Override
    public String toString() {
        return this.getErrorMessage();
    }
}
