package fr.avatar_returns.organisation.exception;

import fr.avatar_returns.organisation.organisations.Group;

public class AlreadyVassalException extends OrganisationException {

    private Group first;
    private Group second;

    public AlreadyVassalException(Group first, Group second) {

        super(second.getName()+" est déja le vasalle de "+first.getName());
        this.first = first;
        this.second = second;
    }

    public Group getFirst() {
        return first;
    }

    public Group getSecond() {
        return second;
    }
}
