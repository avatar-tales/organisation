package fr.avatar_returns.organisation.exception;

public abstract class OrganisationException extends Exception {

    private String errorMessage;
    OrganisationException(String errorMessage){
        this.errorMessage = errorMessage;
        System.err.println(errorMessage);
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
