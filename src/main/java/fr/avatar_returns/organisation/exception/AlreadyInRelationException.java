package fr.avatar_returns.organisation.exception;

import fr.avatar_returns.organisation.organisations.Group;

public class AlreadyInRelationException extends OrganisationException{

    private Group first;
    private Group second;

    public AlreadyInRelationException(Group first, Group second) {

        super(second.getName()+" est déja en relation avec "+first.getName());
        this.first = first;
        this.second = second;
    }

    public Group getFirst() {
        return first;
    }

    public Group getSecond() {
        return second;
    }
}
