package fr.avatar_returns.organisation.exception;

import fr.avatar_returns.organisation.organisations.Group;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class OrganisationGroupPlayerException extends OrganisationException {

    private Group group;
    private OfflinePlayer player;
    public OrganisationGroupPlayerException(Group group, OfflinePlayer player){

        super(group.getName()+": "+"["+player.getUniqueId()+"] "+player.getName());
        this.group= group;
        this.player = player;
    }

    public Group getGroup() {
        return group;
    }

    public OfflinePlayer getOfflinePlayer() {
        return player;
    }

    @Override
    public String toString() {
        return this.getErrorMessage();
    }
}
