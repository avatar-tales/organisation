package fr.avatar_returns.organisation.exception;

import fr.avatar_returns.organisation.organisations.Group;

public class OrganisationGroupException extends OrganisationException {

    private Group group;

    public OrganisationGroupException(Group group, String errorMessage) {
        super(group.getName()+" "+errorMessage);
        this.group = group;
    }

    public Group getGroup() {
        return group;
    }

    @Override
    public String toString() {
        return this.getErrorMessage();
    }
}
