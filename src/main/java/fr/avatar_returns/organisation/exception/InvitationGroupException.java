package fr.avatar_returns.organisation.exception;


import fr.avatar_returns.organisation.commands.needClass.Invitation;

public class InvitationGroupException extends OrganisationException {

    private Invitation invitation;

    public InvitationGroupException(Invitation invitation) {
        super(invitation.getOPlayerHost().getOfflinePlayer().getName()+" try to invite "+invitation.getOPlayerInvite().getName()+" in "+invitation.getGroupHost().getName()+" were he is not member");
    }

    public Invitation getInvitation() {
        return invitation;
    }
}
