package fr.avatar_returns.organisation.manager;

import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.storage.DBUtils;
import fr.avatar_returns.organisation.storedInventory.MineUtils;

public class DailyTaskManager implements Runnable {

    @Override
    public void run() {

        int milisToDay = 1000*60*60*24;
        long currentDay = (System.currentTimeMillis()/milisToDay) * milisToDay;

        if(currentDay <= Organisation.lastDayRunningTask)return;
        Organisation.lastDayRunningTask = currentDay + 1;

        Organisation.log.info("Running daily log");

        DBUtils.updateDailyRunningTask();
        for(LargeGroup largeGroup : LargeGroup.getLstLargeGroup()){
            // TODO : Improve reset byu executing only one SQL request.
            largeGroup.setDailyRaid(0);
            largeGroup.setCanUnClaim(true);
        }

        MineUtils.weeklyAutoRotate();
    }
}
