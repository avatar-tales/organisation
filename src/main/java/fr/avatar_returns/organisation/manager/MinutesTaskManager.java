package fr.avatar_returns.organisation.manager;

import fr.avatar_returns.organisation.rabbitMQPackets.OrganisationPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.VLandPacket;
import fr.avatar_returns.organisation.rabbitMQPackets.VMinePacket;
import fr.avatar_returns.organisation.territory.Land;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.territory.VMine;
import fr.avatar_returns.organisation.utils.GeneralMethods;

public class MinutesTaskManager implements Runnable {

    private long lastMinute = -1;

    @Override
    public void run() {

        int milisToMinutes = 1000*60;
        long currentMinute = (System.currentTimeMillis()/milisToMinutes) * milisToMinutes;

        if(currentMinute <= lastMinute)return;
        lastMinute = currentMinute + 1;
        //Organisation.log.info("Minutes task manager launched");

        // On synchronise les Land être sur qu'ils soientt mis à jour
        for(Land land: Land.getAllLand()) {
            VLand vland = VLand.getVlandById(land.getId());
            if (vland != null) {
                GeneralMethods.sendPacket(new VLandPacket(vland, OrganisationPacket.Action.UPDATE));
            }

        }

        for(VMine vMine: VMine.getLstVMine()){
            if(Land.getLandById(vMine.getVLand().getId()) != null){
                GeneralMethods.sendPacket(new VMinePacket(vMine, OrganisationPacket.Action.UPDATE));
            }
        }
    }
}

