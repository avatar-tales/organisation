package fr.avatar_returns.organisation.manager;

import fr.alessevan.api.AvatarAPI;
import fr.alessevan.api.hooks.defaults.IMoneyHook;
import fr.avatar_returns.organisation.Organisation;
import fr.avatar_returns.organisation.OrganisationPlayer;
import fr.avatar_returns.organisation.hook.AvatarRoadsHook;
import fr.avatar_returns.organisation.hook.MythicsMobsHook;
import fr.avatar_returns.organisation.organisations.Group;
import fr.avatar_returns.organisation.organisations.LargeGroup;
import fr.avatar_returns.organisation.storedInventory.MineUtils;
import fr.avatar_returns.organisation.territory.VLand;
import fr.avatar_returns.organisation.utils.GeneralMethods;
import fr.avatar_returns.organisation.utils.configuration.ConfigManager;
import fr.avatar_returns.organisation.utils.teleportation.MultiServerTPMethodimplements;
import fr.avatar_returns.organisation.view.ui.InventoryUI;
import io.lumine.mythic.bukkit.utils.lib.jooq.impl.QOM;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import static fr.avatar_returns.organisation.listener.GlobalEventManager.mapOPlayerLeaving;

public class SecondeTaskManager implements Runnable {

    private long lastSeconde = -1;
    public static ConcurrentHashMap<OrganisationPlayer, Long> mapPlayerToTPSpawn = new ConcurrentHashMap<>();
    public static CopyOnWriteArrayList<OrganisationPlayer> playerInFightMode = new CopyOnWriteArrayList<>();
    public static ConcurrentHashMap<OrganisationPlayer, Long> mapPlayerToTPFHome = new ConcurrentHashMap<>();
    public static ConcurrentHashMap<OrganisationPlayer, VLand> mapPlayerToTPFHomeVland = new ConcurrentHashMap<>();
    public static ConcurrentHashMap<OrganisationPlayer, OrganisationPlayer> mapPlayerToTPPrespawnTarget = new ConcurrentHashMap<>();
    public static ConcurrentHashMap<OrganisationPlayer, Long> mapPlayerToTPPrespawn = new ConcurrentHashMap<>();

    @Override
    public void run() {

        int milisToSeconde = 1000;
        long currentSecond = (System.currentTimeMillis()/milisToSeconde) * milisToSeconde;

        if(currentSecond <= lastSeconde)return;
        lastSeconde = currentSecond + 1;

        MineUtils.filleMines();
        if(Organisation.getHook(MythicsMobsHook.class).isPresent())MythicsMobsHook.despawnMobsHostileInZoneRP();

        for (OrganisationPlayer oPlayer: mapOPlayerLeaving.keySet()){
            if(System.currentTimeMillis() > mapOPlayerLeaving.get(oPlayer)){
                mapOPlayerLeaving.remove(oPlayer);
                oPlayer.setIsConnected(false);
            }
        }
        for (OrganisationPlayer oPlayer : mapPlayerToTPSpawn.keySet()){

            if(oPlayer.getOfflinePlayer() == null){
                mapPlayerToTPSpawn.remove(oPlayer);
                continue;
            }
            if(oPlayer.getOfflinePlayer().getPlayer() == null){
                mapPlayerToTPSpawn.remove(oPlayer);
                continue;
            }
            if(oPlayer.isInFightMod() && !(oPlayer.getOfflinePlayer().getPlayer().isOp() || oPlayer.getOfflinePlayer().getPlayer().hasPermission("organisation.command.spawn.admin"))){
                GeneralMethods.sendPlayerErrorMessage(oPlayer.getOfflinePlayer(), "", true);
                mapPlayerToTPSpawn.remove(oPlayer);
                continue;
            }

            if(System.currentTimeMillis() > mapPlayerToTPSpawn.get(oPlayer)){

                mapPlayerToTPSpawn.remove(oPlayer);

                if(!(oPlayer.getOfflinePlayer().getPlayer().isOp() || oPlayer.isYoung() || oPlayer.getOfflinePlayer().getPlayer().hasPermission("organisation.command.home.admin"))) {

                    AtomicBoolean hasPaid = new AtomicBoolean(true);
                    int cost = GeneralMethods.getIntConf("Organisation.Spawn.Teleportation.Cost");

                    AvatarAPI.getAPI().getServiceHook(IMoneyHook.class)
                            .ifPresent(hook -> {

                                // We check if player has already money. To prevent use bug.
                                if (hook.getMoney(oPlayer.getOfflinePlayer().getPlayer()) < cost && !(oPlayer.getOfflinePlayer().getPlayer().isOp() || oPlayer.isYoung() || oPlayer.getOfflinePlayer().getPlayer().hasPermission("organisation.command.spawn.admin"))) {
                                    GeneralMethods.sendPlayerErrorMessage(oPlayer.getOfflinePlayer().getPlayer(), "Commands.Spawn.Teleportation.NotEnouthMoney", true);
                                    hasPaid.set(false);
                                } else {
                                    hook.takeMoney(oPlayer.getOfflinePlayer().getPlayer(), cost);
                                }
                            });

                    if(!hasPaid.get())continue;
                }
                if(oPlayer.isLocalyInSafeZone() && !oPlayer.hasPermission("organisation.command.spawn.admin")){
                    GeneralMethods.sendPlayerErrorMessage(oPlayer.getOfflinePlayer(), "Organisation.TP.CantTPFromSafeZone", true);
                    continue;
                }

                String spawnServer = GeneralMethods.getStringConf("Organisation.Spawn.Config.ServerId");
                String worldName = GeneralMethods.getStringConf("Organisation.Spawn.Config.world");

                int x = GeneralMethods.getIntConf("Organisation.Spawn.Config.x");
                int y = GeneralMethods.getIntConf("Organisation.Spawn.Config.y");
                int z = GeneralMethods.getIntConf("Organisation.Spawn.Config.z");

                if(spawnServer.equals(ConfigManager.getServerId())){

                    World world = Organisation.plugin.getServer().getWorld(worldName);
                    if(world == null) return;
                    if(oPlayer.getOfflinePlayer() == null) return;
                    if(oPlayer.getOfflinePlayer().getPlayer() == null) return;

                    oPlayer.getOfflinePlayer().getPlayer().teleport(new Location(world, x,y,z));
                }
                else {
                    MultiServerTPMethodimplements.sendTPCommand(spawnServer, oPlayer.getName(), x, y, z, worldName);
                }

            }
            else{

                long timeToWait = (mapPlayerToTPSpawn.get(oPlayer)/1000) - (currentSecond/1000);
                GeneralMethods.sendPlayerMessage(oPlayer, ""+timeToWait);
            }
        }
        for (OrganisationPlayer oPlayer : mapPlayerToTPFHome.keySet()){

            if(oPlayer.getOfflinePlayer() == null){
                mapPlayerToTPFHome.remove(oPlayer);
                mapPlayerToTPFHomeVland.remove(oPlayer);
                continue;
            }
            if(oPlayer.getOfflinePlayer().getPlayer() == null){
                mapPlayerToTPFHome.remove(oPlayer);
                mapPlayerToTPFHomeVland.remove(oPlayer);
                continue;
            }
            if(oPlayer.isInFightMod() && !(oPlayer.getOfflinePlayer().getPlayer().isOp() || oPlayer.getOfflinePlayer().getPlayer().hasPermission("organisation.command.home.admin"))){
                GeneralMethods.sendPlayerErrorMessage(oPlayer.getOfflinePlayer(), "Commands.Home.Teleportation.Cancel", true);
                mapPlayerToTPFHome.remove(oPlayer);
                mapPlayerToTPFHomeVland.remove(oPlayer);
                continue;
            }

            if(System.currentTimeMillis() > mapPlayerToTPFHome.get(oPlayer)) {


                VLand vLandToTeleport = mapPlayerToTPFHomeVland.get(oPlayer);

                mapPlayerToTPFHome.remove(oPlayer);
                mapPlayerToTPFHomeVland.remove(oPlayer);

                if (!(oPlayer.getOfflinePlayer().getPlayer().isOp() || oPlayer.isYoung() || oPlayer.getOfflinePlayer().getPlayer().hasPermission("organisation.command.home.admin"))) {
                    AtomicBoolean hasPaid = new AtomicBoolean(true);
                    int cost = GeneralMethods.getIntConf("Organisation.Home.Teleportation.Cost");

                    AvatarAPI.getAPI().getServiceHook(IMoneyHook.class)
                            .ifPresent(hook -> {

                                // We check if player has already money. To prevent use bug.
                                if (hook.getMoney(oPlayer.getOfflinePlayer().getPlayer()) < cost) {
                                    GeneralMethods.sendPlayerErrorMessage(oPlayer.getOfflinePlayer().getPlayer(), "Commands.Home.Teleportation.NotEnouthMoney", true);
                                    hasPaid.set(false);

                                } else {
                                    hook.takeMoney(oPlayer.getOfflinePlayer().getPlayer(), cost);
                                }
                            });

                    if (!hasPaid.get()) continue;
                }
                if(oPlayer.isLocalyInSafeZone() && !oPlayer.hasPermission("organisation.command.home.admin")){
                    GeneralMethods.sendPlayerErrorMessage(oPlayer.getOfflinePlayer(), "Organisation.TP.CantTPFromSafeZone", true);
                    continue;
                }

                String spawnServer = vLandToTeleport.getServerId();
                String worldName = vLandToTeleport.getFhome().split(":")[0];

                int x = Integer.parseInt(vLandToTeleport.getFhome().split(":")[1]);
                int y = Integer.parseInt(vLandToTeleport.getFhome().split(":")[2]);
                int z = Integer.parseInt(vLandToTeleport.getFhome().split(":")[3]);

                if(spawnServer.equals(ConfigManager.getServerId())){

                    World world = Organisation.plugin.getServer().getWorld(worldName);
                    if(world == null) return;
                    if(oPlayer.getOfflinePlayer() == null) return;
                    if(oPlayer.getOfflinePlayer().getPlayer() == null) return;

                    oPlayer.getOfflinePlayer().getPlayer().teleport(new Location(world, x,y,z));
                }
                else {
                    MultiServerTPMethodimplements.sendTPCommand(spawnServer, oPlayer.getName(), x, y, z, worldName);
                }
            }
            else{

                long timeToWait = (mapPlayerToTPFHome.get(oPlayer)/1000) - (currentSecond/1000);
                GeneralMethods.sendPlayerMessage(oPlayer, ""+timeToWait);
            }
        }
        for (OrganisationPlayer oPlayer: mapPlayerToTPPrespawnTarget.keySet()){

            int nbSupportTeleporting = 0;
            OrganisationPlayer oTargetPlayer = mapPlayerToTPPrespawnTarget.get(oPlayer);

            // Count nbSupportTeleporting
            for (OrganisationPlayer support : SecondeTaskManager.mapPlayerToTPPrespawnTarget.keySet()) {
                if(SecondeTaskManager.mapPlayerToTPPrespawnTarget.get(support).getUuid() == oTargetPlayer.getUuid()) nbSupportTeleporting++;
            }

            if(oPlayer.getOfflinePlayer() == null){
                mapPlayerToTPPrespawn.remove(oPlayer);
                mapPlayerToTPPrespawnTarget.remove(oPlayer);
                if(nbSupportTeleporting == 1)GeneralMethods.sendPlayerMessage(SecondeTaskManager.mapPlayerToTPPrespawnTarget.get(oPlayer) , "Commands.Prespawn.Teleportation.Inform.Cancel", true);
                continue;
            }
            if(oPlayer.getOfflinePlayer().getPlayer() == null){
                mapPlayerToTPPrespawn.remove(oPlayer);
                mapPlayerToTPPrespawnTarget.remove(oPlayer);
                if(nbSupportTeleporting == 1)GeneralMethods.sendPlayerMessage(SecondeTaskManager.mapPlayerToTPPrespawnTarget.get(oPlayer) , "Commands.Prespawn.Teleportation.Inform.Cancel", true);
                continue;
            }
            if(oPlayer.isInFightMod() && !(oPlayer.getOfflinePlayer().getPlayer().isOp() || oPlayer.getOfflinePlayer().getPlayer().hasPermission("organisation.command.home.admin"))){
                GeneralMethods.sendPlayerMessage(oPlayer, "La téléportation a " + SecondeTaskManager.mapPlayerToTPPrespawnTarget.get(oPlayer).getName() + " a bien été annulée.");
                mapPlayerToTPPrespawn.remove(oPlayer);
                mapPlayerToTPPrespawnTarget.remove(oPlayer);
                if(nbSupportTeleporting == 1)GeneralMethods.sendPlayerMessage(SecondeTaskManager.mapPlayerToTPPrespawnTarget.get(oPlayer) , "Commands.Prespawn.Teleportation.Inform.Cancel", true);
                continue;
            }

            if(System.currentTimeMillis() > mapPlayerToTPPrespawn.get(oPlayer)){
                MultiServerTPMethodimplements.sendTPCommand(SecondeTaskManager.mapPlayerToTPPrespawnTarget.get(oPlayer).getLastServerSeenName(), oPlayer.getName(), -1, -1, -1, "#tpTo:"+SecondeTaskManager.mapPlayerToTPPrespawnTarget.get(oPlayer).getUuid());
                mapPlayerToTPPrespawn.remove(oPlayer);
                mapPlayerToTPPrespawnTarget.remove(oPlayer);
            }
            else{
                long timeToWait = (mapPlayerToTPPrespawn.get(oPlayer)/1000) - (currentSecond/1000);
                GeneralMethods.sendPlayerMessage(oPlayer, ""+timeToWait);
            }
        }

        for (OrganisationPlayer oPlayer : SecondeTaskManager.playerInFightMode){
            if(!oPlayer.isInFightMod()){
                GeneralMethods.sendPlayerMessage(oPlayer, "Organisation.FightMod.Alert.End", true);

                if(Organisation.getHook(AvatarRoadsHook.class).isPresent() && oPlayer.getOfflinePlayer().getPlayer() != null) {
                    Organisation.getHook(AvatarRoadsHook.class).get().enableRoadsForPlayer(oPlayer.getOfflinePlayer().getPlayer());
                }

                if(SecondeTaskManager.playerInFightMode.contains(oPlayer)) SecondeTaskManager.playerInFightMode.remove(oPlayer);
            }
            else if (AvatarRoadsHook.isRoadEnableForPlayer(oPlayer.getOfflinePlayer())) {
                if(Organisation.getHook(AvatarRoadsHook.class).isPresent())Organisation.getHook(AvatarRoadsHook.class).get().disableRoadsForPlayer(oPlayer.getOfflinePlayer().getPlayer());
            }
        }
        for(Player player: Organisation.plugin.getServer().getOnlinePlayers()){
            boolean findGroup = false;
            for(LargeGroup largeGroup: LargeGroup.getLstLargeGroup()){
                if(largeGroup.getLocalPlayers().contains(player)){
                    findGroup = true;
                    if(!largeGroup.getTeam().hasPlayer(player)){
                        largeGroup.getTeam().addEntry(player.getName());
                    }
                }
            }

            if(!findGroup && !Organisation.getDefaultTeam().hasPlayer(player)){
                Organisation.getDefaultTeam().addPlayer(player);
            }
        }

    }
}
